﻿using CMS;
using CMS.DataEngine;
using CMS.OnlineForms;
using Newtonsoft.Json;
using Pedagogy.Custom.Models;
using Pedagogy.Custom.Modules;
using System.IO;
using System.Net;
using System.Text;

[assembly: RegisterModule(typeof(CustomFormSubmissionModule))]
namespace Pedagogy.Custom.Modules
{
    public class CustomFormSubmissionModule : Module
    {
        public CustomFormSubmissionModule()
            : base("CustomFormSubmissionHandlers")
        {
        }

        protected override void OnInit()
        {
            base.OnInit();

            BizFormItemEvents.Insert.Before += FormItem_InsertBeforeHandler;
        }

        private void FormItem_InsertBeforeHandler(object sender, BizFormItemEventArgs e)
        {
            BizFormItem formDataItem = e.Item;

            if (!string.IsNullOrEmpty(formDataItem.GetStringValue("reCAPTCHAInputComponent", "")))
            {
                var captchaIsValid = IsCaptchaValid(formDataItem.GetStringValue("reCAPTCHAInputComponent", ""));

                if (!captchaIsValid)
                {
                    e.Dispose();
                }
            }
        }

        private bool IsCaptchaValid(string responseToken)
        {
            if (string.IsNullOrEmpty(responseToken))
            {
                return false;
            }
            string secretKey = SettingsKeyInfoProvider.GetValue("CMSReCaptchaPrivateKey");
            var request = (HttpWebRequest)WebRequest.Create("https://www.google.com/recaptcha/api/siteverify");

            var postData = "response=" + responseToken;
            postData += "&secret=" + secretKey;
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            var res = JsonConvert.DeserializeObject<CaptchaResponseViewModel>(responseString);
            if (res == null || !res.Success || res.Score < 0.5)
            {
                return false;
            }

            return true;
        }
    }
}
