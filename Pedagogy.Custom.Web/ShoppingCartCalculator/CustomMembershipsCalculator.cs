﻿using CMS.Core;
using CMS.CustomTables;
using CMS.CustomTables.Types.Pedagogy;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Ecommerce;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using CMS.Taxonomy;
using Pedagogy.DataAccess.Services.Cache;
using System;
using System.Linq;

namespace Pedagogy.Custom.ShoppingCartCalculations
{
    public class CustomMembershipsCalculator : IShoppingCartCalculator
    {
        public void Calculate(CalculatorData calculationData)
        {
            CalculationRequest request = calculationData.Request;
            CalculationResult result = calculationData.Result;

            decimal total = 0;

            var customer = request.Customer;
            if(customer != null)
            {
                var cart = ShoppingCartInfoProvider.GetShoppingCartInfo(customer.CustomerUserID, request.Site.ObjectCodeName);

                if (cart != null)
                {
                    var value = cart.ShoppingCartCustomData.TryGetValue("MembershipPurchase", out object memPurchase);
                    if (value)
                    {
                        if (memPurchase.ToString() == "True")
                        {
                            double contactHours = 0;
                            var cacheMins = CacheService.CACHEMINUTES;

                            foreach (var i in cart.CartItems)
                            {
                                if (!i.IsProductOption)
                                {
                                    Func<double> dataLoadMethod = () => DocumentHelper.GetDocuments<Course>()
                                       .OnCurrentSite()
                                       .Published()
                                       .PublishedVersion()
                                       .Where(x => x.NodeSKUID == i.SKUID)
                                       .Select(x => x.CourseContactHours)
                                       .FirstOrDefault();

                                    var cacheSettings = new CacheSettings(cacheMins, $"{nameof(CustomMembershipsCalculator)}|GetCourseContactHours|{i.SKUID}", SiteContext.CurrentSiteName, LocalizationContext.CurrentCulture.CultureName)
                                    {
                                        GetCacheDependency = () => CacheHelper.GetCacheDependency($"{nameof(CustomMembershipsCalculator)}|GetCourseContactHours|{i.SKUID}".ToLowerInvariant())
                                    };

                                    var hours = CacheHelper.Cache(dataLoadMethod, cacheSettings);

                                    contactHours += hours;
                                }
                            }

                            var qty = 0;
                            var memQtyNullCheck = cart.ShoppingCartCustomData.TryGetValue("MembershipQty", out object memQty);
                            if (memQtyNullCheck) qty = int.Parse(memQty.ToString());
                            var categoryName = CookieHelper.GetExistingCookie("MembershipCategory");

                            if (categoryName != null)
                            {
                                var categoryID = CategoryInfoProvider.GetCategories()
                                    .Where(x => x.CategoryName == categoryName.Value)
                                    .Select(x => x.CategoryID)
                                    .FirstOrDefault();

                                decimal memDiscount;
                                if (qty > 200)
                                {
                                    Func<decimal> dataLoadMethod = () => CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                                        .Where(x => x.FacilityMembershipCategory == categoryID.ToString())
                                        .Where(x => x.FacilityMembershipEndingStaffNumber == 200)
                                        .Select(x => x.FacilityMembershipDollarAmtPerYear)
                                        .FirstOrDefault();

                                    var cacheSettings = new CacheSettings(cacheMins, $"{nameof(CustomMembershipsCalculator)}|GetOver200StaffPrice|{categoryID}", SiteContext.CurrentSiteName, LocalizationContext.CurrentCulture.CultureName)
                                    {
                                        GetCacheDependency = () => CacheHelper.GetCacheDependency($"{nameof(CustomMembershipsCalculator)}|GetOver200StaffPrice|{categoryID}".ToLowerInvariant())
                                    };

                                    memDiscount = CacheHelper.Cache(dataLoadMethod, cacheSettings);
                                }
                                else
                                {
                                    Func<decimal> dataLoadMethod = () => CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                                        .Where(x => x.FacilityMembershipCategory == categoryID.ToString())
                                        .Where(x => x.FacilityMembershipStartingContactHours <= contactHours)
                                        .Where(x => x.FacilityMembershipEndingContactHours >= contactHours)
                                        .Where(x => x.FacilityMembershipStartingStaffNumber <= qty)
                                        .Where(x => x.FacilityMembershipEndingStaffNumber >= qty)
                                        .Select(x => x.FacilityMembershipDollarAmtPerYear)
                                        .FirstOrDefault();

                                    var cacheSettings = new CacheSettings(cacheMins, $"{nameof(CustomMembershipsCalculator)}|GetUnder200StaffPrice|{categoryID}|{contactHours}|{qty}", SiteContext.CurrentSiteName, LocalizationContext.CurrentCulture.CultureName)
                                    {
                                        GetCacheDependency = () => CacheHelper.GetCacheDependency($"{nameof(CustomMembershipsCalculator)}|GetUnder200StaffPrice|{categoryID}|{contactHours}|{qty}".ToLowerInvariant())
                                    };

                                    memDiscount = CacheHelper.Cache(dataLoadMethod, cacheSettings);
                                }

                                if (memDiscount != 0)
                                {
                                    total = memDiscount * qty;
                                }
                            }

                            ICurrencyConverterFactory currencyConverterFactory = Service.Resolve<ICurrencyConverterFactory>();
                            ICurrencyConverter currencyConverter = currencyConverterFactory.GetCurrencyConverter(calculationData.Request.Site);

                            string siteMainCurrencyCode = Service.Resolve<ISiteMainCurrencySource>().GetSiteMainCurrencyCode(calculationData.Request.Site);
                            string currencyCode = calculationData.Request.Currency.CurrencyCode;

                            total = currencyConverter.Convert(total, siteMainCurrencyCode, currencyCode);

                            result.OrderDiscount = 0;
                            result.Total = total;
                            result.GrandTotal = total;
                        }
                    }
                }
            }            
        }
    }
}
