﻿using CMS;
using CMS.DataEngine;
using CMS.Ecommerce;

using Pedagogy.Custom.ShoppingCartCalculations;
using Pedagogy.Custom.ShoppingCartCalculator;

using System.Collections.Generic;

[assembly: RegisterImplementation(typeof(IShoppingCartCalculationFactory), typeof(CustomShoppingCartCalculationFactory))]
namespace Pedagogy.Custom.ShoppingCartCalculator
{
    public class CustomShoppingCartCalculationFactory : IShoppingCartCalculationFactory
    {
        public IShoppingCartCalculator GetCalculator(SiteInfoIdentifier siteIdentifier)
        {
            return new ShoppingCartCalculatorCollection(customCalculationSteps);
        }

        private static readonly List<IShoppingCartCalculator> customCalculationSteps = new List<IShoppingCartCalculator>()
        {
            new UnitPriceCalculator(),
            new CartItemDiscountCalculator(),
            new TotalValuesCalculator(),
            new OrderDiscountsCalculator(),
            new TotalValuesCalculator(),
            new TaxCalculator(),
            new TotalValuesCalculator(),
            new OtherPaymentsCalculator(),
            new TotalValuesCalculator(),
            new CustomMembershipsCalculator()
        };
    }
}
