﻿using System;
using System.Web;
using Pedagogy.DataAccess.Services.Context;
using CMS.Helpers;

namespace Pedagogy.DataAccess.Services.Cache
{
    public class CacheService : ICacheService
    {
        public ISiteContextService SiteContextService { get; }

        public const int CACHEMINUTES = 1440; // One day

        // Injects a service which holds the site name and current culture code name
        public CacheService(ISiteContextService siteContextService)
        {
            SiteContextService = siteContextService;
        }

        public int GetCacheMinutesConst()
        {
            return CACHEMINUTES;
        }

        // Returns a dummy cache key for pages ("nodes") in the content tree
        // "nodes|<site name>|<generated class name>|< type of cached data >"
        // which is touched when pages are modified
        public string GetNodesCacheDependencyKey(string className, CacheDependencyType dependencyType)
        {
            return $"nodes|{SiteContextService.SiteName}|{className}|{dependencyType}".ToLowerInvariant();
        }

        // Returns a dummy cache key for a page ("node") in the content tree identified by its "NodeGuid"
        // "nodeguid|<site name>| <given node in the content tree>"
        // which is touched when the page is modified
        public string GetNodeCacheDependencyKey(Guid nodeGuid)
        {
            return $"nodeguid|{SiteContextService.SiteName}|{nodeGuid}".ToLowerInvariant();
        }

        public string GetNodeCacheName(string className, Guid nodeGuid)
        {
            return $"{SiteContextService.SiteName}|data|{className}|{nodeGuid}".ToLowerInvariant();
        }

        public string GetNodesCacheName(string className, CacheDependencyType dependencyType)
        {
            return $"{SiteContextService.SiteName}|data|{className}|{dependencyType}".ToLowerInvariant();
        }

        public void SetOutputCacheDependency(Guid nodeGuid)
        {
            var dependencyCacheKey = GetNodeCacheDependencyKey(nodeGuid);

            // Ensures that the dummy key cache item exists
            CacheHelper.EnsureDummyKey(dependencyCacheKey);

            // Sets cache dependency to clear the cache when there is any change to node with given GUID in Kentico
            HttpContext.Current.Response.AddCacheItemDependency(dependencyCacheKey);
        }

        public TData Cache<TData>(Func<TData> dataLoadMethod, int cacheForMinutes, string cacheItemName, string cacheDependencyKey)
        {
            var cacheSettings = new CacheSettings(cacheForMinutes, cacheItemName, SiteContextService.SiteName, SiteContextService.CurrentSiteCulture)
            {
                GetCacheDependency = () => CacheHelper.GetCacheDependency(cacheDependencyKey.ToLowerInvariant())
            };

            return CacheHelper.Cache(dataLoadMethod, cacheSettings);
        }
    }
}
