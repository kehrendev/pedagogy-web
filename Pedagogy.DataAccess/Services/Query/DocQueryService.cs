﻿using System;
using Pedagogy.DataAccess.Services.Context;
using CMS.DocumentEngine;

namespace Pedagogy.DataAccess.Services.Query
{
    public class DocQueryService : IDocQueryService
    {
        private ISiteContextService SiteContext { get; }
        public DocQueryService(ISiteContextService siteContext)
        {
            SiteContext = siteContext;
        }

        public DocumentQuery<TDocument> GetDocument<TDocument>(Guid nodeGuid) where TDocument : TreeNode, new()
        {
            return GetDocuments<TDocument>()
                .TopN(1)
                .WhereEquals("NodeGUID", nodeGuid);
        }

        public DocumentQuery<TDocument> GetDocuments<TDocument>() where TDocument : TreeNode, new()
        {
            var query = DocumentHelper.GetDocuments<TDocument>();

            //Loads the latest version of documents as preview mode is enabled
            if (SiteContext.IsPreviewEnabled)
            {
                query = query
                    .AddColumns("NodeSiteID") //Required for preview mode in Admin UI
                    .OnSite(SiteContext.SiteName)
                    .LatestVersion()
                    .Culture(SiteContext.PreviewCulture);
            }
            else
            {
                query = query
                    .OnSite(SiteContext.SiteName)
                    .Published()
                    .PublishedVersion()
                    .Culture(SiteContext.CurrentSiteCulture);
            }

            return query;
        }
    }
}
