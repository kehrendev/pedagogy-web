﻿using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using Pedagogy.Custom.Generated.ModuleClasses;

[assembly: RegisterObjectType(typeof(SalesInfo), SalesInfo.OBJECT_TYPE)]

namespace Pedagogy.Custom.Generated.ModuleClasses
{
    /// <summary>
    /// Data container class for <see cref="SalesInfo"/>.
    /// </summary>
    [Serializable]
    public partial class SalesInfo : AbstractInfo<SalesInfo>
    {
        /// <summary>
        /// Object type.
        /// </summary>
        public const string OBJECT_TYPE = "pedagogy.sales";


        /// <summary>
        /// Type information.
        /// </summary>
        public static readonly ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(SalesInfoProvider), OBJECT_TYPE, "Pedagogy.Sales", "SalesID", "SalesLastModified", "SalesGuid", null, null, null, null, null, null)
        {
            ModuleName = "pedagogySettings",
            TouchCacheDependencies = true,
        };


        /// <summary>
        /// Sales ID.
        /// </summary>
        [DatabaseField]
        public virtual int SalesID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SalesID"), 0);
            }
            set
            {
                SetValue("SalesID", value);
            }
        }


        /// <summary>
        /// Sales guid.
        /// </summary>
        [DatabaseField]
        public virtual Guid SalesGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("SalesGuid"), Guid.Empty);
            }
            set
            {
                SetValue("SalesGuid", value);
            }
        }


        /// <summary>
        /// Sales last modified.
        /// </summary>
        [DatabaseField]
        public virtual DateTime SalesLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SalesLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SalesLastModified", value);
            }
        }


        /// <summary>
        /// Sale date.
        /// </summary>
        [DatabaseField]
        public virtual DateTime SaleDate
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SaleDate"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SaleDate", value, DateTimeHelper.ZERO_TIME);
            }
        }


        /// <summary>
        /// Sale class ID.
        /// </summary>
        [DatabaseField]
        public virtual int SaleClassID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SaleClassID"), 0);
            }
            set
            {
                SetValue("SaleClassID", value, 0);
            }
        }


        /// <summary>
        /// Sale class name.
        /// </summary>
        [DatabaseField]
        public virtual string SaleClassName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SaleClassName"), String.Empty);
            }
            set
            {
                SetValue("SaleClassName", value, String.Empty);
            }
        }


        /// <summary>
        /// Sale author name.
        /// </summary>
        [DatabaseField]
        public virtual string SaleAuthorName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SaleAuthorName"), String.Empty);
            }
            set
            {
                SetValue("SaleAuthorName", value, String.Empty);
            }
        }


        /// <summary>
        /// Sale quantity.
        /// </summary>
        [DatabaseField]
        public virtual int SaleQuantity
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SaleQuantity"), 0);
            }
            set
            {
                SetValue("SaleQuantity", value, 0);
            }
        }


        /// <summary>
        /// Sale type.
        /// </summary>
        [DatabaseField]
        public virtual string SaleType
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SaleType"), String.Empty);
            }
            set
            {
                SetValue("SaleType", value, String.Empty);
            }
        }


        /// <summary>
        /// Sale invoice number.
        /// </summary>
        [DatabaseField]
        public virtual int SaleInvoiceNumber
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SaleInvoiceNumber"), 0);
            }
            set
            {
                SetValue("SaleInvoiceNumber", value, 0);
            }
        }


        /// <summary>
        /// Sale note.
        /// </summary>
        [DatabaseField]
        public virtual string SaleNote
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SaleNote"), String.Empty);
            }
            set
            {
                SetValue("SaleNote", value, String.Empty);
            }
        }


        /// <summary>
        /// Sale cost.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleCost
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleCost"), 0m);
            }
            set
            {
                SetValue("SaleCost", value, 0m);
            }
        }


        /// <summary>
        /// Sale discount.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleDiscount
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleDiscount"), 0m);
            }
            set
            {
                SetValue("SaleDiscount", value, 0m);
            }
        }


        /// <summary>
        /// Sale coupon discount.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleCouponDiscount
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleCouponDiscount"), 0m);
            }
            set
            {
                SetValue("SaleCouponDiscount", value, 0m);
            }
        }


        /// <summary>
        /// Sale subtotal.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleSubtotal
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleSubtotal"), 0m);
            }
            set
            {
                SetValue("SaleSubtotal", value, 0m);
            }
        }


        /// <summary>
        /// Sale E commerce fee.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleECommerceFee
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleECommerceFee"), 0m);
            }
            set
            {
                SetValue("SaleECommerceFee", value, 0m);
            }
        }


        /// <summary>
        /// Sale net total.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleNetTotal
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleNetTotal"), 0m);
            }
            set
            {
                SetValue("SaleNetTotal", value, 0m);
            }
        }


        /// <summary>
        /// Sale commission.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleCommission
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleCommission"), 0m);
            }
            set
            {
                SetValue("SaleCommission", value, 0m);
            }
        }


        /// <summary>
        /// Sale author commission.
        /// </summary>
        [DatabaseField]
        public virtual decimal SaleAuthorCommission
        {
            get
            {
                return ValidationHelper.GetDecimal(GetValue("SaleAuthorCommission"), 0m);
            }
            set
            {
                SetValue("SaleAuthorCommission", value, 0m);
            }
        }


        /// <summary>
        /// Sale affiliate code.
        /// </summary>
        [DatabaseField]
        public virtual string SaleAffiliateCode
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SaleAffiliateCode"), String.Empty);
            }
            set
            {
                SetValue("SaleAffiliateCode", value, String.Empty);
            }
        }


        /// <summary>
        /// Sale reconciled on.
        /// </summary>
        [DatabaseField]
        public virtual DateTime SaleReconciledOn
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SaleReconciledOn"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SaleReconciledOn", value, DateTimeHelper.ZERO_TIME);
            }
        }


        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            SalesInfoProvider.DeleteSalesInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            SalesInfoProvider.SetSalesInfo(this);
        }


        /// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info.</param>
        /// <param name="context">Streaming context.</param>
        protected SalesInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Creates an empty instance of the <see cref="SalesInfo"/> class.
        /// </summary>
        public SalesInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Creates a new instances of the <see cref="SalesInfo"/> class from the given <see cref="DataRow"/>.
        /// </summary>
        /// <param name="dr">DataRow with the object data.</param>
        public SalesInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }
    }
}