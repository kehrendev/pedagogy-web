﻿using CMS.DocumentEngine.Types.Pedagogy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedagogy.DataAccess.Dto.Solutions
{
    public class SolutionItemDto : BaseDto
    {
        public SolutionItemDto()
        {

        }

        public SolutionItemDto(Solution solution) : base(solution)
        {
            SolutionName = solution.SolutionName;
            SolutionSummary = solution.SolutionSummary;
            SolutionTeaserImg = solution.SolutionTeaserImg;
            SolutionTeaserImgAltText = solution.SolutionTeaserImgAltText;
            SolutionLinkText = solution.SolutionLinkText;
            SolutionDescription = solution.SolutionDescription;
        }


        public string SolutionName { get; set; }
        public string SolutionSummary { get; set; }
        public string SolutionTeaserImg { get; set; }
        public string SolutionTeaserImgAltText { get; set; }
        public string SolutionLinkText { get; set; }
        public string SolutionDescription { get; set; }
    }
}
