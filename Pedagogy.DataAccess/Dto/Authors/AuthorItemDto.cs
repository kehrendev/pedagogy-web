﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using System.Linq;

namespace Pedagogy.DataAccess.Dto.Authors
{
    public class AuthorItemDto : BaseDto
    {
        public AuthorItemDto()
        {

        }

        public AuthorItemDto(Author author) : base(author)
        {
            AuthorName = author.AuthorName;
            AuthorSpecialties = author.AuthorSpecialties;
            AuthorImg = author.AuthorImg;
            AuthorImgAltText = author.AuthorImgAltText;
            AuthorBio = author.AuthorBio;
            AuthorShortBio = author.AuthorShortBio;
            AuthorAreaOfExpertise = author.AuthorAreaOfExpertise;
            AuthorAssociation = author.AuthorAssociation;
            AuthorWebsiteLink1 = author.AuthorWebsiteLink1;
            AuthorWebsiteLink2 = author.AuthorWebsiteLink2;
            AuthorWebsiteLink3 = author.AuthorWebsiteLink3;
            Url = author.NodeAliasPath;


            var siblings = AuthorProvider.GetAuthors()
                .Path(author.Parent.NodeAliasPath, PathTypeEnum.Children)
                .OrderBy("NodeOrder")
                .Where(x => x.ClassName == Author.CLASS_NAME)
                .ToList();

            var authorClone = siblings.Where(x => x.DocumentID == author.DocumentID).FirstOrDefault() ?? null;
            int currentAuthorIndex = -1;
            if (authorClone != null) currentAuthorIndex = siblings.IndexOf(authorClone);

            if (currentAuthorIndex != -1)
            {
                PrevAuthor = currentAuthorIndex == 0 ? null : new AuthorItemDto() { AuthorName = siblings.ElementAt(currentAuthorIndex - 1).AuthorName, Url = siblings.ElementAt(currentAuthorIndex - 1).NodeAliasPath, AuthorImg = siblings.ElementAt(currentAuthorIndex - 1).AuthorImg };
                NextAuthor = currentAuthorIndex == siblings.Count() - 1 ? null : new AuthorItemDto() { AuthorName = siblings.ElementAt(currentAuthorIndex + 1).AuthorName, Url = siblings.ElementAt(currentAuthorIndex + 1).NodeAliasPath, AuthorImg = siblings.ElementAt(currentAuthorIndex + 1).AuthorImg };
            }

        }


        public string AuthorName { get; set; }
        public string AuthorSpecialties { get; set; }
        public string AuthorImg { get; set; }
        public string AuthorImgAltText { get; set; }
        public string AuthorBio { get; set; }
        public string AuthorShortBio { get; set; }
        public string AuthorAreaOfExpertise { get; set; }
        public string AuthorAssociation { get; set; }
        public string AuthorWebsiteLink1 { get; set; }
        public string AuthorWebsiteLink2 { get; set; }
        public string AuthorWebsiteLink3 { get; set; }


        public string Url { get; set; }
        public AuthorItemDto NextAuthor { get; set; }
        public AuthorItemDto PrevAuthor { get; set; }
    }
}
