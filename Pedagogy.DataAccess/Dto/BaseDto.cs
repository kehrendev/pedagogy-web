﻿using CMS.Base;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using System;
using System.Linq;

namespace Pedagogy.DataAccess.Dto
{
    public class BaseDto : IDto
    {
        public BaseDto()
        {

        }

        public BaseDto(TreeNode node)
        {
            node.LoadInheritedValues(new[] { nameof(TreeNode.DocumentPageTitle), nameof(TreeNode.DocumentPageDescription), nameof(TreeNode.DocumentPageKeyWords) });
            //node.LoadInheritedValues(new[] { "DocumentPageTitle", "DocumentPageDescription", "DocumentPageKeyWords" });
            MetaTitle = (string.IsNullOrEmpty(node.DocumentName) ? node.DocumentPageTitle : node.DocumentName);
            MetaDescription = node.DocumentPageDescription;
            MetaKeywords = node.DocumentPageKeyWords;
            UsePageBuilder = true;
            ShowInNavigation = node.GetBooleanValue("ShowInNavigation", true);
            ExcludeFromSearch = node.GetBooleanValue("ExcludeFromSearch", false);
            InfoBarIsVisible = node.GetBooleanValue("InfoBarIsVisible", true);
        }

        public BaseDto(Guid nodeGuid)
        {
            var node = DocumentHelper.GetDocuments()
                .OnSite(SiteContext.CurrentSiteID)
                .Culture("en-us")
                .Published(true)
                .Columns("DocumentPageTitle", "DocumentPageDescription", "DocumentPageKeywords", "ClassIsMenuItemType", "ShowInNavigation", "ExcludeFromSearch", "InfoBarIsVisible", "DocumentName")
                //.Columns(nameof(TreeNode.DocumentPageTitle), nameof(TreeNode.DocumentPageDescription), nameof(TreeNode.DocumentPageKeyWords), nameof(BasePage.ShowInNavigation), nameof(BasePage.ExcludeFromSearch), nameof(BasePage.InfoBarIsVisible), nameof(BasePage.DocumentName))
                .WhereEquals(nameof(TreeNode.NodeGUID), nodeGuid)
                .FirstOrDefault();

            node.LoadInheritedValues(new[] { nameof(TreeNode.DocumentPageTitle), nameof(TreeNode.DocumentPageDescription), nameof(TreeNode.DocumentPageKeyWords) });
            //node.LoadInheritedValues(new[] { "DocumentPageTitle", "DocumentPageDescription", "DocumentPageKeyWords" });
            MetaTitle = (string.IsNullOrEmpty(node.DocumentName) ? node.DocumentPageTitle : node.DocumentName);
            MetaDescription = node.DocumentPageDescription;
            MetaKeywords = node.DocumentPageKeyWords;
            UsePageBuilder = true;
            ShowInNavigation = node.GetBooleanValue("ShowInNavigation", true);
            ExcludeFromSearch = node.GetBooleanValue("ExcludeFromSearch", false);
            InfoBarIsVisible = node.GetBooleanValue("InfoBarIsVisible", true);
        }


        // Base Required Data
        public string MetaTitle { get; set; } = "";
        public string MetaDescription { get; set; } = "";
        public string MetaKeywords { get; set; } = "";
        public bool UsePageBuilder { get; set; } = true;
        public bool ShowInNavigation { get; set; } = true;
        public bool ExcludeFromSearch { get; set; } = false;
        public bool InfoBarIsVisible { get; set; } = true;
    }

    public class BaseDto<T> : BaseDto where T : ITreeNode
    {
        public T Node { get; set; }
    }
}
