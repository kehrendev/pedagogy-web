﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Ecommerce;

using System;

namespace Pedagogy.DataAccess.Dto.Courses
{
    public class CourseItemDto : BaseDto
    {
        public CourseItemDto() { }

        public CourseItemDto(SKUInfo sku, string title, double contactHours, int nodeSKUID, string objectives)
        {
            CourseTitle = title;
            SKU = sku;
            CourseContactHours = contactHours;
            CourseObjectives = objectives;
            NodeSKUID = nodeSKUID;
        }

        public CourseItemDto(Course course) : base(course)
        {
            CourseTitle = course.CourseTitle;
            CourseShortName = course.CourseShortName;
            CourseDemoClassID = course.CourseDemoClassID;
            CoursePrice = course.CoursePrice;
            CourseContactHours = course.CourseContactHours;
            CourseAuthor = course.CourseAuthor;
            CourseSummary = course.CourseSummary;
            CourseDescription = course.CourseDescription;
            CourseObjectives = course.CourseObjectives;
            CourseCurriculum = course.CourseCurriculum;
            CourseLicenseText = course.CourseLicenseText;
            CourseImg = course.CourseImg;
            CourseImgAltText = course.CourseImgAltText;
            Url = course.NodeAliasPath;
            CourseCategories = course.CourseCategories;
            PriceTBD = course.CoursePriceTBD;

            Course = course;

            Name = course.DocumentName;
            Description = course.DocumentSKUDescription;
            ShortDescription = course.DocumentSKUShortDescription;
            SKUID = course.NodeSKUID;
            SKU = course.SKU;
            NodeSKUID = course.NodeSKUID;
            ProductPageGuid = new Guid();
            ProductPageAlias = course.NodeAlias;
            SKUTreeNode = TreeNode.New();
            PublicStatus = course.Product.PublicStatus?.PublicStatusDisplayName;

           //PriceDetail = new ProductCatalogPrices(course.CoursePrice, course.CoursePrice, 0, );
        }

        public CourseItemDto(CourseMembership course) : base(course)
        {
            CourseTitle = course.CourseTitle;
            CourseShortName = course.CourseShortName;
            CourseDemoClassID = course.CourseDemoClassID;
            CoursePrice = course.CoursePrice;
            CourseContactHours = course.CourseContactHours;
            CourseAuthor = course.CourseAuthor;
            CourseSummary = course.CourseSummary;
            CourseDescription = course.CourseDescription;
            CourseObjectives = course.CourseObjectives;
            CourseCurriculum = course.CourseCurriculum;
            CourseLicenseText = course.CourseLicenseText;
            CourseImg = course.CourseImg;
            CourseImgAltText = course.CourseImgAltText;
            Url = course.NodeAliasPath;
            CourseCategories = course.CourseCategories;
            PriceTBD = course.CoursePriceTBD;

            CourseMembership = course;

            Name = course.DocumentName;
            Description = course.DocumentSKUDescription;
            ShortDescription = course.DocumentSKUShortDescription;
            SKUID = course.NodeSKUID;
            SKU = course.SKU;
            ProductPageGuid = new Guid();
            ProductPageAlias = course.NodeAlias;
            SKUTreeNode = TreeNode.New();
            PublicStatus = course.Product.PublicStatus?.PublicStatusDisplayName;

            //PriceDetail = new ProductCatalogPrices(course.CoursePrice, course.CoursePrice, 0, );
        }


        public string CourseTitle { get; set; }
        public string CourseShortName { get; set; }
        public int CourseDemoClassID { get; set; }
        public double CoursePrice { get; set; }
        public double CourseContactHours { get; set; }
        public string CourseAuthor { get; set; }
        public string CourseSummary { get; set; }
        public string CourseDescription { get; set; }
        public string CourseObjectives { get; set; }
        public string CourseCurriculum { get; set; }
        public string CourseLicenseText { get; set; }
        public string CourseImg { get; set; }
        public string CourseImgAltText { get; set; }
        public string CourseCategories { get; set; }
        public string Url { get; set; }
        public bool CourseIsForFacilityAdmin { get; set; }
        public bool PriceTBD { get; set; }

        public Course Course { get; set; }

        public CourseMembership CourseMembership { get; set; }

        //Product info
        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public int SKUID { get; set; }
        public int NodeSKUID { get; set; }
        public SKUInfo SKU { get; set; }
        public string CourseSKUNumber { get; set; }
        public Guid ProductPageGuid { get; set; }
        public string ProductPageAlias { get; set; }
        public string ImagePath { get; set; }
        public ProductCatalogPrices PriceDetail { get; set; }
        public TreeNode SKUTreeNode { get; set; }
        public string PublicStatus { get; set; }
    }
}
