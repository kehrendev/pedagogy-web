﻿namespace Pedagogy.DataAccess.Dto.Courses
{
    public class PriceItemDto : IDto
    {
        public decimal Price;
        public decimal ListPrice;
        public string CurrencyFormatString;
    }
}
