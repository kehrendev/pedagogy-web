﻿using CMS.DocumentEngine.Types.Pedagogy;

namespace Pedagogy.DataAccess.Dto.Maps
{
    public class MapItemDto : BaseDto
    {
        public MapItemDto()
        {

        }

        public MapItemDto(Map map) : base(map)
        {
            MapTitle = map.MapTitle;
            MapText = map.MapText;
            MapSummaryText = map.MapSummaryText;
            Url = map.NodeAliasPath;
        }

        public string MapTitle { get; set; }
        public string MapText { get; set; }
        public string MapSummaryText { get; set; }
        public string Url { get; set; }
    }
}
