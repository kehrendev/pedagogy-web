﻿using CMS.DocumentEngine.Types.Pedagogy;

namespace Pedagogy.DataAccess.Dto.pedagogyPage
{
    public class PedagogyPageDto : BaseDto
    {
        public PedagogyPageDto()
        {

        }

        public PedagogyPageDto(PedagogyPage page) : base(page)
        {
            PageName = page.DocumentName;
        }

        public string PageName { get; set; }
    }
}
