﻿namespace Pedagogy.DataAccess.Dto.Files
{
    public class FileItemDto : IDto
    {
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public string FileTeaserImg { get; set; }
        public string FileTeaserImgAltText { get; set; }
    }
}
