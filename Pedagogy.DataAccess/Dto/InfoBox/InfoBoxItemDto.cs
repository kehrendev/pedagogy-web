﻿namespace Pedagogy.DataAccess.Dto.InfoBox
{
    public class InfoBoxItemDto : IDto
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string BackgroundImg { get; set; }
        public string BtnText { get; set; }
        public string BtnUrl { get; set; }
    }
}
