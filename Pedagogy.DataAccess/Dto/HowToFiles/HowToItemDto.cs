﻿using CMS.DocumentEngine.Types.Pedagogy;

namespace Pedagogy.DataAccess.Dto.HowToFiles
{
    public class HowToItemDto : BaseDto
    {
        public HowToItemDto()
        {

        }

        public HowToItemDto(HowTo howToItem) : base(howToItem)
        {
            HowToTitle = howToItem.HowToTitle;
            HowToFileURL = howToItem.HowToFileUrl;
            HowToText = howToItem.HowToText;
            HowToForFacilityAdmin = howToItem.HowToForFacilityAdmin;
            ParentNodeName = howToItem.Parent.NodeName;
            HowToUrl = howToItem.NodeAliasPath;
        }

        public string HowToTitle { get; set; }
        public string HowToFileURL { get; set; }
        public string HowToText { get; set; }
        public bool HowToForFacilityAdmin { get; set; }
        public string ParentNodeName { get; set; }
        public string HowToUrl { get; set; }
    }
}
