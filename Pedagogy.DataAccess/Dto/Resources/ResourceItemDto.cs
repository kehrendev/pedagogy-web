﻿using CMS.DocumentEngine.Types.Pedagogy;
using Pedagogy.DataAccess.Dto.Maps;

namespace Pedagogy.DataAccess.Dto.Resources
{
    public class ResourceItemDto : BaseDto
    {
        public ResourceItemDto()
        {

        }

        public ResourceItemDto(Resource resource) : base(resource)
        {
            ResourceName = resource.ResourceName;
            ResourceSummary = resource.ResourceSummary;
            ResourceDescription = resource.ResourceDescription;
            ResourceImg = resource.ResourceImg;
            ResourceImgAltText = resource.ResourceImgAltText;
            Url = resource.NodeAliasPath;
        }

        public string ResourceName { get; set; }
        public string ResourceSummary { get; set; }
        public string ResourceDescription { get; set; }
        public string ResourceImg { get; set; }
        public string ResourceImgAltText { get; set; }
        public string Url { get; set; }
    }
}
