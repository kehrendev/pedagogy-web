﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using System;
using System.Linq;

namespace Pedagogy.DataAccess.Dto.News
{
    public class NewsItemDto : BaseDto
    {
        public NewsItemDto()
        {

        }

        public NewsItemDto(CMS.DocumentEngine.Types.Pedagogy.News news) : base(news)
        {
            NewsTitle = news.NewsTitle;
            NewsReleaseDate = news.NewsReleaseDate;
            NewsText = news.NewsText;
            NewsSummary = news.NewsSummary;
            NewsImg = news.NewsImg;
            NewsImgAltText = news.NewsImgAltText;
            NewsCategory = news.NewsCategory;
            Url = news.NodeAliasPath;

            Year = news.NewsReleaseDate.ToString("yyyy");
            Month = news.NewsReleaseDate.ToString("MMMM");
            Day = news.NewsReleaseDate.ToString("dd");

            var siblings = NewsProvider.GetNews()
                .Path(news.Parent.NodeAliasPath, PathTypeEnum.Children)
                .OrderBy("NodeOrder")
                .Where(x => x.ClassName == CMS.DocumentEngine.Types.Pedagogy.News.CLASS_NAME)
                .ToList();

            var newsClone = siblings.Where(x => x.DocumentID == news.DocumentID).FirstOrDefault() ?? null;
            int currentNewsArticleIndex = -1;
            if (newsClone != null) currentNewsArticleIndex = siblings.IndexOf(newsClone);

            if (currentNewsArticleIndex != -1)
            {
                PrevNewsArticle = currentNewsArticleIndex == 0 ? null : new NewsItemDto() { NewsTitle = siblings.ElementAt(currentNewsArticleIndex - 1).NewsTitle, Url = siblings.ElementAt(currentNewsArticleIndex - 1).NodeAliasPath, NewsImg = siblings.ElementAt(currentNewsArticleIndex - 1).NewsImg };
                NextNewsArticle = currentNewsArticleIndex == siblings.Count() - 1 ? null : new NewsItemDto() { NewsTitle = siblings.ElementAt(currentNewsArticleIndex + 1).NewsTitle, Url = siblings.ElementAt(currentNewsArticleIndex + 1).NodeAliasPath, NewsImg = siblings.ElementAt(currentNewsArticleIndex + 1).NewsImg };
            }
        }


        public string NewsTitle { get; set; }
        public DateTime NewsReleaseDate { get; set; }
        public string NewsText { get; set; }
        public string NewsSummary { get; set; }
        public string NewsImg { get; set; }
        public string NewsImgAltText { get; set; }
        public string NewsCategory { get; set; }

        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }


        public string Url { get; set; }
        public NewsItemDto NextNewsArticle { get; set; }
        public NewsItemDto PrevNewsArticle { get; set; }
    }
}
