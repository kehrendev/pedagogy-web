﻿
namespace Pedagogy.DataAccess.Dto.Navigation
{
    public class SocialMediaItemDto
    {
        public string SocialName { get; set; }
        public string SocialUrl { get; set; }
        public string SocialIcon { get; set; }
    }
}
