﻿namespace Pedagogy.DataAccess.Dto.Navigation
{
    public class NavigationItemDto : IDto
    {
        //Main nav button properties
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }

        //Sub nav button properties
        public int NodeID { get; set; }
        public int ParentID { get; set; }
        public int NestingLevel { get; set; }
        public bool HasChildren { get; set; }
    }
}