﻿using CMS.CustomTables.Types.Pedagogy;

namespace Pedagogy.DataAccess.Dto.FacilityWaitList
{
    public class FacilityWaitListItemDto : BaseDto
    {
        public FacilityWaitListItemDto()
        {

        }

        public FacilityWaitListItemDto(FacilityWaitListItem item)
        {
            AdminUserID = item.AdminUserID;
            AdminFirstName = item.AdminFirstName;
            AdminLastName = item.AdminLastName;
            AdminEmail = item.AdminEmail;
            IsFacility = item.IsFacility;
            FacilityName = item.FacilityName;
            FacilityAcctingCode = item.FacilityAcctingCode;
            FacilityAddress1 = item.FacilityAddress1;
            FacilityAddress2 = item.FacilityAddress2;
            FacilityCity = item.FacilityCity;
            FacilityState = item.FacilityState;
            FacilityZip = item.FacilityZip;
            NoApplicableParentCorp = item.NoApplicableParentCorporation;
            ParentCorpName = item.ParentCorporationName;
            ParentCorpWebsiteURL = item.ParentCorporationWebsiteURL;
            ItemID = item.ItemID;
        }

        public int AdminUserID { get; set; }
        public string AdminFirstName { get; set; }
        public string AdminLastName { get; set; }
        public string AdminEmail { get; set; }
        public bool IsFacility { get; set; }
        public string FacilityName { get; set; }
        public string FacilityAcctingCode { get; set; }
        public string FacilityAddress1 { get; set; }
        public string FacilityAddress2 { get; set; }
        public string FacilityCity { get; set; }
        public string FacilityState { get; set; }
        public string FacilityZip { get; set; }
        public bool NoApplicableParentCorp { get; set; }
        public string ParentCorpName { get; set; }
        public string ParentCorpWebsiteURL { get; set; }
        public int ItemID { get; set; }
    }
}
