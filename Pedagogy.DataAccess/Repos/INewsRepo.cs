﻿using CMS.Taxonomy;
using Pedagogy.DataAccess.Dto.News;
using System;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface INewsRepo : IRepo
    {
        NewsItemDto GetNewsItemByNodeGuid(Guid nodeGuid, bool isPublished);
        IEnumerable<NewsItemDto> GetAllNewsArticles();
        NewsItemDto GetNewsArticleByTitle(string title);
        IEnumerable<NewsItemDto> GetNewsArticlesByYear(string year);
        IEnumerable<NewsItemDto> GetNewsArticlesByCategory(string category);
        IEnumerable<CategoryInfo> GetAllNewsArticleCategories();
    }
}
