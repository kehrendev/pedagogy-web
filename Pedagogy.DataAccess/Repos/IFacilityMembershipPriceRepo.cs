﻿namespace Pedagogy.DataAccess.Repos
{
    public interface IFacilityMembershipPriceRepo : IRepo
    {
        double GetFacilityMembershipPrice(string category, string status);
        decimal GetFacilityMembershipDiscount(string category, bool over200 = false, int qty = 0, double contactHours = 0);

        int GetPriceCategory(string categoryName);
    }
}
