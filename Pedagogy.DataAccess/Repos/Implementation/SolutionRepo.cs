﻿using CMS.DocumentEngine.Types.Pedagogy;
using Pedagogy.DataAccess.Dto.Solutions;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;
using System;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class SolutionRepo : BaseRepo, ISolutionRepo
    {
        public SolutionRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public SolutionItemDto GetSolutionItemByNodeGuid(Guid nodeGuid, bool isPublished)
        {
            Func<Solution> dataLoadMethod = () => SolutionProvider.GetSolutions()
                .Columns(
                        nameof(Solution.DocumentName),
                        nameof(Solution.DocumentPageDescription),
                        nameof(Solution.DocumentPageKeyWords),
                        nameof(Solution.ExcludeFromSearch),
                        nameof(Solution.ShowInNavigation),
                        nameof(Solution.InfoBarIsVisible),
                        nameof(Solution.SolutionName),
                        nameof(Solution.SolutionSummary),
                        nameof(Solution.SolutionTeaserImg),
                        nameof(Solution.SolutionTeaserImgAltText),
                        nameof(Solution.SolutionLinkText),
                        nameof(Solution.SolutionDescription)
                        )
                .WhereEquals(nameof(Solution.NodeGUID), nodeGuid)
                .Published(!isPublished)
                .OnCurrentSite()
                .TypedResult
                .FirstOrDefault();

            var res = CacheService.Cache<Solution>(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(Solution.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid));

            var dto = new SolutionItemDto(res);
            return dto;
        }
    }
}
