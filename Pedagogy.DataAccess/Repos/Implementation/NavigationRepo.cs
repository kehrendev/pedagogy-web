﻿using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.SiteProvider;

using Pedagogy.DataAccess.Dto.Navigation;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class NavigationRepo : BaseRepo, INavigationRepo
    {
        public NavigationRepo(IDocQueryService dqs, ICacheService cs) : base(dqs, cs) { }
        private string[] allowedTypes = { PedagogyPage.CLASS_NAME };

        private string[] columns = {
            nameof(TreeNode.DocumentName),
            nameof(TreeNode.NodeAliasPath),
            nameof(TreeNode.NodeID),
            nameof(TreeNode.NodeLevel),
            nameof(TreeNode.NodeParentID),
            nameof(TreeNode.NodeHasChildren)
        };

        public IEnumerable<NavigationItemDto> GetCategoryNavigation(string categoryName, int nestingLevel = -1)
        {
            Func<IEnumerable<TreeNode>> dataLoadMethod = () => DocumentHelper.GetDocuments()
                .OnSite(SiteContext.CurrentSiteName)
                .Culture(CMS.Localization.LocalizationContext.CurrentCulture.CultureCode)
                .Types(allowedTypes)
                .InCategories(categoryName)
                .NestingLevel(nestingLevel)
                .Columns(columns)
                .OrderBy(nameof(TreeNode.NodeLevel), nameof(TreeNode.NodeOrder), nameof(TreeNode.NodeName))
                .PublishedVersion()
                .Published()
                .TypedResult;

            var res = CacheService.Cache<IEnumerable<TreeNode>>(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodesCacheName(TreeNode.OBJECT_TYPE + "|" + categoryName, CacheDependencyType.ALL), 
                CacheService.GetNodesCacheDependencyKey(TreeNode.OBJECT_TYPE + "|" + categoryName, CacheDependencyType.ALL));

            return res.Select(x => new NavigationItemDto
            {
                LinkText = x.DocumentName,
                LinkUrl = x.NodeAliasPath,
                NodeID = x.NodeID,
                ParentID = x.NodeParentID,
                NestingLevel = x.NodeLevel,
                HasChildren = NodeHasCategoryChildren(x.Children, categoryName)
            });
        }

        private bool NodeHasCategoryChildren(TreeNodeCollection children, string categoryName)
        {
            foreach (TreeNode child in children)
            {
                IEnumerable<BaseInfo> categories = ((IEnumerable<BaseInfo>)child.Categories).ToList();
                foreach (var category in categories)
                {
                    if (category.GetValue("CategoryName").ToString().ToLowerInvariant().Equals(categoryName.ToLowerInvariant()))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public IEnumerable<TreeNode> GetPagesByCategory(string[] categories)
        {
            Func<IEnumerable<TreeNode>> dataLoadMethod = () => DocumentHelper.GetDocuments()
                .InCategories(categories)
                .Columns(nameof(TreeNode.NodeAliasPath), nameof(TreeNode.DocumentName))
                .Published()
                .PublishedVersion()
                .OnCurrentSite()
                .TypedResult;

            return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodesCacheName(TreeNode.OBJECT_TYPE + "|" + string.Join("|", categories), CacheDependencyType.ALL),
                CacheService.GetNodesCacheDependencyKey(TreeNode.OBJECT_TYPE + "|" + string.Join("|", categories), CacheDependencyType.ALL));
        }
    }
}
