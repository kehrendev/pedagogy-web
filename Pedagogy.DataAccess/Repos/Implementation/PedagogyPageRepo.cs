﻿using CMS.DocumentEngine.Types.Pedagogy;

using Pedagogy.DataAccess.Dto.pedagogyPage;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class PedagogyPageRepo : BaseRepo, IPedagogyPageRepo
    {
        public PedagogyPageRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public PedagogyPageDto GetPedagogyPageByNodeGuid(Guid nodeGuid, bool isPublished = true)
        {
            //Func<PedagogyPage> dataLoadMethod = () => PedagogyPageProvider.GetPedagogyPages()
            //    .Columns(
            //            nameof(PedagogyPage.DocumentName),
            //            nameof(PedagogyPage.DocumentPageDescription),
            //            nameof(PedagogyPage.DocumentPageKeyWords),
            //            nameof(PedagogyPage.ExcludeFromSearch),
            //            nameof(PedagogyPage.ShowInNavigation),
            //            nameof(PedagogyPage.InfoBarIsVisible)
            //            )
            //    .WhereEquals(nameof(PedagogyPage.NodeGUID), nodeGuid)
            //    .Published(!isPublished)
            //    .OnCurrentSite()
            //    .CheckPermissions()
            //    .TypedResult
            //    .FirstOrDefault();

            //var res = CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(),
            //    CacheService.GetNodeCacheName(PedagogyPage.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid));

            //if (res != null) return new PedagogyPageDto(res);

            var page = PedagogyPageProvider.GetPedagogyPages()
                .Columns(
                        nameof(PedagogyPage.DocumentName),
                        nameof(PedagogyPage.DocumentPageDescription),
                        nameof(PedagogyPage.DocumentPageKeyWords),
                        nameof(PedagogyPage.ExcludeFromSearch),
                        nameof(PedagogyPage.ShowInNavigation),
                        nameof(PedagogyPage.InfoBarIsVisible)
                        )
                .WhereEquals(nameof(PedagogyPage.NodeGUID), nodeGuid)
                .Published(!isPublished)
                .PublishedVersion()
                .OnCurrentSite()
                .CheckPermissions()
                .TypedResult
                .FirstOrDefault();

            if (page == null) return null;

            return new PedagogyPageDto(page);
        }
    }
}
