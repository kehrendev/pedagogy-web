﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Ecommerce;
using CMS.Helpers;
using CMS.Localization;
using CMS.Relationships;
using CMS.SiteProvider;

using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class CourseRepo : BaseRepo, ICourseRepo
    {
        public CourseRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public CourseItemDto GetCourseByNodeGuid(Guid nodeGuid)
        {
            Func<Course> dataLoadMethod = () => CourseProvider.GetCourses()
                .WhereEquals(nameof(Course.NodeGUID), nodeGuid)
                .Published(true)
                .PublishedVersion()
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .Columns(
                    nameof(Course.DocumentName),
                    nameof(Course.DocumentPageDescription),
                    nameof(Course.DocumentPageKeyWords),
                    nameof(Course.ExcludeFromSearch),
                    nameof(Course.ShowInNavigation),
                    nameof(Course.InfoBarIsVisible),
                    nameof(Course.CourseAuthor),
                    nameof(Course.CourseCategories),
                    nameof(Course.CourseContactHours),
                    nameof(Course.CourseCurriculum),
                    nameof(Course.CourseDemoClassID),
                    nameof(Course.CourseDescription),
                    nameof(Course.CourseImg),
                    nameof(Course.CourseImgAltText),
                    nameof(Course.CourseLicenseText),
                    nameof(Course.CourseObjectives),
                    nameof(Course.CoursePrice),
                    nameof(Course.CourseShortName),
                    nameof(Course.CourseSummary),
                    nameof(Course.CourseTitle),
                    nameof(Course.NodeAliasPath),
                    nameof(Course.CoursePriceTBD)
                )
                .OnSite(SiteContext.CurrentSiteName)
                .TypedResult
                .FirstOrDefault();

            return new CourseItemDto(CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(Course.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid)));
        }

        public CourseItemDto GetCourseMembershipByNodeGuid(Guid nodeGuid)
        {
            Func<CourseMembership> dataLoadMethod = () => CourseMembershipProvider.GetCourseMemberships()
                .WhereEquals(nameof(Course.NodeGUID), nodeGuid)
                .Published(true)
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .Columns(
                    nameof(CourseMembership.DocumentName),
                    nameof(CourseMembership.DocumentPageDescription),
                    nameof(CourseMembership.DocumentPageKeyWords),
                    nameof(CourseMembership.ExcludeFromSearch),
                    nameof(CourseMembership.ShowInNavigation),
                    nameof(CourseMembership.InfoBarIsVisible),
                    nameof(CourseMembership.CourseAuthor),
                    nameof(CourseMembership.CourseCategories),
                    nameof(CourseMembership.CourseContactHours),
                    nameof(CourseMembership.CourseCurriculum),
                    nameof(CourseMembership.CourseDemoClassID),
                    nameof(CourseMembership.CourseDescription),
                    nameof(CourseMembership.CourseImg),
                    nameof(CourseMembership.CourseImgAltText),
                    nameof(CourseMembership.CourseLicenseText),
                    nameof(CourseMembership.CourseObjectives),
                    nameof(CourseMembership.CoursePrice),
                    nameof(CourseMembership.CourseShortName),
                    nameof(CourseMembership.CourseSummary),
                    nameof(CourseMembership.CourseTitle),
                    nameof(CourseMembership.NodeAliasPath),
                    nameof(CourseMembership.CoursePriceTBD)
                )
                .OnSite(SiteContext.CurrentSiteName)
                .TypedResult
                .FirstOrDefault();

            return new CourseItemDto(CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(CourseMembership.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid)));
        }

        public IEnumerable<CourseItemDto> GetRelatedCourses(CourseItemDto currentCourse)
        {
            var course = CourseProvider.GetCourses()
                .Columns(
                    nameof(Course.NodeID)
                    )
                .Path(currentCourse.Url)
                .OnSite(SiteContext.CurrentSiteName)
                .FirstOrDefault();

            var relationshipDisplayName = "related course";
            var relationshipName = RelationshipNameInfoProvider.GetRelationshipNames()
                .WhereEquals("RelationshipDisplayName", relationshipDisplayName)
                .FirstOrDefault();

            var relationships = RelationshipInfoProvider.GetRelationships()
                .WhereEquals("LeftNodeID", course.NodeID)
                .WhereEquals("RelationshipNameID", relationshipName.RelationshipNameId);

            var relationshipRightSideIDs = relationships.Select(x => (int)x["RightNodeID"]).ToList();

            var relatedPages = DocumentHelper.GetDocuments()
                .WithCoupledColumns()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereIn("NodeID", relationshipRightSideIDs)
                .ToList()
                ?? new List<TreeNode>();


            List<CourseItemDto> relatedCourses = new List<CourseItemDto>();
            foreach (var item in relatedPages)
            {
                relatedCourses.Add(new CourseItemDto
                {
                    CourseShortName = item.DocumentName,
                    Url = item.NodeAliasPath
                });
            }

            return relatedCourses;
        }

        public List<CourseItemDto> GetCoursesByCategory(string[] categoryNames)
        {
            Func<List<Course>> dataLoadMethod = () => CourseProvider.GetCourses()
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .OnSite(SiteContext.CurrentSiteName)
                .InCategories(categoryNames)
                .Columns(nameof(Course.CourseShortName),
                    nameof(Course.CourseTitle),
                    nameof(Course.CourseSummary),
                    nameof(Course.CoursePrice),
                    nameof(Course.CourseContactHours),
                    nameof(Course.NodeSKUID),
                    nameof(Course.SKU.SKUNumber),
                    nameof(Course.SKU.SKUNeedsShipping)
                    )
                .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                .Where(x => !x.SKU.SKUNeedsShipping)
                .ToList();

            return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(CourseRepo)}|{nameof(GetCoursesByCategory)}|{string.Join("|", categoryNames)}",
                $"{nameof(CourseRepo)}|{nameof(GetCoursesByCategory)}|{string.Join("|", categoryNames)}")
                .Select(x => new CourseItemDto
                {
                    CourseShortName = x.CourseShortName,
                    CourseTitle = x.CourseTitle,
                    CoursePrice = x.CoursePrice,
                    CourseContactHours = x.CourseContactHours,
                    CourseSummary = x.CourseSummary,
                    Url = x.NodeAliasPath,
                    CourseSKUNumber = SKUInfoProvider.GetSKUInfo(x.NodeSKUID).SKUNumber,
                    SKUID = x.NodeSKUID
                })
                .ToList();
        }

        public CourseItemDto GetCourseByTitle(string title)
        {
            Func<Course> dataLoadMethod = () => CourseProvider.GetCourses()
                .Published(true)
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .OnSite(SiteContext.CurrentSiteName)
                .WhereEquals(nameof(Course.CourseTitle), title)
                .Columns(
                    nameof(Course.CourseTitle),
                    nameof(Course.CourseShortName),
                    nameof(Course.NodeGUID),
                    nameof(Course.NodeSKUID),
                    nameof(Course.CoursePrice),
                    nameof(Course.CourseContactHours),
                    nameof(Course.CourseSummary),
                    nameof(Course.NodeAliasPath),
                    nameof(Course.CourseImg),
                    nameof(Course.CourseImgAltText),
                    nameof(Course.SKU.SKUPrice),
                    nameof(Course.DocumentName),
                    nameof(Course.DocumentPageDescription),
                    nameof(Course.DocumentPageKeyWords),
                    nameof(Course.ExcludeFromSearch),
                    nameof(Course.ShowInNavigation),
                    nameof(Course.InfoBarIsVisible),
                    nameof(Course.SKU.SKUNumber),
                    nameof(Course.SKU.SKUNeedsShipping)
                    )
                .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                .Where(x => !x.SKU.SKUNeedsShipping)
                .FirstOrDefault();

            var res = CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(CourseRepo)}|{nameof(GetCourseByTitle)}|{title}",
                $"{nameof(CourseRepo)}|{nameof(GetCourseByTitle)}|{title}");

            return new CourseItemDto
                {
                    CourseTitle = res.CourseTitle,
                    CourseShortName = res.CourseShortName,
                    CoursePrice = res.CoursePrice,
                    CourseContactHours = res.CourseContactHours,
                    CourseCategories = res.CourseCategories,
                    CourseSummary = res.CourseSummary,
                    Url = res.NodeAliasPath,
                    CourseImg = res.CourseImg,
                    CourseImgAltText = res.CourseImgAltText,
                    SKUID = res.NodeSKUID,
                    SKU = res.SKU,
                    PublicStatus = res.Product.PublicStatus?.PublicStatusDisplayName,
                    Course = res
                };
        }

        public List<CourseItemDto> GetCoursesByAuthor(string authorUrl)
        {
            List<Course> dataLoadMethod() => CourseProvider.GetCourses()
                .OnCurrentSite()
                .Published()
                .PublishedVersion()
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .WhereEquals(nameof(Course.CourseAuthor), authorUrl)
                .Columns(
                    nameof(Course.CourseShortName),
                    nameof(Course.CourseImg),
                    nameof(Course.CourseImgAltText),
                    nameof(Course.CourseAuthor),
                    nameof(Course.NodeAliasPath),
                    nameof(Course.SKU.SKUNumber),
                    nameof(Course.SKU.SKUNeedsShipping)
                    )
                .WhereFalse(nameof(Course.SKU.SKUNeedsShipping))
                .WhereNotNull(nameof(Course.SKU.SKUNumber))
                .WhereNotEmpty(nameof(Course.SKU.SKUNumber))
                .ToList();

            return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(CourseRepo)}|{nameof(GetCoursesByAuthor)}|{authorUrl}",
                $"{nameof(CourseRepo)}|{nameof(GetCoursesByAuthor)}|{authorUrl}")
                .Select(x => new CourseItemDto
                {
                    CourseShortName = x.CourseShortName,
                    CourseImg = x.CourseImg,
                    CourseImgAltText = x.CourseImgAltText,
                    Url = x.NodeAliasPath
                })
                .OrderBy(x => x.CourseShortName)
                .ToList();
        }

        public IEnumerable<CourseItemDto> GetProductTypeCourses()
        {
            List<Course> dataLoadMethod() => DocumentHelper.GetDocuments<Course>()
                .Published()
                .PublishedVersion()
                .OnCurrentSite()
                .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                .Where(x => !x.SKU.SKUNeedsShipping)
                .Where(x => x.SKU.SKUProductType == SKUProductTypeEnum.Product)
                .OrderBy(x => x.CourseTitle)
                .Select(x => new Course { SKU = x.SKU, CourseTitle = x.CourseTitle, CourseContactHours = x.CourseContactHours, NodeSKUID = x.NodeSKUID, CourseObjectives = x.CourseObjectives })
                .ToList();

            return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(CourseRepo)}|{nameof(GetProductTypeCourses)}|all",
                $"{nameof(CourseRepo)}|{nameof(GetProductTypeCourses)}|all")
                .Select(x => new CourseItemDto(x.SKU, x.CourseTitle, x.CourseContactHours, x.NodeSKUID, x.CourseObjectives))
                .ToList();
        }

        public CourseItemDto GetCourseBySKUNumber(string classID, Func<Course, Course> select)
        {
            Func<Course> dataLoadMethod = () => DocumentHelper.GetDocuments<Course>()
                .OnCurrentSite()
                .Published()
                .PublishedVersion()
                .Where(x => x.SKU.SKUNumber == classID)
                .Select(select)
                .FirstOrDefault();

            return new CourseItemDto(CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(CourseRepo)}|{nameof(GetCourseBySKUNumber)}|{classID}",
                $"{nameof(CourseRepo)}|{nameof(GetCourseBySKUNumber)}|{classID}"));
        }
    }
}