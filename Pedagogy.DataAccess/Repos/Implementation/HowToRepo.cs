﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.SiteProvider;
using Pedagogy.DataAccess.Dto.HowToFiles;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class HowToRepo : BaseRepo, IHowToRepo
    {
        public HowToRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }
        private string[] columns = { nameof(HowTo.HowToTitle), nameof(HowTo.HowToFileUrl), nameof(HowTo.HowToText), nameof(HowTo.NodeAliasPath), nameof(HowTo.HowToForFacilityAdmin),
                nameof(HowTo.NodeParentID) };

        public IEnumerable<HowToItemDto> GetAllHowTos()
        {
            Func<IEnumerable<HowTo>> dataLoadMethod = () => HowToProvider.GetHowToes()
            .Columns(columns)
            .Published()
            .PublishedVersion()
            .OnSite(SiteContext.CurrentSiteName)
            .TypedResult
            .ToList();

            var res = CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), CacheService.GetNodesCacheName(HowTo.CLASS_NAME, CacheDependencyType.ALL), 
                CacheService.GetNodesCacheDependencyKey(HowTo.CLASS_NAME, CacheDependencyType.ALL));

            var items = new List<HowToItemDto>();
            foreach(var i in res)
            {
                items.Add(new HowToItemDto(i)
                {
                    ParentNodeName = GetParentNodeName(i.NodeParentID)
                });
            }

            return items;
        }

        public IEnumerable<HowToItemDto> GetAllStudentHowTos()
        {
            Func<IEnumerable<HowTo>> dataLoadMethod = () => HowToProvider.GetHowToes()
            .Columns(columns)
            .Published()
            .PublishedVersion()
            .OnSite(SiteContext.CurrentSiteName)
            .WhereFalse(nameof(HowTo.HowToForFacilityAdmin))
            .TypedResult
            .ToList();

            var res = CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), CacheService.GetNodesCacheName("StudentHowTos", CacheDependencyType.ALL),
                CacheService.GetNodesCacheDependencyKey(HowTo.CLASS_NAME, CacheDependencyType.ALL));

            var items = new List<HowToItemDto>();
            foreach (var i in res)
            {
                items.Add(new HowToItemDto(i)
                {
                    ParentNodeName = GetParentNodeName(i.NodeParentID)
                });
            }

            return items;
        }

        private string GetParentNodeName(int parentNodeID)
        {
            return DocumentHelper.GetDocuments()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereEquals(nameof(TreeNode.NodeID), parentNodeID)
                .FirstOrDefault().NodeName;
        }

        public HowToItemDto GetHowToItemByNodeGuid(Guid nodeGuid, bool isPublished)
        {
            Func<HowTo> dataLoadMethod = () => HowToProvider.GetHowToes()
                .Columns(
                        nameof(HowTo.HowToTitle),
                        nameof(HowTo.HowToFileUrl),
                        nameof(HowTo.HowToText)
                        )
                .WhereEquals(nameof(Solution.NodeGUID), nodeGuid)
                .Published(!isPublished)
                .OnCurrentSite()
                .TypedResult
                .FirstOrDefault();

            var res = CacheService.Cache<HowTo>(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(HowTo.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid));

            var dto = new HowToItemDto(res);
            return dto;
        }
    }
}
