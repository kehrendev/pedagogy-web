﻿using CMS.CustomTables;
using CMS.CustomTables.Types.Pedagogy;

using Pedagogy.DataAccess.Dto.FacilityWaitList;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class FacilityWaitListRepo : BaseRepo, IFacilityWaitListRepo
    {
        public FacilityWaitListRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public IEnumerable<FacilityWaitListItemDto> GetAllFacilities() 
        {
            var items = CustomTableItemProvider.GetItems<FacilityWaitListItem>().ToList();

            return items.Select(x => new FacilityWaitListItemDto(x)).ToList();
        }

        public IEnumerable<FacilityWaitListItemDto> GetFacilitiesByStatus(string status = "Pending")
        {
            var items = CustomTableItemProvider.GetItems<FacilityWaitListItem>()
                .Where(x => x.ApprovalStatus == status)
                .ToList();

            return items.Select(x => new FacilityWaitListItemDto(x)).ToList();
        }
    }
}
