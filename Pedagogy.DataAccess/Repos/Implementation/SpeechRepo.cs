﻿using Pedagogy.DataAccess.Dto.Contact;
using Pedagogy.DataAccess.Dto.Speech;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;
using Pedagogy.Models.ClassBuilder;
using Pedagogy.Models.ClassMembers;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class SpeechRepo : BaseRepo, ISpeechRepo
    {
        public SpeechRepo(IDocQueryService docQueryService, ICacheService cacheService) : base(docQueryService, cacheService) { }

        public List<SpeechRateDto> GetAllSpeechRates(Func<SpeechRate, SpeechRateDto> select)
        {
            using(var db = new ClassMembers())
            {
                return db.SpeechRates
                    .Select(select)
                    .ToList();
            }
        }

        public List<SpeechVoiceDto> GetAllSpeechVoices(Func<TU_SpeechVoices, SpeechVoiceDto> select)
        {
            using(var db = new ClassBuilder())
            {
                return db.TU_SpeechVoices
                .Select(select)
                .ToList();
            }
        }

        public int GetDefaultSpeechRateID()
        {
            using (var db = new ClassMembers())
            {
                return db.SpeechRates
                    .Where(sr => sr.Rate.Contains("1.0"))
                    .Select(sr => sr.SpeechRateID)
                    .FirstOrDefault();
            }
        }

        public int GetDefaultSpeechVoiceID()
        {
            using (var db = new ClassBuilder())
            {
                return db.TU_SpeechVoices
                    .Where(x => x.IsDefault.Value)
                    .Select(x => x.SpeechVoiceId)
                    .FirstOrDefault();
            }
        }

        public void UpdateSpeechRateID(int contactID, int newID)
        {
            using (var db = new ClassMembers())
            {
                var contact = db.Contacts.Where(x => x.ContactID == contactID).FirstOrDefault();

                contact.SpeechRateID = newID;
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void UpdateSpeechVoiceID(int contactID, int newID)
        {
            using(var db = new ClassMembers())
            {
                var contact = db.Contacts.Where(x => x.ContactID == contactID).FirstOrDefault();

                contact.SpeechVoiceID = newID;
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
