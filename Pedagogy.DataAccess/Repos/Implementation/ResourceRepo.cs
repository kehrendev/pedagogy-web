﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Localization;
using CMS.Relationships;
using CMS.SiteProvider;
using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.DataAccess.Dto.Resources;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class ResourceRepo : BaseRepo, IResourceRepo
    {
        public ResourceRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public ResourceItemDto GetResourceByNodeGuid(Guid nodeGuid, bool isPublished)
        {
            Func<Resource> dataLoadMethod = () => ResourceProvider.GetResources()
                .WhereEquals(nameof(Resource.NodeGUID), nodeGuid)
                .Published(!isPublished)
                .OnCurrentSite()
                .TypedResult
                .FirstOrDefault();

            var res = CacheService.Cache<Resource>(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(Resource.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid));

            var dto = new ResourceItemDto(res);

            return dto;
        }

        public IEnumerable<ResourceItemDto> GetAllResourcesByPath(string path, int childNestingLevel)
        {
            return DocQueryService.GetDocuments<Resource>()
                .OnSite(SiteContext.CurrentSiteName)
                .Path(path, PathTypeEnum.Section)
                .NestingLevel(childNestingLevel)
                .OrderBy(nameof(TreeNode.NodeOrder))
                .ToList()
                .Select(m => new ResourceItemDto()
                {
                    ResourceName = m.ResourceName,
                    ResourceSummary = m.ResourceSummary,
                    ResourceImg = m.ResourceImg,
                    ResourceImgAltText = m.ResourceImgAltText,
                    Url = m.NodeAliasPath
                });
        }

        public IEnumerable<ResourceItemDto> GetResourcesByCategory(string categoryName)
        {
            Func<IEnumerable<Resource>> dataLoadMethod = () => ResourceProvider.GetResources()
                .OnSite(SiteContext.CurrentSiteName)
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .InCategories(categoryName)
                .PublishedVersion()
                .Published()
                .TypedResult;

            var res = CacheService.Cache<IEnumerable<Resource>>(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodesCacheName(Resource.OBJECT_TYPE + "|" + categoryName, CacheDependencyType.ALL),
                CacheService.GetNodesCacheDependencyKey(Resource.OBJECT_TYPE + "|" + categoryName, CacheDependencyType.ALL));

            List<ResourceItemDto> items = new List<ResourceItemDto>();
            foreach(var item in res)
            {
                items.Add(new ResourceItemDto
                {
                    ResourceName = item.ResourceName,
                    ResourceSummary = item.ResourceSummary,
                    ResourceDescription = item.ResourceDescription,
                    ResourceImg = item.ResourceImg,
                    ResourceImgAltText = item.ResourceImgAltText,
                    Url = item.NodeAliasPath
                });
            }

            return items;
        }

        public IEnumerable<CourseItemDto> GetRelatedCourses(ResourceItemDto currentResource)
        {
            var resource = ResourceProvider.GetResources()
                .Path(currentResource.Url)
                .OnSite(SiteContext.CurrentSiteName)
                .FirstOrDefault();

            var relationshipDisplayName = "related course";
            var relationshipName = RelationshipNameInfoProvider.GetRelationshipNames()
                .WhereEquals("RelationshipDisplayName", relationshipDisplayName)
                .FirstOrDefault();

            var relationships = RelationshipInfoProvider.GetRelationships()
                .WhereEquals("LeftNodeID", resource.NodeID)
                .WhereEquals("RelationshipNameID", relationshipName.RelationshipNameId);

            var relationshipRightSideIDs = relationships.Select(x => (int)x["RightNodeID"]).ToList();

            var relatedPages = DocumentHelper.GetDocuments()
                .WithCoupledColumns()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereIn("NodeID", relationshipRightSideIDs)
                .ToList();

            List<CourseItemDto> relatedCourses = new List<CourseItemDto>();
            foreach(var item in relatedPages)
            {
                relatedCourses.Add(new CourseItemDto
                {
                    CourseShortName = item.DocumentName,
                    Url = item.NodeAliasPath
                });
            }

            return relatedCourses;
        }

        public ResourceItemDto GetResourceByTitle(string title)
        {
            var resource = ResourceProvider.GetResources()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereEquals(nameof(Resource.ResourceName), title)
                .FirstOrDefault();

            return new ResourceItemDto
            {
                ResourceName = resource.ResourceName,
                ResourceSummary = resource.ResourceSummary,
                Url = resource.NodeAliasPath,
                ResourceImg = resource.ResourceImg,
                ResourceImgAltText = resource.ResourceImgAltText
            };
        }
    }
}
