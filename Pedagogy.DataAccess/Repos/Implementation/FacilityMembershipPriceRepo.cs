﻿using CMS.CustomTables;
using CMS.CustomTables.Types.Pedagogy;
using CMS.Taxonomy;

using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class FacilityMembershipPriceRepo : BaseRepo, IFacilityMembershipPriceRepo
    {
        public FacilityMembershipPriceRepo(IDocQueryService docQueryService, ICacheService cacheService) : base(docQueryService, cacheService) { }

        public decimal GetFacilityMembershipDiscount(string category, bool over200 = false, int qty = 0, double contactHours = 0)
        {
            if (over200)
            {
                Func<decimal> dataLoadMethod = () => CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                    .Where(x => x.FacilityMembershipCategory == category)
                    .Where(x => x.FacilityMembershipEndingStaffNumber == 200)
                    .Select(x => x.FacilityMembershipDollarAmtPerYear)
                    .FirstOrDefault();

                return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetFacilityMembershipDiscount)}|{category}|{over200}",
                    $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetFacilityMembershipDiscount)}|{category}|{over200}");
            }
            else
            {
                Func<decimal> dataLoadMethod = () => CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                    .Where(x => x.FacilityMembershipCategory == category)
                    .Where(x => x.FacilityMembershipStartingContactHours <= contactHours)
                    .Where(x => x.FacilityMembershipEndingContactHours >= contactHours)
                    .Where(x => x.FacilityMembershipStartingStaffNumber <= qty)
                    .Where(x => x.FacilityMembershipEndingStaffNumber >= qty)
                    .Select(x => x.FacilityMembershipDollarAmtPerYear)
                    .FirstOrDefault();

                return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetFacilityMembershipDiscount)}|{category}|{qty}|{contactHours}",
                    $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetFacilityMembershipDiscount)}|{category}|{qty}|{contactHours}");
            }
        }

        public double GetFacilityMembershipPrice(string category, string status)
        {
            Func<double> dataLoadMethod = () => CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                .Where(x => x.FacilityMembershipCategory == category)
                .Where(x => x.FacilityMembershipValidationStatus == status)
                .Select(x => x.FacilityMembershipEndingContactHours)
                .FirstOrDefault();

            return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetFacilityMembershipPrice)}|{category}|{status}",
                $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetFacilityMembershipPrice)}|{category}|{status}");
        }

        public int GetPriceCategory(string categoryName)
        {
            Func<int> dataLoadMethod = () => CategoryInfoProvider.GetCategories()
                .Where(x => x.CategoryName == categoryName)
                .Select(x => x.CategoryID)
                .FirstOrDefault();

            return CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(), $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetPriceCategory)}|{categoryName}",
                $"{nameof(FacilityMembershipPriceRepo)}|{nameof(GetPriceCategory)}|{categoryName}");
        }
    }
}
