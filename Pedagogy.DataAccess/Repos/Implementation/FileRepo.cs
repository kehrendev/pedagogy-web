﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using Pedagogy.DataAccess.Dto.Files;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class FileRepo : BaseRepo, IFileRepo
    {
        public FileRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public IEnumerable<FileItemDto> GetFilesByPath(string path)
        {
            return DocQueryService.GetDocuments<File>()
                .Path(path, PathTypeEnum.Children)
                .Select(f => new FileItemDto
                {
                    FileName = f.FileName,
                    FileUrl = f.FileUrl,
                    FileTeaserImg = f.FileTeaserImg,
                    FileTeaserImgAltText = f.FileTeaserImgAltText
                }).ToList();
        }
    }
}
