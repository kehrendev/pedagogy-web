﻿using Pedagogy.DataAccess.Dto.InfoBox;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class InfoBoxRepo : BaseRepo, IInfoBoxRepo
    {
        public InfoBoxRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public IEnumerable<InfoBoxItemDto> GetInfoBoxItems()
        {
            return DocQueryService.GetDocuments<CMS.DocumentEngine.Types.Pedagogy.InformationBoxItem>()
                .ToList()
                .Select(m => new InfoBoxItemDto()
                {
                    Title = m.Title,
                    Text = m.Text,
                    BackgroundImg = m.BackgroundImg,
                    BtnText = m.ButtonText,
                    BtnUrl = m.ButtonUrl
                });
        }
    }
}
