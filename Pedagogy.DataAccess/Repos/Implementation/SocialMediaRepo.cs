﻿using CMS.DocumentEngine;

using Pedagogy.DataAccess.Dto.Navigation;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class SocialMediaRepo : BaseRepo, ISocialMediaRepo
    {
        public SocialMediaRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public IEnumerable<SocialMediaItemDto> GetSocialMediaItemsByPath(string path)
        {
            return DocQueryService.GetDocuments<CMS.DocumentEngine.Types.Pedagogy.SocialMedia>()
                .Path(path, PathTypeEnum.Children)
                .ToList()
                .Select(m => new SocialMediaItemDto()
                {
                    SocialName = m.SocialName,
                    SocialUrl = m.SocialUrl,
                    SocialIcon = m.SocialIcon
                });
        }
    }
}
