﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using Pedagogy.DataAccess.Dto.Maps;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class MapRepo : BaseRepo, IMapRepo
    {
        public MapRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }


        public MapItemDto GetMapByNodeGuid(Guid nodeGuid, bool isPublished)
        {
            Func<Map> dataLoadMethod = () => MapProvider.GetMaps()
                .WhereEquals(nameof(Map.NodeGUID), nodeGuid)
                .Published(!isPublished)
                .OnCurrentSite()
                .TypedResult
                .FirstOrDefault();

            var res = CacheService.Cache<Map>(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(Map.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid));

            var dto = new MapItemDto(res);

            return dto;
        }

        public IEnumerable<MapItemDto> GetMapsByPath(string path)
        {
            return DocQueryService.GetDocuments<Map>()
                .Path(path, PathTypeEnum.Children)
                .ToList()
                .Select(m => new MapItemDto()
                {
                    MapTitle = m.MapTitle,
                    MapText = m.MapText,
                    MapSummaryText = m.MapSummaryText,
                    Url = m.NodeAliasPath
                });
        }
    }
}
