﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.SiteProvider;

using Pedagogy.DataAccess.Dto.Authors;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;

using System;
using System.Linq;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class AuthorRepo : BaseRepo, IAuthorRepo
    {
        public AuthorRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }
        private string[] allowedTypes = { Author.CLASS_NAME };

        public AuthorItemDto GetAuthorByNodeGuid(Guid nodeGuid, bool isPublished)
        {
            Func<Author> dataLoadMethod = () => AuthorProvider.GetAuthors()
                //.Columns(
                //    nameof(Author.AuthorName),
                //    nameof(Author.AuthorBio),
                //    nameof(Author.NodeAliasPath),
                //    nameof(Author.AuthorSpecialties),
                //    nameof(Author.AuthorAssociation),
                //    nameof(Author.AuthorWebsiteLink1),
                //    nameof(Author.AuthorWebsiteLink2),
                //    nameof(Author.AuthorWebsiteLink3),
                //    nameof(Author.AuthorAreaOfExpertise),
                //    nameof(Author.AuthorImg),
                //    nameof(Author.AuthorImgAltText)
                //)
                .WhereEquals(nameof(Author.NodeGUID), nodeGuid)
                .Published(!isPublished)
                .OnSite(SiteContext.CurrentSiteName)
                .TypedResult
                .FirstOrDefault();

            return new AuthorItemDto(CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(Author.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid)));
        }

        public AuthorItemDto GetAuthorByPath(string path, bool isPublished)
        {
            Func<Author> dataLoadMethod = () => AuthorProvider.GetAuthors()
                .Path(path, PathTypeEnum.Single)
                .OnSite(SiteContext.CurrentSiteName)
                .Published(!isPublished)
                .PublishedVersion()
                .TypedResult
                .FirstOrDefault();

            var res = CacheService.Cache(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                $"{nameof(AuthorRepo)}|{nameof(GetAuthorByPath)}|{path}|{PathTypeEnum.Single}|{isPublished}", $"{nameof(AuthorRepo)}|{nameof(GetAuthorByPath)}|{path}|{PathTypeEnum.Single}|{isPublished}");

            if (res == null) return null;

            return new AuthorItemDto(res);
        }
    }
}
