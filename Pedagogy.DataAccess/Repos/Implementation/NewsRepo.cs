﻿using CMS.Base;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Localization;
using CMS.SiteProvider;
using CMS.Taxonomy;
using Pedagogy.DataAccess.Dto.News;
using Pedagogy.DataAccess.Services.Cache;
using Pedagogy.DataAccess.Services.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedagogy.DataAccess.Repos.Implementation
{
    public class NewsRepo : BaseRepo, INewsRepo
    {
        public NewsRepo(IDocQueryService dqs, ICacheService cacheService) : base(dqs, cacheService) { }

        public NewsItemDto GetNewsItemByNodeGuid(Guid nodeGuid, bool isPublished)
        {
            Func<News> dataLoadMethod = () => NewsProvider.GetNews()
                .WhereEquals(nameof(News.NodeGUID), nodeGuid)
                .Published(!isPublished)
                .OnCurrentSite()
                .TypedResult
                .FirstOrDefault();

            var res = CacheService.Cache<News>(dataLoadMethod, CacheService.GetCacheMinutesConst(),
                CacheService.GetNodeCacheName(News.CLASS_NAME, nodeGuid), CacheService.GetNodeCacheDependencyKey(nodeGuid));

            var dto = new NewsItemDto(res);
            return dto;
        }

        public IEnumerable<NewsItemDto> GetAllNewsArticles()
        {
            return DocQueryService.GetDocuments<News>()
                .ToList()
                .Select(m => new NewsItemDto()
                {
                    NewsTitle = m.NewsTitle,
                    NewsReleaseDate = m.NewsReleaseDate,
                    Year = m.NewsReleaseDate.ToString("yyyy"),
                    Month = m.NewsReleaseDate.ToString("MMMM"),
                    Day = m.NewsReleaseDate.ToString("dd"),
                    NewsSummary = m.NewsSummary,
                    NewsText = m.NewsText,
                    NewsImg = m.NewsImg,
                    NewsImgAltText = m.NewsImgAltText,
                    NewsCategory = m.NewsCategory,
                    Url = m.NodeAliasPath
                })
                .OrderByDescending(d => d.NewsReleaseDate);
        }

        public NewsItemDto GetNewsArticleByTitle(string title)
        {
            var newsArticle = NewsProvider.GetNews()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereEquals(nameof(News.NewsTitle), title)
                .FirstOrDefault();

            return new NewsItemDto
            {
                NewsTitle = newsArticle.NewsTitle,
                NewsSummary = newsArticle.NewsSummary,
                Url = newsArticle.NodeAliasPath,
                NewsImg = newsArticle.NewsImg,
                NewsImgAltText = newsArticle.NewsImgAltText,
                NewsReleaseDate = newsArticle.NewsReleaseDate,
                Year = newsArticle.NewsReleaseDate.ToString("yyyy"),
                Month = newsArticle.NewsReleaseDate.ToString("MMMM"),
                Day = newsArticle.NewsReleaseDate.ToString("dd")
            };
        }

        public IEnumerable<NewsItemDto> GetNewsArticlesByYear(string year)
        {
            var newsArticles = NewsProvider.GetNews()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereContains(nameof(News.NewsReleaseDate), year)
                .ToList();

            var dtos = new List<NewsItemDto>();
            foreach(var article in newsArticles)
            {
                dtos.Add(new NewsItemDto
                {
                    NewsTitle = article.NewsTitle,
                    NewsSummary = article.NewsSummary,
                    Url = article.NodeAliasPath,
                    NewsImg = article.NewsImg,
                    NewsImgAltText = article.NewsImgAltText,
                    NewsReleaseDate = article.NewsReleaseDate,
                    Year = article.NewsReleaseDate.ToString("yyyy"),
                    Month = article.NewsReleaseDate.ToString("MMMM"),
                    Day = article.NewsReleaseDate.ToString("dd")
                });
            }

            return dtos;
        }

        public IEnumerable<NewsItemDto> GetNewsArticlesByCategory(string category)
        {
            List<News> newsArticles = DocumentHelper.GetDocuments<News>()
                .OnSite(SiteContext.CurrentSiteName)
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .InCategories(category)
                .ToList();

            List<NewsItemDto> dtos = new List<NewsItemDto>();
            foreach(var n in newsArticles)
            {
                dtos.Add(new NewsItemDto
                {
                    NewsTitle = n.NewsTitle,
                    NewsSummary = n.NewsSummary,
                    Url = n.NodeAliasPath,
                    NewsImg = n.NewsImg,
                    NewsImgAltText = n.NewsImgAltText,
                    NewsReleaseDate = n.NewsReleaseDate,
                    Year = n.NewsReleaseDate.ToString("yyyy"),
                    Month = n.NewsReleaseDate.ToString("MMMM"),
                    Day = n.NewsReleaseDate.ToString("dd")
                });
            }

            return dtos;
        }

        public IEnumerable<CategoryInfo> GetAllNewsArticleCategories()
        {
            var categories = CategoryInfoProvider.GetCategories()
                .OnSite(SiteContext.CurrentSiteName)
                .OrderBy(x => x.CategoryOrder)
                .Where(x => x.CategoryName.StartsWith("newscategory."));
                //.Select(x => x.CategoryDisplayName);

            //var upper = new List<string>();
            //foreach(var item in categories)
            //{
            //    var firstLetter = item.Substring(0, 1).ToUpper();
            //    var theRest = item.Substring(1, item.Length - 1);
            //    upper.Add(string.Concat(firstLetter, theRest));
            //}

            return categories;
        }
    }
}
