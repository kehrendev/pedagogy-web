﻿using Pedagogy.DataAccess.Dto.FacilityWaitList;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface IFacilityWaitListRepo : IRepo
    {
        IEnumerable<FacilityWaitListItemDto> GetAllFacilities();
        IEnumerable<FacilityWaitListItemDto> GetFacilitiesByStatus(string status = "Pending");
    }
}