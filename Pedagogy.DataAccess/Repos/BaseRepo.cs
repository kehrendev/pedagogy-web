﻿using Pedagogy.DataAccess.Services.Query;
using Pedagogy.DataAccess.Services.Cache;

namespace Pedagogy.DataAccess.Repos
{
    public abstract class BaseRepo
    {
        protected IDocQueryService DocQueryService { get; }
        protected ICacheService CacheService { get; }

        protected BaseRepo(IDocQueryService docQueryService, ICacheService cacheService)
        {
            DocQueryService = docQueryService;
            CacheService = cacheService;
        }
    }
}