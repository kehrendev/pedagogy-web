﻿using CMS.DocumentEngine;
using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.DataAccess.Dto.Resources;
using System;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface IResourceRepo : IRepo
    {
        ResourceItemDto GetResourceByNodeGuid(Guid nodeGuid, bool isPublished);
        IEnumerable<ResourceItemDto> GetAllResourcesByPath(string path, int childNestingLevel);
        IEnumerable<ResourceItemDto> GetResourcesByCategory(string categoryName);
        IEnumerable<CourseItemDto> GetRelatedCourses(ResourceItemDto currentResource);
        ResourceItemDto GetResourceByTitle(string title);
    }
}
