﻿using CMS.DocumentEngine.Types.Pedagogy;

using Pedagogy.DataAccess.Dto.Courses;

using System;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface ICourseRepo : IRepo
    {
        CourseItemDto GetCourseByNodeGuid(Guid nodeGuid);
        CourseItemDto GetCourseMembershipByNodeGuid(Guid nodeGuid);
        IEnumerable<CourseItemDto> GetRelatedCourses(CourseItemDto currentCourse);
        List<CourseItemDto> GetCoursesByCategory(string[] categoryName);
        CourseItemDto GetCourseByTitle(string title);
        List<CourseItemDto> GetCoursesByAuthor(string authorUrl);
        IEnumerable<CourseItemDto> GetProductTypeCourses();
        CourseItemDto GetCourseBySKUNumber(string classID, Func<Course, Course> select);
    }
}