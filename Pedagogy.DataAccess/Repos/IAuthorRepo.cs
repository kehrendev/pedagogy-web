﻿using Pedagogy.DataAccess.Dto.Authors;
using System;

namespace Pedagogy.DataAccess.Repos
{
    public interface IAuthorRepo : IRepo
    {
        AuthorItemDto GetAuthorByNodeGuid(Guid nodeGuid, bool isPublished);
        AuthorItemDto GetAuthorByPath(string path, bool isPublished);
    }
}
