﻿using Pedagogy.DataAccess.Dto.HowToFiles;
using System;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface IHowToRepo : IRepo
    {
        HowToItemDto GetHowToItemByNodeGuid(Guid nodeGuid, bool isPublished);
        IEnumerable<HowToItemDto> GetAllHowTos();
        IEnumerable<HowToItemDto> GetAllStudentHowTos();
    }
}
