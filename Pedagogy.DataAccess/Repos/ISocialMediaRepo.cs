﻿using Pedagogy.DataAccess.Dto.Navigation;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface ISocialMediaRepo : IRepo
    {
        IEnumerable<SocialMediaItemDto> GetSocialMediaItemsByPath(string path);
    }
}
