﻿using Pedagogy.DataAccess.Dto.InfoBox;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface IInfoBoxRepo : IRepo
    {
        IEnumerable<InfoBoxItemDto> GetInfoBoxItems();
    }
}
