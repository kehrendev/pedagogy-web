﻿using CMS.DocumentEngine;

using Pedagogy.DataAccess.Dto.Navigation;

using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface INavigationRepo : IRepo
    {
        IEnumerable<NavigationItemDto> GetCategoryNavigation(string categoryName, int nestingLevel = -1);
        IEnumerable<TreeNode> GetPagesByCategory(string[] categories);
    }
}
