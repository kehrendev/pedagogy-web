﻿using Pedagogy.DataAccess.Dto.Files;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface IFileRepo : IRepo
    {
        IEnumerable<FileItemDto> GetFilesByPath(string path);
    }
}