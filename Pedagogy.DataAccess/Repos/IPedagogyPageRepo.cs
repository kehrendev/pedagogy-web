﻿using CMS.DocumentEngine;

using Pedagogy.DataAccess.Dto.pedagogyPage;

using System;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface IPedagogyPageRepo : IRepo
    {
        PedagogyPageDto GetPedagogyPageByNodeGuid(Guid nodeGuid, bool isPublished);
    }
}
