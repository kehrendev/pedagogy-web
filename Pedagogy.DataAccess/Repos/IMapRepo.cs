﻿using Pedagogy.DataAccess.Dto.Maps;
using System;
using System.Collections.Generic;

namespace Pedagogy.DataAccess.Repos
{
    public interface IMapRepo : IRepo
    {
        MapItemDto GetMapByNodeGuid(Guid nodeGuid, bool isPublished);
        IEnumerable<MapItemDto> GetMapsByPath(string path);
    }
}
