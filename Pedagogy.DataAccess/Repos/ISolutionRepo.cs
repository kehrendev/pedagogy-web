﻿using Pedagogy.DataAccess.Dto.Solutions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedagogy.DataAccess.Repos
{
    public interface ISolutionRepo : IRepo
    {
        SolutionItemDto GetSolutionItemByNodeGuid(Guid nodeGuid, bool isPublished);
    }
}
