﻿using Pedagogy.DataAccess.Repos;
using Pedagogy.DataAccess.Services.Context;

namespace Pedagogy.DataAccess.DependencyInjection
{
    public interface IDataDependencies
    {
        ISiteContextService SiteContextService { get; }
        IPedagogyPageRepo PedagogyPageRepo { get; }
        INavigationRepo NavigationRepo { get; }
        ISocialMediaRepo SocialMediaRepo { get; }
        IInfoBoxRepo InfoBoxRepo { get; }
        ISolutionRepo SolutionRepo { get; }
        IAuthorRepo AuthorRepo { get; }
        INewsRepo NewsRepo { get; }
        ICourseRepo CourseRepo { get; }
        IResourceRepo ResourceRepo { get; }
        IMapRepo MapRepo { get; }
        IFileRepo FileRepo { get; }
        IHowToRepo HowToRepo { get; }
        IFacilityWaitListRepo FacilityWaitListRepo { get; }
        IFacilityMembershipPriceRepo FacilityMembershipPriceRepo { get; }
    }
}