﻿using Pedagogy.DataAccess.Repos;
using Pedagogy.DataAccess.Services.Context;

namespace Pedagogy.DataAccess.DependencyInjection
{
    public class DataDependencies : IDataDependencies
    {
        //Services
        public ISiteContextService SiteContextService { get; }

        //Repos
        public IPedagogyPageRepo PedagogyPageRepo { get; }
        public INavigationRepo NavigationRepo { get; }
        public ISocialMediaRepo SocialMediaRepo { get; }
        public IInfoBoxRepo InfoBoxRepo { get; }
        public ISolutionRepo SolutionRepo { get; }
        public IAuthorRepo AuthorRepo { get; }
        public INewsRepo NewsRepo { get; }
        public ICourseRepo CourseRepo { get; }
        public IResourceRepo ResourceRepo { get; }
        public IMapRepo MapRepo { get; }
        public IFileRepo FileRepo { get; }
        public IHowToRepo HowToRepo { get; }
        public IFacilityWaitListRepo FacilityWaitListRepo { get; }
        public IFacilityMembershipPriceRepo FacilityMembershipPriceRepo { get; }

        public DataDependencies(
            ISiteContextService siteContextService,
            IPedagogyPageRepo pedagogyPageRepo,
            INavigationRepo navigationRepo,
            ISocialMediaRepo socialMediaRepo,
            IInfoBoxRepo infoBoxRepo,
            ISolutionRepo solutionRepo,
            IAuthorRepo authorRepo,
            INewsRepo newsRepo,
            ICourseRepo courseRepo,
            IResourceRepo resourceRepo,
            IMapRepo mapRepo,
            IFileRepo fileRepo,
            IHowToRepo howToRepo,
            IFacilityWaitListRepo facilityWaitListRepo,
            IFacilityMembershipPriceRepo facilityMembershipPriceRepo
            )
        {
            SiteContextService = siteContextService;
            PedagogyPageRepo = pedagogyPageRepo;
            NavigationRepo = navigationRepo;
            SocialMediaRepo = socialMediaRepo;
            InfoBoxRepo = infoBoxRepo;
            SolutionRepo = solutionRepo;
            AuthorRepo = authorRepo;
            NewsRepo = newsRepo;
            CourseRepo = courseRepo;
            ResourceRepo = resourceRepo;
            MapRepo = mapRepo;
            FileRepo = fileRepo;
            HowToRepo = howToRepo;
            FacilityWaitListRepo = facilityWaitListRepo;
            FacilityMembershipPriceRepo = facilityMembershipPriceRepo;
        }

    }
}
