﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine.Types.Pedagogy;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.DataModels;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.BannerWidget;

[assembly:RegisterWidget(
    ComponentIdentifiers.BANNER_WIDGET_IDENTIFIER,
    typeof(BannerWidgetController),
    "Banner Widget",
    Description = "Displays a banner",
    IconClass = "icon-collapse-scheme"
    )]

namespace Pedagogy.Controllers.Widgets
{
    public class BannerWidgetController : WidgetController<BannerWidgetProperties>
    {
        public ActionResult Index()
        {
            var props = GetProperties();

            var model = new BannerWidgetViewModel
            {
                BannerItem = ContentHelper.GetSelectedPage(props.Banner?.FirstOrDefault(), Banner.CLASS_NAME)                
            };
            model.Breadcrumbs = BreadcrumbProvider.GetBreadcrumbs(model.BannerItem) ?? new List<Breadcrumb>();

            return PartialView(ComponentIdentifiers.BANNER_WIDGET_VIEW, model);
        }
    }
}