﻿using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using iTextSharp.text;
using iTextSharp.text.pdf;
using OfficeOpenXml;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.ReportsWidget;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CMS.SiteProvider;
using Pedagogy.Models.ClassMembers;
using Newtonsoft.Json;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassBuilder.DependencyInjection;

[assembly: RegisterWidget(
    ComponentIdentifiers.REPORTS_WIDGET_IDENTIFIER,
    typeof(ReportsWidgetController),
    "Reports Widget",
    Description = "A widget to get different reports",
    IconClass = "icon-graph")]

namespace Pedagogy.Controllers.Widgets
{
    public class ReportsWidgetController : WidgetController<ReportsWidgetProperties>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;
        private readonly IClassBuilderDependencies ClassBuilderDependencies;

        private readonly ExcelPackage ep = new ExcelPackage();

        public const string NOT_STARTED_STATUS = "Not Started";
        public const string IN_PROGRESS_STATUS = "In Progress";
        public const string COMPLETED_STATUS = "Completed";

        public ReportsWidgetController(IClassMemberDependencies classMemberDependencies, IClassBuilderDependencies classBuilderDependencies)
        {
            ClassMemberDependencies = classMemberDependencies;
            ClassBuilderDependencies = classBuilderDependencies;
        }

        public ActionResult Index()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var facilities = ClassMembersHelper.GetFacilitiesForCurrentUser(ClassMemberDependencies, isSiteAdmin, isCorpAdmin, user.UserID, "");

            var model = ReportsWidgetViewModel.GetViewModel(
                new List<ProgressReportViewModel>(),
                new ProgressReportFilterViewModel()
                {
                    Classes = new List<SelectListItem>(),
                    Statuses = new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Value = NOT_STARTED_STATUS,
                        Text = NOT_STARTED_STATUS
                    },
                    new SelectListItem
                    {
                        Value = IN_PROGRESS_STATUS,
                        Text = IN_PROGRESS_STATUS
                    },
                    new SelectListItem
                    {
                        Value = COMPLETED_STATUS,
                        Text = COMPLETED_STATUS
                    }
                },
                    Students = new List<SelectListItem>()
                },
                new List<CorporateReportViewModel>(),
                isSiteAdmin,
                isCorpAdmin,
                GetProperties().CardHeading,
                new SelectList(facilities.Select(x => new { x.FacilityID, x.Name }), nameof(FacilityViewModel.FacilityID), nameof(FacilityViewModel.Name)));

            if(facilities.Count() > 0) model.FacilityID = facilities.FirstOrDefault().FacilityID;

            return PartialView(ComponentIdentifiers.REPORTS_WIDGET_VIEW, model);
        }

        [HttpGet]
        public JsonResult GetProgressReportPagedData(ProgressReportFilterViewModel progressFilter, int pageNumber = 1, int pageSize = 10)
        {
            if (progressFilter.FacilityID == 0)
            {
                return Json(JsonConvert.SerializeObject(new { PagingData = PagingHelper.PagedResult(new List<ProgressReportViewModel>(), pageNumber, pageSize), Filters = new ProgressReportFilterViewModel(), HasReportItems = false }), JsonRequestBehavior.AllowGet);
            }

            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var data = ClassMembersHelper.FilterProgressReportItems(progressFilter, user.UserID, isSiteAdmin, isCorpAdmin, ClassMemberDependencies, ClassBuilderDependencies);

            var pagedData = PagingHelper.PagedResult(data.ProgressReportData.ToList(), pageNumber, pageSize);

            return Json(JsonConvert.SerializeObject(new { PagingData = pagedData, Filters = data.ProgressFilter, HasReportItems = data.HasReportItems }), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCorporateReportPagedData(int pageNumber = 1, int pageSize = 10, int facilityID = 0)
        {
            if(facilityID == 0)
            {
                return Json(PagingHelper.PagedResult(new List<CorporateReportViewModel>(), pageNumber, pageSize), JsonRequestBehavior.AllowGet);
            }

            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var pagedData = PagingHelper.PagedResult(
                ClassMembersHelper.GetCorporateReportData(ClassMemberDependencies, ClassBuilderDependencies, isSiteAdmin, isCorpAdmin, user.UserID, facilityID).ToList(), pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public void ExportProgressReportToExcel(ProgressReportFilterViewModel progressFilter)
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var progData = ClassMembersHelper.FilterProgressReportItems(progressFilter, user.UserID, isSiteAdmin, isCorpAdmin, ClassMemberDependencies, ClassBuilderDependencies).ProgressReportData;

            ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("Progress Report");
            sheet.Cells["A1"].Value = "Facility";
            sheet.Cells["B1"].Value = "Class";
            sheet.Cells["C1"].Value = "Student";
            sheet.Cells["D1"].Value = "Status";
            sheet.Cells["E1"].Value = "Test Score";
            sheet.Cells["F1"].Value = "Last Access";
            sheet.Cells["G1"].Value = "Source";
            sheet.Cells["H1"].Value = "Class Time (hr:min:sec)";
            sheet.Cells["I1"].Value = "Test Time (hr:min:sec)";

            int row = 2;
            foreach (var item in progData)
            {
                sheet.Cells[string.Format("A{0}", row)].Value = item.FacilityName;
                sheet.Cells[string.Format("B{0}", row)].Value = item.ClassName;
                sheet.Cells[string.Format("C{0}", row)].Value = item.StudentName;
                sheet.Cells[string.Format("D{0}", row)].Value = item.Status;
                sheet.Cells[string.Format("E{0}", row)].Value = item.TestScore;
                sheet.Cells[string.Format("F{0}", row)].Value = item.LastAccessDay;
                sheet.Cells[string.Format("G{0}", row)].Value = item.Source;
                sheet.Cells[string.Format("H{0}", row)].Value = item.ClassTime;
                sheet.Cells[string.Format("I{0}", row)].Value = item.TestTime;

                row++;
            }

            var fileName = "ProgressReport.xlsx";

            sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(ep.GetAsByteArray());
            Response.End();
        }

        [HttpGet]
        public void ExportProgressReportToCSV(ProgressReportFilterViewModel progressFilter)
        {
            DataTable dtReport = CreateProgressReportTable(progressFilter);

            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dtReport.Columns.Cast<DataColumn>().Select(column => column.ColumnName);

            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dtReport.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            }

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=ProgressResport.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(sb);
            Response.Flush();
            Response.End();
        }

        private DataTable CreateProgressReportTable(ProgressReportFilterViewModel progressFilter)
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var progData = ClassMembersHelper.FilterProgressReportItems(progressFilter, user.UserID, isSiteAdmin, isCorpAdmin, ClassMemberDependencies, ClassBuilderDependencies).ProgressReportData;

            DataTable dtReport = new DataTable("ProgressReport");
            dtReport.Columns.AddRange(new DataColumn[9] { new DataColumn("Facility"), new DataColumn("Class"), new DataColumn("Student"), new DataColumn("Status"),
                new DataColumn("Test Score"), new DataColumn("Last Access"), new DataColumn("Source"), new DataColumn("Class Time (hr:min:sec)"), new DataColumn("Test Time (hr:min:sec)")});

            foreach (var item in progData)
            {
                dtReport.Rows.Add(item.FacilityName, item.ClassName, item.StudentName, item.Status, item.TestScore, item.LastAccessDay, item.Source, item.ClassTime, item.TestTime);
            }

            return dtReport;
        }

        [HttpGet]
        public void ExportProgressReportToPDF(ProgressReportFilterViewModel progressFilter)
        {
            DataTable dtReport = CreateProgressReportTable(progressFilter);

            if (dtReport.Rows.Count > 0)
            {
                string fileName = "ProgressReport";
                string filePath = Server.MapPath("\\") + fileName + ".pdf";

                Document document = new Document(PageSize.A4.Rotate(), 20F, 20F, 20F, 20F);
                FileStream fs = new FileStream(filePath, FileMode.Create);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.Open();

                Font font1 = FontFactory.GetFont(FontFactory.COURIER_BOLD, 14);
                Font font2 = FontFactory.GetFont(FontFactory.COURIER, 12);

                float[] columnDefinitionSize = { 3F, 5F, 3F, 2F, 2F, 3F, 3F, 3F, 3F };
                PdfPTable table;

                table = new PdfPTable(columnDefinitionSize)
                {
                    WidthPercentage = 100
                };

                table.AddCell(new Phrase("Facility", font1));
                table.AddCell(new Phrase("Class", font1));
                table.AddCell(new Phrase("Student", font1));
                table.AddCell(new Phrase("Status", font1));
                table.AddCell(new Phrase("Test Score", font1));
                table.AddCell(new Phrase("Last Access", font1));
                table.AddCell(new Phrase("Source", font1));
                table.AddCell(new Phrase("Class Time (hr:min:sec)", font1));
                table.AddCell(new Phrase("Test Time (hr:min:sec)", font1));
                table.HeaderRows = 1;

                foreach (DataRow data in dtReport.Rows)
                {
                    table.AddCell(new Phrase(data[0].ToString(), font2));
                    table.AddCell(new Phrase(data[1].ToString(), font2));
                    table.AddCell(new Phrase(data[2].ToString(), font2));
                    table.AddCell(new Phrase(data[3].ToString(), font2));
                    table.AddCell(new Phrase(data[4].ToString(), font2));
                    table.AddCell(new Phrase(data[5].ToString(), font2));
                    table.AddCell(new Phrase(data[6].ToString(), font2));
                    table.AddCell(new Phrase(data[7].ToString(), font2));
                    table.AddCell(new Phrase(data[8].ToString(), font2));
                }

                document.Add(table);
                document.Close();
                document.CloseDocument();
                document.Dispose();
                writer.Close();
                writer.Dispose();
                fs.Close();
                fs.Dispose();

                FileStream sourceFile = new FileStream(filePath, FileMode.Open);
                float fileSize = sourceFile.Length;
                byte[] getContent = new byte[Convert.ToInt32(Math.Truncate(fileSize))];
                sourceFile.Read(getContent, 0, Convert.ToInt32(sourceFile.Length));
                sourceFile.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", getContent.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf;");
                Response.BinaryWrite(getContent);
                Response.Flush();
                Response.End();
            }
        }

        public void ExportCorporateReportToExcel(int facilityID = 0)
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            bool isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            bool isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var corpData = ClassMembersHelper.GetCorporateReportData(ClassMemberDependencies, ClassBuilderDependencies, isSiteAdmin, isCorpAdmin, user.UserID, facilityID);

            ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("Corporate Report");
            sheet.Cells["A1"].Value = "Facility";
            sheet.Cells["B1"].Value = "Class";
            sheet.Cells["C1"].Value = "Not Started";
            sheet.Cells["D1"].Value = "In Progress";
            sheet.Cells["E1"].Value = "Completed";
            sheet.Cells["F1"].Value = "Class Total";

            int row = 2;
            foreach (var item in corpData)
            {
                sheet.Cells[string.Format("A{0}", row)].Value = item.FacilityName;
                sheet.Cells[string.Format("B{0}", row)].Value = item.ClassName;
                sheet.Cells[string.Format("C{0}", row)].Value = item.QtyNotStarted;
                sheet.Cells[string.Format("D{0}", row)].Value = item.QtyInProgress;
                sheet.Cells[string.Format("E{0}", row)].Value = item.QtyCompleted;
                sheet.Cells[string.Format("F{0}", row)].Value = item.ClassTotal;

                row++;
            }

            var fileName = "CorporateReport.xlsx";

            sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(ep.GetAsByteArray());
            Response.End();
        }

        public void ExportCorporateReportToCSV(int facilityID = 0)
        {
            DataTable dtReport = CreateCorporateReportTable(facilityID);

            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dtReport.Columns.Cast<DataColumn>().Select(column => column.ColumnName);

            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dtReport.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            }

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=CorporateResport.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(sb);
            Response.Flush();
            Response.End();
        }

        public void ExportCorporateReportToPDF(int facilityID = 0)
        {
            DataTable dtReport = CreateCorporateReportTable(facilityID);

            if (dtReport.Rows.Count > 0)
            {
                string fileName = "CorporateReport";
                string filePath = Server.MapPath("\\") + fileName + ".pdf";

                Document document = new Document(PageSize.A4.Rotate(), 20F, 20F, 20F, 20F);
                FileStream fs = new FileStream(filePath, FileMode.Create);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.Open();

                Font font1 = FontFactory.GetFont(FontFactory.COURIER_BOLD, 14);
                Font font2 = FontFactory.GetFont(FontFactory.COURIER, 12);

                float[] columnDefinitionSize = { 4F, 5F, 3F, 3F, 3F, 3F };
                PdfPTable table;

                table = new PdfPTable(columnDefinitionSize)
                {
                    WidthPercentage = 100
                };

                table.AddCell(new Phrase("Facility", font1));
                table.AddCell(new Phrase("Class", font1));
                table.AddCell(new Phrase("Not Started", font1));
                table.AddCell(new Phrase("In Progress", font1));
                table.AddCell(new Phrase("Completed", font1));
                table.AddCell(new Phrase("Class Total", font1));
                table.HeaderRows = 1;

                foreach (DataRow data in dtReport.Rows)
                {
                    table.AddCell(new Phrase(data[0].ToString(), font2));
                    table.AddCell(new Phrase(data[1].ToString(), font2));
                    table.AddCell(new Phrase(data[2].ToString(), font2));
                    table.AddCell(new Phrase(data[3].ToString(), font2));
                    table.AddCell(new Phrase(data[4].ToString(), font2));
                    table.AddCell(new Phrase(data[5].ToString(), font2));
                }

                document.Add(table);
                document.Close();
                document.CloseDocument();
                document.Dispose();
                writer.Close();
                writer.Dispose();
                fs.Close();
                fs.Dispose();

                FileStream sourceFile = new FileStream(filePath, FileMode.Open);
                float fileSize = sourceFile.Length;
                byte[] getContent = new byte[Convert.ToInt32(Math.Truncate(fileSize))];
                sourceFile.Read(getContent, 0, Convert.ToInt32(sourceFile.Length));
                sourceFile.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", getContent.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf;");
                Response.BinaryWrite(getContent);
                Response.Flush();
                Response.End();
            }
        }

        private DataTable CreateCorporateReportTable(int facilityID)
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var corpData = ClassMembersHelper.GetCorporateReportData(ClassMemberDependencies, ClassBuilderDependencies, isSiteAdmin, isCorpAdmin, user.UserID, facilityID);

            DataTable dtReport = new DataTable("CorporateReport");
            dtReport.Columns.AddRange(new DataColumn[6] { new DataColumn("Facility"), new DataColumn("Class"), new DataColumn("Not Started"), new DataColumn("In Progress"),
                new DataColumn("Completed"), new DataColumn("Class Total")});

            foreach (var item in corpData)
            {
                dtReport.Rows.Add(item.FacilityName, item.ClassName, item.QtyNotStarted, item.QtyInProgress, item.QtyCompleted, item.ClassTotal);
            }

            return dtReport;
        }
    }
}