﻿using Kentico.PageBuilder.Web.Mvc;
using CMS.Membership;
using CMS.Helpers;
using CMS.Localization;
using CMS.EventLog;
using CMS.SiteProvider;
using CMS.Ecommerce;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.CourseInfoWidget;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Lib.Helpers;
using Pedagogy.ClassBuilder;
using Pedagogy.Custom.Generated.ModuleClasses;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.StudentClass;
using Pedagogy.ClassMembers.Entities.ClassWorkflow;
using Pedagogy.ClassMembers.Entities.StudentClassDetail;
using Pedagogy.ClassMembers.Entities.StudentTestAttempt;
using Pedagogy.ClassBuilder.DependencyInjection;
using Pedagogy.ClassBuilder.Entities.Class;
using Pedagogy.ClassMembers.Entities.StudentLicense;

using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System;

using Newtonsoft.Json;

[assembly: RegisterWidget(
    ComponentIdentifiers.COURSE_INFO_WIDGET_IDENTIFIER,
    typeof(CourseInfoWidgetController),
    "Course Info Widget",
    Description = "A widget to display the current user's current classes, completed classes, and memberships",
    IconClass = "icon-tab"
    )]

namespace Pedagogy.Controllers.Widgets
{
    public class CourseInfoWidgetController : WidgetController<CourseInfoWidgetProperties>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;
        private readonly IClassBuilderDependencies ClassBuilderDependencies;

        public CourseInfoWidgetController(IClassMemberDependencies classMemberDependencies, IClassBuilderDependencies classBuilderDependencies) 
        { 
            ClassMemberDependencies = classMemberDependencies;
            ClassBuilderDependencies = classBuilderDependencies;
        }

        public ActionResult Index()
        {
            try
            {
                // get the current logged in user
                var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

                // check if a site admin is impersonating
                var isSiteAdmin = false;
                var currentUser = CookieHelper.GetExistingCookie(ContentIdentifiers.CURRENT_USERNAME_COOKIE);
                if (currentUser != null)
                {
                    var u = UserInfoProvider.GetUserInfo(currentUser.Value);
                    if (u != null)
                    {
                        if (u.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName)) isSiteAdmin = true;
                    }
                }

                // get the student data
                int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
                var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto(x));

                var current = new List<StudentClassViewModel>();

                if (student != null)
                {
                    var completedWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.COMPLETED_WF_CODE_NAME);
                    var archivedWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.ARCHIVED_WF_CODE_NAME);
                    var startClassWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);

                    // get current courses (this tab displays by default)
                    var currentCourses = ClassMemberDependencies.StudentClassRepo.GetStudentClassesByStudentID(student.ContactID, 
                        x => x.ClassWorkflowID != completedWF && x.ClassWorkflowID != archivedWF, x => new StudentClassDto(x));

                    foreach (var item in currentCourses)
                    {
                        // get the student class details
                        var classDetail = ClassMemberDependencies.StudentClassDetailRepo.GetMostRecentStudentClassDetail(item.StudentClassID,
                            x => new StudentClassDetailDto { ChapterSequence = x.ChapterSequence, PageNumber = x.PageNumber });

                        var step = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflow(item.ClassWorkflowID, x => new ClassWorkflowDto(x));
                        string url = "";

                        if (step.WorkflowCodeName.Contains(ContentIdentifiers.CONTINUE_WF_CODE_NAME) || 
                            step.WorkflowCodeName.Contains(ContentIdentifiers.START_CLASS_WF_CODE_NAME))
                        {
                            url += ClassMembersHelper.BuildCourseUrl(
                                item.StudentClassID, classDetail != null ? classDetail.ChapterSequence : 1, classDetail != null ? classDetail.PageNumber : 1);
                        }

                        if (step.WorkflowCodeName.Contains(ContentIdentifiers.COMPLETE_TEST_WF_CODE_NAME) || 
                            step.WorkflowCodeName.Contains(ContentIdentifiers.RETAKE_TEST_WF_CODE_NAME) || 
                            step.WorkflowCodeName.Contains(ContentIdentifiers.SCORE_TEST_WF_CODE_NAME) ||
                            step.WorkflowCodeName.Contains(ContentIdentifiers.PRINT_CERT_WF_CODE_NAME))
                        {
                            url += ClassMembersHelper.BuildTestUrl(item.StudentClassID, 1, 1);
                        }

                        if (step.WorkflowCodeName.Contains(ContentIdentifiers.COMPLETE_EVAL_CODE_NAME))
                            url += ClassMembersHelper.BuildClassEvalUrl(item.StudentClassID);

                        var attemptUrls = new Dictionary<int, string>();
                        if (item.TestAttemptID.HasValue)
                        {
                            var attempts = ClassMemberDependencies.StudentTestAttemptRepo.GetStudentTestAttempts(item.StudentClassID, 
                                x => new StudentTestAttemptDto { StudentClassID = x.StudentClassID, TestAttemptID = x.TestAttemptID });

                            if (attempts.Count() > 0)
                            {
                                attempts.Remove(attempts.Last());

                                attempts.ForEach(x => { attemptUrls.Add(attempts.IndexOf(x) + 1, ClassMembersHelper.BuildTestAttemptUrl(x.StudentClassID, x.TestAttemptID)); });
                            }
                        }

                        if (step.WorkflowID == startClassWF && item.ExpiresOn.HasValue && item.StudentMembershipID.HasValue)
                        {

                        }
                        else
                        {
                            var c = ClassBuilderDependencies.ClassRepo.GetClass(item.ClassID, x => new ClassDto { ClassName = x.ClassName, CEUValue = x.CEUValue });

                            var newSC = new StudentClassViewModel
                            {
                                StudentClassID = item.StudentClassID,
                                ClassName = c.ClassName,
                                ContactHours = c.CEUValue.Value,
                                WorkflowStep = step.WorkflowDisplayName,
                                ClassPageUrl = url,
                                StudentID = student.ContactID,
                                AttemptUrls = attemptUrls,
                                InvoiceNum = item.InvoiceNumber,
                                InPackage = !string.IsNullOrEmpty(item.InvoiceNumber) && item.InvoiceNumber != "null" && ClassMembersHelper.IsClassInPackage(item.InvoiceNumber, item.ClassID)
                            };

                            current.Add(newSC);
                        }
                    }
                }

                return PartialView(ComponentIdentifiers.COURSE_INFO_WIDGET_VIEW, 
                    CourseInfoWidgetViewModel.GetViewModel(isSiteAdmin, current.OrderBy(x => x.ClassName), GetProperties().CardHeading));
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("CourseInfoWidgetController", "ERROR", e);
                return Content("");
            }
        }

        [HttpGet]
        public JsonResult GetCompletedCourses()
        {
            var completedWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.COMPLETED_WF_CODE_NAME);
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);

            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var student = ClassMemberDependencies.ContactRepo.GetContact(currentUser?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });

            var completed = new List<StudentClassViewModel>();
            if(student != null)
            {
                var completedCourses = ClassMemberDependencies.StudentClassRepo.GetStudentClassesByStudentID(student.ContactID, x => x.ClassWorkflowID == completedWF, 
                    x => new StudentClassDto(x));

                foreach (var i in completedCourses)
                {
                    var attemptUrls = new Dictionary<int, string>();
                    if (i.TestAttemptID.HasValue)
                    {
                        var attempts = ClassMemberDependencies.StudentTestAttemptRepo.GetStudentTestAttempts(i.StudentClassID, 
                            x => new StudentTestAttemptDto { StudentClassID = x.StudentClassID, TestAttemptID = x.TestAttemptID });

                        attempts.ForEach(x => { attemptUrls.Add(attempts.IndexOf(x) + 1, ClassMembersHelper.BuildTestAttemptUrl(x.StudentClassID, x.TestAttemptID)); });
                    }

                    var c = ClassBuilderDependencies.ClassRepo.GetClass(i.ClassID, x => new ClassDto { ClassName = x.ClassName, CEUValue = x.CEUValue });

                    completed.Add(new StudentClassViewModel
                    {
                        ClassName = c.ClassName,
                        ContactHours = c.CEUValue.Value,
                        StudentID = student.ContactID,
                        StudentClassID = i.StudentClassID,
                        ClassPageUrl = ClassMembersHelper.BuildCourseUrl(i.StudentClassID, 1, 1),
                        TestScore = i.TestScore.HasValue ? Math.Round(i.TestScore.Value, 2) : 0,
                        DateCompletedOn = i.TestCompletedOn.HasValue ? i.TestCompletedOn.Value.ToString("MM/dd/yyyy") : "",
                        PrintCertUrl = i.CertID.HasValue ? ClassMembersHelper.BuildPrintCertUrl(i.StudentClassID, i.CertID.Value) : "",
                        AttemptUrls = attemptUrls,
                        InvoiceNum = i.InvoiceNumber,
                        InPackage = ClassMembersHelper.IsClassInPackage(i.InvoiceNumber, i.ClassID)
                    });
                }
            }

            return Json(JsonConvert.SerializeObject(new { Courses = completed.OrderBy(x => x.ClassName).ToList() }), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetMemberships()
        {
            var startClassWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);

            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var student = ClassMemberDependencies.ContactRepo.GetContact(currentUser?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });

            var memberships = new List<StudentClassViewModel>();
            var mems = new List<MembershipViewModel>();

            if(student != null)
            {
                var membershipClasses = ClassMemberDependencies.StudentClassRepo.GetStudentMembershipClasses(student.ContactID, x => new StudentClassDto(x));

                var memNames = new List<string>();
                foreach (var x in membershipClasses)
                {
                    var c = ClassBuilderDependencies.ClassRepo.GetClass(x.ClassID, x => new ClassDto { ClassName = x.ClassName, CEUValue = x.CEUValue });

                    var memName = ClassMemberDependencies.StudentMembershipRepo.GetStudentMembershipName(x.StudentMembershipID.Value);
                    if (!memNames.Contains(memName)) memNames.Add(memName);

                    memberships.Add(new StudentClassViewModel
                    {
                        StudentClassID = x.StudentClassID,
                        ClassName = c.ClassName,
                        ContactHours = c.CEUValue.Value,
                        WorkflowStep = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowDisplayNameByID(x.ClassWorkflowID),
                        OnStart = x.ClassWorkflowID == startClassWF,
                        ClassPageUrl = ClassMembersHelper.BuildCourseUrl(x.StudentClassID, 1, 1),
                        StudentID = student.ContactID,
                        ExpDate = x.ExpiresOn.HasValue ? x.ExpiresOn.Value.ToString("MM/dd/yyyy h:m:s tt") : "",
                        MemName = memName,
                        InvoiceNum = x.InvoiceNumber,
                        InPackage = ClassMembersHelper.IsClassInPackage(x.InvoiceNumber, x.ClassID)
                    });
                }

                foreach (var t in memNames)
                {
                    var classes = new List<StudentClassViewModel>();
                    var x = new MembershipViewModel { };
                    foreach (var i in memberships)
                    {
                        if (t == i.MemName)
                        {
                            x.MembershipName = t;
                            classes.Add(i);
                        }
                    }
                    x.Classes = classes.OrderBy(x => x.ClassName);
                    mems.Add(x);
                }
            }

            return Json(JsonConvert.SerializeObject(new { Memberships = mems }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveStudentClass(int studentClassID)
        {
            var studentClass = ClassMemberDependencies.StudentClassRepo.GetStudentClass(studentClassID, x => new StudentClassDto { InvoiceNumber = x.InvoiceNumber, ClassID = x.ClassID });

            var orderItem = OrderItemInfoProvider.GetOrderItems()
                   .Where(x => x.OrderItemOrderID == int.Parse(studentClass.InvoiceNumber))
                   .Where(x => x.OrderItemSKU.SKUNumber == studentClass.ClassID.ToString())
                   .FirstOrDefault();

            var order = OrderInfoProvider.GetOrderInfo(int.Parse(studentClass.InvoiceNumber));

            //if it is a package
            if (orderItem.OrderItemBundleGUID != null && orderItem.OrderItemBundleGUID != Guid.Empty)
            {
                var parentBundleItem = OrderItemInfoProvider.GetOrderItems()
                    .Where(x => x.OrderItemOrderID == int.Parse(studentClass.InvoiceNumber))
                    .Where(x => x.OrderItemGUID == orderItem.OrderItemBundleGUID)
                    .FirstOrDefault();

                var packageItems = OrderItemInfoProvider.GetOrderItems()
                    .Where(x => x.OrderItemOrderID == int.Parse(studentClass.InvoiceNumber))
                    .Where(x => x.OrderItemBundleGUID == orderItem.OrderItemBundleGUID)
                    .ToList();

                order.OrderTotalPrice -= parentBundleItem.OrderItemUnitPrice;
                order.OrderGrandTotal -= parentBundleItem.OrderItemUnitPrice;
                order.OrderTotalPriceInMainCurrency -= parentBundleItem.OrderItemUnitPrice;
                order.OrderGrandTotalInMainCurrency -= parentBundleItem.OrderItemUnitPrice;
                
                var printOption = OrderItemInfoProvider.GetOrderItems()
                     .Where(x => x.OrderItemOrderID == int.Parse(studentClass.InvoiceNumber))
                     .Where(x => x.OrderItemSKU.IsProductOption)
                     .Where(x => x.OrderItemParentGUID == parentBundleItem.OrderItemGUID)
                     .FirstOrDefault();

                if (printOption != null)
                {
                    order.OrderTotalPrice -= printOption.OrderItemUnitPrice;
                    order.OrderGrandTotal -= printOption.OrderItemUnitPrice;
                    order.OrderTotalPriceInMainCurrency -= printOption.OrderItemUnitPrice;
                    order.OrderGrandTotalInMainCurrency -= printOption.OrderItemUnitPrice;

                    OrderItemInfoProvider.DeleteOrderItemInfo(printOption);
                }

                //need to remove all these items and their sales info and their student classes
                foreach (var item in packageItems)
                {
                    int? salesItem = SalesInfoProvider.GetSales()
                        .Where(x => x.SaleInvoiceNumber == int.Parse(item.OrderItemOrderID.ToString()))
                        .Where(x => x.SaleClassID == int.Parse(item.OrderItemSKU.SKUNumber))
                        .Select(x => x.SalesID)
                        .FirstOrDefault();

                    if (salesItem.HasValue) SalesInfoProvider.DeleteSalesInfo(salesItem.Value);
                    OrderItemInfoProvider.DeleteOrderItemInfo(item);

                    var classID = int.Parse(item.OrderItemSKU.SKUNumber);
                    var invoice = item.OrderItemOrderID.ToString();

                    var sc = ClassMemberDependencies.StudentClassRepo.GetStudentClassByInvoiceClassID(classID, invoice, x => new StudentClassDto { StudentClassID = x.StudentClassID });

                    if(sc != null) ClassMemberDependencies.StudentClassRepo.RemoveStudentClass(sc.StudentClassID);
                }

                OrderItemInfoProvider.DeleteOrderItemInfo(parentBundleItem);
            }
            //if it is just 1 regular course
            else
            {
                order.OrderTotalPrice -= orderItem.OrderItemUnitPrice;
                order.OrderGrandTotal -= orderItem.OrderItemUnitPrice;
                order.OrderTotalPriceInMainCurrency -= orderItem.OrderItemUnitPrice;
                order.OrderGrandTotalInMainCurrency -= orderItem.OrderItemUnitPrice;

                var printOption = OrderItemInfoProvider.GetOrderItems()
                    .Where(x => x.OrderItemOrderID == int.Parse(studentClass.InvoiceNumber))
                    .Where(x => x.OrderItemSKU.IsProductOption)
                    .Where(x => x.OrderItemParentGUID == orderItem.OrderItemGUID)
                    .FirstOrDefault();

                if (printOption != null)
                {
                    order.OrderTotalPrice -= printOption.OrderItemUnitPrice;
                    order.OrderGrandTotal -= printOption.OrderItemUnitPrice;
                    order.OrderTotalPriceInMainCurrency -= printOption.OrderItemUnitPrice;
                    order.OrderGrandTotalInMainCurrency -= printOption.OrderItemUnitPrice;

                    OrderItemInfoProvider.DeleteOrderItemInfo(printOption);
                }

                int? salesItem = SalesInfoProvider.GetSales()
                        .Where(x => x.SaleInvoiceNumber == int.Parse(studentClass.InvoiceNumber))
                        .Where(x => x.SaleClassID == studentClass.ClassID)
                        .Select(x => x.SalesID)
                        .FirstOrDefault();

                if (salesItem.HasValue) SalesInfoProvider.DeleteSalesInfo(salesItem.Value);
                OrderItemInfoProvider.DeleteOrderItemInfo(orderItem);

                ClassMemberDependencies.StudentClassRepo.RemoveStudentClass(studentClassID);
            }

            int status = 0;
            if(order.OrderGrandTotal == 0)
            {
                status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
            }
            else
            {
                status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PARTIAL_REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
            }

            order.OrderStatusID = status;
            OrderInfoProvider.SetOrderInfo(order);

            return Redirect(ContentIdentifiers.DASHBOARD);
        }

        [HttpPost]
        public ActionResult CheckRequirements(string classUrl)
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            
            // get the student data
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });

            var hasLicenses = false;

            // check if the student has at least one license
            var studentLicenses = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(student.ContactID, x => new StudentLicenseDto { StudentLicenseID = x.StudentLicenseID });
            if (studentLicenses.Count() > 0) hasLicenses = true;

            // check if the user has changed their password yet
            bool isDefaultPass = false;
            var data = user.UserSettings.UserCustomData.GetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
            if (data != null)
            {
                if (data.ToString().ToLower() == "true") isDefaultPass = true;
            }

            if (isDefaultPass || !hasLicenses)
            {
                var err = new List<string>();
                if (isDefaultPass)
                    err.Add(ResHelper.GetString("Dashboard.IsDefaultPasswordWarningMsg", LocalizationContext.CurrentCulture.CultureCode));

                if (!hasLicenses)
                    err.Add(ResHelper.GetString("Dashboard.NoStudentLicensesWarningMsg", LocalizationContext.CurrentCulture.CultureCode));

                return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = err, Url = "" }), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "", Url = classUrl }), JsonRequestBehavior.AllowGet);
            }
        }
    }
}