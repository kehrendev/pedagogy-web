﻿using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.PrintStudentCertificatesWidget;

using System.Collections.Generic;
using System.Web.Mvc;

[assembly:RegisterWidget(
    ComponentIdentifiers.PRINT_STUDENT_CERTIFICATES_WIDGET_IDENTIFIER,
    typeof(PrintStudentCertificatesWidgetController),
    "Print Student Certificates Widget",
    Description = "Used to display a list of student to print their certificates",
    IconClass = "icon-printer")]

namespace Pedagogy.Controllers.Widgets
{
    public class PrintStudentCertificatesWidgetController : WidgetController<PrintStudentCertificatesWidgetProperties>
    {
        public ActionResult Index(int facilityID = 0)
        {
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);

            var completedWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.COMPLETED_WF_CODE_NAME);
            var printWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.PRINT_CERT_WF_CODE_NAME);
            var archivedWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.ARCHIVED_WF_CODE_NAME);

            var facilityCode = ClassMembersHelper.GetFacility(facilityID, x => new Facility { FacilityCode = x.FacilityCode }).FacilityCode;
            var facilityContacts = ClassMembersHelper.GetContactFacilities(facilityID, x => new ContactFacility { ContactID = x.ContactID });

            var students = new Dictionary<int, string>();
            foreach(var item in facilityContacts)
            {
                var contact = ClassMembersHelper.GetContact(item.ContactID, x => x.ContactTypeID == studentType, 
                    x => new Contact { ContactFirstName = x.ContactFirstName, ContactLastName = x.ContactLastName });

                if(contact != null)
                {
                    var studentCertCount = ClassMembersHelper.GetStudentClassCertificateCount(item.ContactID, facilityCode);

                    if(studentCertCount > 0)
                    {
                        students.Add(item.ContactID, $"{contact.ContactFirstName} {contact.ContactLastName}");
                    }
                }
            }

            return PartialView(ComponentIdentifiers.PRINT_STUDENT_CERTIFICATES_WIDGET_VIEW, 
                PrintStudentCertificatesWidgetViewModel.GetViewModel(GetProperties().CardHeading, facilityID, students));
        }

        [HttpPost]
        public ActionResult SendDataToCertHandler(IEnumerable<int> ids)
        {
            return Redirect(ClassMembersHelper.BuildPrintStudentsCertsUrl(ids));
        }
    }
}