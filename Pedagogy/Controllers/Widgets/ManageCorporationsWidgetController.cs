﻿using CMS.DocumentEngine;
using CMS.Globalization;
using CMS.Membership;
using CMS.SiteProvider;
using CMS.EventLog;
using CMS.Localization;
using CMS.Helpers;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Membership;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.ManageCorporationsWidget;
using Pedagogy.Lib.DataModels;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Facility;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.ContactFacility;
using Pedagogy.ClassMembers.Entities.FacilityClass;
using Pedagogy.ClassMembers.Entities.StudentClass;
using Pedagogy.ClassMembers.Entities.FacilityMembership;

using System.Collections.Generic;
using System.Web.Mvc;
using System.Web;
using System;
using System.Linq;

using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

using Newtonsoft.Json;

[assembly: RegisterWidget(
    ComponentIdentifiers.MANAGE_CORPORATIONS_WIDGET_IDENTIFIER,
    typeof(ManageCorporationsWidgetController),
    "Manage Corporations Widget",
    Description = "A widget used to display all the data that an admin can manage for a corporation",
    IconClass = "icon-building")]

namespace Pedagogy.Controllers.Widgets
{
    public class ManageCorporationsWidgetController : WidgetController<ManageCorporationsWidgetProperties>
    {
        #region Dependency Injection

        private readonly IClassMemberDependencies ClassMemberDependencies;

        public ManageCorporationsWidgetController(IClassMemberDependencies classMemberDependencies)
        {
            ClassMemberDependencies = classMemberDependencies;
        }

        public UserManager UserManager => HttpContext.GetOwinContext().Get<UserManager>();

        #endregion

        #region Initial Page Listing Methods

        public ActionResult Index(int corporationID = 0)
        {
            string returnUrl = GetPage<TreeNode>().NodeAliasPath;
            if (corporationID > 0)
            {
                returnUrl = GetPage<TreeNode>().NodeAliasPath + $"?corporationID={corporationID}";
            }

            var contactTypes = ClassMemberDependencies.ContactTypeRepo.GetFilteredContactTypes(new List<string> { ContentIdentifiers.FACILITY_ADMIN_CODE_NAME, ContentIdentifiers.STUDENT_CODE_NAME });

            return PartialView(ComponentIdentifiers.MANAGE_CORPORATIONS_WIDGET_VIEW,
                ManageCorporationsWidgetViewModel.GetViewModel(User.Identity.Name.ToString(), returnUrl, GetProperties().CardHeading, corporationID, contactTypes));
        }

        [HttpGet]
        public JsonResult GetPagedData(ManageCorporationsWidgetViewModel model, int pageNumber = 1, int pageSize = 16)
        {
            string state = "";
            if (model.CorporationData.StateID > 0) state = StateInfoProvider.GetStateInfo(model.CorporationData.StateID).StateCode;

            List<FacilityViewModel> corps = new List<FacilityViewModel>();
            if (model.CurrentCorpID != 0)
            {
                var currentCorp = ClassMemberDependencies.FacilityRepo.GetFacility(model.CurrentCorpID,
                    x => new FacilityDto { FacilityName = x.Name, FacilityID = x.FacilityID, FacilityCode = x.FacilityCode });

                corps = new List<FacilityViewModel>
                {
                    FacilityViewModel.GetViewModel(model.ReturnUrl, currentCorp.FacilityID, currentCorp.FacilityName, currentCorp.FacilityCode, model.IsSiteAdmin,
                        ClassMemberDependencies.FacilityRepo.CorporationHasChildFacilities(currentCorp.FacilityID))
                };
            }
            else
            {
                corps = ClassMembersHelper.GetCorporationsForCurrentUser(ClassMemberDependencies, model.IsSiteAdmin, model.CurrentUserID, model.ReturnUrl);
            }

            corps = ClassMembersHelper.FilterFacilities(model.CorporationData, state, corps);
            PagedData<FacilityViewModel> pagedData;

            if (!string.IsNullOrEmpty(state) || !string.IsNullOrEmpty(model.ContactModel.FirstName) || !string.IsNullOrEmpty(model.ContactModel.LastName) ||
                !string.IsNullOrEmpty(model.ContactModel.Phone))
            {

                var type = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);
                var c = ClassMemberDependencies.ContactRepo.GetContacts(type,
                    x => new ContactDto { ContactID = x.ContactID, ContactFirstName = x.ContactFirstName, ContactLastName = x.ContactLastName, ContactPhone = x.ContactPhone });

                var contacts = new List<ContactViewModel>();
                c.ForEach(x => contacts.Add(ContactViewModel.GetViewModel(x.ContactID, x.ContactFirstName, x.ContactLastName, x.ContactPhone)));

                var filteredContacts = ClassMemberDependencies.ContactRepo.GetFilteredFacilityStaffMembers(model.ContactModel.FacilityID, model.ContactModel.FirstName, model.ContactModel.LastName);
                var cs = new List<ContactViewModel>();
                filteredContacts.ForEach(x => { cs.Add(new ContactViewModel(x)); });

                pagedData = PagingHelper.PagedResult(
                    ClassMembersHelper.SetUpFacilityViewModel(ClassMemberDependencies, cs, corps),
                    pageNumber,
                    pageSize);

                return Json(pagedData, JsonRequestBehavior.AllowGet);
            }

            pagedData = PagingHelper.PagedResult(corps, pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void RemoveCorporation(int corpID)
        {
            var contactFacility = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByFacilityID(corpID, x => new ContactFacilityDto(x));
            var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);

            foreach (var i in contactFacility)
            {
                var contact = ClassMemberDependencies.ContactRepo.GetContact(i.ContactID, x => new ContactDto { ContactID = x.ContactID });
                if (contact != null)
                {
                    if (contact.ContactTypeID != studentType) ClassMemberDependencies.ContactRepo.RemoveContact(contact.ContactID);
                }

                ClassMemberDependencies.ContactFacilityRepo.RemoveContactFacility(i.ContactFacilityID);
            }

            var childFacilities = ClassMemberDependencies.FacilityRepo.GetChildFacilities(corpID, x => new FacilityDto(x));
            foreach (var child in childFacilities)
            {
                var facilityCode = ClassMemberDependencies.FacilityRepo.GetFacility(child.FacilityID, x => new FacilityDto { FacilityCode = x.FacilityCode }).FacilityCode;

                var facilityClasses = ClassMemberDependencies.FacilityClassRepo.GetFacilityClasses(child.FacilityID, x => new FacilityClassDto(x));
                facilityClasses.ForEach(x =>
                {
                    var studentClasses = ClassMemberDependencies.StudentClassRepo.GetAllFacilityStudentClasses(facilityCode, x => x.ClassWorkflowID == startWF, x => new StudentClassDto(x));
                    studentClasses.ForEach(y => { ClassMemberDependencies.StudentClassRepo.RemoveStudentClass(y.StudentClassID); });

                    ClassMemberDependencies.FacilityClassRepo.RemoveFacilityClass(x.FacilityClassID);
                });

                var facilityMems = ClassMemberDependencies.FacilityMembershipRepo.GetAllFacilityMemberships(child.FacilityID, x => new FacilityMembershipDto(x));
                facilityMems.ForEach(x => { ClassMemberDependencies.FacilityMembershipRepo.RemoveFacilityMembership(x.FacilityMembershipID); });

                var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByFacilityID(child.FacilityID, x => new ContactFacilityDto(x));
                foreach (var item in cf)
                {
                    var contact = ClassMemberDependencies.ContactRepo.GetContact(item.ContactID, x => new ContactDto(x));
                    if (contact != null)
                    {
                        if (contact.ContactTypeID != studentType) ClassMemberDependencies.ContactRepo.RemoveContact(contact.ContactID);
                    }

                    ClassMemberDependencies.ContactFacilityRepo.RemoveContactFacility(item.ContactFacilityID);
                }

                if (child.FacilityPreferenceID.HasValue)
                {
                    ClassMemberDependencies.FacilityPreferenceRepo.RemoveFacilityPreference(child.FacilityPreferenceID.Value);
                }

                ClassMemberDependencies.FacilityRepo.RemoveFacility(child.FacilityID);
            }

            var f = ClassMemberDependencies.FacilityRepo.GetFacility(corpID, f => new FacilityDto { FacilityID = f.FacilityID });
            if (f != null) ClassMemberDependencies.FacilityRepo.RemoveFacility(f.FacilityID);
        }

        #endregion

        #region TAB 1 Methods: Corporation Info

        public JsonResult GetCorpData(int corpID)
        {
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(corpID, x => new FacilityDto(x));

            var model = new FacilityViewModel(facility);
            return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateCorpInfo(FacilityViewModel corporationData)
        {
            var corporationDto = new FacilityDto
            {
                FacilityID = corporationData.FacilityID,
                FacilityName = corporationData.Name,
                AccountingCode = corporationData.AcctingCode,
                FacilityCity = corporationData.City,
                FacilityState = corporationData.StateID != 0 ? StateInfoProvider.GetStateInfo(corporationData.StateID).StateCode : "",
                Enabled = corporationData.Enabled,
                FacilityIsCorporation = true,
                FacilityAddress1 = corporationData.AddressLine1,
                FacilityAddress2 = corporationData.AddressLine2,
                FacilityZip = corporationData.Zip
            };

            var addedFacility = ClassMemberDependencies.FacilityRepo.CreateCorporation(corporationDto);

            return Json(JsonConvert.SerializeObject(new FacilityViewModel(addedFacility)), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region TAB 2 Methods: Corporation Contacts

        public JsonResult GetCorpContacts(int corpID)
        {
            var corpContacts = ClassMemberDependencies.ContactRepo.GetCorporationContacts(corpID).Select(x => new ContactViewModel(x) { TypeName = x.TypeName }).ToList();

            return Json(JsonConvert.SerializeObject(corpContacts), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetContactData(int contactID)
        {
            var contact = ClassMemberDependencies.ContactRepo.GetContact(contactID, x => new ContactDto(x));

            return Json(JsonConvert.SerializeObject(new ContactViewModel(contact)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateContact(ManageCorporationsWidgetViewModel model)
        {
            // user needs user acct
            if (model.ContactModel.NeedsUserAcct)
            {
                var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
                var adminType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);
                var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

                var kenticoUser = new User
                {
                    FirstName = model.ContactModel.FirstName,
                    LastName = model.ContactModel.LastName,
                    Enabled = true
                };

                if (!string.IsNullOrEmpty(model.ContactModel.Email))
                {
                    kenticoUser.Email = model.ContactModel.Email;
                    kenticoUser.UserName = model.ContactModel.Email;
                }
                else
                {
                    kenticoUser.UserName = $"{model.ContactModel.FirstName.Replace("'", "").Replace("-", "")}.{model.ContactModel.LastName.Replace("'", "").Replace("-", "")}";

                    var userNameTest = UserInfoProvider.GetUserInfo(kenticoUser.UserName);
                    if (userNameTest != null)
                    {
                        for (var x = 1; x > 0; x++)
                        {
                            kenticoUser.UserName += x;
                            if (UserInfoProvider.GetUserInfo(kenticoUser.UserName) == null) break;
                            else kenticoUser.UserName = kenticoUser.UserName.Replace(x.ToString(), "");
                        }
                    }
                }

                var defaultPass = UserInfoProvider.GenerateNewPassword(SiteContext.CurrentSiteName);

                // Attempts to create the user in the Kentico database
                IdentityResult registerResult = IdentityResult.Failed();
                try
                {
                    registerResult = UserManager.Create(kenticoUser, defaultPass);
                }
                catch (Exception ex)
                {
                    // Logs an error into the Kentico event log if the creation of the user fails
                    EventLogProvider.LogException("ManageCorporationsWidget", "UserRegistration", ex);
                }

                //If the registration was not successful, displays the registration form with an error message
                if (!registerResult.Succeeded)
                {
                    var errors = "";
                    foreach (string error in registerResult.Errors)
                    {
                        errors += error + " ";
                    }

                    return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = errors, Contacts = new List<ContactViewModel>() }), JsonRequestBehavior.AllowGet);
                }

                var userInfo = UserInfoProvider.GetUserInfo(kenticoUser.Id);
                var s = UserSettingsInfoProvider.GetUserSettingsInfo(userInfo.UserSettings.UserSettingsID);
                s.SetValue(nameof(UserSettingsInfo.UserPhone), model.ContactModel.Phone);
                s.Update();

                s.UserCustomData.SetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN, true);
                s.Update();

                if (!string.IsNullOrEmpty(model.ContactModel.Email))
                {
                    userInfo.SetValue(nameof(UserInfo.Email), model.ContactModel.Email);
                    userInfo.Update();

                    Dictionary<string, object> data = new Dictionary<string, object>
                                {
                                    { "DefaultPassword", defaultPass }
                                };

                    EmailHelper.SendEmail(model.ContactModel.Email, ContentIdentifiers.CONTACT_CREATION_ET_INDENTIFIER, data);
                }
                else
                {
                    Dictionary<string, object> data = new Dictionary<string, object>
                                {
                                    { "Contact", model.CorporationData.ContactModel },
                                    { "Username", kenticoUser.UserName },
                                    { "DefaultPassword", defaultPass }
                                };

                    EmailHelper.SendEmail(model.ContactModel.UserPassEmailRecipient, ContentIdentifiers.CONTACT_CREATION_FACILITY_ET_IDENTIFIER, data);
                }

                if (model.ContactModel.TypeID == adminType)
                {
                    UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);
                }
                else
                {
                    UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName);
                }

                if (model.ContactModel.AllowOnlinePurchases)
                {
                    UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
                }

                var c = ClassMemberDependencies.ContactRepo.GetContact(model.ContactModel.ContactID, model.ContactModel.TypeID, x => new ContactDto(x));
                if (c == null)
                {
                    var contactDto = new ContactDto
                    {
                        ContactUserID = userInfo?.UserID,
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID
                    };

                    var contact = ClassMemberDependencies.ContactRepo.CreateContact(contactDto);

                    var studentContactID = ClassMemberDependencies.ContactRepo.GetContact(contact.ContactID, studentType, x => new ContactDto(x)).ContactID;

                    if (studentContactID == 0)
                    {
                        contactDto.ContactTypeID = studentType;

                        studentContactID = ClassMemberDependencies.ContactRepo.CreateContact(contactDto).ContactID;
                    }

                    ClassMemberDependencies.ContactRepo.AddHowToCourseToContact(studentContactID);
                    ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(contact.ContactID, model.CorporationData.FacilityID, true);

                    if (contact.ContactTypeID == adminType)
                    {
                        var facility = ClassMemberDependencies.FacilityRepo.GetCorporation(model.CorporationData.FacilityID, x => new FacilityDto { FacilityName = x.Name });

                        var adminCreatedEventType = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeIDByCodeName(ContentIdentifiers.ADMIN_CREATED_ET_CODENAME);
                        ClassMemberDependencies.EventLogRepo.CreateNewEvent(currentUser, contact, adminCreatedEventType, facility.FacilityName);
                    }
                }
                else
                {
                    var contactDto = new ContactDto
                    {
                        ContactUserID = userInfo?.UserID,
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID
                    };

                    ClassMemberDependencies.ContactRepo.UpdateContact(c.ContactID, contactDto);

                    var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(c.ContactID, model.CorporationData.FacilityID, x => new ContactFacilityDto(x));
                    if (cf == null) ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(c.ContactID, model.CorporationData.FacilityID, true);
                }
            }
            // user already has an acct or doesn't need an acct
            else
            {
                var user = model.ContactModel.ContactUserID.HasValue ? UserInfoProvider.GetUserInfo(model.ContactModel.ContactUserID.Value) : null;
                if (user == null)
                {
                    user = UserInfoProvider.GetUserInfo(model.ContactModel.Email);
                    if (user == null)
                    {
                        //user = UserInfoProvider.GetUserInfo($"{model.ContactModel.FirstName}.{model.ContactModel.LastName}");
                    }
                    else
                    {
                        UserInfoProvider.AddUserToRole(user.UserName, ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
                    }
                }

                if (model.ContactModel.AllowOnlinePurchases)
                {
                    if (user == null)
                    {
                        return Json(JsonConvert.SerializeObject(new
                        {
                            Err = true,
                            ErrMsg = ResHelper.GetString("Dashboard.FacilityPurchaserRoleErrMsg", LocalizationContext.CurrentCulture.CultureCode),
                            Contacts = new List<ContactViewModel>()
                        }), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (user != null)
                    {
                        if (user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName))
                        {
                            UserManager.RemoveFromRole(user.UserID, ContentIdentifiers.FACILITY_PURCHASER_ROLE);
                        }
                    }
                }

                if (user != null)
                {
                    if (user.UserSettings != null)
                    {
                        var s = UserSettingsInfoProvider.GetUserSettingsInfo(user.UserSettings.UserSettingsID);
                        s.SetValue(nameof(UserSettingsInfo.UserPhone), model.ContactModel.Phone);
                        s.Update();
                    }

                    user.SetValue(nameof(UserInfo.Email), model.ContactModel.Email);
                    user.Update();

                    var corpAdminType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);
                    if (model.ContactModel.TypeID == corpAdminType)
                    {
                        if (user.IsInRole(ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName)) UserInfoProvider.RemoveUserFromRole(user.UserName, ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName);
                        if (user.IsInRole(ContentIdentifiers.FACILITY_ADMIN_ROLE, SiteContext.CurrentSiteName)) UserInfoProvider.RemoveUserFromRole(user.UserName, ContentIdentifiers.FACILITY_ADMIN_ROLE, SiteContext.CurrentSiteName);

                        UserInfoProvider.AddUserToRole(user.UserName, ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);
                    }
                }

                var contact = ClassMemberDependencies.ContactRepo.GetContact(model.ContactModel.ContactID, x => new ContactDto(x));
                if (contact == null)
                {
                    var contactDto = new ContactDto
                    {
                        ContactUserID = user?.UserID,
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID
                    };

                    var newContact = ClassMemberDependencies.ContactRepo.CreateContact(contactDto);

                    ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(newContact.ContactID, model.CorporationData.FacilityID, true);
                }
                else
                {
                    var contactDto = new ContactDto
                    {
                        ContactUserID = user?.UserID,
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID
                    };

                    ClassMemberDependencies.ContactRepo.UpdateContact(contact.ContactID, contactDto);

                    var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(contact.ContactID, model.CorporationData.FacilityID, x => new ContactFacilityDto(x));
                    if (cf == null) ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(contact.ContactID, model.CorporationData.FacilityID, true);
                }
            }

            return Json(JsonConvert.SerializeObject(
                new { Err = false, ErrMsg = "", Contacts = ClassMemberDependencies.ContactRepo.GetCorporationContacts(model.CorporationData.FacilityID).Select(x => new ContactViewModel(x) { TypeName = x.TypeName }).ToList() }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveContact(int corpID, int contactID)
        {
            var contactFacility = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(contactID, corpID, x => new ContactFacilityDto(x));
            if (contactFacility != null) ClassMemberDependencies.ContactFacilityRepo.RemoveContactFacility(contactFacility.ContactFacilityID);

            var contact = ClassMemberDependencies.ContactRepo.GetContact(contactID, x => new ContactDto(x));
            if (contact != null) ClassMemberDependencies.ContactRepo.RemoveContact(contact.ContactID);

            return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "", Contacts = ClassMemberDependencies.ContactRepo.GetCorporationContacts(corpID).Select(x => new ContactViewModel(x) { TypeName = x.TypeName }).ToList() }),
                JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}