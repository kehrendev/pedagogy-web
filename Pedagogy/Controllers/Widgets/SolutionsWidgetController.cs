﻿using CMS.DocumentEngine.Types.Pedagogy;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.SolutionsWidget;

using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.SOLUTIONS_WIDGET_IDENTIFIER,
    typeof(SolutionsWidgetController),
    "Solutions Widget",
    Description = "This displays a widget containing the solution cards with a title and some text",
    IconClass = "icon-light-bulb"
    )]

namespace Pedagogy.Controllers.Widgets
{
    public class SolutionsWidgetController : WidgetController<SolutionsWidgetProperties>
    {
        public ActionResult Index()
        {
            var props = GetProperties();

            var solutions = SolutionProvider.GetSolutions().Path("/Education-Solutions/%");    

            var model = new SolutionsWidgetViewModel
            {
                Title = props.SolutionTitle,
                Text = props.SolutionText,
                Solutions = solutions
            };

            return PartialView(ComponentIdentifiers.SOLUTIONS_WIDGET_VIEW, model);
        }
    }
}