﻿using CMS.CustomTables;
using CMS.CustomTables.Types.Pedagogy;
using CMS.Membership;
using CMS.SiteProvider;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.Facility;
using Pedagogy.Controllers.Widgets;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.ApproveFacilitiesWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.APPROVE_FACILITIES_WIDGET_IDENTIFIER,
    typeof(ApproveFacilitiesWidgetController),
    "Approve Facilities Widget",
    Description = "A widget to approve facilities and corporations that have signed up through the registration form.",
    IconClass = "icon-check-circle")]

namespace Pedagogy.Controllers.Widgets
{
    public class ApproveFacilitiesWidgetController : WidgetController<ApproveFacilitiesWidgetProperties>
    {
        private readonly IDataDependencies Dependencies;
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public ApproveFacilitiesWidgetController(IClassMemberDependencies classMemberDependencies, IDataDependencies dependencies) 
        { 
            ClassMemberDependencies = classMemberDependencies;
            Dependencies = dependencies;
        }

        public ActionResult Index()
        {
            var isSiteAdmin = UserInfoProvider.IsUserInRole(User.Identity.Name, ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            if (!isSiteAdmin) return Redirect(ContentIdentifiers.NOT_FOUND);

            var approveFacilityErrMsg = (string)TempData["ApproveFacilityErr"];

            var props = GetProperties();

            var corps = ClassMemberDependencies.FacilityRepo.GetAllCorporations(x => new FacilityDto { FacilityID = x.FacilityID, FacilityName = x.Name });

            return PartialView(ComponentIdentifiers.APPROVE_FACILITIES_WIDGET_VIEW, ApproveFacilitiesWidgetViewModel.GetViewModel(props.FacilitiesPerPage, 
                new SelectList(corps, nameof(FacilityDto.FacilityID), nameof(FacilityDto.FacilityName)), props.CardHeading, approveFacilityErrMsg));
        }

        [HttpGet]
        public JsonResult GetPendingPagedData(int pageNumber = 1, int pageSize = 6)
        {
            if (pageSize == 0) pageSize = 6;

            var list = Dependencies.FacilityWaitListRepo.GetFacilitiesByStatus(ContentIdentifiers.PENDING_STATUS).ToList();

            var pagedData = PagingHelper.PagedResult(list, pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetRejectedPagedData(int pageNumber = 1, int pageSize = 6)
        {
            if (pageSize == 0) pageSize = 6;

            var list = Dependencies.FacilityWaitListRepo.GetFacilitiesByStatus(ContentIdentifiers.REJECTED_STATUS).ToList();

            var pagedData = PagingHelper.PagedResult(list, pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ApproveFacility(int itemID, int corpID = 0)
        {
            var item = CustomTableItemProvider.GetItems<FacilityWaitListItem>()
                .WhereEquals(nameof(FacilityWaitListItem.ItemID), itemID).FirstOrDefault();

            var user = UserInfoProvider.GetUserInfo(item.AdminUserID);

            if (user == null)
            {
                var errMsg = $"Unable to approve {item.FacilityName}. This user no longer exists in Kentico.";
                return Json(new { ErrMsg = errMsg, Err = true, ReturnUrl = "" }, JsonRequestBehavior.AllowGet);
            }

            user.Enabled = true;
            user.Update();

            UserInfoProvider.AddUserToRole(user.UserName, item.IsFacility ? ContentIdentifiers.FACILITY_ADMIN_ROLE : ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);
            UserInfoProvider.AddUserToRole(user.UserName, ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);

            var corpAdminType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);
            var facilityAdminType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.FACILITY_ADMIN_CODE_NAME);
            var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);

            // create a new admin contact
            var adminContactData = new ContactDto
            {
                ContactUserID = item.AdminUserID,
                ContactFirstName = item.AdminFirstName,
                ContactLastName = item.AdminLastName,
                ContactTypeID = item.IsFacility ? facilityAdminType : corpAdminType,
                ContactEmail = item.AdminEmail
            };

            var adminContact = ClassMemberDependencies.ContactRepo.CreateContact(adminContactData);

            // create a new student contact
            var studentContactData = new ContactDto
            {
                ContactUserID = item.AdminUserID,
                ContactFirstName = item.AdminFirstName,
                ContactLastName = item.AdminLastName,
                ContactTypeID = studentType,
                ContactEmail = item.AdminEmail
            };

            var studentContact = ClassMemberDependencies.ContactRepo.CreateContact(studentContactData);

            // assing the how to course to the student contact
            ClassMemberDependencies.ContactRepo.AddHowToCourseToContact(studentContact.ContactID);

            // create a new facility
            var facility = new FacilityDto();

            if (item.IsFacility)
            {
                var facilityData = new FacilityDto
                {
                    FacilityName = item.FacilityName,
                    AccountingCode = item.FacilityAcctingCode,
                    FacilityCity = item.FacilityCity,
                    FacilityState = item.FacilityState,
                    Enabled = true,
                    FacilityIsCorporation = false,
                    FacilityAddress1 = item.FacilityAddress1,
                    FacilityAddress2 = item.FacilityAddress2,
                    FacilityZip = item.FacilityZip,
                    CreatedOn = DateTime.Now,
                    FacilityParentID = corpID
                };

                facility = ClassMemberDependencies.FacilityRepo.CreateFacility(facilityData);
            }
            // create a new corporation
            else
            {
                var corpData = new FacilityDto
                {
                    FacilityName = item.FacilityName,
                    AccountingCode = item.FacilityAcctingCode,
                    FacilityCity = item.FacilityCity,
                    FacilityState = item.FacilityState,
                    Enabled = true,
                    FacilityIsCorporation = true,
                    FacilityAddress1 = item.FacilityAddress1,
                    FacilityAddress2 = item.FacilityAddress2,
                    FacilityZip = item.FacilityZip,
                    CreatedOn = DateTime.Now
                };

                facility = ClassMemberDependencies.FacilityRepo.CreateCorporation(corpData);
            }

            // create contact facilities for both contacts
            ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(adminContact.ContactID, facility.FacilityID, true);
            ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(studentContact.ContactID, facility.FacilityID, true);

            // create email template data
            Dictionary<string, object> data = new Dictionary<string, object>
            {
                { "FacilityWaitListItem", item },
                { "FacilityNumber", facility.FacilityCode }
            };

            // send the email to the admin
            EmailHelper.SendEmail(item.AdminEmail, ContentIdentifiers.FACILITY_APPROVAL_ET_IDENTIFIER, data);

            // remove the item from the custom table
            CustomTableItemProvider.DeleteItem(item);

            return Json(new { ErrMsg = "", Err = false, ReturnUrl = ContentIdentifiers.APPROVE_FACILITIES }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveFacility(int itemID, string rejectionMsg = "")
        {
            var item = CustomTableItemProvider.GetItems<FacilityWaitListItem>()
                .WhereEquals(nameof(FacilityWaitListItem.ItemID), itemID).FirstOrDefault();

            item.RejectionMessage = rejectionMsg;
            item.ApprovalStatus = ContentIdentifiers.REJECTED_STATUS;
            item.Update();

            Dictionary<string, object> data = new Dictionary<string, object>
            {
                { "FacilityWaitListItem", item }
            };

            EmailHelper.SendEmail(item.AdminEmail, ContentIdentifiers.FACILITY_REJECTION_ET_IDENTIFIER, data);

            return Redirect(ContentIdentifiers.APPROVE_FACILITIES);
        }

        [HttpPost]
        public ActionResult RemoveFacilityPermanently(int itemID)
        {
            var item = CustomTableItemProvider.GetItem<FacilityWaitListItem>(itemID);

            CustomTableItemProvider.DeleteItem(item);

            return Redirect(ContentIdentifiers.APPROVE_FACILITIES);
        }
    }
}