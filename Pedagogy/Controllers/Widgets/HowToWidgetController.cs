﻿using CMS.Membership;
using CMS.SiteProvider;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Dto.HowToFiles;
using Pedagogy.Models.Widgets.HowToWidget;

using System.Collections.Generic;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.HOW_TO_WIDGET_IDENTIFIER,
    typeof(HowToWidgetController),
    "How To Widget",
    Description = "A widget used to display all the help/how to files based on a path",
    IconClass = "icon-modal-question")]

namespace Pedagogy.Controllers.Widgets
{
    public class HowToWidgetController : WidgetController<HowToWidgetProperties>
    {
        private readonly IDataDependencies Dependencies;

        public HowToWidgetController(IDataDependencies dependencies) { Dependencies = dependencies; }

        public ActionResult Index()
        {
            var usr = UserInfoProvider.GetUserInfo(User.Identity.Name);

            IEnumerable<HowToItemDto> howToItems;
            IEnumerable<string> categories;

            if (usr.IsInRole(ContentIdentifiers.FACILITY_ADMIN_ROLE, SiteContext.CurrentSiteName) || usr.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName) || usr.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName))
            {
                howToItems = Dependencies.HowToRepo.GetAllHowTos();

                var headings = new List<string>();
                foreach (var item in howToItems)
                {
                    if (!headings.Contains(item.ParentNodeName)) headings.Add(item.ParentNodeName);
                }
                categories = headings;
            }
            else
            {
                howToItems = Dependencies.HowToRepo.GetAllStudentHowTos();

                var headings = new List<string>();
                foreach (var item in howToItems)
                {
                    if (!headings.Contains(item.ParentNodeName)) headings.Add(item.ParentNodeName);
                }
                categories = headings;
            }

            return PartialView(ComponentIdentifiers.HOW_TO_WIDGET_VIEW, HowToWidgetViewModel.GetViewModel(howToItems, categories, GetProperties().CardHeading));
        }
    }
}