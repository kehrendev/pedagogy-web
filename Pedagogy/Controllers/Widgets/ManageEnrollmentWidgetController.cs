﻿using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassBuilder;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.ManageEnrollmentWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly:RegisterWidget(
    ComponentIdentifiers.MANAGE_ENROLLMENT_WIDGET_IDENTIFIER,
    typeof(ManageEnrollmentWidgetController),
    "Manage Enrollment Widget",
    Description = "A widget to manage a facilities classes enrollment",
    IconClass = "icon-clipboard-checklist")]

namespace Pedagogy.Controllers.Widgets
{
    public class ManageEnrollmentWidgetController : WidgetController<ManageEnrollmentWidgetProperties>
    {
        public ActionResult Index(int facilityID = 0, int classID = 0, string invoiceNum = "")
        {
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var startWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.START_CLASS_WF_CODE_NAME);

            var s = new List<FacilityClassViewModel>();
            var cf = ClassMembersHelper.GetContactFacilities(facilityID, x => x);
            var facility = ClassMembersHelper.GetFacility(facilityID, x => new Facility { FacilityCode = x.FacilityCode });

            FacilityClass facilityClass;

            if (!string.IsNullOrEmpty(invoiceNum))
            {
                facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNum);
            }
            else
            {
                facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID);
            }

            foreach (var item in cf)
            {
                var contact = ClassMembersHelper.GetContact(item.ContactID, studentType);
                if (contact != null)
                {
                    var fc = new FacilityClassViewModel()
                    {
                        FacilityID = item.FacilityID,
                        ClassID = classID,
                        ClassName = ClassBuilderHelper.GetClass(classID, x => new TU_Class { ClassName = x.ClassName }).ClassName,
                        Status = "Not Assigned",
                        Assigned = false,
                        IsLocked = false,
                        InvoiceNumber = invoiceNum,
                        StudentID = contact.ContactID,
                        StudentName = $"{contact.ContactFirstName} {contact.ContactLastName}",
                        ExpDate = ""
                    };

                    var studentClass = ClassMembersHelper.GetStudentClass(item.ContactID, classID, facility.FacilityCode);
                    if(studentClass != null)
                    {
                        fc.Assigned = true;
                        fc.Status = ClassMembersHelper.GetWorkflow(studentClass.ClassWorkflowID, x => new ClassWorkflow { WorkflowStep = x.WorkflowStep }).WorkflowStep;
                        fc.IsLocked = studentClass.ClassWorkflowID != startWF;
                    }

                    if(facilityClass != null && facilityClass.FacilityMembershipID != null)
                    {
                        var expDate = ClassMembersHelper.GetFacilityMembership(facilityID, facilityClass.FacilityMembershipID.Value, 
                            x => new FacilityMembership { ExpirationDate = x.ExpirationDate }).ExpirationDate;

                        if(expDate != null) fc.ExpDate = expDate.ToString("MM/dd/yyyy h:m:s tt");
                    }

                    s.Add(fc);
                }
            }

            var className = classID != 0 ? ClassBuilderHelper.GetClass(classID, x => new TU_Class { ClassName = x.ClassName }).ClassName : "";
            var limitReached = facilityClass != null && facilityClass.Quantity <= facilityClass.QuantityUsed;
            var amountUnused = facilityClass != null ? (facilityClass.Quantity - (facilityClass.QuantityUsed ?? 0)) : 0;

            return PartialView(ComponentIdentifiers.MANAGE_ENROLLMENT_WIDGET_VIEW, 
                ManageEnrollmentWidgetViewModel.GetViewModel(facilityID, className, limitReached, s, amountUnused, GetProperties().CardHeading));
        }


        [HttpPost]
        public ActionResult AssignClass(int classID, int studentID, int facilityID, string returnUrl, string invoiceNumber, string expDate = "", int facilityMemID = 0)
        {
            var facility = ClassMembersHelper.GetFacility(facilityID, x => x);

            var studentClass = ClassMembersHelper.CreateNewStudentClass(studentID, classID, facility.FacilityCode, invoiceNumber);

            if (!string.IsNullOrEmpty(expDate))
            {
                studentClass.ExpiresOn = DateTime.Parse(expDate);
            }

            FacilityClass facilityClass;
            if (facilityMemID != 0)
            {
                if (!string.IsNullOrEmpty(invoiceNumber)) facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber, facilityMemID);
                else facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, facilityMemID);

                studentClass.FacilityMembershipID = facilityMemID;
            }
            else
            {
                if (!string.IsNullOrEmpty(invoiceNumber)) facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber);
                else facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID);
            }

            ClassMembersHelper.SaveChanges();

            if (facilityClass.Quantity > facilityClass.QuantityUsed)
            {
                facilityClass.QuantityUsed += 1;
                ClassMembersHelper.SaveChanges();
            }
            if (facilityClass.QuantityUsed == null)
            {
                facilityClass.QuantityUsed = 1;
                ClassMembersHelper.SaveChanges();
            }

            var assignClassLogType = ClassMembersHelper.GetEventLogTypeID(ContentIdentifiers.COURSE_ASSIGNED_ET_CODENAME);
            var student = ClassMembersHelper.GetContact(studentID);


            var classData = ClassBuilderHelper.GetClass(classID, x => new TU_Class { ClassName = x.ClassName });
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            ClassMembersHelper.CreateNewEvent(currentUser, student, assignClassLogType, classData.ClassName);

            return Redirect(returnUrl);
        }

        [HttpPost]
        public ActionResult UnassignClass(int classID, int studentID, int facilityID, string returnUrl, string invoiceNumber, int facilityMemID = 0)
        {
            var paymentSource = ClassMembersHelper.GetFacility(facilityID, x => new Facility { FacilityCode = x.FacilityCode }).FacilityCode;

            StudentClass studentClass;
            if (!string.IsNullOrEmpty(invoiceNumber)) studentClass = ClassMembersHelper.GetStudentClass(studentID, classID, paymentSource, invoiceNumber);
            else studentClass = ClassMembersHelper.GetStudentClass(studentID, classID, paymentSource);

            if (studentClass != null) ClassMembersHelper.RemoveStudentClass(studentClass);

            FacilityClass facilityClass;
            if(!string.IsNullOrEmpty(invoiceNumber))
            {
                if (facilityMemID != 0) facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber, facilityMemID);
                else facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber);
            }
            else
            {
                if (facilityMemID != 0) facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, facilityMemID);
                else facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID);
            }

            ClassMembersHelper.UpdateFacilityClassQty(facilityClass.QuantityUsed);

            var unassignClassLogType = ClassMembersHelper.GetEventLogTypeID(ContentIdentifiers.COURSE_UNASSIGNED_ET_CODENAME);
            var student = ClassMembersHelper.GetContact(studentID);
            var classData = ClassBuilderHelper.GetClass(classID, x => new TU_Class { ClassName = x.ClassName });
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            ClassMembersHelper.CreateNewEvent(currentUser, student, unassignClassLogType, classData.ClassName);

            return Redirect(returnUrl);
        }
    }
}