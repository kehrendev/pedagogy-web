﻿using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using CMS.SiteProvider;
using Kentico.Membership;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.ManageStaffWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.MANAGE_STAFF_WIDGET_IDENTIFIER,
    typeof(ManageStaffWidgetController),
    "Manage Staff Widget",
    Description = "A widget used to display all the data that an admin can manage for staff",
    IconClass = "icon-personalisation-variants")]

namespace Pedagogy.Controllers.Widgets
{
    public class ManageStaffWidgetController : WidgetController<ManageStaffWidgetProperties>
    {
        public UserManager UserManager => HttpContext.GetOwinContext().Get<UserManager>();
        
        public ActionResult Index(int facilityID = 0)
        {
            var cardHeading = GetProperties().CardHeading;

            if (HttpContext.Kentico().PageBuilder().EditMode)
            {
                return PartialView(ComponentIdentifiers.MANAGE_STAFF_WIDGET_VIEW, ManageStaffWidgetViewModel.GetViewModel(cardHeading));
            }

            if (facilityID == 0)
            {
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }

            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var f = ClassMembersHelper.GetFacility(facilityID, x => x);
            var contactFacilities = ClassMembersHelper.GetContactFacilities(facilityID, x => new ContactFacility { ContactID = x.ContactID });
            var facility = new FacilityViewModel(f);
            var students = new List<ContactViewModel>();

            foreach (var cf in contactFacilities)
            {
                var contact = ClassMembersHelper.GetContact(cf.ContactID, studentType);
                if (contact != null)
                {
                    UserInfo user = null;
                    if(contact.ContactUserID.HasValue) user = UserInfoProvider.GetUserInfo(contact.ContactUserID.Value);

                    students.Add(new ContactViewModel(contact)
                    {
                        IsVerified = user != null && user.Enabled,
                        ReturnUrl = GetPage<TreeNode>().NodeAliasPath + $"?facilityID={facilityID}",
                        FacilityName = f.Name,
                        FacilityID = f.FacilityID
                    });
                }
            }

            facility.Students = students;
            var returnUrl = GetPage<TreeNode>().NodeAliasPath + $"?facilityID={facilityID}";

            return PartialView(ComponentIdentifiers.MANAGE_STAFF_WIDGET_VIEW, ManageStaffWidgetViewModel.GetViewModel(cardHeading, facility, returnUrl));
        }

        [HttpPost]
        public ActionResult FilterStudents(ManageStaffWidgetViewModel model)
        {
            var contactFacilities = ClassMembersHelper.GetContactFacilities(model.FacilityData.FacilityID, x => x);
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var filteredStudents = new List<ContactViewModel>();

            foreach (var item in contactFacilities)
            {
                var student = ClassMembersHelper.GetContact(item.ContactID, studentType);

                if (student != null)
                {
                    UserInfo user = null;
                    if (student.ContactUserID.HasValue) user = UserInfoProvider.GetUserInfo(student.ContactUserID.Value);
                    var facility = ClassMembersHelper.GetFacility(item.FacilityID, x => new Facility { Name = x.Name });

                    var s = new ContactViewModel(student)
                    {
                        IsVerified = user != null && user.Enabled,
                        FacilityName = facility != null ? facility.Name : "",
                        ReturnUrl = model.ReturnUrl
                    };

                    if (string.IsNullOrEmpty(model.ContactModel.FirstName) && string.IsNullOrEmpty(model.ContactModel.LastName))
                    {
                        if (!filteredStudents.Contains(s))
                        {
                            filteredStudents.Add(s);
                        }
                    }
                    else if (!string.IsNullOrEmpty(model.ContactModel.FirstName) && !string.IsNullOrEmpty(model.ContactModel.LastName))
                    {
                        if (model.ContactModel.FirstName.ToLower() == student.ContactFirstName.ToLower() && model.ContactModel.LastName.ToLower() == student.ContactLastName.ToLower())
                        {
                            filteredStudents.Add(s);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.ContactModel.FirstName))
                        {
                            if (model.ContactModel.FirstName.ToLower() == student.ContactFirstName.ToLower())
                            {
                                if (!filteredStudents.Contains(s))
                                {
                                    filteredStudents.Add(s);
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(model.ContactModel.LastName))
                        {
                            if (model.ContactModel.LastName.ToLower() == student.ContactLastName.ToLower())
                            {
                                if (!filteredStudents.Contains(s))
                                {
                                    filteredStudents.Add(s);
                                }
                            }
                        }
                    }
                }
            }

            return PartialView(ComponentIdentifiers.STUDENT_TABLE_PARTIAL_VIEW, filteredStudents);
        }

        [HttpPost]
        public async Task<ActionResult> CreateNewStudent(ManageStaffWidgetViewModel model)
        {
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var today = DateTime.Now;

            string err = "";
            foreach(var i in model.Contacts)
            {
                if (!string.IsNullOrEmpty(i.Email))
                {
                    var usr = UserInfoProvider.GetUserInfo(i.Email);

                    if(usr != null)
                    {
                        err += "Username " + i.Email + " is already taken. ";
                    }
                }
                else if (string.IsNullOrEmpty(i.Email) && string.IsNullOrEmpty(model.ContactModel.UserPassEmailRecipient))
                {
                    err += ResHelper.GetString("Dashboard.ManageStaffWidget.AdminEmailValidationMsg", LocalizationContext.CurrentCulture.CultureCode);

                    return Json(err, JsonRequestBehavior.AllowGet);
                }
            }

            if (!string.IsNullOrEmpty(err))
            {
                return Json(err, JsonRequestBehavior.AllowGet);
            }

            var staffList = new List<object>();
            foreach (var item in model.Contacts)
            {
                var kenticoUser = new User
                {
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Enabled = true
                };

                if (!string.IsNullOrEmpty(item.Email))
                {
                    kenticoUser.UserName = item.Email;
                }
                else
                {
                    kenticoUser.UserName = $"{item.FirstName}.{item.LastName}";

                    var userNameTest = UserInfoProvider.GetUserInfo(kenticoUser.UserName);
                    if (userNameTest != null)
                    {
                        for (var x = 1; x > 0; x++)
                        {
                            kenticoUser.UserName += x;
                            if (UserInfoProvider.GetUserInfo(kenticoUser.UserName) == null) break;
                            else kenticoUser.UserName = kenticoUser.UserName.Replace(x.ToString(), "");
                        }
                    }
                }

                var defaultPass = UserInfoProvider.GenerateNewPassword(SiteContext.CurrentSiteName);

                IdentityResult registerResult = IdentityResult.Failed();
                try
                {
                    registerResult = await UserManager.CreateAsync(kenticoUser, defaultPass);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("LoginManagerController", "UserRegistration", ex);
                    ModelState.AddModelError(String.Empty, "Unable to create this user");
                }

                if (!registerResult.Succeeded)
                {
                    var errors = "";
                    foreach (string error in registerResult.Errors)
                    {
                        errors += error + " ";
                    }

                    return Json(errors, JsonRequestBehavior.AllowGet);
                }

                var userInfo = UserInfoProvider.GetUserInfo(kenticoUser.Id);
                var s = UserSettingsInfoProvider.GetUserSettingsInfo(userInfo.UserSettings.UserSettingsID);

                s.UserCustomData.SetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN, true);
                s.Update();

                UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName);

                if (!string.IsNullOrEmpty(item.Email))
                {
                    var d = new Dictionary<string, object>
                    {
                        { "UserName", kenticoUser.UserName },
                        { "Password", defaultPass }
                    };

                    EmailHelper.SendEmail(item.Email, ContentIdentifiers.NEW_FACILITY_STAFF_ET_IDENTIFIER, d);
                }
                else
                {
                    staffList.Add(new { kenticoUser.UserName, DefaultPassword = defaultPass });
                }

                var c = ClassMembersHelper.GetContact(userInfo.UserID, studentType, x => x);
                if (c == null)
                {
                    item.TypeID = studentType;
                    var contact = ClassMembersHelper.CreateNewContact(item, userInfo.UserID);

                    ClassMembersHelper.AddHowToCourseToContactCourses(contact.ContactID);
                    ClassMembersHelper.CreateContactFacility(contact.ContactID, model.FacilityData.FacilityID, model.FacilityData.Enabled);

                    var studentCreatedLogType = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.STUDENT_CREATED_ET_CODENAME);

                    ClassMembersHelper.CreateNewEvent(currentUser, contact, studentCreatedLogType);
                }
                else
                {
                    var cf = ClassMembersHelper.GetContactFacility(c.ContactID, model.FacilityData.FacilityID);
                    if (cf == null)
                    {
                        ClassMembersHelper.CreateContactFacility(c.ContactID, model.FacilityData.FacilityID, model.FacilityData.Enabled);
                    }
                }
            }

            var facilityName = ClassMembersHelper.GetFacility(model.FacilityData.FacilityID, x => new Facility { Name = x.Name }).Name;

            var data = new Dictionary<string, object>
            {
                { "FacilityName", facilityName },
                { "StaffList", staffList }
            };

            EmailHelper.SendEmail(model.ContactModel.UserPassEmailRecipient, ContentIdentifiers.NEW_FACILITY_STAFF_LISTING_ET_IDENTIFIER, data);

            return Json(model.ReturnUrl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddExistingStudent(ManageStaffWidgetViewModel model)
        {
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var today = DateTime.Now;
            var contactFacility = new ContactFacility { };
            var user = UserInfoProvider.GetUserInfo(model.ContactModel.UserName);

            if (user != null)
            {
                var contact = ClassMembersHelper.GetContact(user.UserID, studentType, x => new Contact { ContactID = x.ContactID, ContactEmail = x.ContactEmail });

                if (contact == null)
                {
                    model.ContactModel.TypeID = studentType;
                    var c = ClassMembersHelper.CreateNewContact(model.ContactModel, user.UserID);

                    ClassMembersHelper.CreateContactFacility(model.ContactModel.ContactID, model.FacilityData.FacilityID, model.FacilityData.Enabled);
                }
                else
                {
                    var cf = ClassMembersHelper.GetContactFacility(contact.ContactID, model.FacilityData.FacilityID);

                    if (cf == null) ClassMembersHelper.CreateContactFacility(contact.ContactID, model.FacilityData.FacilityID, model.FacilityData.Enabled);
                }

                var data = new Dictionary<string, object>
                {
                    { "FacilityName", model.FacilityData.Name }
                };

                EmailHelper.SendEmail(contact.ContactEmail, ContentIdentifiers.EXISTING_USER_ADDED_TO_FACILITY_ET_IDENTIFER, data);
            }
            else
            {
                return Json($"Unable to find a user with the username of {model.ContactModel.UserName}", JsonRequestBehavior.AllowGet);
            }

            return Json(model.ReturnUrl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveStudent(string returnUrl, int studentID, int facilityID)
        {
            var startWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            var facility = ClassMembersHelper.GetFacility(facilityID, x => x);

            var studentClasses = ClassMembersHelper.GetStudentClasses(studentID, facility.FacilityCode, x => x.ClassWorkflowID == startWF, x => x);

            if(studentClasses.Count() > 0)
            {
                foreach (var item in studentClasses)
                {
                    var facilityClasses = ClassMembersHelper.GetFacilityClass(facilityID, item.ClassID);

                    if(facilityClasses != null)
                    {
                        if(facilityClasses.QuantityUsed.HasValue && facilityClasses.QuantityUsed.Value != 0)
                        {
                            facilityClasses.QuantityUsed -= 1;
                        }
                    }

                    ClassMembersHelper.RemoveStudentClass(item.StudentClassID, false);
                }
                ClassMembersHelper.SaveChanges();
            }

            var student = ClassMembersHelper.GetContactFacility(studentID, facilityID);
            ClassMembersHelper.RemoveContactFacility(student);

            var studentRemovedLogType = ClassMembersHelper.GetEventLogTypeID(ContentIdentifiers.STUDENT_REMOVED_FROM_FACILITY_ET_CODENAME);
            var contact = ClassMembersHelper.GetContact(studentID);
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            ClassMembersHelper.CreateNewEvent(currentUser, contact, studentRemovedLogType);

            return Redirect(returnUrl);
        }                        
    }
}