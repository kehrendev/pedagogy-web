﻿using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Models.Widgets.AccountDetailsWidget;

using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.ACCOUNT_DETAILS_WIDGET_IDENTIFIER,
    typeof(AccountDetailsWidgetController),
    "Account Details Widget",
    Description = "A widget used to display all the Account Details pages",
    IconClass = "icon-custom-element")]

namespace Pedagogy.Controllers.Widgets
{
    public class AccountDetailsWidgetController : WidgetController<AccountDetailsWidgetProperties>
    {
        private readonly IDataDependencies Dependencies;

        public AccountDetailsWidgetController(IDataDependencies dependencies) { Dependencies = dependencies; }

        public ActionResult Index()
        {
            return PartialView(ComponentIdentifiers.ACCOUNT_DETAILS_WIDGET_VIEW, AccountDetailsWidgetViewModel.GetViewModel(
                Dependencies.NavigationRepo.GetPagesByCategory(new string[] { ContentIdentifiers.NAV_ACCOUNT_ACTIONS_CATEGORY }), GetProperties().CardHeading));
        }
    }
}