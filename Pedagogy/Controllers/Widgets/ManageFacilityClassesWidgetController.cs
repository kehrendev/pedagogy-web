﻿using CMS.DataEngine;
using CMS.Ecommerce;
using CMS.SiteProvider;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Custom.Generated.ModuleClasses;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassBuilder;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.ManageFacilityClassesWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.MANAGE_FACILITY_CLASSES_WIDGET_IDENTIFIER,
    typeof(ManageFacilityClassesWidgetController),
    "Manage Facility Classes Widget",
    Description = "A widget used to manage facility classes",
    IconClass = "icon-graduate-cap")]

namespace Pedagogy.Controllers.Widgets
{
    public class ManageFacilityClassesWidgetController : WidgetController<ManageFacilityClassesWidgetProperties>
    {
        public ActionResult Index(int facilityID = 0)
        {
            if (HttpContext.Kentico().PageBuilder().EditMode)
            {
                return PartialView(ComponentIdentifiers.MANAGE_FACILITY_CLASSES_WIDGET_VIEW, ManageFacilityClassesWidgetViewModel.GetViewModel(GetProperties().CardHeading));
            }

            if (facilityID == 0)
            {
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }
            else
            {
                var facility = ClassMembersHelper.GetFacility(facilityID, x => new Facility { Name = x.Name, FacilityCode = x.FacilityCode });

                var startWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
                var completedWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.COMPLETED_WF_CODE_NAME);

                var facilityClasses = ClassMembersHelper.GetFacilityClasses(facilityID, false);

                var fc = new List<FacilityClassViewModel>();
                foreach (var item in facilityClasses)
                {
                    int amountNotStarted;
                    int amountInProgress;
                    int amountCompleted;

                    if (!string.IsNullOrEmpty(item.InvoiceNumber))
                    {
                        amountNotStarted = ClassMembersHelper.GetStudentClassCount(item.ClassID, item.InvoiceNumber, facility.FacilityCode, 
                            x => x.ClassWorkflowID == startWF);

                        amountInProgress = ClassMembersHelper.GetStudentClassCount(item.ClassID, item.InvoiceNumber, facility.FacilityCode, 
                            x => x.ClassWorkflowID != startWF && x.ClassWorkflowID != completedWF);

                        amountCompleted = ClassMembersHelper.GetStudentClassCount(item.ClassID, item.InvoiceNumber, facility.FacilityCode, 
                            x => x.ClassWorkflowID == completedWF);
                    }
                    else
                    {
                        amountNotStarted = ClassMembersHelper.GetStudentClassCount(item.ClassID, facility.FacilityCode, x => x.ClassWorkflowID == startWF);

                        amountInProgress = ClassMembersHelper.GetStudentClassCount(item.ClassID, facility.FacilityCode, 
                            x => x.ClassWorkflowID != startWF && x.ClassWorkflowID != completedWF);

                        amountCompleted = ClassMembersHelper.GetStudentClassCount(item.ClassID, facility.FacilityCode, x => x.ClassWorkflowID == completedWF);
                    }

                    var facilityModel = new FacilityClassViewModel
                    {
                        FacilityID = item.FacilityID,
                        ClassID = item.ClassID,
                        ClassName = ClassBuilderHelper.GetClass(item.ClassID, x => new TU_Class { ClassName = x.ClassName }).ClassName,
                        AmountUsed = item.QuantityUsed ?? 0,
                        QuantityPurchased = item.Quantity,
                        AmountNotStarted = amountNotStarted,
                        AmountInProgress = amountInProgress,
                        AmountCompleted = amountCompleted,
                        InvoiceNumber = item.InvoiceNumber,
                        LimitReached = item.Quantity <= item.QuantityUsed,
                        InPackage = ClassMembersHelper.IsClassInPackage(item.InvoiceNumber, item.ClassID)
                    };

                    fc.Add(facilityModel);
                }

                var facilityMems = ClassMembersHelper.GetFacilityMemberships(facilityID);
                var mems = new List<FacilityClassViewModel>();
                foreach (var item in facilityMems)
                {
                    var classes = ClassMembersHelper.GetFacilityClasses(facilityID, item.FacilityMembershipID);
                    foreach (var i in classes)
                    {
                        int amountNotStarted;
                        int amountInProgress;
                        int amountCompleted;

                        if (!string.IsNullOrEmpty(i.InvoiceNumber))
                        {
                            amountNotStarted = ClassMembersHelper.GetStudentClassCount(i.ClassID, i.InvoiceNumber, facility.FacilityCode, 
                                x => x.ClassWorkflowID == startWF);

                            amountInProgress = ClassMembersHelper.GetStudentClassCount(i.ClassID, i.InvoiceNumber, facility.FacilityCode,
                                x => x.ClassWorkflowID != startWF && x.ClassWorkflowID != completedWF);

                            amountCompleted = ClassMembersHelper.GetStudentClassCount(i.ClassID, i.InvoiceNumber, facility.FacilityCode, 
                                x => x.ClassWorkflowID == completedWF);
                        }
                        else
                        {
                            amountNotStarted = ClassMembersHelper.GetStudentClassCount(i.ClassID, facility.FacilityCode, x => x.ClassWorkflowID == startWF);

                            amountInProgress = ClassMembersHelper.GetStudentClassCount(i.ClassID, facility.FacilityCode, 
                                x => x.ClassWorkflowID != startWF && x.ClassWorkflowID != completedWF);

                            amountCompleted = ClassMembersHelper.GetStudentClassCount(i.ClassID, facility.FacilityCode, x => x.ClassWorkflowID == completedWF);
                        }

                        var expDate = ClassMembersHelper.GetFacilityMembership(i.FacilityID, i.FacilityMembershipID.Value,
                            x => new FacilityMembership { ExpirationDate = x.ExpirationDate }).ExpirationDate;

                        var memModel = new FacilityClassViewModel
                        {
                            FacilityID = item.FacilityID,
                            ClassID = i.ClassID,
                            ClassName = ClassBuilderHelper.GetClass(i.ClassID, x => new TU_Class { ClassName = x.ClassName }).ClassName,
                            AmountUsed = i.QuantityUsed ?? 0,
                            QuantityPurchased = i.Quantity,
                            FacilityMembershipID = i.FacilityMembershipID ?? 0,
                            ExpDate = expDate != null ? expDate.ToString("MM/dd/yyyy h:m:s tt") : "",
                            AmountNotStarted = amountNotStarted,
                            AmountInProgress = amountInProgress,
                            AmountCompleted = amountCompleted,
                            InvoiceNumber = i.InvoiceNumber,
                            LimitReached = i.Quantity <= i.QuantityUsed,
                            InPackage = ClassMembersHelper.IsClassInPackage(i.InvoiceNumber, i.ClassID)
                        };

                        mems.Add(memModel);
                    }
                }

                return PartialView(ComponentIdentifiers.MANAGE_FACILITY_CLASSES_WIDGET_VIEW, 
                    ManageFacilityClassesWidgetViewModel.GetViewModel(facility.Name, facilityID, GetProperties().CardHeading, User.Identity.Name, fc, mems));
            }
        }

        [HttpPost]
        public ActionResult EditFacilityClassQty(int classID, int facilityID, int quantity, string invoiceNumber)
        {
            var orderItem = OrderItemInfoProvider.GetOrderItems()
                    .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                    .Where(x => x.OrderItemSKU.SKUNumber == classID.ToString())
                    .FirstOrDefault();

            if(orderItem.OrderItemUnitCount != quantity)
            {
                decimal transactionPercentFee = SettingsKeyInfoProvider.GetDecimalValue("authorizeNetTransactionFeePercent") / 100;
                decimal transactionCentsFee = SettingsKeyInfoProvider.GetDecimalValue("authorizeNetTransactionFeeCents");
                decimal salesCommissionPercent = SettingsKeyInfoProvider.GetDecimalValue("SalesCommissionPercent") / 100;

                var order = OrderInfoProvider.GetOrderInfo(int.Parse(invoiceNumber));

                //it's in a package
                //return to the facility class page
                if ((orderItem.OrderItemBundleGUID != null && orderItem.OrderItemBundleGUID != Guid.Empty) || orderItem == null)
                {
                    var parentBundleItem = OrderItemInfoProvider.GetOrderItems()
                        .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                        .Where(x => x.OrderItemGUID == orderItem.OrderItemBundleGUID)
                        .FirstOrDefault();

                    var packageItems = OrderItemInfoProvider.GetOrderItems()
                        .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                        .Where(x => x.OrderItemBundleGUID == orderItem.OrderItemBundleGUID)
                        .ToList();

                    var printOption = OrderItemInfoProvider.GetOrderItems()
                         .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                         .Where(x => x.OrderItemSKU.IsProductOption)
                         .Where(x => x.OrderItemParentGUID == parentBundleItem.OrderItemGUID)
                         .FirstOrDefault();

                    //remove the whole bundle
                    //update the order total
                    //remove the sales data for each of the items in the bundle
                    if (quantity == 0)
                    {
                        order.OrderTotalPrice -= parentBundleItem.OrderItemUnitPrice * parentBundleItem.OrderItemUnitCount;
                        order.OrderGrandTotal -= parentBundleItem.OrderItemUnitPrice * parentBundleItem.OrderItemUnitCount;
                        order.OrderTotalPriceInMainCurrency -= parentBundleItem.OrderItemUnitPrice * parentBundleItem.OrderItemUnitCount;
                        order.OrderGrandTotalInMainCurrency -= parentBundleItem.OrderItemUnitPrice * parentBundleItem.OrderItemUnitCount;

                        if (printOption != null)
                        {
                            order.OrderTotalPrice -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;
                            order.OrderGrandTotal -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;
                            order.OrderGrandTotalInMainCurrency -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;
                            order.OrderTotalPriceInMainCurrency -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;

                            OrderItemInfoProvider.DeleteOrderItemInfo(printOption);
                        }

                        OrderInfoProvider.SetOrderInfo(order);

                        foreach (var item in packageItems)
                        {
                            ClassMembersHelper.RemoveClassFromSalesTable(item.OrderItemOrderID.ToString(), int.Parse(item.OrderItemSKU.SKUNumber));
                            OrderItemInfoProvider.DeleteOrderItemInfo(item);

                            var courseID = int.Parse(item.OrderItemSKU.SKUNumber);
                            var fc = ClassMembersHelper.GetFacilityClass(facilityID, courseID, invoiceNumber);
                            ClassMembersHelper.RemoveFacilityClass(fc);
                        }
                    }
                    //update all the courses in the bundles quantity
                    //update the order total
                    //update the values in sales table
                    else
                    {
                        order.OrderTotalPrice -= parentBundleItem.OrderItemUnitPrice * quantity;
                        order.OrderGrandTotal -= parentBundleItem.OrderItemUnitPrice * quantity;
                        order.OrderTotalPriceInMainCurrency -= parentBundleItem.OrderItemUnitPrice * quantity;
                        order.OrderGrandTotalInMainCurrency -= parentBundleItem.OrderItemUnitPrice * quantity;

                        if (printOption != null)
                        {
                            order.OrderTotalPrice -= printOption.OrderItemUnitPrice * quantity;
                            order.OrderGrandTotal -= printOption.OrderItemUnitPrice * quantity;
                            order.OrderGrandTotalInMainCurrency -= printOption.OrderItemUnitPrice * quantity;
                            order.OrderTotalPriceInMainCurrency -= printOption.OrderItemUnitPrice * quantity;
                        }

                        OrderInfoProvider.SetOrderInfo(order);

                        foreach (var item in packageItems)
                        {
                            var salesItem = SalesInfoProvider.GetSales()
                                .Where(x => x.SaleInvoiceNumber == int.Parse(invoiceNumber))
                                .Where(x => x.SaleClassID == classID)
                                .FirstOrDefault();

                            if (salesItem != null)
                            {
                                var totalPrice = decimal.Round(item.OrderItemBundleGUID == null || item.OrderItemBundleGUID == Guid.Empty ? item.OrderItemTotalPrice : GetTotalItemPrice(item) * quantity, 2);
                                decimal discount = decimal.Round(totalPrice * new decimal(.1), 2);
                                decimal printPrice = new decimal(0.00);

                                var items = OrderItemInfoProvider.GetOrderItems(item.OrderItemOrderID);
                                var options = items.Where(x => x.OrderItemParentGUID != null).ToList();

                                if (item.OrderItemBundleGUID != null && item.OrderItemBundleGUID != Guid.Empty)
                                {
                                    var price = options.Where(x => x.OrderItemParentGUID == item.OrderItemBundleGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
                                    printPrice = decimal.Round(price / items.Where(x => x.OrderItemBundleGUID == item.OrderItemBundleGUID).Count(), 2);
                                }
                                else
                                {
                                    printPrice = options.Where(x => x.OrderItemParentGUID == item.OrderItemGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
                                }

                                var ecomFee = decimal.Round((totalPrice - discount + printPrice) != 0 ? ((totalPrice - discount + printPrice) * transactionPercentFee) + transactionCentsFee : new decimal(0.00), 2);
                                var net = decimal.Round((totalPrice - discount + printPrice) != 0 ? (totalPrice - discount + printPrice) - ecomFee : 0, 2);

                                salesItem.SaleQuantity = quantity;
                                salesItem.SaleCost = totalPrice;
                                salesItem.SaleSubtotal = decimal.Round(totalPrice - discount + printPrice, 2);
                                salesItem.SaleECommerceFee = ecomFee;
                                salesItem.SaleNetTotal = net;
                                salesItem.SaleCommission = decimal.Round(net * salesCommissionPercent, 2);
                                salesItem.SaleAuthorCommission = decimal.Round((net - (net * salesCommissionPercent)) / 2, 2);
                                salesItem.SaleDiscount = discount;

                                salesItem.Update();
                            }

                            var courseID = int.Parse(item.OrderItemSKU.SKUNumber);
                            var fc = ClassMembersHelper.GetFacilityClass(facilityID, courseID, invoiceNumber);

                            fc.Quantity = quantity;
                            ClassMembersHelper.SaveChanges();
                        }
                    }

                    int status = 0;
                    if (order.OrderGrandTotalInMainCurrency > 0)
                    {
                        status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PARTIAL_REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                    }
                    else
                    {
                        status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                    }

                    order.OrderStatusID = status;
                    OrderInfoProvider.SetOrderInfo(order);

                    return Json(ContentIdentifiers.MANAGE_FACILITY_CLASSES + "?facilityID=" + facilityID, JsonRequestBehavior.AllowGet);
                }
                //just a single course
                //return json qty and classid
                else
                {
                    var salesItem = SalesInfoProvider.GetSales()
                        .Where(x => x.SaleInvoiceNumber == int.Parse(invoiceNumber))
                        .Where(x => x.SaleClassID == classID)
                        .FirstOrDefault();

                    var printOption = OrderItemInfoProvider.GetOrderItems()
                        .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                        .Where(x => x.OrderItemSKU.IsProductOption)
                        .Where(x => x.OrderItemParentGUID == orderItem.OrderItemGUID)
                        .FirstOrDefault();

                    var facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber);

                    //remove the course
                    //remove the sales data
                    if (quantity == 0)
                    {
                        order.OrderTotalPrice -= orderItem.OrderItemUnitPrice * orderItem.OrderItemUnitCount;
                        order.OrderGrandTotal -= orderItem.OrderItemUnitPrice * orderItem.OrderItemUnitCount;
                        order.OrderGrandTotalInMainCurrency -= orderItem.OrderItemUnitPrice * orderItem.OrderItemUnitCount;
                        order.OrderTotalPriceInMainCurrency -= orderItem.OrderItemUnitPrice * orderItem.OrderItemUnitCount;

                        if (printOption != null)
                        {
                            order.OrderTotalPrice -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;
                            order.OrderGrandTotal -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;
                            order.OrderGrandTotalInMainCurrency -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;
                            order.OrderTotalPriceInMainCurrency -= printOption.OrderItemUnitPrice * printOption.OrderItemUnitCount;

                            OrderItemInfoProvider.DeleteOrderItemInfo(printOption);
                        }

                        OrderInfoProvider.SetOrderInfo(order);
                        OrderItemInfoProvider.DeleteOrderItemInfo(orderItem);

                        if (salesItem != null)
                        {
                            SalesInfoProvider.DeleteSalesInfo(salesItem);
                        }

                        ClassMembersHelper.RemoveFacilityClass(facilityClass);
                    }
                    //update the quantity
                    //update the order total
                    //update the values in the sales table
                    else
                    {
                        order.OrderTotalPrice -= orderItem.OrderItemUnitPrice * quantity;
                        order.OrderGrandTotal -= orderItem.OrderItemUnitPrice * quantity;
                        order.OrderGrandTotalInMainCurrency -= orderItem.OrderItemUnitPrice * quantity;
                        order.OrderTotalPriceInMainCurrency -= orderItem.OrderItemUnitPrice * quantity;

                        if (printOption != null)
                        {
                            order.OrderTotalPrice -= printOption.OrderItemUnitPrice * quantity;
                            order.OrderGrandTotal -= printOption.OrderItemUnitPrice * quantity;
                            order.OrderGrandTotalInMainCurrency -= printOption.OrderItemUnitPrice * quantity;
                            order.OrderTotalPriceInMainCurrency -= printOption.OrderItemUnitPrice * quantity;
                        }

                        OrderInfoProvider.SetOrderInfo(order);

                        if (salesItem != null)
                        {
                            var totalPrice = decimal.Round(orderItem.OrderItemBundleGUID == null || orderItem.OrderItemBundleGUID == Guid.Empty ? orderItem.OrderItemTotalPrice : GetTotalItemPrice(orderItem) * quantity, 2);
                            decimal discount = decimal.Round(totalPrice * new decimal(.1), 2);
                            decimal printPrice = new decimal(0.00);

                            var items = OrderItemInfoProvider.GetOrderItems(orderItem.OrderItemOrderID);
                            var options = items.Where(x => x.OrderItemParentGUID != null).ToList();

                            if (orderItem.OrderItemBundleGUID != null && orderItem.OrderItemBundleGUID != Guid.Empty)
                            {
                                var price = options.Where(x => x.OrderItemParentGUID == orderItem.OrderItemBundleGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
                                printPrice = decimal.Round(price / items.Where(x => x.OrderItemBundleGUID == orderItem.OrderItemBundleGUID).Count(), 2);
                            }
                            else
                            {
                                printPrice = options.Where(x => x.OrderItemParentGUID == orderItem.OrderItemGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
                            }

                            var ecomFee = decimal.Round((totalPrice - discount + printPrice) != 0 ? ((totalPrice - discount + printPrice) * transactionPercentFee) + transactionCentsFee : new decimal(0.00), 2);
                            var net = decimal.Round((totalPrice - discount + printPrice) != 0 ? (totalPrice - discount + printPrice) - ecomFee : 0, 2);

                            salesItem.SaleQuantity = quantity;
                            salesItem.SaleCost = totalPrice;
                            salesItem.SaleSubtotal = decimal.Round(totalPrice - discount + printPrice, 2);
                            salesItem.SaleECommerceFee = ecomFee;
                            salesItem.SaleNetTotal = net;
                            salesItem.SaleCommission = decimal.Round(net * salesCommissionPercent, 2);
                            salesItem.SaleAuthorCommission = decimal.Round((net - (net * salesCommissionPercent)) / 2, 2);
                            salesItem.SaleDiscount = discount;

                            salesItem.Update();
                        }

                        facilityClass.Quantity = quantity;
                        ClassMembersHelper.SaveChanges();
                    }

                    int status = 0;
                    if(order.OrderGrandTotalInMainCurrency > 0)
                    {
                        status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PARTIAL_REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                    }
                    else
                    {
                        status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                    }

                    order.OrderStatusID = status;
                    OrderInfoProvider.SetOrderInfo(order);

                    return Json(new { Qty = quantity, ClassID = classID }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Qty = quantity, ClassID = classID }, JsonRequestBehavior.AllowGet);
        }

        private decimal GetTotalItemPrice(OrderItemInfo item)
        {
            if (item.OrderItemBundleGUID != null && item.OrderItemBundleGUID != Guid.Empty)
            {
                return SKUInfoProvider.GetSKUInfo(item.OrderItemSKUID).SKUPrice;
            }

            return item.OrderItemTotalPrice;
        }
    }
}