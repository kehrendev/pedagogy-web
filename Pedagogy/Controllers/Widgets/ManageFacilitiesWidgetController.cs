﻿using CMS.CustomTables;
using CMS.CustomTables.Types.Pedagogy;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Ecommerce;
using CMS.EventLog;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using CMS.MembershipProvider;
using CMS.SiteProvider;
using CMS.Taxonomy;
using Kentico.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using Newtonsoft.Json;

using Pedagogy;
using Pedagogy.ClassBuilder.DependencyInjection;
using Pedagogy.ClassBuilder.Entities.Class;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.ClassWorkflow;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.ContactFacility;
using Pedagogy.ClassMembers.Entities.Facility;
using Pedagogy.ClassMembers.Entities.FacilityClass;
using Pedagogy.ClassMembers.Entities.FacilityMembership;
using Pedagogy.ClassMembers.Entities.FacilityPreference;
using Pedagogy.ClassMembers.Entities.LicenseTypes;
using Pedagogy.ClassMembers.Entities.StudentClass;
using Pedagogy.ClassMembers.Entities.StudentLicense;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Custom.Generated.ModuleClasses;
using Pedagogy.Lib.DataModels;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.LoginManager;
using Pedagogy.Models.Widgets.ManageFacilitiesWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.MANAGE_FACILITIES_WIDGET_IDENTIFIER,
    typeof(ManageFacilitiesWidgetController),
    "Manage Facilities Widget",
    Description = "A widget used to display all the data that an admin can manage for facilities",
    IconClass = "icon-choice-multi-scheme")]

namespace Pedagogy.Controllers.Widgets
{
    public class ManageFacilitiesWidgetController : WidgetController<ManageFacilitiesWidgetProperties>
    {
        #region Dependency Injection

        private readonly IClassMemberDependencies ClassMemberDependencies;
        private readonly IClassBuilderDependencies ClassBuilderDependencies;

        public ManageFacilitiesWidgetController(IClassMemberDependencies classMemberDependencies, IClassBuilderDependencies classBuilderDependencies)
        {
            ClassMemberDependencies = classMemberDependencies;
            ClassBuilderDependencies = classBuilderDependencies;
        }

        public UserManager UserManager => HttpContext.GetOwinContext().Get<UserManager>();

        #endregion

        #region Initial Page Listing Methods

        public ActionResult Index(int corporationID = 0)
        {
            var returnUrl = GetPage<TreeNode>().NodeAliasPath;
            if (corporationID > 0) returnUrl = GetPage<TreeNode>().NodeAliasPath + $"?corporationID={corporationID}";

            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            bool isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            bool isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var parentCorps = ClassMembersHelper.GetCorporationsForCurrentUser(ClassMemberDependencies, isSiteAdmin, user.UserID, returnUrl);
            var facilities = ClassMembersHelper.GetFacilitiesForCurrentUser(ClassMemberDependencies, isSiteAdmin, isCorpAdmin, user.UserID, returnUrl);
            if (parentCorps.Count() == 0)
            {
                var c = new List<FacilityViewModel>();
                foreach (var f in facilities)
                {
                    if (f.FacilityParentID.HasValue)
                    {
                        var corp = ClassMemberDependencies.FacilityRepo.GetFacility(f.FacilityParentID.Value, x => new FacilityDto { FacilityName = x.Name, FacilityID = x.FacilityID });

                        if (corp != null)
                        {
                            c.Add(FacilityViewModel.GetViewModel(corp.FacilityName, corp.FacilityID));
                        }
                    }
                }

                parentCorps = c;
            }

            var licenseTypes = ClassMemberDependencies.LicenseTypeRepo.GetAllLicenseTypes(x => new LicenseTypeDto { LicenseTypeID = x.LicenseTypeID, LicenseDisplayName = x.Name });
            var contactTypes = ClassMemberDependencies.ContactTypeRepo.GetFilteredContactTypes(new List<string> { ContentIdentifiers.CORP_ADMIN_CODE_NAME, ContentIdentifiers.STUDENT_CODE_NAME });


            return PartialView(ComponentIdentifiers.MANAGE_FACILITIES_WIDGET_VIEW,
                ManageFacilitiesWidgetViewModel.GetViewModel(User.Identity.Name, corporationID, returnUrl, GetProperties().CardHeading, parentCorps, facilities, licenseTypes, contactTypes));
        }

        [HttpGet]
        public JsonResult GetPagedData(ManageFacilitiesWidgetViewModel model, int pageNumber = 1, int pageSize = 16)
        {
            string state = "";
            if (model.FacilityData.StateID > 0) state = StateInfoProvider.GetStateInfo(model.FacilityData.StateID).StateCode;

            List<FacilityViewModel> facilities = new List<FacilityViewModel>();
            if (model.ParentFacilityID != 0)
            {
                var currentFacilities = ClassMemberDependencies.FacilityRepo.GetChildFacilities(model.ParentFacilityID,
                    x => new FacilityDto { FacilityName = x.Name, FacilityCity = x.City, FacilityState = x.State, FacilityID = x.FacilityID, FacilityCode = x.FacilityCode });

                currentFacilities.ForEach(x => 
                { 
                    facilities.Add(FacilityViewModel.GetViewModel(
                        model.ReturnUrl, x.FacilityID, x.FacilityName, x.FacilityCity, x.FacilityState, model.IsSiteAdmin, model.ParentFacilityID, x.FacilityCode)); 
                });
            }
            else
            {
                facilities = ClassMembersHelper.GetFacilitiesForCurrentUser(ClassMemberDependencies, model.IsSiteAdmin, model.IsCorporateAdmin, model.CurrentUserID, model.ReturnUrl);
            }

            facilities = ClassMembersHelper.FilterFacilities(model.FacilityData, state, facilities);
            PagedData<FacilityViewModel> pagedData;

            if (!string.IsNullOrEmpty(model.ContactModel.FirstName) || !string.IsNullOrEmpty(model.ContactModel.LastName))
            {
                int? type = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);

                var c = ClassMemberDependencies.ContactRepo.GetContacts(type, 
                    x => new ContactDto { ContactID = x.ContactID, ContactFirstName = x.ContactFirstName, ContactLastName = x.ContactLastName, ContactPhone = x.ContactPhone });

                var contacts = new List<ContactViewModel>();
                c.ForEach(x => contacts.Add(ContactViewModel.GetViewModel(x.ContactID, x.ContactFirstName, x.ContactLastName, x.ContactPhone)));

                var filteredContacts = ClassMemberDependencies.ContactRepo.GetFilteredFacilityStaffMembers(model.ContactModel.FacilityID, model.ContactModel.FirstName, model.ContactModel.LastName);
                var cs = new List<ContactViewModel>();
                filteredContacts.ForEach(x => { cs.Add(new ContactViewModel(x)); });

                pagedData = PagingHelper.PagedResult(
                    ClassMembersHelper.SetUpFacilityViewModel(ClassMemberDependencies, cs, facilities),
                    pageNumber,
                    pageSize);

                return Json(pagedData, JsonRequestBehavior.AllowGet);
            }

            pagedData = PagingHelper.PagedResult(facilities, pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void RemoveFacility(int facilityID)
        {
            var contactFacility = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByFacilityID(facilityID, x => new ContactFacilityDto(x));
            var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var corpAdminType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);

            var facilityCode = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityCode = x.FacilityCode }).FacilityCode;

            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);

            var facilityClasses = ClassMemberDependencies.FacilityClassRepo.GetFacilityClasses(facilityID, x => new FacilityClassDto(x));
            facilityClasses.ForEach(x =>
            {
                var studentClasses = ClassMemberDependencies.StudentClassRepo.GetAllFacilityStudentClasses(facilityCode, x => x.ClassWorkflowID == startWF, x => new StudentClassDto(x));
                studentClasses.ForEach(y => { ClassMemberDependencies.StudentClassRepo.RemoveStudentClass(y.StudentClassID); });

                ClassMemberDependencies.FacilityClassRepo.RemoveFacilityClass(x.FacilityClassID);
            });

            var facilityMems = ClassMemberDependencies.FacilityMembershipRepo.GetAllFacilityMemberships(facilityID, x => new FacilityMembershipDto { FacilityID = x.FacilityID });
            facilityMems.ForEach(x => { ClassMemberDependencies.FacilityMembershipRepo.RemoveFacilityMembership(x.FacilityMembershipID); });

            contactFacility.ForEach(x =>
            {
                var contact = ClassMemberDependencies.ContactRepo.GetContact(x.ContactID, x => new ContactDto { ContactID = x.ContactID });
                if (contact != null)
                {
                    if (contact.ContactTypeID != studentType && contact.ContactTypeID != corpAdminType) ClassMemberDependencies.ContactRepo.RemoveContact(contact.ContactID);
                }

                ClassMemberDependencies.ContactFacilityRepo.RemoveContactFacility(x.ContactFacilityID);
            });

            var f = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityID = x.FacilityID });
            if (f.FacilityPreferenceID.HasValue)
            {
                var prefs = ClassMemberDependencies.FacilityPreferenceRepo.GetFacilityPreference(f.FacilityPreferenceID.Value, x => new FacilityPreferenceDto { FacilityPreferenceID = x.FacilityPreferenceID });
                ClassMemberDependencies.FacilityPreferenceRepo.RemoveFacilityPreference(prefs.FacilityPreferenceID);
            }

            if (f != null) ClassMemberDependencies.FacilityRepo.RemoveFacility(f.FacilityID);
        }

        #endregion

        #region TAB 1 Methods: Facility Info
        public JsonResult GetFacilityData(int facilityID)
        {
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto(x));

            return Json(JsonConvert.SerializeObject(new FacilityViewModel(facility)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateFacilityInfo(FacilityViewModel facilityData)
        {
            var facilityDto = new FacilityDto
            {
                FacilityID = facilityData.FacilityID,
                FacilityName = facilityData.Name,
                FacilityCode = facilityData.FacilityCode,
                FacilityAddress1 = facilityData.AddressLine1,
                FacilityAddress2 = facilityData.AddressLine2,
                FacilityCity = facilityData.City,
                FacilityState = facilityData.StateID != 0 ? StateInfoProvider.GetStateInfo(facilityData.StateID).StateCode : "",
                FacilityZip = facilityData.Zip,
                Enabled = facilityData.Enabled,
                AccountingCode = facilityData.AcctingCode,
                FacilityParentID = facilityData.FacilityParentID,
                FacilityIsCorporation = false,
                FacilityPreferenceID = facilityData.FacilityPreferenceID
            };

            var addedFacility = ClassMemberDependencies.FacilityRepo.CreateFacility(facilityDto);

            if (addedFacility.FacilityPreferenceID.HasValue)
            {
                ClassMemberDependencies.FacilityPreferenceRepo.UpdateFacilityPreference(
                    addedFacility.FacilityPreferenceID.Value, User.Identity.Name, facilityData.EmailMonthlyProgress, facilityData.EmailMonthlyProgress);
            }
            else
            {
                ClassMemberDependencies.FacilityPreferenceRepo.CreateFacilityPreference(
                    addedFacility.FacilityID, User.Identity.Name, facilityData.EmailMonthlyProgress, facilityData.AssignHowToPedagogy);
            }

            return Json(JsonConvert.SerializeObject(new FacilityViewModel(addedFacility)), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region TAB 2 Methods: Facility Preferences

        public JsonResult GetFacilityPreferences(int facilityID)
        {
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityPreferenceID = x.FacilityPreferenceID });

            bool assignHowTo = false;
            bool emailMonthly = false;

            if (facility.FacilityPreferenceID.HasValue)
            {
                var facilityPrefs = ClassMemberDependencies.FacilityPreferenceRepo.GetFacilityPreference(facility.FacilityPreferenceID.Value, 
                    x => new FacilityPreferenceDto { EmailMonthlyProgressReport = x.MonthlyProgressReport, AutoAssignHowToCourse = x.HowToPedagogyDefault });

                assignHowTo = facilityPrefs.AutoAssignHowToCourse;
                emailMonthly = facilityPrefs.EmailMonthlyProgressReport;
            }
            else
            {
                ClassMemberDependencies.FacilityPreferenceRepo.CreateFacilityPreference(facilityID, User.Identity.Name, false, false);
            }

            return Json(JsonConvert.SerializeObject(new { assignHowTo, emailMonthly }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateFacilityPreferences(FacilityViewModel facilityData)
        {
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityData.FacilityID, x => new FacilityDto { FacilityPreferenceID = x.FacilityPreferenceID });

            if(facility != null)
            {
                if (facility.FacilityPreferenceID.HasValue)
                {
                    ClassMemberDependencies.FacilityPreferenceRepo.UpdateFacilityPreference(
                        facility.FacilityPreferenceID.Value, User.Identity.Name, facilityData.EmailMonthlyProgress, facilityData.EmailMonthlyProgress);
                }
                else
                {
                    ClassMemberDependencies.FacilityPreferenceRepo.CreateFacilityPreference(
                        facilityData.FacilityID, User.Identity.Name, facilityData.EmailMonthlyProgress, facilityData.AssignHowToPedagogy);
                }
            }

            return Json(JsonConvert.SerializeObject(new { FacilityID = facilityData.FacilityID }), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region TAB 3 Methods: Facility Contacts

        public JsonResult GetFacilityContacts(int facilityID)
        {
            var facilityContacts = ClassMemberDependencies.ContactRepo.GetFacilityContacts(facilityID).Select(x => new ContactViewModel(x) { TypeName = x.TypeName }).ToList();

            return Json(JsonConvert.SerializeObject(facilityContacts), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetContactData(int contactID)
        {
            var contact = ClassMemberDependencies.ContactRepo.GetContact(contactID, x => new ContactDto(x));

            return Json(JsonConvert.SerializeObject(new ContactViewModel(contact)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateContact(ManageFacilitiesWidgetViewModel model)
        {
            // user needs user acct
            if (model.ContactModel.NeedsUserAcct)
            {
                var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
                var adminType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.FACILITY_ADMIN_CODE_NAME);
                var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

                var kenticoUser = new User
                {
                    FirstName = model.ContactModel.FirstName,
                    LastName = model.ContactModel.LastName,
                    Enabled = true
                };

                if (!string.IsNullOrEmpty(model.ContactModel.Email))
                {
                    kenticoUser.Email = model.ContactModel.Email;
                    kenticoUser.UserName = model.ContactModel.Email;
                }
                else
                {
                    kenticoUser.UserName = $"{model.ContactModel.FirstName.Replace("'", "").Replace("-", "")}.{model.ContactModel.LastName.Replace("'", "").Replace("-", "")}";

                    var userNameTest = UserInfoProvider.GetUserInfo(kenticoUser.UserName);
                    if (userNameTest != null)
                    {
                        for (var x = 1; x > 0; x++)
                        {
                            kenticoUser.UserName += x;
                            if (UserInfoProvider.GetUserInfo(kenticoUser.UserName) == null) break;
                            else kenticoUser.UserName = kenticoUser.UserName.Replace(x.ToString(), "");
                        }
                    }
                }

                var defaultPass = UserInfoProvider.GenerateNewPassword(SiteContext.CurrentSiteName);

                // Attempts to create the user in the Kentico database
                IdentityResult registerResult = IdentityResult.Failed();
                try
                {
                    registerResult = UserManager.Create(kenticoUser, defaultPass);
                }
                catch (Exception ex)
                {
                    // Logs an error into the Kentico event log if the creation of the user fails
                    EventLogProvider.LogException("ManageFacilitiesWidget", "UserRegistration", ex);
                }

                //If the registration was not successful, displays the registration form with an error message
                if (!registerResult.Succeeded)
                {
                    var errors = "";
                    foreach (string error in registerResult.Errors)
                    {
                        errors += error + " ";
                    }

                    return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = errors, Contacts = new List<ContactViewModel>() }), JsonRequestBehavior.AllowGet);
                }

                var userInfo = UserInfoProvider.GetUserInfo(kenticoUser.Id);
                var s = UserSettingsInfoProvider.GetUserSettingsInfo(userInfo.UserSettings.UserSettingsID);
                s.SetValue(nameof(UserSettingsInfo.UserPhone), model.ContactModel.Phone);
                s.Update();

                s.UserCustomData.SetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN, true);
                s.Update();

                if (!string.IsNullOrEmpty(model.ContactModel.Email))
                {
                    userInfo.SetValue(nameof(UserInfo.Email), model.ContactModel.Email);
                    userInfo.Update();

                    Dictionary<string, object> data = new Dictionary<string, object>
                                {
                                    { "DefaultPassword", defaultPass }
                                };

                    EmailHelper.SendEmail(model.ContactModel.Email, ContentIdentifiers.CONTACT_CREATION_ET_INDENTIFIER, data);
                }
                else
                {
                    Dictionary<string, object> data = new Dictionary<string, object>
                                {
                                    { "Contact", model.FacilityData.ContactModel },
                                    { "Username", kenticoUser.UserName },
                                    { "DefaultPassword", defaultPass }
                                };

                    EmailHelper.SendEmail(model.ContactModel.UserPassEmailRecipient, ContentIdentifiers.CONTACT_CREATION_FACILITY_ET_IDENTIFIER, data);
                }

                if (model.ContactModel.TypeID == adminType)
                {
                    UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.FACILITY_ADMIN_ROLE, SiteContext.CurrentSiteName);
                }
                else
                {
                    UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName);
                }

                if (model.ContactModel.AllowOnlinePurchases)
                {
                    UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
                }

                var c = ClassMemberDependencies.ContactRepo.GetContact(model.ContactModel.ContactID, model.ContactModel.TypeID, x => new ContactDto(x));
                if (c == null)
                {
                    var contactDto = new ContactDto
                    {
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID,
                        ContactUserID = userInfo?.UserID
                    };

                    var contact = ClassMemberDependencies.ContactRepo.CreateContact(contactDto);

                    ClassMemberDependencies.ContactRepo.AddHowToCourseToContact(contact.ContactID);
                    ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(contact.ContactID, model.FacilityData.FacilityID, true);

                    if (contact.ContactTypeID == adminType)
                    {
                        var facility = ClassMemberDependencies.FacilityRepo.GetFacility(model.FacilityData.FacilityID, x => new FacilityDto { FacilityName = x.Name });

                        var adminCreatedEventType = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeIDByCodeName(ContentIdentifiers.ADMIN_CREATED_ET_CODENAME);
                        ClassMemberDependencies.EventLogRepo.CreateNewEvent(currentUser, contact, adminCreatedEventType, facility.FacilityName);
                    }
                }
                else
                {
                    var contactDto = new ContactDto
                    {
                        ContactUserID = userInfo?.UserID,
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID,
                    };

                    ClassMemberDependencies.ContactRepo.UpdateContact(c.ContactID, contactDto);

                    var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(c.ContactID, model.FacilityData.FacilityID, x => new ContactFacilityDto(x));

                    if (cf == null) ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(c.ContactID, model.FacilityData.FacilityID, true);
                }
            }
            // user already has an acct or doesn't need an acct
            else
            {
                var user = model.ContactModel.ContactUserID.HasValue ? UserInfoProvider.GetUserInfo(model.ContactModel.ContactUserID.Value) : null;
                if (user == null)
                {
                    user = UserInfoProvider.GetUserInfo(model.ContactModel.Email);
                    if (user == null)
                    {
                        //user = UserInfoProvider.GetUserInfo($"{model.ContactModel.FirstName}.{model.ContactModel.LastName}");
                    }
                }

                if (model.ContactModel.AllowOnlinePurchases)
                {
                    if (user == null)
                    {
                        return Json(JsonConvert.SerializeObject(new
                        {
                            Err = true,
                            ErrMsg = ResHelper.GetString("Dashboard.FacilityPurchaserRoleErrMsg", LocalizationContext.CurrentCulture.CultureCode),
                            Contacts = new List<ContactViewModel>()
                        }), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        UserInfoProvider.AddUserToRole(user.UserName, ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
                    }
                }
                else
                {
                    if (user != null)
                    {
                        if (user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName))
                        {
                            UserManager.RemoveFromRole(user.UserID, ContentIdentifiers.FACILITY_PURCHASER_ROLE);
                        }
                    }
                }

                if (user != null)
                {
                    if (user.UserSettings != null)
                    {
                        var s = UserSettingsInfoProvider.GetUserSettingsInfo(user.UserSettings.UserSettingsID);
                        s.SetValue(nameof(UserSettingsInfo.UserPhone), model.ContactModel.Phone);
                        s.Update();
                    }

                    user.SetValue(nameof(UserInfo.Email), model.ContactModel.Email);
                    user.Update();

                    var facilityAdminType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.FACILITY_ADMIN_CODE_NAME);
                    if(model.ContactModel.TypeID == facilityAdminType)
                    {
                        if (user.IsInRole(ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName)) UserInfoProvider.RemoveUserFromRole(user.UserName, ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName);

                        UserInfoProvider.AddUserToRole(user.UserName, ContentIdentifiers.FACILITY_ADMIN_ROLE, SiteContext.CurrentSiteName);
                    }
                }

                var contact = ClassMemberDependencies.ContactRepo.GetContact(model.ContactModel.ContactID, x => new ContactDto(x));
                if (contact == null)
                {
                    var contactDto = new ContactDto
                    {
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID,
                        ContactUserID = user?.UserID                        
                    };

                    var newContact = ClassMemberDependencies.ContactRepo.CreateContact(contactDto);

                    ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(newContact.ContactID, model.FacilityData.FacilityID, true);
                    ClassMemberDependencies.ContactRepo.AddHowToCourseToContact(newContact.ContactID);
                }
                else
                {
                    var contactDto = new ContactDto
                    {
                        ContactUserID = user?.UserID,
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactEmail = model.ContactModel.Email,
                        ContactPhone = model.ContactModel.Phone,
                        ContactTypeID = model.ContactModel.TypeID
                    };

                    ClassMemberDependencies.ContactRepo.UpdateContact(contact.ContactID, contactDto);

                    var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(contact.ContactID, model.FacilityData.FacilityID, x => new ContactFacilityDto(x));

                    if (cf == null) ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(contact.ContactID, model.FacilityData.FacilityID, true);
                }
            }

            var facilityContacts = ClassMemberDependencies.ContactRepo.GetFacilityContacts(model.FacilityData.FacilityID).Select(x => new ContactViewModel(x) { TypeName = x.TypeName }).ToList();

            return Json(JsonConvert.SerializeObject(
                new { Err = false, ErrMsg = "", Contacts = facilityContacts }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveContact(int facilityID, int contactID)
        {
            var contactFacility = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(contactID, facilityID, x => new ContactFacilityDto { ContactFacilityID = x.ContactFacilityID });
            if (contactFacility != null) ClassMemberDependencies.ContactFacilityRepo.RemoveContactFacility(contactFacility.ContactFacilityID);

            var contact = ClassMemberDependencies.ContactRepo.GetContact(contactID, x => new ContactDto { ContactID = x.ContactID });
            if (contact != null) ClassMemberDependencies.ContactRepo.RemoveContact(contact.ContactID);

            var facilityContacts = ClassMemberDependencies.ContactRepo.GetFacilityContacts(facilityID).Select(x => new ContactViewModel(x) { TypeName = x.TypeName }).ToList();

            return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "", Contacts = facilityContacts }),
                JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region TAB 4 Methods: Facility Staff Members

        public JsonResult GetFacilityStaffMembers(ManageFacilitiesWidgetViewModel model, int pageNumber = 1, int pageSize = 10)
        {
            var students = ClassMemberDependencies.ContactRepo.GetFilteredFacilityStaffMembers(model.FacilityID, model.ContactModel.FirstName, model.ContactModel.LastName);

            var pagedData = PagingHelper.PagedResult(students.Select(x => new ContactViewModel(x)).ToList(), pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentData(int contactID)
        {
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var notApplicableType = ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeIDByCodeName(ContentIdentifiers.LT_NA_CODE_NAME);

            var student = ClassMemberDependencies.ContactRepo.GetContact(contactID, studentType, x => new ContactDto(x));
            var currentLicenses = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(contactID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));

            var licenses = new List<StudentLicenseViewModel>();
            bool notApplicable = false;

            currentLicenses.ForEach(x => 
            { 
                licenses.Add(new StudentLicenseViewModel(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID))); 

                if (x.LicenseTypeID == notApplicableType) notApplicable = true; 
            });

            return Json(JsonConvert.SerializeObject(new ContactViewModel(student)
            {
                NALicense = notApplicable,
                StudentLicenses = licenses
            }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentLicenseData(int licenseID, int studentID)
        {
            var license = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicense(licenseID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));
            var typeDisplayName = ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(license.LicenseTypeID);

            return Json(JsonConvert.SerializeObject(new StudentLicenseViewModel(license, typeDisplayName)), JsonRequestBehavior.AllowGet);
        }

        public void AddNotApplicableLicense(int studentID)
        {
            var notApplicableType = ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeIDByCodeName(ContentIdentifiers.LT_NA_CODE_NAME);

            var currentLicenses = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(studentID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));

            if (currentLicenses.Count() > 0)
            {
                currentLicenses.ForEach(x => { ClassMemberDependencies.StudentLicenseRepo.RemoveStudentLicense(x.StudentLicenseID); });
            }

            ClassMemberDependencies.StudentLicenseRepo.CreateStudentLicense(studentID, notApplicableType, "", "");
        }

        public void RemoveNotApplicableLicense(int studentID)
        {
            var notApplicableType = ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeIDByCodeName(ContentIdentifiers.LT_NA_CODE_NAME);

            var currentLicenses = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(studentID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));

            currentLicenses.ForEach(x => { if (x.LicenseTypeID == notApplicableType) ClassMemberDependencies.StudentLicenseRepo.RemoveStudentLicense(x.StudentLicenseID); });
        }

        [HttpPost]
        public JsonResult AddNewStaffLicense(StudentLicenseViewModel licensingData)
        {
            var state = StateInfoProvider.GetStateInfo(licensingData.StateID).StateCode;

            if (licensingData.LicenseID == 0)
            {
                ClassMemberDependencies.StudentLicenseRepo.CreateStudentLicense(licensingData.StudentID, licensingData.LicenseTypeID, licensingData.LicenseNumber, state);
            }
            else
            {
                ClassMemberDependencies.StudentLicenseRepo.UpdateStudentLicense(licensingData.LicenseID, licensingData.LicenseTypeID, licensingData.LicenseNumber, state);
            }

            var licenses = new List<StudentLicenseViewModel>();
            var sl = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(licensingData.StudentID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));
                
            sl.ForEach(x => { licenses.Add(new StudentLicenseViewModel(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID))); });

            return Json(JsonConvert.SerializeObject(licenses), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void UpdateStudentInformation(ManageFacilitiesWidgetViewModel model)
        {
            var student = ClassMemberDependencies.ContactRepo.GetContact(model.ContactModel.ContactID, x => new ContactDto { ContactID = x.ContactID });

            UserInfo user = null;
            if (student.ContactUserID.HasValue) user = UserInfoProvider.GetUserInfo(student.ContactUserID.Value);

            var contactDto = new ContactDto
            {
                ContactUserID = student.ContactUserID,
                ContactFirstName = model.ContactModel.CertFirstName,
                ContactLastName = model.ContactModel.CertLastName,
                ContactEmail = model.ContactModel.Email,
                ContactPhone = model.ContactModel.Phone,
                ContactTypeID = model.ContactModel.TypeID
            };

            ClassMemberDependencies.ContactRepo.UpdateContact(student.ContactID, contactDto);

            if (user != null)
            {
                user.SetValue(nameof(UserInfo.Email), model.ContactModel.Email);
                user.Update();
            }

            if (model.FacilityData.FacilityID != model.FacilityID)
            {
                var contactFacility = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(model.ContactModel.ContactID, model.FacilityData.FacilityID, x => new ContactFacilityDto(x));
                ClassMemberDependencies.ContactFacilityRepo.RemoveContactFacility(contactFacility.ContactFacilityID);

                ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(model.ContactModel.ContactID, model.FacilityID, true);
            }
        }

        [HttpPost]
        public void UpdateStudentUserData(ContactViewModel contactModel)
        {
            var user = UserInfoProvider.GetUserInfo(contactModel.ContactUserID.Value);

            user.SetValue(nameof(UserInfo.FirstName), contactModel.FirstName);
            user.SetValue(nameof(UserInfo.LastName), contactModel.LastName);
            user.SetValue(nameof(UserInfo.Enabled), contactModel.Enabled);

            user.Update();
        }

        [HttpPost]
        public JsonResult ChangeStudentPassword(RegisterViewModel registerModel, ContactViewModel contactModel)
        {
            var user = UserInfoProvider.GetUserInfo(contactModel.ContactUserID.Value);
            var password = new CMSMembershipProvider().ResetPassword(user.UserName, "");
            var defaultPass = ResHelper.GetString("Dashboard.DefaultStudentPassword", LocalizationContext.CurrentCulture.CultureCode);

            var msgs = new List<string>();
            IdentityResult resetResult;
            if (!registerModel.DefaultPassword)
            {
                resetResult = UserManager.ChangePassword(user.UserID, password, registerModel.Password);

                var data = user.UserSettings.UserCustomData.GetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
                if (data != null)
                {
                    user.UserSettings.UserCustomData.Remove(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
                    user.Update();
                }
            }
            else
            {
                resetResult = UserManager.ChangePassword(user.UserID, password, defaultPass);

                var data = user.UserSettings.UserCustomData.GetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
                if (data == null)
                {
                    user.UserSettings.UserCustomData.SetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN, true);
                    user.Update();
                }
            }

            if (!resetResult.Succeeded)
            {
                foreach (string error in resetResult.Errors)
                {
                    msgs.Add(error);
                }
                return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = msgs }), JsonRequestBehavior.AllowGet);
            }

            return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "" }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddExisitingStudent(ManageFacilitiesWidgetViewModel model)
        {
            var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var user = UserInfoProvider.GetUserInfo(model.ContactModel.UserName);

            if (user != null)
            {
                var contact = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID, ContactEmail = x.ContactEmail });

                if (contact == null)
                {
                    var contactDto = new ContactDto
                    {
                        ContactFirstName = model.ContactModel.FirstName,
                        ContactLastName = model.ContactModel.LastName,
                        ContactPhone = model.ContactModel.Phone,
                        ContactEmail = model.ContactModel.Email,
                        ContactTypeID = studentType,
                        ContactUserID = user?.UserID
                    };

                    contact = ClassMemberDependencies.ContactRepo.CreateContact(contactDto);

                    ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(model.ContactModel.ContactID, model.FacilityData.FacilityID, model.FacilityData.Enabled);
                }
                else
                {
                    var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(contact.ContactID, model.FacilityData.FacilityID, x => new ContactFacilityDto(x));

                    if (cf == null) ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(contact.ContactID, model.FacilityData.FacilityID, model.FacilityData.Enabled);
                }

                var data = new Dictionary<string, object>
                {
                    { "FacilityName", model.FacilityData.Name }
                };

                EmailHelper.SendEmail(contact.ContactEmail, ContentIdentifiers.EXISTING_USER_ADDED_TO_FACILITY_ET_IDENTIFER, data);

                return Json(JsonConvert.SerializeObject(new
                {
                    Err = false,
                    ErrMsg = "",
                    Students = ClassMemberDependencies.ContactRepo.GetFacilityStaffMembers(model.FacilityData.FacilityID).Select(x => new ContactViewModel(x)).ToList()
                }), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(JsonConvert.SerializeObject(new
                {
                    Err = true,
                    ErrMsg = $"Unable to find a user with the username of {model.ContactModel.UserName}",
                    Students = new List<ContactViewModel>()
                }), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddNewStudent(ManageFacilitiesWidgetViewModel model, string emailRecipient)
        {
            var emails = model.ContactModels.Where(x => !string.IsNullOrEmpty(x.Email)).Select(x => x.Email).ToList();

            // validation checks
            foreach (var i in model.ContactModels)
            {
                // if email is empty make sure there is an email recipient
                if (string.IsNullOrEmpty(i.Email) && string.IsNullOrEmpty(emailRecipient))
                {
                    return Json(JsonConvert.SerializeObject(new
                    {
                        Err = true,
                        ErrMsg = ResHelper.GetString("Dashboard.ManageFacilitiesWidget.UsernamePasswordEmailRecipientValidation", LocalizationContext.CurrentCulture.CultureCode),
                        Students = new List<ContactViewModel>()
                    }), JsonRequestBehavior.AllowGet);
                }

                // if the email isn't null, check to make sure the user doesn't already exist
                if (!string.IsNullOrEmpty(i.Email))
                {
                    var user = UserInfoProvider.GetUserInfo(i.Email);
                    if (user != null)
                    {
                        return Json(JsonConvert.SerializeObject(new
                        {
                            Err = true,
                            ErrMsg = i.Email + " " + ResHelper.GetString("Dashboard.MnageFacilitiesWidget.UsernameAlreadyExistsValidation", LocalizationContext.CurrentCulture.CultureCode),
                            Students = new List<ContactViewModel>()
                        }), JsonRequestBehavior.AllowGet);
                    }
                }

                // remove the current email
                emails.Remove(i.Email);

                if(emails.Count() > 0)
                {
                    // check to make sure none of the other emails equal this one
                    foreach (var email in emails)
                    {
                        if (i.Email == email)
                        {
                            return Json(JsonConvert.SerializeObject(new
                            {
                                Err = true,
                                ErrMsg = i.Email + " is already listed in this list.",
                                Students = new List<ContactViewModel>()
                            }), JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                emails.Add(i.Email);
            }

            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var studentCreated = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeIDByCodeName(ContentIdentifiers.STUDENT_CREATED_ET_CODENAME);
            
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var staffList = new List<string>();
            foreach (var item in model.ContactModels)
            {
                var kenticoUser = new User
                {
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Enabled = true
                };

                if (!string.IsNullOrEmpty(item.Email))
                {
                    kenticoUser.Email = item.Email;
                    kenticoUser.UserName = item.Email;
                }
                else
                {
                    kenticoUser.UserName = $"{item.FirstName}.{item.LastName}";

                    var userNameTest = UserInfoProvider.GetUserInfo(kenticoUser.UserName);
                    if (userNameTest != null)
                    {
                        for (var x = 1; x > 0; x++)
                        {
                            kenticoUser.UserName += x;
                            if (UserInfoProvider.GetUserInfo(kenticoUser.UserName) == null) break;
                            else kenticoUser.UserName = kenticoUser.UserName.Replace(x.ToString(), "");
                        }
                    }
                }

                var defaultPass = UserInfoProvider.GenerateNewPassword(SiteContext.CurrentSiteName);

                // Attempts to create the user in the Kentico database
                IdentityResult registerResult = IdentityResult.Failed();
                try
                {
                    registerResult = UserManager.Create(kenticoUser, defaultPass);
                }
                catch (Exception ex)
                {
                    // Logs an error into the Kentico event log if the creation of the user fails
                    EventLogProvider.LogException("ManageFacilitiesWidget", "UserRegistration", ex);
                }

                //If the registration was not successful, displays the registration form with an error message
                if (!registerResult.Succeeded)
                {
                    var errs = "";
                    foreach (string error in registerResult.Errors)
                    {
                        errs += error + " ";
                    }

                    return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = errs }), JsonRequestBehavior.AllowGet);
                }

                var userInfo = UserInfoProvider.GetUserInfo(kenticoUser.Id);
                var s = UserSettingsInfoProvider.GetUserSettingsInfo(userInfo.UserSettings.UserSettingsID);

                s.UserCustomData.SetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN, true);
                s.Update();

                if (!string.IsNullOrEmpty(item.Email))
                {
                    Dictionary<string, object> d = new Dictionary<string, object>
                    {
                        { "UserName", kenticoUser.UserName },
                        { "Password", defaultPass }
                    };

                    EmailHelper.SendEmail(item.Email, ContentIdentifiers.NEW_FACILITY_STAFF_ET_IDENTIFIER, d);
                }
                else
                {
                    staffList.Add($"<strong>Username:</strong> {kenticoUser.UserName}, <strong>Password:</strong> {defaultPass}");
                }

                UserInfoProvider.AddUserToRole(kenticoUser.UserName, ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName);

                var c = ClassMemberDependencies.ContactRepo.GetContact(userInfo?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });
                item.TypeID = studentType;
                if (c == null)
                {
                    var contact = ClassMemberDependencies.ContactRepo.MapUserToStudent(userInfo);

                    ClassMemberDependencies.ContactRepo.AddHowToCourseToContact(contact.ContactID);
                    ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(contact.ContactID, model.FacilityData.FacilityID, true);

                    var facility = ClassMemberDependencies.FacilityRepo.GetFacility(model.FacilityData.FacilityID, x => new FacilityDto { FacilityName = x.Name });
                    ClassMemberDependencies.EventLogRepo.CreateNewEvent(currentUser, contact, studentCreated, facility.FacilityName);
                }
                else
                {
                    var updatedData = new ContactDto
                    {
                        ContactFirstName = item.FirstName,
                        ContactLastName = item.LastName,
                        ContactTypeID = item.TypeID,
                        ContactPhone = item.Phone,
                        ContactEmail = item.Email,
                        ContactUserID = userInfo?.UserID
                    };

                    ClassMemberDependencies.ContactRepo.UpdateContact(c.ContactID, updatedData);

                    var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(c.ContactID, model.FacilityData.FacilityID, x => new ContactFacilityDto(x));
                    if (cf == null) ClassMemberDependencies.ContactFacilityRepo.CreateContactFacility(c.ContactID, model.FacilityData.FacilityID, true);
                }
            }

            if (staffList.Count() > 0)
            {
                var facilityName = ClassMemberDependencies.FacilityRepo.GetFacility(model.FacilityData.FacilityID, x => new FacilityDto { FacilityName = x.Name }).FacilityName;

                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    { "FacilityName", facilityName },
                    { "StaffList", staffList }
                };

                EmailHelper.SendEmail(emailRecipient, ContentIdentifiers.NEW_FACILITY_STAFF_LISTING_ET_IDENTIFIER, data);
            }

            return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "" }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveStudent(int contactID, int facilityID)
        {
            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityName = x.Name, FacilityCode = x.FacilityCode });

            var regularStudentClasses = ClassMemberDependencies.StudentClassRepo.GetRegularFacilityStudentClasses(contactID, facility.FacilityCode, x => x.ClassWorkflowID == startWF, x => new StudentClassDto(x));

            if (regularStudentClasses.Count() > 0)
            {
                regularStudentClasses.ForEach(x =>
                {
                    var facilityClass = ClassMemberDependencies.FacilityClassRepo.GetRegularFacilityClass(facilityID, x.ClassID, "", x => new FacilityClassDto(x));

                    if(facilityClass != null)
                    {
                        if(facilityClass.QuantityUsed.HasValue && facilityClass.QuantityUsed.Value != 0)
                        {
                            ClassMemberDependencies.FacilityClassRepo.UpdateFacilityClassQtyUsed(facilityClass.FacilityClassID, Operation.Decrease, 1);
                        }
                    }

                    ClassMemberDependencies.StudentClassRepo.RemoveStudentClass(x.StudentClassID);
                });
            }

            var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacility(contactID, facilityID, x => new ContactFacilityDto(x));
            if(cf != null) ClassMemberDependencies.ContactFacilityRepo.RemoveContactFacility(cf.ContactFacilityID);

            var studentRemovedLogType = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeIDByCodeName(ContentIdentifiers.STUDENT_REMOVED_FROM_FACILITY_ET_CODENAME);
            var contact = ClassMemberDependencies.ContactRepo.GetContact(contactID, x => new ContactDto(x));
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            ClassMemberDependencies.EventLogRepo.CreateNewEvent(currentUser, contact, studentRemovedLogType, facility.FacilityName);

            return Json(JsonConvert.SerializeObject(ClassMemberDependencies.ContactRepo.GetFacilityStaffMembers(facilityID).Select(x => new ContactViewModel(x)).ToList()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveStudentLicense(int licenseID, int studentID)
        {
            var license = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicense(licenseID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));
            if (license != null) ClassMemberDependencies.StudentLicenseRepo.RemoveStudentLicense(licenseID);

            var licenses = new List<StudentLicenseViewModel>();
            var sl = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(studentID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));

            sl.ForEach(x => { licenses.Add(new StudentLicenseViewModel(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID))); });

            return Json(JsonConvert.SerializeObject(licenses), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetManageStudentClassesData(int contactID, int facilityID)
        {
            var student = ClassMemberDependencies.ContactRepo.GetContact(contactID, x => new ContactDto(x));

            return Json(JsonConvert.SerializeObject(new { FacilityClasses = SetUpManageStudentClasses(facilityID, student), StudentName = student.ContactName, StudentID = contactID }), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region TAB 5 Methods: Print Student Certificates

        public JsonResult GetFacilityStaffCerts(int facilityID)
        {
            var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);

            var facilityCode = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityCode = x.FacilityCode }).FacilityCode;
            var facilityContacts = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByFacilityID(facilityID, x => new ContactFacilityDto { ContactID = x.ContactID });

            var students = new Dictionary<int, string>();
            foreach (var item in facilityContacts)
            {
                var contact = ClassMemberDependencies.ContactRepo.GetContact(item.ContactID, 
                    x => new ContactDto { ContactName = x.ContactFirstName + " " + x.ContactLastName });

                if (contact != null)
                {
                    var studentCertCount = ClassMemberDependencies.StudentClassRepo.GetStudentClassCertificateCount(item.ContactID, facilityCode);

                    if (studentCertCount > 0) students.Add(item.ContactID, contact.ContactName);
                }
            }

            return Json(JsonConvert.SerializeObject(new { Students = students }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendDataToCertHandler(IEnumerable<int> ids)
        {
            return Redirect(ClassMembersHelper.BuildPrintStudentsCertsUrl(ids));
        }

        #endregion

        #region TAB 6 Methods: Facility Memberships

        public JsonResult GetFacilityMemberships(int facilityID)
        {
            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            var completedWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.COMPLETED_WF_CODE_NAME);

            var facilityCode = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityCode = x.FacilityCode }).FacilityCode;
            var facilityMems = ClassMemberDependencies.FacilityMembershipRepo.GetAllFacilityMemberships(facilityID, x => new FacilityMembershipDto(x)).OrderByDescending(x => x.ExpirationDate).ToList();

            var mems = new List<FacilityMembershipViewModel>();
            foreach (var item in facilityMems)
            {
                var classes = ClassMemberDependencies.FacilityClassRepo.GetFacilityMembershipClasses(facilityID, item.FacilityMembershipID, x => new FacilityClassDto(x));
                var memClasses = new List<FacilityClassViewModel>();

                foreach (var i in classes)
                {
                    var amountNotStarted = ClassMemberDependencies.StudentClassRepo.GetStudentClassCount(i.ClassID, facilityCode, x => x.ClassWorkflowID == startWF, i.InvoiceNumber);

                    var amountInProgress = ClassMemberDependencies.StudentClassRepo.GetStudentClassCount(i.ClassID, facilityCode, x => x.ClassWorkflowID != startWF && x.ClassWorkflowID != completedWF, 
                        i.InvoiceNumber);

                    var amountCompleted = ClassMemberDependencies.StudentClassRepo.GetStudentClassCount(i.ClassID, facilityCode, x => x.ClassWorkflowID == completedWF, i.InvoiceNumber);

                    var total = amountCompleted + amountNotStarted + amountInProgress;
                    var qty = i.Quantity;
                    var qtyUsed = i.QuantityUsed ?? 0;

                    //if (total < i.Quantity) qty = total;
                    //if (total < qtyUsed) qtyUsed = total;

                    var memClassModel = new FacilityClassViewModel
                    {
                        FacilityClassID = i.FacilityClassID,
                        ClassID = i.ClassID,
                        ClassName = ClassBuilderDependencies.ClassRepo.GetClass(i.ClassID, x => new ClassDto { ClassName = x.ClassName }).ClassName,
                        AmountUsed = qtyUsed,
                        QuantityPurchased = qty,
                        AmountNotStarted = amountNotStarted,
                        AmountInProgress = amountInProgress,
                        AmountCompleted = amountCompleted,
                        InvoiceNumber = i.InvoiceNumber,
                        LimitReached = qty <= qtyUsed,
                        InPackage = ClassMembersHelper.IsClassInPackage(i.InvoiceNumber, i.ClassID)
                    };

                    memClasses.Add(memClassModel);
                }

                var mem = new FacilityMembershipViewModel
                {
                    MembershipID = item.FacilityMembershipID,
                    MembershipName = item.MembershipName,
                    MembershipExpDate = item.ExpirationDate.ToString("MM/dd/yyyy h:m:s tt"),
                    FacilityID = item.FacilityID,
                    FacilityClasses = memClasses,
                    InvoiceNumber = memClasses.Count() > 0 ? memClasses.First().InvoiceNumber : "",
                    MembershipQty = memClasses.Count() > 0 ? memClasses.First().QuantityPurchased : 0
                };

                mems.Add(mem);
            }

            return Json(JsonConvert.SerializeObject(mems), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateFacilityMembershipQty(int facilityID, int membershipID, string invoiceNumber, int quantity)
        {
            // get the membership classes
            var memClasses = ClassMemberDependencies.FacilityClassRepo.GetFacilityMembershipClasses(facilityID, membershipID, x => new FacilityClassDto(x));

            // initialize return variables
            var removed = false;

            // the quantity has decreased
            if (memClasses.FirstOrDefault().Quantity != quantity)
            {
                // get the order
                var order = OrderInfoProvider.GetOrderInfo(int.Parse(invoiceNumber));

                // check if the quantity is 0
                if (quantity == 0) removed = true;

                foreach (var item in memClasses)
                {
                    // get the order item
                    var orderItem = OrderItemInfoProvider.GetOrderItems()
                        .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                        .Where(x => x.OrderItemSKU.SKUNumber == item.ClassID.ToString())
                        .FirstOrDefault();

                    // update the qty
                    if(orderItem != null)
                    {
                        UpdateOrderItemQty(orderItem, quantity);

                        // check for a print option(there won't be one now but nice to have for later)
                        var printOption = OrderItemInfoProvider.GetOrderItems()
                            .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                            .Where(x => x.OrderItemSKU.IsProductOption)
                            .Where(x => x.OrderItemParentGUID == orderItem.OrderItemGUID)
                            .FirstOrDefault();

                        if (printOption != null)
                        {
                            UpdateOrderItemQty(printOption, quantity);
                        }
                    }
                    
                    // check for a sales portal item
                    var salesItem = SalesInfoProvider.GetSales()
                        .Where(x => x.SaleInvoiceNumber == int.Parse(invoiceNumber))
                        .Where(x => x.SaleClassID == item.ClassID)
                        .FirstOrDefault();

                    if (salesItem != null)
                    {
                        // if the qty is 0 remove the sales portal item
                        if (quantity == 0) SalesInfoProvider.DeleteSalesInfo(salesItem);
                        // else update the sales portal item
                        else UpdateSalesPortalItem(orderItem, salesItem, quantity);
                    }

                    // get the facility class to update the qty
                    var facilityClass = ClassMemberDependencies.FacilityClassRepo.GetFacilityMembershipClass(facilityID, item.ClassID, invoiceNumber, membershipID, x => new FacilityClassDto(x));
                    if (facilityClass != null)
                    {
                        if(quantity == 0)
                        {
                            ClassMemberDependencies.FacilityClassRepo.RemoveFacilityClass(facilityClass.FacilityClassID);
                        }
                        else
                        {
                            ClassMemberDependencies.FacilityClassRepo.UpdateFacilityClassQty(facilityClass.FacilityClassID, quantity);
                        }
                    }
                }

                if (quantity == 0)
                {
                    var mem = ClassMemberDependencies.FacilityMembershipRepo.GetFacilityMembership(membershipID, x => new FacilityMembershipDto { FacilityMembershipID = x.FacilityMembershipID });

                    if(mem != null) ClassMemberDependencies.FacilityMembershipRepo.RemoveFacilityMembership(mem.FacilityMembershipID);
                }

                // update the orders total price
                // fix to calculate new membership price
                UpdateOrderTotalPrice(order, true);

                // update the order status
                int status = 0;
                if (order.OrderGrandTotalInMainCurrency > 0)
                {
                    status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PARTIAL_REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                }
                else
                {
                    status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                }

                order.OrderStatusID = status;
                OrderInfoProvider.SetOrderInfo(order);

                return Json(JsonConvert.SerializeObject(new { Removed = removed, Quantity = quantity, MembershipID = membershipID }), JsonRequestBehavior.AllowGet);
            }

            // basically a cancel action since the qty was not updated
            return Json(JsonConvert.SerializeObject(new { Removed = removed, Quantity = quantity, MembershipID = membershipID }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFacilityMembershipClassEnrollmentData(int facilityID, int classID, int membershipID, string invoiceNum = "")
        {
            var s = SetUpEnrollmentClasses(facilityID, classID, invoiceNum, membershipID);

            var className = classID != 0 ? ClassBuilderDependencies.ClassRepo.GetClass(classID, x => new ClassDto { ClassName = x.ClassName }).ClassName : "";
            //var limitReached = facilityClass != null && facilityClass.Quantity <= facilityClass.QuantityUsed;
            //var amountUnused = facilityClass != null ? (facilityClass.Quantity - (facilityClass.QuantityUsed ?? 0)) : 0;

            return Json(JsonConvert.SerializeObject(new { Students = s, ClassName = className, MembershipID = membershipID }), JsonRequestBehavior.AllowGet);
        }

        #endregion
        
        #region TAB 7 Methods: Facility Classes

        public JsonResult GetFacilityClasses(int facilityID)
        {
            var facilityClasses = SetUpFacilityClassViewModel(facilityID);

            return Json(JsonConvert.SerializeObject(facilityClasses), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateFacilityClassQty(int facilityClassID, int classID, int facilityID, int quantity, string invoiceNumber)
        {
            // get the item being updated
            var orderItem = OrderItemInfoProvider.GetOrderItems()
                    .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                    .Where(x => x.OrderItemSKU.SKUNumber == classID.ToString())
                    .FirstOrDefault();

            // initialize return variables
            var updatedClassIDs = new List<int>();
            var removed = false;

            // the quantity has decreased
            if (orderItem.OrderItemUnitCount != quantity)
            {
                // get the order
                var order = OrderInfoProvider.GetOrderInfo(int.Parse(invoiceNumber));

                // check if the item is in a package
                if ((orderItem.OrderItemBundleGUID != null && orderItem.OrderItemBundleGUID != Guid.Empty) || orderItem == null)
                {
                    // get the bundle item
                    var parentBundleItem = OrderItemInfoProvider.GetOrderItems()
                        .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                        .Where(x => x.OrderItemGUID == orderItem.OrderItemBundleGUID)
                        .FirstOrDefault();

                    // get all the products in the bundle
                    var packageItems = OrderItemInfoProvider.GetOrderItems()
                        .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                        .Where(x => x.OrderItemBundleGUID == orderItem.OrderItemBundleGUID)
                        .ToList();

                    // get the print option for the bundle if there is one
                    var printOption = OrderItemInfoProvider.GetOrderItems()
                         .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                         .Where(x => x.OrderItemSKU.IsProductOption)
                         .Where(x => x.OrderItemParentGUID == parentBundleItem.OrderItemGUID)
                         .FirstOrDefault();

                    // update the bundles qty/total price
                    UpdateOrderItemQty(parentBundleItem, quantity);

                    if (printOption != null)
                    {
                        // update the print option qty/total price
                        UpdateOrderItemQty(printOption, quantity);
                    }

                    // remove the whole bundle, update the order total, remove the sales data for each of the items in the bundle
                    if (quantity == 0)
                    {
                        removed = true;
                        // remove all sales portal data
                        foreach (var item in packageItems)
                        {
                            // remove bundle item from sales portal
                            ClassMembersHelper.RemoveClassFromSalesTable(item.OrderItemOrderID.ToString(), int.Parse(item.OrderItemSKU.SKUNumber));

                            // remove bundle item from the order
                            OrderItemInfoProvider.DeleteOrderItemInfo(item);

                            // get the facility class record
                            var courseID = int.Parse(item.OrderItemSKU.SKUNumber);
                            var fc = ClassMemberDependencies.FacilityClassRepo.GetRegularFacilityClass(facilityID, courseID, invoiceNumber, 
                                x => new FacilityClassDto { FacilityClassID = x.FacilityClassID});

                            // add to ID list
                            updatedClassIDs.Add(fc.FacilityClassID);

                            // remove the facility class
                            ClassMemberDependencies.FacilityClassRepo.RemoveFacilityClass(fc.FacilityClassID);
                        }
                    }
                    // update all the courses in the bundles quantity, update the order total, update the values in sales table
                    else
                    {
                        foreach (var item in packageItems)
                        {
                            // update each items price/qty
                            UpdateOrderItemQty(item, quantity);

                            // get the bundle items sales portal info
                            var salesItem = SalesInfoProvider.GetSales()
                                .Where(x => x.SaleInvoiceNumber == int.Parse(invoiceNumber))
                                .Where(x => x.SaleClassID == int.Parse(item.OrderItemSKU.SKUNumber))
                                .FirstOrDefault();

                            if (salesItem != null)
                            {
                                // update the sales portal record for the bundle item
                                UpdateSalesPortalItem(item, salesItem, quantity);
                            }

                            // get the facility class record
                            var courseID = int.Parse(item.OrderItemSKU.SKUNumber);

                            var fc = ClassMemberDependencies.FacilityClassRepo.GetRegularFacilityClass(facilityID, courseID, invoiceNumber,
                                x => new FacilityClassDto { FacilityClassID = x.FacilityClassID });

                            // add to ID list
                            updatedClassIDs.Add(fc.FacilityClassID);

                            // update the facility class qty purchased
                            ClassMemberDependencies.FacilityClassRepo.UpdateFacilityClassQty(fc.FacilityClassID, quantity);
                        }
                    }
                }
                //just a single course being updated
                else
                {
                    // get the sales portal record
                    var salesItem = SalesInfoProvider.GetSales()
                        .Where(x => x.SaleInvoiceNumber == int.Parse(invoiceNumber))
                        .Where(x => x.SaleClassID == classID)
                        .FirstOrDefault();

                    // get the print option if applicable
                    var printOption = OrderItemInfoProvider.GetOrderItems()
                        .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                        .Where(x => x.OrderItemSKU.IsProductOption)
                        .Where(x => x.OrderItemParentGUID == orderItem.OrderItemGUID)
                        .FirstOrDefault();

                    // get the facility class to update the qty
                    var facilityClass = ClassMemberDependencies.FacilityClassRepo.GetRegularFacilityClass(facilityID, classID, invoiceNumber, 
                        x => new FacilityClassDto { FacilityClassID = x.FacilityClassID });

                    // check if the print option is null
                    if (printOption != null)
                    {
                        // update or remove print option from the order
                        UpdateOrderItemQty(printOption, quantity);
                    }

                    // update or remove order item from the order
                    UpdateOrderItemQty(orderItem, quantity);

                    // if the quantity is 0 remove from the sales portal and remove the facility class record
                    if (quantity == 0)
                    {
                        removed = true;
                        if (salesItem != null)
                        {
                            // remove from sales portal
                            SalesInfoProvider.DeleteSalesInfo(salesItem);
                        }

                        // remove from the facility listing
                        ClassMemberDependencies.FacilityClassRepo.RemoveFacilityClass(facilityClass.FacilityClassID);
                    }
                    //update the quantity, update the order total, update the values in the sales table
                    else
                    {
                        if (salesItem != null)
                        {
                            // update the sales portal item
                            UpdateSalesPortalItem(orderItem, salesItem, quantity);
                        }

                        // update the facility classes qty
                        ClassMemberDependencies.FacilityClassRepo.UpdateFacilityClassQty(facilityClass.FacilityClassID, quantity);
                    }
                }

                // update the orders total price
                UpdateOrderTotalPrice(order, false);

                // update the order status
                int status = 0;
                if (order.OrderGrandTotalInMainCurrency > 0)
                {
                    status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PARTIAL_REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                }
                else
                {
                    status = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.REFUND_PAYMENT_STATUS, SiteContext.CurrentSiteName).StatusID;
                }

                order.OrderStatusID = status;
                OrderInfoProvider.SetOrderInfo(order);

                return Json(JsonConvert.SerializeObject(new { Removed = removed, Quantity = quantity, FacilityClassID = facilityClassID, UpdatedClasses = updatedClassIDs }), JsonRequestBehavior.AllowGet);
            }

            // basically a cancel action since the qty was not updated
            return Json(JsonConvert.SerializeObject(new { Removed = removed, Quantity = quantity, FacilityClassID = facilityClassID, UpdatedClasses = updatedClassIDs }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFacilityClassEnrollmentData(int facilityID, int classID, string invoiceNum = "")
        {
            var s = SetUpEnrollmentClasses(facilityID, classID, invoiceNum, 0); 

            var className = classID != 0 ? ClassBuilderDependencies.ClassRepo.GetClass(classID, x => new ClassDto { ClassName = x.ClassName }).ClassName : "";
            //var limitReached = facilityClass != null && facilityClass.Quantity <= facilityClass.QuantityUsed;
            //var amountUnused = facilityClass != null ? (facilityClass.Quantity - (facilityClass.QuantityUsed ?? 0)) : 0;

            return Json(JsonConvert.SerializeObject(new { Students = s, ClassName = className }), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Global Methods

        [HttpPost]
        public JsonResult AssignClass(int classID, int studentID, int facilityID, string invoiceNumber, int? facilityMemID = null, bool facilityClassListing = false)
        {
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityName = x.Name, FacilityCode = x.FacilityCode });

            DateTime? expDate = null;
            if (facilityMemID.HasValue && facilityMemID.Value != 0)
            {
                expDate = ClassMemberDependencies.FacilityMembershipRepo.GetFacilityMembership(facilityMemID.Value, x => new FacilityMembershipDto { ExpirationDate = x.ExpirationDate }).ExpirationDate;
            }

            var facilityClass = ClassMemberDependencies.FacilityClassRepo.GetFacilityClass(facilityID, classID, x => new FacilityClassDto(x), invoiceNumber, facilityMemID);
            bool canPrint = false;
            if(facilityClass != null) canPrint = facilityClass.CanPrint;

            var studentClass = ClassMemberDependencies.StudentClassRepo.CreateStudentClass(studentID, classID, facility.FacilityCode, invoiceNumber, canPrint, facilityMemID, expDate);

            if (!facilityClass.QuantityUsed.HasValue)
            {
                ClassMemberDependencies.FacilityClassRepo.UpdateFacilityClassQtyUsed(facilityClass.FacilityClassID, Operation.Change, 1);
            }
            else if(facilityClass.Quantity > facilityClass.QuantityUsed.Value)
            {
                ClassMemberDependencies.FacilityClassRepo.UpdateFacilityClassQtyUsed(facilityClass.FacilityClassID, Operation.Increase, 1);
            }

            var assignClassLogType = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeIDByCodeName(ContentIdentifiers.COURSE_ASSIGNED_ET_CODENAME);
            var student = ClassMemberDependencies.ContactRepo.GetContact(studentID, x => new ContactDto(x));

            var classData = ClassBuilderDependencies.ClassRepo.GetClass(classID, x => new ClassDto { ClassName = x.ClassName });
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);


            ClassMemberDependencies.EventLogRepo.CreateNewEvent(currentUser, student, assignClassLogType, facility.FacilityName, classData.ClassName);

            if (facilityClassListing)
            {
                return Json(JsonConvert.SerializeObject(new { Students = SetUpManageStudentClasses(facilityID, student), StudentID = studentID, FacilityClassListing = facilityClassListing }), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(JsonConvert.SerializeObject(new { Students = SetUpEnrollmentClasses(facilityID, classID, invoiceNumber, facilityMemID), MembershipID = facilityMemID, FacilityClassListing = facilityClassListing }), JsonRequestBehavior.AllowGet);
            }            
        }

        [HttpPost]
        public JsonResult UnassignClass(int classID, int studentID, int facilityID, string invoiceNumber, int? facilityMemID = null, bool facilityClassListing = false)
        {
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityName = x.Name });

            var facilityClass = ClassMemberDependencies.FacilityClassRepo.GetFacilityClass(facilityID, classID, x => new FacilityClassDto(x), invoiceNumber, facilityMemID);

            var studentClass = ClassMemberDependencies.StudentClassRepo.GetStudentClass(studentID, classID, x => new StudentClassDto(x),  invoiceNumber, facilityClass.CanPrint, facilityMemID, 
                ClassType.FacilityMembership);

            if (studentClass != null) ClassMemberDependencies.StudentClassRepo.RemoveStudentClass(studentClass.StudentClassID);

            if (facilityClass != null)
            {
                if(facilityClass.QuantityUsed.HasValue && facilityClass.QuantityUsed.Value > 0)
                {
                    ClassMemberDependencies.FacilityClassRepo.UpdateFacilityClassQtyUsed(facilityClass.FacilityClassID, Operation.Decrease, 1);
                }
            }

            var unassignClassLogType = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeIDByCodeName(ContentIdentifiers.COURSE_UNASSIGNED_ET_CODENAME);
            var student = ClassMemberDependencies.ContactRepo.GetContact(studentID, x => new ContactDto(x));
            var classData = ClassBuilderDependencies.ClassRepo.GetClass(classID, x => new ClassDto { ClassName = x.ClassName });
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            ClassMemberDependencies.EventLogRepo.CreateNewEvent(currentUser, student, unassignClassLogType, facility.FacilityName, classData.ClassName);

            if (facilityClassListing)
            {
                return Json(JsonConvert.SerializeObject(new { Students = SetUpManageStudentClasses(facilityID, student), StudentID = studentID, FacilityClassListing = facilityClassListing }), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(JsonConvert.SerializeObject(new { Students = SetUpEnrollmentClasses(facilityID, classID, invoiceNumber, facilityMemID), MembershipID = facilityMemID, FacilityClassListing = facilityClassListing }), JsonRequestBehavior.AllowGet);
            }

        }

        private List<FacilityClassViewModel> SetUpEnrollmentClasses(int facilityID, int classID, string invoiceNum, int? facilityMemID = null)
        {
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);

            var s = new List<FacilityClassViewModel>();
            var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByFacilityID(facilityID, x => new ContactFacilityDto(x));
            var facilityCode = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityCode = x.FacilityCode }).FacilityCode;

            FacilityClassDto facilityClass;

            if (facilityMemID.HasValue && facilityMemID.Value != 0)
            {
                facilityClass = ClassMemberDependencies.FacilityClassRepo.GetFacilityMembershipClass(facilityID, classID, invoiceNum, facilityMemID.Value,
                    x => new FacilityClassDto { Quantity = x.Quantity, QuantityUsed = x.QuantityUsed, FacilityMembershipID = x.FacilityMembershipID });
            }
            else
            {
                facilityClass = ClassMemberDependencies.FacilityClassRepo.GetRegularFacilityClass(facilityID, classID, invoiceNum,
                    x => new FacilityClassDto { Quantity = x.Quantity, QuantityUsed = x.QuantityUsed, FacilityMembershipID = x.FacilityMembershipID });
            }

            foreach (var item in cf)
            {
                var contact = ClassMemberDependencies.ContactRepo.GetContact(item.ContactID, studentType, 
                    x => new ContactDto { ContactID = x.ContactID, ContactFirstName = x.ContactFirstName, ContactLastName = x.ContactLastName });

                if (contact != null)
                {
                    var fc = new FacilityClassViewModel()
                    {
                        FacilityID = item.FacilityID,
                        ClassID = classID,
                        ClassName = ClassBuilderDependencies.ClassRepo.GetClass(classID, x => new ClassDto { ClassName = x.ClassName }).ClassName,
                        Status = "Not Assigned",
                        Unassignable = false,
                        InvoiceNumber = invoiceNum,
                        LimitReached = facilityClass.Quantity == (facilityClass.QuantityUsed ?? 0),
                        StudentID = contact.ContactID,
                        StudentName = $"{contact.ContactFirstName} {contact.ContactLastName}",
                        ExpDate = "",
                        AmountUsed = facilityClass != null ? facilityClass.QuantityUsed ?? 0 : 0,
                        QuantityPurchased = facilityClass != null ? facilityClass.Quantity : 0,
                        FacilityMembershipID = facilityClass != null ? facilityClass.FacilityMembershipID : null
                    };

                    var studentClasses = ClassMemberDependencies.StudentClassRepo.GetRegularFacilityStudentClasses(item.ContactID, classID, facilityCode, x => new StudentClassDto { ClassWorkflowID = x.ClassWorkflowID });
                    if (studentClasses.Count() > 0)
                    {
                        fc.Status = "Assigned";
                        if (studentClasses.Count() > 1) fc.Status = $"Assigned Multiple Times ({studentClasses.Count()})";

                        studentClasses.ForEach(x =>
                        {
                            if (x.ClassWorkflowID == startWF) fc.Unassignable = true;
                        });
                    }

                    if (facilityClass != null && facilityClass.FacilityMembershipID.HasValue)
                    {
                        var expDate = ClassMemberDependencies.FacilityMembershipRepo.GetFacilityMembershipExpirationDate(facilityClass.FacilityMembershipID.Value);

                        if (expDate != null && expDate != new DateTime()) fc.ExpDate = expDate.ToString("MM/dd/yyyy h:m:s tt");
                    }

                    s.Add(fc);
                }
            }

            return s;
        }

        private List<FacilityClassViewModel> SetUpManageStudentClasses(int facilityID, ContactDto student)
        {
            var studentName = $"{student.ContactFirstName} {student.ContactLastName}";

            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);

            var facilityClasses = ClassMemberDependencies.FacilityClassRepo.GetFacilityClasses(facilityID, x => new FacilityClassDto(x));
            var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto(x));

            var fc = new List<FacilityClassViewModel>();
            foreach (var item in facilityClasses)
            {
                var classData = ClassBuilderDependencies.ClassRepo.GetClass(item.ClassID, x => new ClassDto { ClassName = x.ClassName, PassingGrade = x.PassingGrade });

                var facilityModel = new FacilityClassViewModel
                {
                    FacilityID = item.FacilityID,
                    ClassID = item.ClassID,
                    ClassName = classData.ClassName,
                    Unassignable = false,
                    LimitReached = item.Quantity == item.QuantityUsed,
                    AmountUsed = item.QuantityUsed.HasValue ? item.QuantityUsed.Value < 0 ? 0 : item.QuantityUsed.Value : 0,
                    QuantityPurchased = item.Quantity,
                    InvoiceNumber = item.InvoiceNumber,
                    FacilityMembershipID = item.FacilityMembershipID,
                    Status = "Not Assigned"
                };

                List<StudentClassDto> studentClasses;
                // get classes
                if (item.FacilityMembershipID.HasValue)
                {
                    studentClasses = ClassMemberDependencies.StudentClassRepo.GetMembershipFacilityStudentClasses(student.ContactID, item.ClassID, facility.FacilityCode,
                        item.FacilityMembershipID, x => new StudentClassDto { ClassWorkflowID = x.ClassWorkflowID });
                }
                else
                {
                    studentClasses = ClassMemberDependencies.StudentClassRepo.GetRegularFacilityStudentClasses(student.ContactID, item.ClassID, facility.FacilityCode, 
                        x => new StudentClassDto { ClassWorkflowID = x.ClassWorkflowID });
                }

                if (studentClasses.Count() > 0)
                {
                    facilityModel.Status = "Assigned";
                    if (studentClasses.Count() > 1) facilityModel.Status = $"Assigned Multiple Times ({studentClasses.Count()})";

                    // check if any student classes are on the start wf and if they are allow the course to be unassigned
                    // if there are none then the course can not be unassigned
                    studentClasses.ForEach(x =>
                    {
                        if(x.ClassWorkflowID == startWF) facilityModel.Unassignable = true;
                    });
                }

                fc.Add(facilityModel);
            }

            return fc;
        }

        private List<FacilityClassViewModel> SetUpFacilityClassViewModel(int facilityID)
        {
            var facilityCode = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto { FacilityCode = x.FacilityCode }).FacilityCode;
            var classes = ClassMemberDependencies.FacilityClassRepo.GetRegularFacilityClasses(facilityID, x => new FacilityClassDto(x));

            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            var completedWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.COMPLETED_WF_CODE_NAME);

            var facilityClasses = new List<FacilityClassViewModel>();
            classes.ForEach(x =>
            {
                int amountNotStarted;
                int amountInProgress;
                int amountCompleted;

                amountNotStarted = ClassMemberDependencies.StudentClassRepo.GetStudentClassCount(x.ClassID, facilityCode, x => x.ClassWorkflowID == startWF, x.InvoiceNumber);

                amountInProgress = ClassMemberDependencies.StudentClassRepo.GetStudentClassCount(x.ClassID, facilityCode, x => x.ClassWorkflowID != startWF && x.ClassWorkflowID != completedWF, 
                    x.InvoiceNumber);

                amountCompleted = ClassMemberDependencies.StudentClassRepo.GetStudentClassCount(x.ClassID, facilityCode, x => x.ClassWorkflowID == completedWF, x.InvoiceNumber);

                var classData = ClassBuilderDependencies.ClassRepo.GetClass(x.ClassID, x => new ClassDto { ClassName = x.ClassName });

                facilityClasses.Add(new FacilityClassViewModel()
                {
                    FacilityClassID = x.FacilityClassID,
                    FacilityID = x.FacilityID,
                    ClassID = x.ClassID,
                    ClassName = classData != null ? classData.ClassName : "",
                    AmountUsed = x.QuantityUsed ?? 0,
                    QuantityPurchased = x.Quantity,
                    AmountNotStarted = amountNotStarted,
                    AmountInProgress = amountInProgress,
                    AmountCompleted = amountCompleted,
                    InvoiceNumber = x.InvoiceNumber,
                    LimitReached = x.Quantity <= (x.QuantityUsed ?? 0),
                    InPackage = ClassMembersHelper.IsClassInPackage(x.InvoiceNumber, x.ClassID)
                });
            });

            return facilityClasses.OrderBy(x => x.ClassName).ToList();
        }

        private decimal GetTotalItemPrice(OrderItemInfo item)
        {
            if (item.OrderItemBundleGUID != null && item.OrderItemBundleGUID != Guid.Empty)
            {
                return SKUInfoProvider.GetSKUInfo(item.OrderItemSKUID).SKUPrice;
            }

            return item.OrderItemTotalPrice;
        }

        private void UpdateOrderItemQty(OrderItemInfo orderItem, int quantity)
        {
            if (quantity > 0)
            {
                orderItem.OrderItemUnitCount = quantity;
                orderItem.OrderItemTotalPrice = orderItem.OrderItemUnitPrice * quantity;

                OrderItemInfoProvider.SetOrderItemInfo(orderItem);
            }
            else
            {
                OrderItemInfoProvider.DeleteOrderItemInfo(orderItem);
            }
        }

        private void UpdateSalesPortalItem(OrderItemInfo orderItem, SalesInfo salesItem, int quantity)
        {
            // get sales portal calculation numbers
            decimal transactionPercentFee = SettingsKeyInfoProvider.GetDecimalValue("authorizeNetTransactionFeePercent") / 100;
            decimal transactionCentsFee = SettingsKeyInfoProvider.GetDecimalValue("authorizeNetTransactionFeeCents");
            decimal salesCommissionPercent = SettingsKeyInfoProvider.GetDecimalValue("SalesCommissionPercent") / 100;

            var totalPrice = decimal.Round(orderItem.OrderItemBundleGUID == null || orderItem.OrderItemBundleGUID == Guid.Empty ? orderItem.OrderItemTotalPrice : GetTotalItemPrice(orderItem) * quantity, 2);
            decimal discount = decimal.Round(totalPrice * new decimal(.1), 2);
            decimal printPrice = new decimal(0.00);

            var items = OrderItemInfoProvider.GetOrderItems(orderItem.OrderItemOrderID);
            var options = items.Where(x => x.OrderItemParentGUID != null).ToList();

            if (orderItem.OrderItemBundleGUID != null && orderItem.OrderItemBundleGUID != Guid.Empty)
            {
                var price = options.Where(x => x.OrderItemParentGUID == orderItem.OrderItemBundleGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
                printPrice = decimal.Round(price / items.Where(x => x.OrderItemBundleGUID == orderItem.OrderItemBundleGUID).Count(), 2);
            }
            else
            {
                printPrice = options.Where(x => x.OrderItemParentGUID == orderItem.OrderItemGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
            }

            var ecomFee = decimal.Round((totalPrice - discount + printPrice) != 0 ? ((totalPrice - discount + printPrice) * transactionPercentFee) + transactionCentsFee : new decimal(0.00), 2);
            var net = decimal.Round((totalPrice - discount + printPrice) != 0 ? (totalPrice - discount + printPrice) - ecomFee : 0, 2);

            salesItem.SaleQuantity = quantity;
            salesItem.SaleCost = totalPrice;
            salesItem.SaleSubtotal = decimal.Round(totalPrice - discount + printPrice, 2);
            salesItem.SaleECommerceFee = ecomFee;
            salesItem.SaleNetTotal = net;
            salesItem.SaleCommission = decimal.Round(net * salesCommissionPercent, 2);
            salesItem.SaleAuthorCommission = decimal.Round((net - (net * salesCommissionPercent)) / 2, 2);
            salesItem.SaleDiscount = discount;

            salesItem.Update();
        }

        private void UpdateOrderTotalPrice(OrderInfo order, bool isMembership)
        {
            var orderItems = OrderItemInfoProvider.GetOrderItems(order.OrderID);
            decimal total = 0;

            if (isMembership)
            {
                double contactHours = 0;

                if(orderItems.Count() > 0)
                {
                    var qty = orderItems.FirstOrDefault().OrderItemUnitCount;

                    foreach (var i in orderItems)
                    {
                        if (!i.OrderItemSKU.IsProductOption)
                        {
                            var course = DocumentHelper.GetDocuments()
                                .OnCurrentSite()
                                .Type("Pedagogy.Course")
                                .Where(x => x.NodeSKUID == i.OrderItemSKUID)
                                .FirstOrDefault();

                            contactHours += course.GetDoubleValue("CourseContactHours", 0);
                        }
                    }

                    var categoryName = order.OrderCustomData.GetValue(ContentIdentifiers.MEMBERSHIP_CATEGORY_CUSTOM_DATA_COLUMN);
                    if (categoryName != null)
                    {
                        var categoryID = CategoryInfoProvider.GetCategories()
                            .Where(x => x.CategoryName == categoryName.ToString())
                            .Select(x => x.CategoryID)
                            .FirstOrDefault();

                        FacilityMembershipPricesItem memDiscount;
                        if (qty > 200)
                        {
                            memDiscount = CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                                .Where(x => x.FacilityMembershipCategory == categoryID.ToString())
                                .Where(x => x.FacilityMembershipEndingStaffNumber == 200)
                                .FirstOrDefault();
                        }
                        else
                        {
                            memDiscount = CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                                .Where(x => x.FacilityMembershipCategory == categoryID.ToString())
                                .Where(x => x.FacilityMembershipStartingContactHours <= contactHours)
                                .Where(x => x.FacilityMembershipEndingContactHours >= contactHours)
                                .Where(x => x.FacilityMembershipStartingStaffNumber <= qty)
                                .Where(x => x.FacilityMembershipEndingStaffNumber >= qty)
                                .FirstOrDefault();
                        }

                        if (memDiscount != null)
                        {
                            total = memDiscount.FacilityMembershipDollarAmtPerYear * qty;
                        }
                    }

                    var qtyCustomCol = order.OrderCustomData.GetValue(ContentIdentifiers.MEMBERSHIP_QTY_CUSTOM_DATA_COLUMN);
                    if (qtyCustomCol != null) order.OrderCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_QTY_CUSTOM_DATA_COLUMN, qty);
                }                
            }
            else
            {
                foreach (var item in orderItems)
                {
                    total += item.OrderItemUnitPrice * item.OrderItemUnitCount;
                }
            }

            order.OrderTotalPrice = total;
            order.OrderTotalPriceInMainCurrency = total;
            order.OrderGrandTotal = total;
            order.OrderGrandTotalInMainCurrency = total;

            OrderInfoProvider.SetOrderInfo(order);
        }

        #endregion
    }
}