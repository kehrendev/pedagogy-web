﻿using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.AdminFunctionsWidget;

using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.ADMIN_FUNCTIONS_WIDGET_IDENTIFIER,
    typeof(AdminFunctionsWidgetController),
    "Admin Functions Widget",
    Description = "A widget used to display all the Admin Function pages",
    IconClass = "icon-checklist")]

namespace Pedagogy.Controllers.Widgets
{
    public class AdminFunctionsWidgetController : WidgetController<AdminFunctionsWidgetProperties>
    {
        private readonly IDataDependencies Dependencies;

        public AdminFunctionsWidgetController(IDataDependencies dependencies) { Dependencies = dependencies; }

        public ActionResult Index()
        {
            var usr = UserInfoProvider.GetUserInfo(User.Identity.Name);

            return PartialView(ComponentIdentifiers.ADMIN_FUNCTIONS_WIDGET_VIEW,AdminFunctionsWidgetViewModel.GetViewModel(
                Dependencies.NavigationRepo.GetPagesByCategory(new string[] { ContentIdentifiers.NAV_ADMIN_FUNCTIONS_CATEGORY }), usr, GetProperties().CardHeading));
        }

        [HttpGet]
        public JsonResult GetPagedData(AdminFunctionsWidgetViewModel model, int pageNumber = 1, int pageSize = 10)
        {
            var users = UserInfoProvider.GetUsers()
                .Where(x => x.Enabled)
                .Select(x => new { x.UserName, x.FirstName, x.LastName, x.UserID, x.Email});

            if (model != null && model.FilterModel != null)
            {
                if (!string.IsNullOrEmpty(model.FilterModel.UserName)) users = users.Where(x => x.UserName.ToLower().Contains(model.FilterModel.UserName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.FirstName)) users = users.Where(x => x.FirstName.ToLower().Contains(model.FilterModel.FirstName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.LastName)) users = users.Where(x => x.LastName.ToLower().Contains(model.FilterModel.LastName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.Email)) users = users.Where(x => x.Email.ToLower().Contains(model.FilterModel.Email.ToLower()));
            }

            var pagedData = PagingHelper.PagedResult(users.Select(x => new { x.UserName, x.UserID }).OrderBy(x => x.UserName).ToList(), pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }
    }
}