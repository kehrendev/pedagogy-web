﻿using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassBuilder;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.ManageStudentClassesWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.MANAGE_STUDENT_CLASSES_WIDGET_IDENTIFIER,
    typeof(ManageStudentClassesWidgetController),
    "Manage Student Classes Widget",
    Description = "A widget used for managing a students classes",
    IconClass = "icon-clipboard-checklist")]

namespace Pedagogy.Controllers.Widgets
{
    public class ManageStudentClassesWidgetController : WidgetController<ManageStudentClassesWidgetProperties>
    {
        public ActionResult Index(int studentID = 0, int facilityID = 0)
        {
            var cardHeading = GetProperties().CardHeading;

            if (HttpContext.Kentico().PageBuilder().EditMode)
            {
                return PartialView(ComponentIdentifiers.MANAGE_STUDENT_CLASSES_WIDGET_VIEW, ManageStudentClassesWidgetViewModel.GetViewModel(cardHeading));
            }

            if (studentID != 0 && facilityID != 0)
            {
                var student = ClassMembersHelper.GetContact(studentID);
                var studentName = $"{student.ContactFirstName} {student.ContactLastName}";

                var startWF = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.START_CLASS_WF_CODE_NAME);

                var facilityClasses = ClassMembersHelper.GetFacilityClasses(facilityID, false);
                var facility = ClassMembersHelper.GetFacility(facilityID, x => x);

                var fc = new List<FacilityClassViewModel>();
                foreach (var item in facilityClasses)
                {
                    var classData = ClassBuilderHelper.GetClass(item.ClassID, x => new TU_Class { ClassName = x.ClassName, PassingGrade = x.PassingGrade });

                    var facilityModel = new FacilityClassViewModel
                    {
                        FacilityID = item.FacilityID,
                        ClassID = item.ClassID,
                        ClassName = classData.ClassName,
                        Status = "Not Assigned",
                        Assigned = false,
                        IsLocked = false,
                        LimitReached = item.Quantity <= item.QuantityUsed,
                        AmountUsed = item.QuantityUsed ?? 0,
                        QuantityPurchased = item.Quantity,
                        InvoiceNumber = item.InvoiceNumber
                    };

                    var studentClass = ClassMembersHelper.GetStudentClass(studentID, item.ClassID, facility.FacilityCode, false);

                    if (studentClass != null)
                    {
                        if (studentClass.ClassWorkflowID != startWF)
                        {
                            facilityModel.IsLocked = true;
                        }
                        
                        facilityModel.Assigned = true;

                        facilityModel.Status = ClassMembersHelper.GetWorkflow(studentClass.ClassWorkflowID, 
                            x => new ClassWorkflow { WorkflowStep = x.WorkflowStep }).WorkflowStep;
                    }

                    fc.Add(facilityModel);
                }

                var facilityMems = ClassMembersHelper.GetFacilityMemberships(facilityID);
                var mems = new List<FacilityClassViewModel>();
                foreach (var item in facilityMems)
                {
                    var classes = ClassMembersHelper.GetFacilityClasses(facilityID, item.FacilityMembershipID);
                    foreach (var i in classes)
                    {
                        var classData = ClassBuilderHelper.GetClass(i.ClassID, x => new TU_Class { ClassName = x.ClassName, PassingGrade = x.PassingGrade });

                        var memModel = new FacilityClassViewModel
                        {
                            FacilityID = item.FacilityID,
                            ClassID = i.ClassID,
                            ClassName = classData.ClassName,
                            Status = "Not Assigned",
                            Assigned = false,
                            IsLocked = false,
                            LimitReached = i.Quantity <= i.QuantityUsed,
                            AmountUsed = i.QuantityUsed ?? 0,
                            QuantityPurchased = i.Quantity,
                            FacilityMembershipID = i.FacilityMembershipID ?? 0,
                            ExpDate = item.ExpirationDate != null ? item.ExpirationDate.ToString("MM/dd/yyyy h:m:s tt") : "",
                            InvoiceNumber = i.InvoiceNumber
                        };

                        var studentClass = ClassMembersHelper.GetStudentClass(studentID, i.ClassID, item.FacilityMembershipID);
                        if (studentClass != null)
                        {
                            memModel.Assigned = true;
                            memModel.Status = ClassMembersHelper.GetWorkflow(studentClass.ClassWorkflowID, x => new ClassWorkflow { WorkflowStep = x.WorkflowStep }).WorkflowStep;
                            memModel.IsLocked = studentClass.ClassWorkflowID != startWF;
                        }

                        mems.Add(memModel);
                    }
                }

                return PartialView(ComponentIdentifiers.MANAGE_STUDENT_CLASSES_WIDGET_VIEW, 
                    ManageStudentClassesWidgetViewModel.GetViewModel(facilityID, studentID, cardHeading, studentName, fc, mems));
            }
            else
            {
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }
        }

        [HttpPost]
        public ActionResult AssignClass(int classID, int studentID, int facilityID, string returnUrl, string invoiceNumber, string expDate = "", int facilityMemID = 0)
        {
            var facility = ClassMembersHelper.GetFacility(facilityID, x => new Facility { FacilityCode = x.FacilityCode });

            var studentClass = ClassMembersHelper.CreateNewStudentClass(studentID, classID, facility.FacilityCode, invoiceNumber);


            if (!string.IsNullOrEmpty(expDate)) studentClass.ExpiresOn = DateTime.Parse(expDate);

            FacilityClass facilityClasses;
            if (facilityMemID != 0)
            {
                if (!string.IsNullOrEmpty(invoiceNumber)) facilityClasses = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber, facilityMemID);
                else facilityClasses = ClassMembersHelper.GetFacilityClass(facilityID, classID, facilityMemID);

                studentClass.FacilityMembershipID = facilityMemID;
            }
            else
            {
                if (!string.IsNullOrEmpty(invoiceNumber)) facilityClasses = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber);
                else facilityClasses = ClassMembersHelper.GetFacilityClass(facilityID, classID);
            }

            ClassMembersHelper.SaveChanges();

            if (facilityClasses.Quantity > facilityClasses.QuantityUsed)
            {
                facilityClasses.QuantityUsed += 1;
                ClassMembersHelper.SaveChanges();
            }
            if (facilityClasses.QuantityUsed == null)
            {
                facilityClasses.QuantityUsed = 1;
                ClassMembersHelper.SaveChanges();
            }

            var assignClassLogType = ClassMembersHelper.GetEventLogTypeID(ContentIdentifiers.COURSE_ASSIGNED_ET_CODENAME);
            var student = ClassMembersHelper.GetContact(studentID);
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var className = ClassBuilderHelper.GetClass(classID, x => new TU_Class { ClassName = x.ClassName }).ClassName;

            ClassMembersHelper.CreateNewEvent(currentUser, student, assignClassLogType, className);

            return Redirect(returnUrl);
        }

        [HttpPost]
        public ActionResult UnassignClass(int classID, int studentID, int facilityID, string returnUrl, string invoiceNumber, int facilityMemID = 0)
        {
            var paymentSource = ClassMembersHelper.GetFacility(facilityID, x => new Facility { FacilityCode = x.FacilityCode }).FacilityCode;
            StudentClass studentClass;

            if (!string.IsNullOrEmpty(invoiceNumber)) studentClass = ClassMembersHelper.GetStudentClass(studentID, classID, paymentSource, invoiceNumber);
            else studentClass = ClassMembersHelper.GetStudentClass(studentID, classID, paymentSource);

            if (studentClass != null)
            {
                ClassMembersHelper.RemoveStudentClass(studentClass);
            }

            FacilityClass facilityClass;
            if (facilityMemID != 0)
            {
                if (!string.IsNullOrEmpty(invoiceNumber)) facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber, facilityMemID);
                else facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, facilityMemID);
            }
            else
            {
                if (!string.IsNullOrEmpty(invoiceNumber)) facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID, invoiceNumber);
                else facilityClass = ClassMembersHelper.GetFacilityClass(facilityID, classID);
            }

            ClassMembersHelper.UpdateFacilityClassQty(facilityClass.QuantityUsed);

            var unassignClassLogType = ClassMembersHelper.GetWorkflowID(ContentIdentifiers.COURSE_UNASSIGNED_ET_CODENAME);
            var student = ClassMembersHelper.GetContact(studentID);
            var classData = ClassBuilderHelper.GetClass(classID, x => new TU_Class { ClassName = x.ClassName });
            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);

            ClassMembersHelper.CreateNewEvent(currentUser, student, unassignClassLogType);

            return Redirect(returnUrl);
        }
    }
}