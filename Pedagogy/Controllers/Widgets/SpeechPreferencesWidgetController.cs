﻿using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.ClassBuilder.DependencyInjection;
using Pedagogy.ClassBuilder.Entities.SpeechVoice;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.SpeechRate;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.SpeechPreferencesWidget;

using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.SPEECH_PREFERENCES_WIDGET_IDENTIFIER,
    typeof(SpeechPreferencesWidgetController),
    "Speech Preferences Widget",
    Description = "A widget used to allow the current user to select their speech preferences",
    IconClass = "icon-microphone")]

namespace Pedagogy.Controllers.Widgets
{
    public class SpeechPreferencesWidgetController : WidgetController<SpeechPreferencesWidgetProperties>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;
        private readonly IClassBuilderDependencies ClassBuilderDependencies;

        public SpeechPreferencesWidgetController(IClassMemberDependencies classMemberDependencies, IClassBuilderDependencies classBuilderDependencies)
        {
            ClassMemberDependencies = classMemberDependencies;
            ClassBuilderDependencies = classBuilderDependencies;
        }

        public ActionResult Index()
        {
            var isStudent = false;
            var studentID = 0;

            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto(x));

            var defaultVoice = ClassBuilderDependencies.SpeechVoiceRepo.GetDefaultSpeechVoiceID();
            var defaultRate = ClassMemberDependencies.SpeechRateRepo.GetDefaultSpeechRateID();

            if (student != null)
            {
                if (!student.SpeechVoiceID.HasValue)
                {
                    ClassMemberDependencies.ContactRepo.UpdateSpeechVoiceID(student.ContactID, defaultVoice);
                }
                if (!student.SpeechRateID.HasValue)
                {
                    ClassMemberDependencies.ContactRepo.UpdateSpeechRateID(student.ContactID, defaultRate);
                }
                

                isStudent = true;
                studentID = student.ContactID;
            }

            var speechVoiceID = student != null && student.SpeechVoiceID.HasValue ? student.SpeechVoiceID.Value : defaultVoice;
            var speechRateID = student != null && student.SpeechRateID.HasValue ? student.SpeechRateID.Value : defaultRate;

            var preferredVoices = new SelectList(
                ClassBuilderDependencies.SpeechVoiceRepo.GetAllSpeechVoices(x => new SpeechVoiceDto { SpeechVoiceID = x.SpeechVoiceId, SpeechVoiceName = x.VoiceName }), 
                nameof(SpeechVoiceDto.SpeechVoiceID), nameof(SpeechVoiceDto.SpeechVoiceName));

            var preferredSpeed = new SelectList(
                ClassMemberDependencies.SpeechRateRepo.GetAllSpeechRates(x => new SpeechRateDto { SpeechRateID = x.SpeechRateID, SpeechRate = x.Rate }), 
                nameof(SpeechRateDto.SpeechRateID), nameof(SpeechRateDto.SpeechRate));

            return PartialView(ComponentIdentifiers.SPEECH_PREFERENCES_WIDGET_VIEW, 
                SpeechPreferencesWidgetViewModel.GetViewModel(GetProperties().CardHeading, speechVoiceID, preferredVoices, speechRateID, preferredSpeed, isStudent, studentID));
        }

        [HttpPost]
        public void SaveSpeechPreferences(int studentID, int speechVoiceID, int speechRateID)
        {
            ClassMemberDependencies.ContactRepo.UpdateSpeechRateID(studentID, speechRateID);
            ClassMemberDependencies.ContactRepo.UpdateSpeechVoiceID(studentID, speechVoiceID);
        }
    }
}