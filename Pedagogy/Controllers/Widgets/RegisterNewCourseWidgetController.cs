﻿using CMS.Ecommerce;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using CMS.SiteProvider;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.ContactFacility;
using Pedagogy.ClassMembers.Entities.Facility;
using Pedagogy.Controllers.Widgets;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.RegsiterNewCourseWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.REGISTER_NEW_COURSE_WIDGET_IDENTIFIER,
    typeof(RegisterNewCourseWidgetController),
    "Register New Course Widget",
    Description = "A widget to allow users to add courses to their cart",
    IconClass = "icon-shopping-cart")]

namespace Pedagogy.Controllers.Widgets
{
    public class RegisterNewCourseWidgetController : WidgetController<RegisterNewCourseWidgetProperties>
    {
        private readonly IShoppingService mShoppingService;
        private readonly IDataDependencies Dependencies;
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public RegisterNewCourseWidgetController(IShoppingService iss, IDataDependencies dependencies, IClassMemberDependencies classMemberDependencies)
        {
            Dependencies = dependencies;
            mShoppingService = iss;
            ClassMemberDependencies = classMemberDependencies;
        }

        public ActionResult Index()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var courseData = Dependencies.CourseRepo.GetProductTypeCourses();
            var list = new List<SelectListItem>();

            foreach (var item in courseData)
            {
                list.Add(new SelectListItem()
                {
                    Value = item.SKU.SKUNumber,
                    Text = $"{item.CourseTitle} - ${Math.Round(item.SKU.SKUPrice, 2)}"
                });
            }

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var facilities = ClassMembersHelper.GetFacilitiesForCurrentUser(ClassMemberDependencies, isSiteAdmin, isCorpAdmin, user.UserID, "");
            var f = new List<SelectListItem>();

            facilities.OrderBy(x => x.Name).ToList().ForEach(x =>
            {
                f.Add(new SelectListItem()
                {
                    Value = x.FacilityID.ToString(),
                    Text = x.Name
                });
            });

            var isAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName) || user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);

            return PartialView(ComponentIdentifiers.REGISTER_NEW_COURSE_WIDGET_VIEW, RegisterNewCourseWidgetViewModel.GetViewModel(list, f, courseData, isAdmin, GetProperties().CardHeading));
        }
        
        public JsonResult AddToCart(int itemSKUID, string courseTitle, int itemQuantity)
        {
            var optsSkuid = CheckoutController.GetProductOptions(itemSKUID);

            var cartItemParams = new ShoppingCartItemParameters(itemSKUID, itemQuantity, optsSkuid);

            mShoppingService.AddItemToCart(cartItemParams);

            return Json($"Added {courseTitle} to your cart!", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddToDashboard(int itemSKUID, int classID, string returnUrl, int qty = 1)
        {
            mShoppingService.RemoveAllItemsFromCart();
            var paymentOption = PaymentOptionInfoProvider.GetPaymentOptionInfo(ContentIdentifiers.CREDIT_CARD_PAYMENT_METHOD, SiteContext.CurrentSiteName);
            var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var facilityID = CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null ? int.Parse(CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE).Value) : 0;

            if (facilityID != 0)
            {
                CustomerInfo customer = null;
                var facility = ClassMemberDependencies.FacilityRepo.GetFacility(facilityID, x => new FacilityDto(x));
                var facilityAddress = AddressInfoProvider.GetAddresses()
                    .Where(x => x.AddressLine1 == facility.FacilityAddress1)
                    .Where(x => x.AddressLine2 == facility.FacilityAddress2)
                    .Where(x => x.AddressName.Contains("Facility Address"))
                    .FirstOrDefault();

                if(facilityAddress == null)
                {
                    facilityAddress = new AddressInfo()
                    {
                        AddressName = "Facility Address",
                        AddressLine1 = facility.FacilityAddress1,
                        AddressLine2 = facility.FacilityAddress2,
                        AddressCity = facility.FacilityCity,
                        AddressZip = facility.FacilityZip,
                        AddressCountryID = CountryInfoProvider.GetCountryInfo("USA").CountryID,
                        AddressStateID = StateInfoProvider.GetStateInfoByCode(facility.FacilityState).StateID,
                        AddressPersonalName = $"{facility.FacilityName} Address"
                    };
                }

                if (user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName))
                {
                    var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByFacilityID(facilityID, x => new ContactFacilityDto(x));
                    foreach (var x in cf)
                    {
                        var contact = ClassMemberDependencies.ContactRepo.GetContact(x.ContactID, x => new ContactDto(x));
                        if (contact != null && contact.ContactUserID.HasValue && contact.ContactTypeID != studentType)
                        {
                            var usr = UserInfoProvider.GetUserInfo(contact.ContactUserID.Value);
                            if (usr != null)
                            {
                                if (usr.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName))
                                {
                                    customer = CustomerInfoProvider.GetCustomerInfoByUserID(contact.ContactUserID.Value);
                                    if (customer == null)
                                    {
                                        customer = CustomerHelper.MapToCustomer(usr);
                                    }
                                    mShoppingService.SetCustomer(customer);
                                    break;
                                }
                            }
                        }
                    }
                }

                if (customer == null)
                {
                    customer = CustomerInfoProvider.GetCustomerInfoByUserID(user.UserID);
                    if(customer == null)
                    {
                        customer = CustomerHelper.MapToCustomer(user);
                    }
                    mShoppingService.SetCustomer(customer);
                }

                var optsSkuid = CheckoutController.GetProductOptions(itemSKUID);
                var cartItemParams = new ShoppingCartItemParameters(itemSKUID, qty, optsSkuid);

                mShoppingService.AddItemToCart(cartItemParams);
                mShoppingService.SetPaymentOption(paymentOption.PaymentOptionID);
                mShoppingService.SetBillingAddress(facilityAddress);

                var order = mShoppingService.CreateOrder();
                var orderPaidStatus = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PAID_PAYMENT_STATUS, SiteContext.CurrentSiteName);
                order.OrderStatusID = orderPaidStatus.StatusID;
                order.OrderNote = ResHelper.GetString("Checkout.OrderNote.FreeCourse", LocalizationContext.CurrentCulture.CultureCode);
                order.Update();

                ClassMemberDependencies.FacilityClassRepo.CreateRegularFacilityClass(facilityID, classID, order.OrderInvoiceNumber, qty);
            }
            else
            {
                var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
                var studentID = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID }).ContactID;

                var customer = CustomerInfoProvider.GetCustomerInfoByUserID(user.UserID);
                mShoppingService.SetCustomer(customer);

                if (studentID == 0) studentID = ClassMemberDependencies.ContactRepo.MapUserToStudent(user).ContactID; 

                var optsSkuid = CheckoutController.GetProductOptions(itemSKUID);
                var cartItemParams = new ShoppingCartItemParameters(itemSKUID, 1, optsSkuid);
                mShoppingService.AddItemToCart(cartItemParams);

                mShoppingService.SetPaymentOption(paymentOption.PaymentOptionID); 
                
                var billing = AddressInfoProvider.GetAddresses(customer.CustomerID).FirstOrDefault();
                if (billing == null)
                {
                    billing = new AddressInfo()
                    {
                        AddressName = "FREE COURSE ADDRESS",
                        AddressLine1 = "FREE",
                        AddressCity = "FREE",
                        AddressZip = "FREE",
                        AddressCustomerID = customer.CustomerID,
                        AddressCountryID = 271,
                        AddressPersonalName = "FREE COURSE ADDRESS"
                    };
                }
                mShoppingService.SetBillingAddress(billing);

                var order = mShoppingService.CreateOrder();
                var orderPaidStatus = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PAID_PAYMENT_STATUS, SiteContext.CurrentSiteName);
                order.OrderStatusID = orderPaidStatus.StatusID;
                order.OrderNote = ResHelper.GetString("Checkout.OrderNote.FreeCourse", LocalizationContext.CurrentCulture.CultureCode);
                order.Update();

                if (studentID == 0) studentID = ClassMemberDependencies.ContactRepo.MapUserToStudent(user).ContactID;

                ClassMemberDependencies.StudentClassRepo.CreateStudentClass(studentID, classID, "", order.OrderInvoiceNumber);
            }            

            return Redirect(returnUrl);
        }
    }
}