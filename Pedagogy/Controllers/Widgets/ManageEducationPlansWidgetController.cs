﻿using CMS.Membership;
using CMS.SiteProvider;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.ManageEducationPlansWidget;

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.MANAGE_EDUCATION_PLANS_WIDGET_IDENTIFIER,
    typeof(ManageEducationPlansWidgetController),
    "Manage Education Plans Widget",
    Description = "A widget used to display all the data that an admin can manage for education plans",
    IconClass = "icon-clipboard-list")]

namespace Pedagogy.Controllers.Widgets
{
    public class ManageEducationPlansWidgetController : WidgetController<ManageEducationPlansWidgetProperties>
    {
        private ClassMembers db = new ClassMembers();

        public ActionResult Index()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var contact = db.Contacts.Where(c => c.ContactUserID == user.UserID).Where(c => c.ContactTypeID != studentType).FirstOrDefault();
            var facilityItems = new List<SelectListItem>();

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            var facilities = ClassMembersHelper.GetFacilitiesForCurrentUser(isSiteAdmin, isCorpAdmin, user.UserID, "");
            foreach (var item in facilities)
            {
                var facility = db.Facilities.Where(f => f.FacilityID == item.FacilityID).FirstOrDefault();

                facilityItems.Add(new SelectListItem
                {
                    Value = facility.FacilityID.ToString(),
                    Text = facility.Name
                });
            }            

            var availClasses = ClassMembersHelper.GetAvailableClasses();

            return PartialView(ComponentIdentifiers.MANAGE_EDUCATION_PLANS_WIDGET_VIEW, ManageEducationPlansWidgetViewModel.GetViewModel(facilityItems, availClasses, GetProperties().CardHeading));
        }

        [HttpGet]
        public JsonResult GetAddClassesPagedData(int pageNumber = 1, int pageSize = 10)
        {
            var pagedData = PagingHelper.PagedResult(ClassMembersHelper.GetAvailableClasses(), pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddClasses()
        {

            return Json(JsonRequestBehavior.AllowGet);
        }
    }
}