﻿using CMS.DocumentEngine;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using CMS.MembershipProvider;
using CMS.SiteProvider;
using Kentico.Membership;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.LicensingInfoWidget;
using Pedagogy.Models.Widgets.UpdateStaffInfoWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.UPDATE_STAFF_INFO_WIDGET_IDENTIFIER,
    typeof(UpdateStaffInfoWidgetController),
    "Update Staff Info Widget",
    Description = "A widget used to update staff info",
    IconClass = "icon-i-circle")]
namespace Pedagogy.Controllers.Widgets
{
    public class UpdateStaffInfoWidgetController : WidgetController<UpdateStaffInfoWidgetProperties>
    {
        public UserManager UserManager => HttpContext.GetOwinContext().Get<UserManager>();

        public ActionResult Index(int studentID = 0, int facilityID = 0)
        {
            var cardHeading = GetProperties().CardHeading;

            if (HttpContext.Kentico().PageBuilder().EditMode)
            {
                return PartialView(ComponentIdentifiers.UPDATE_STAFF_INFO_WIDGET_VIEW, UpdateStaffInfoWidgetViewModel.GetViewModel(cardHeading));
            }

            if (studentID == 0 || facilityID == 0) return Redirect(ContentIdentifiers.NOT_FOUND);

            var facility = ClassMembersHelper.GetFacility(facilityID, x => x);
            var student = ClassMembersHelper.GetContact(studentID);

            var notApplicableType = ClassMembersHelper.GetLicenseTypeID(ContentIdentifiers.LT_NA_CODE_NAME);

            if(facility != null && student != null)
            {
                var currentLicenses = ClassMembersHelper.GetStudentLicenses(student.ContactID, x => x);

                bool notApplicable = false;
                currentLicenses.ForEach(x => { if (x.LicenseTypeID == notApplicableType) notApplicable = true; });

                var lt = ClassMembersHelper.GetFilteredLicenseTypes(new List<string> { ContentIdentifiers.LT_NA_CODE_NAME }).ToList();

                var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
                var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
                var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

                var facilities = ClassMembersHelper.GetFacilitiesForCurrentUser(isSiteAdmin, isCorpAdmin, user.UserID, "");
                var returnUrl = GetPage<TreeNode>().NodeAliasPath + $"?studentID={studentID}&facilityID={facilityID}";

                return PartialView(ComponentIdentifiers.UPDATE_STAFF_INFO_WIDGET_VIEW, 
                    UpdateStaffInfoWidgetViewModel.GetViewModel(cardHeading, facility, student, facilities, returnUrl, LicensingInfoWidgetViewModel.GetViewModel(notApplicable, studentID, lt, currentLicenses)));
            }
            
            return PartialView(ComponentIdentifiers.UPDATE_STAFF_INFO_WIDGET_VIEW, UpdateStaffInfoWidgetViewModel.GetViewModel(cardHeading));
        }

        [HttpPost]
        public void UpdateStudentInformation(UpdateStaffInfoWidgetViewModel model)
        {
            var student = ClassMembersHelper.GetContact(model.ContactModel.ContactID);

            UserInfo user = null;
            if (student.ContactUserID.HasValue) user = UserInfoProvider.GetUserInfo(student.ContactUserID.Value);

            ClassMembersHelper.UpdateContact(student, model.ContactModel, student.ContactUserID.Value);

            if (user != null)
            {
                user.SetValue(nameof(UserInfo.Email), model.ContactModel.Email);
                user.Update();
            }

            if (model.FacilityID != model.ContactModel.FacilityID)
            {
                var contactFacility = ClassMembersHelper.GetContactFacility(model.ContactModel.ContactID, model.ContactModel.FacilityID);
                ClassMembersHelper.RemoveContactFacility(contactFacility);

                ClassMembersHelper.CreateContactFacility(model.ContactModel.ContactID, model.FacilityID, true);
            }
        }

        [HttpPost]
        public JsonResult ChangeStudentPassword(int userID, bool defaultPassword, string newPassword = "", string confirmPassword = "")
        {
            var user = UserInfoProvider.GetUserInfo(userID);
            var password = new CMSMembershipProvider().ResetPassword(user.UserName, "");
            var defaultPass = ResHelper.GetString("Dashboard.DefaultStudentPassword", LocalizationContext.CurrentCulture.CultureCode);

            var msgs = new List<string>();
            IdentityResult resetResult;
            if (!defaultPassword)
            {
                resetResult = UserManager.ChangePassword(user.UserID, password, newPassword);
            }
            else
            {
                resetResult = UserManager.ChangePassword(user.UserID, password, defaultPass);

                var data = user.UserSettings.UserCustomData.GetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
                if (data != null)
                {
                    user.UserSettings.UserCustomData.Remove(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
                    user.Update();
                }
            }

            if (!resetResult.Succeeded)
            {
                foreach (string error in resetResult.Errors)
                {
                    msgs.Add(error);
                }
                return Json(msgs, JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void SaveUsersName(int userID, ContactViewModel contactModel)
        {
            var user = UserInfoProvider.GetUserInfo(userID);

            user.SetValue(nameof(UserInfo.FirstName), contactModel.FirstName);
            user.SetValue(nameof(UserInfo.LastName), contactModel.LastName);
            user.SetValue(nameof(UserInfo.Enabled), contactModel.Enabled);

            user.Update();
        }

        public void AddNotApplicableLicense(int studentID)
        {
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var notApplicableType = ClassMembersHelper.GetLicenseTypeID(ContentIdentifiers.LT_NA_CODE_NAME);

            var currentLicenses = ClassMembersHelper.GetStudentLicenses(studentID, x => x);

            if (currentLicenses.Count() == 0)
            {
                ClassMembersHelper.CreateStudentLicense(studentID, notApplicableType, "", "");
            }
            else
            {
                currentLicenses.ForEach(x => { if (x.LicenseTypeID != notApplicableType) ClassMembersHelper.CreateStudentLicense(studentID, notApplicableType, "", ""); });
            }
        }

        public void RemoveNotApplicableLicense(int studentID)
        {
            var studentType = ClassMembersHelper.GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);
            var notApplicableType = ClassMembersHelper.GetLicenseTypeID(ContentIdentifiers.LT_NA_CODE_NAME);

            var contact = ClassMembersHelper.GetContact(studentID, studentType);
            var currentLicenses = ClassMembersHelper.GetStudentLicenses(contact.ContactID, x => x);

            currentLicenses.ForEach(x => { if (x.LicenseTypeID == notApplicableType) ClassMembersHelper.RemoveStudentLicense(x); });
        }

        [HttpPost]
        public ActionResult AddNewLicense(string returnUrl, ContactViewModel contactModel, LicensingInfoWidgetViewModel licensingData)
        {
            if (licensingData.StudentLicenseID != 0)
            {
                ClassMembersHelper.UpdateStudentLicense(
                    licensingData.StudentLicenseID, contactModel.ContactID, licensingData.LicenseTypeID, licensingData.LicenseNum, licensingData.StateID);
            }
            else
            {

                ClassMembersHelper.CreateStudentLicense(licensingData.StudentID, licensingData.LicenseTypeID, licensingData.LicenseNum,
                    licensingData.StateID != 0 ? StateInfoProvider.GetStateInfo(licensingData.StateID).StateCode : "");
            }

            return Redirect(returnUrl);
        }

        public ActionResult DeleteLicense(string returnUrl, int licenseID, int studentID)
        {
            var license = ClassMembersHelper.GetStudentLicense(licenseID, studentID, x => x);
            ClassMembersHelper.RemoveStudentLicense(license);

            return Redirect(returnUrl);
        }
    }
}