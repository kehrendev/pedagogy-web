﻿using CMS.DocumentEngine;
using CMS.Ecommerce;
using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Newtonsoft.Json;

using Pedagogy;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.PersonalInfoWidget;

using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.PERSONAL_INFO_WIDGET_IDENTIFIER,
    typeof(PersonalInfoWidgetController),
    "Personal Info Widget",
    Description = "A widget used to display the current users personal details",
    IconClass = "icon-user")]

namespace Pedagogy.Controllers.Widgets
{
    public class PersonalInfoWidgetController : WidgetController<PersonalInfoWidgetProperties>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public PersonalInfoWidgetController(IClassMemberDependencies classMemberDependencies) { ClassMemberDependencies = classMemberDependencies; }

        public ActionResult Index()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            CustomerInfo customer = CustomerInfoProvider.GetCustomerInfoByUserID(user.UserID);
            if (customer == null)
            {
                customer = CustomerHelper.MapToCustomer(user);
            }

            var returnUrl = GetPage<TreeNode>().NodeAliasPath;

            return PartialView(ComponentIdentifiers.PERSONAL_INFO_WIDGET_VIEW, PersonalInfoWidgetViewModel.GetViewModel(GetProperties().CardHeading, customer, user, returnUrl));
        }

        [HttpPost]
        public void UpdateCustomerInfo(PersonalInfoWidgetViewModel model)
        {
            var contacts = ClassMemberDependencies.ContactRepo.GetContacts(model.UserID, x => new ContactDto { ContactEmail = x.ContactEmail, ContactPhone = x.ContactPhone, ContactID = x.ContactID });

            contacts.ForEach(x => { ClassMemberDependencies.ContactRepo.UpdateContactEmailPhone(x.ContactID, model.Email, model.PhoneNumber); });

            var user = UserInfoProvider.GetUserInfo(model.UserID);
            user.UserSettings.SetValue(nameof(UserSettingsInfo.UserPhone), model.PhoneNumber);
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.Update();

            var customer = CustomerInfoProvider.GetCustomerInfo(model.CustomerId);

            customer.CustomerFirstName = model.FirstName;
            customer.CustomerLastName = model.LastName;
            customer.CustomerEmail = model.Email;
            customer.CustomerPhone = model.PhoneNumber;

            CustomerInfoProvider.SetCustomerInfo(customer);
            CustomerInfoProvider.CopyDataFromCustomerToUser(customer, customer.CustomerUser);
        }

        [HttpPost]
        public JsonResult GetAddressToUpdate(int addressID, int customerID)
        {
            var address = AddressInfoProvider.GetAddressInfo(addressID);

            var model = new PersonalInfoWidgetViewModel
            {
                AddressID = address.AddressID,
                CustomerId = customerID,
                Line1 = address.AddressLine1,
                Line2 = address.AddressLine2,
                City = address.AddressCity,
                PostalCode = address.AddressZip,
                CountryID = address.AddressCountryID,
                StateID = address.AddressStateID
            };

            return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateAddressInfo(PersonalInfoWidgetViewModel model)
        {
            var customer = CustomerInfoProvider.GetCustomerInfo(model.CustomerId);

            AddressInfo address;

            if (model.AddressID == 0)
            {
                address = new AddressInfo
                {
                    AddressLine1 = model.Line1,
                    AddressLine2 = model.Line2,
                    AddressCity = model.City,
                    AddressZip = model.PostalCode,
                    AddressCountryID = model.CountryID,
                    AddressStateID = model.StateID,
                    AddressCustomerID = model.CustomerId,
                    AddressPhone = customer.CustomerPhone,
                    AddressPersonalName = $"{customer.CustomerFirstName} {customer.CustomerLastName}",
                    AddressName = $"{customer.CustomerFirstName} {customer.CustomerLastName}, {model.Line1}"
                };
                if (model.Line2 != "")
                {
                    address.AddressName += $", {model.Line2}";
                }
                address.AddressName += $", {model.City}";

                AddressInfoProvider.SetAddressInfo(address);
            }
            else
            {
                address = AddressInfoProvider.GetAddressInfo(model.AddressID);
                address.AddressLine1 = model.Line1;
                address.AddressLine2 = model.Line2;
                address.AddressCity = model.City;
                address.AddressZip = model.PostalCode;
                address.AddressCountryID = model.CountryID;
                address.AddressStateID = model.StateID;
                address.AddressCustomerID = model.CustomerId;
                address.AddressPhone = customer.CustomerPhone;
                address.AddressPersonalName = $"{customer.CustomerFirstName} {customer.CustomerLastName}";
                address.AddressName = $"{customer.CustomerFirstName} {customer.CustomerLastName}, {model.Line1}";
                if (model.Line2 != "")
                {
                    address.AddressName += $", {model.Line2}";
                }
                address.AddressName += $", {model.City}";

                AddressInfoProvider.SetAddressInfo(address);
            }

            return Redirect(model.ReturnUrl);
        }

        public ActionResult RemoveAddress(string returnUrl, int AddressID)
        {
            AddressInfoProvider.DeleteAddressInfo(AddressInfoProvider.GetAddressInfo(AddressID));

            return Redirect(returnUrl);
        }
    }
}