﻿using CMS.DocumentEngine;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.LicenseTypes;
using Pedagogy.ClassMembers.Entities.StudentLicense;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.LicensingInfoWidget;

using System.Collections.Generic;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.LICENSING_INFO_WIDGET_IDENTIFIER,
    typeof(LicensingInfoWidgetController),
    "Licensing Info Widget",
    Description = "A widget used to allow the current user to update their licensing info",
    IconClass = "icon-id-card")]

namespace Pedagogy.Controllers.Widgets
{
    public class LicensingInfoWidgetController : WidgetController<LicensingInfoWidgetProperites>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public LicensingInfoWidgetController(IClassMemberDependencies classMemberDependencies) { ClassMemberDependencies = classMemberDependencies; }

        public ActionResult Index()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var contact = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });
            var notApplicableType = ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeIDByCodeName(ContentIdentifiers.LT_NA_CODE_NAME);

            if (contact != null)
            {
                var currentLicenses = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(contact.ContactID, 
                    x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));

                bool notApplicable = false;
                currentLicenses.ForEach(x => { if (x.LicenseTypeID == notApplicableType) notApplicable = true; });

                var licenseTypes = ClassMemberDependencies.LicenseTypeRepo.GetFilteredLicenseTypes(new List<string> { ContentIdentifiers.LT_NA_CODE_NAME }, x => new LicenseTypeDto(x));

                return PartialView(ComponentIdentifiers.LICENSING_INFO_WIDGET_VIEW, 
                    LicensingInfoWidgetViewModel.GetViewModel(notApplicable, contact.ContactID, licenseTypes, currentLicenses, GetPage<TreeNode>().NodeAliasPath, GetProperties().CardHeading));
            }
            else
            {
                return PartialView(ComponentIdentifiers.LICENSING_INFO_WIDGET_VIEW, 
                    LicensingInfoWidgetViewModel.GetViewModel(ResHelper.GetString("Dashboard.NotAStudentWarningMsg.AddLicense", LocalizationContext.CurrentCulture.CultureCode)));
            }
        }

        [HttpPost]
        public ActionResult AddNewLicense(LicensingInfoWidgetViewModel model)
        {
            if(model.StudentLicenseID != 0)
            {
                ClassMemberDependencies.StudentLicenseRepo.UpdateStudentLicense(model.StudentLicenseID, model.LicenseTypeID, model.LicenseNum,
                    model.StateID != 0 ? StateInfoProvider.GetStateInfo(model.StateID).StateCode : "");
            }
            else
            {
                ClassMemberDependencies.StudentLicenseRepo.CreateStudentLicense(model.StudentID, model.LicenseTypeID, model.LicenseNum,
                    model.StateID != 0 ? StateInfoProvider.GetStateInfo(model.StateID).StateCode : "");
            }

            return Redirect(model.ReturnUrl);
        }

        public void AddNotApplicableLicense()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var contact = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });
            var notApplicableType = ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeIDByCodeName(ContentIdentifiers.LT_NA_CODE_NAME);

            var currentLicenses = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(contact.ContactID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));

            if (currentLicenses.Count == 0)
            {
                ClassMemberDependencies.StudentLicenseRepo.CreateStudentLicense(contact.ContactID, notApplicableType, "", "");
            }
            else
            {
                currentLicenses.ForEach(x =>
                {
                    if(x.LicenseTypeID != notApplicableType)
                    {
                        ClassMemberDependencies.StudentLicenseRepo.CreateStudentLicense(contact.ContactID, x.LicenseTypeID, "", "");
                    }
                });
            }
        }

        public void RemoveNotApplicableLicense()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var contact = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });
            var notApplicableType = ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeIDByCodeName(ContentIdentifiers.LT_NA_CODE_NAME);

            var currentLicenses = ClassMemberDependencies.StudentLicenseRepo.GetStudentLicenses(contact.ContactID, 
                x => new StudentLicenseDto(x, ClassMemberDependencies.LicenseTypeRepo.GetLicenseTypeDisplayName(x.LicenseTypeID)));
            currentLicenses.ForEach(x => { if (x.LicenseTypeID == notApplicableType) ClassMemberDependencies.StudentLicenseRepo.RemoveStudentLicense(x.StudentLicenseID); });
        }

        public ActionResult DeleteLicense(string returnUrl, int licenseID, int studentID)
        {
            ClassMemberDependencies.StudentLicenseRepo.RemoveStudentLicense(licenseID);

            return Redirect(returnUrl);
        }
    }
}