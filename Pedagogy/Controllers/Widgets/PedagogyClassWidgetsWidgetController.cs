﻿using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.PedagogyClassWidgetsWidget;

using System.Collections.Generic;
using System.Web.Mvc;

[assembly:RegisterWidget(
    ComponentIdentifiers.PEDAGOGY_CLASS_WIDGETS_WIDGET_IDENTIFIER,
    typeof(PedagogyClassWidgetsWidgetController),
    "Pedagogy Class Widgets Widget",
    Description = "A widget to generate scripts for the Pedagogy Class Widgets",
    IconClass = "icon-cogwheels")]

namespace Pedagogy.Controllers.Widgets
{
    public class PedagogyClassWidgetsWidgetController : WidgetController<PedagogyClassWidgetsWidgetProperties>
    {
        public ActionResult Index()
        {
            var model = new PedagogyClassWidgetsWidgetViewModel
            {
                WidgetTypes = new List<SelectListItem>()
                {
                    new SelectListItem
                    {
                        Text = "Class Listing",
                        Value = "classListing",
                        Selected = true
                    },
                    new SelectListItem
                    {
                        Text = "Featured Class Scroller",
                        Value = "featuredClassScroller",
                        Selected = false
                    }
                },
                ColorStyle = "Red"
            };

            return PartialView(ComponentIdentifiers.PEDAGOGY_CLASS_WIDGETS_WIDGET_VIEW, model);
        }
    }
}