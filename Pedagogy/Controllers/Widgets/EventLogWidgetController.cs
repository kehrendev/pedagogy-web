﻿using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.EventLog;
using Pedagogy.ClassMembers.Entities.EventLogType;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Widgets.EventLogWidget;

using System;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
        ComponentIdentifiers.EVENT_LOG_WIDGET_IDENTIFIER,
        typeof(EventLogWidgetController),
        "Event Log Widget",
        Description = "A widget to display the event log",
        IconClass = "icon-list")]

namespace Pedagogy.Controllers.Widgets
{
    public class EventLogWidgetController : WidgetController<EventLogWidgetProperties>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public EventLogWidgetController(IClassMemberDependencies classMemberDependencies) { ClassMemberDependencies = classMemberDependencies; }

        public ActionResult Index()
        {
            var props = GetProperties();

            var allEvents = ClassMemberDependencies.EventLogRepo.GetAllEvents(x => new EventLogDto(x) { EventLogTypeName = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeNameByID(x.EventLogTypeID) });

            var filterModel = new EventLogFilterViewModel
            {
                Exceptions = new SelectList(
                    ClassMemberDependencies.EventLogTypeRepo.GetAllEventLogTypes(x => new EventLogTypeDto { EventLogTypeID = x.EventLogTypeID, EventLogTypeDisplayName = x.EventLogTypeName }), 
                    nameof(EventLogTypeDto.EventLogTypeID), 
                    nameof(EventLogTypeDto.EventLogTypeDisplayName))
            };

            return PartialView(ComponentIdentifiers.EVENT_LOG_WIDGET_VIEW, EventLogWidgetViewModel.GetViewModel(allEvents.ToList(), props.EventsPerPage, filterModel, props.CardHeading));
        }

        [HttpGet]
        public JsonResult GetPagedData(EventLogWidgetViewModel model, int pageNumber = 1, int pageSize = 15) 
        {
            var events = ClassMemberDependencies.EventLogRepo.GetAllEvents(x => new EventLogDto(x) { EventLogTypeName = ClassMemberDependencies.EventLogTypeRepo.GetEventLogTypeNameByID(x.EventLogTypeID) })
                .OrderByDescending(e => e.EventCreatedOn).AsEnumerable();

            if (model.FilterModel != null)
            {
                if (model.FilterModel.FromDate != new DateTime()) events = events.Where(e => e.EventCreatedOn >= model.FilterModel.FromDate);
                if (model.FilterModel.ToDate != new DateTime()) events = events.Where(e => e.EventCreatedOn <= model.FilterModel.ToDate);
                if (model.FilterModel.ExceptionID != 0) events = events.Where(e => e.EventLogTypeID == model.FilterModel.ExceptionID);
                if (!string.IsNullOrEmpty(model.FilterModel.OrganizationName)) events = events.Where(x => !string.IsNullOrEmpty(x.OrganizationName) && x.OrganizationName.ToLower().Contains(model.FilterModel.OrganizationName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.CourseName)) events = events.Where(e => !string.IsNullOrEmpty(e.CourseName) && e.CourseName.ToLower().Contains(model.FilterModel.CourseName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.UsersName)) events = events.Where(e => e.UsersName.ToLower().Contains(model.FilterModel.UsersName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.UsersUserName)) events = events.Where(e => e.UsersUserName.ToLower().Contains(model.FilterModel.UsersUserName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.StudentName)) events = events.Where(e => e.StudentName.ToLower().Contains(model.FilterModel.StudentName.ToLower()));
                if (!string.IsNullOrEmpty(model.FilterModel.StudentUserName)) events = events.Where(e => e.StudentUserName.ToLower().Contains(model.FilterModel.StudentUserName.ToLower()));
            }

            var pagedData = PagingHelper.PagedResult(events.ToList(), pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteEvent(string returnUrl, int eventID)
        {
            ClassMemberDependencies.EventLogRepo.DeleteEvent(eventID);

            return Redirect(returnUrl);
        }

        [HttpPost]
        public ActionResult DeleteAllEvents(string returnUrl)
        {
            ClassMemberDependencies.EventLogRepo.DeleteAllEvents();

            return Redirect(returnUrl);
        }
    }
}