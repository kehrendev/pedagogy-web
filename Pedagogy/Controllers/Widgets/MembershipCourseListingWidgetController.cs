﻿using CMS.DocumentEngine;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Models.Widgets.MembershipCourseListingWidget;

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.MEMBERSHIP_COURSE_lISTING_WIDGET_IDENTIFIER,
    typeof(MembershipCourseListingWidgetController),
    "Membership Course Listing Widget",
    Description = "Displays a listing of all the courses in a facility membership based on the selected category",
    IconClass = "icon-list-bullets")]

namespace Pedagogy.Controllers.Widgets
{
    public class MembershipCourseListingWidgetController : WidgetController<MembershipCourseListingWidgetProperties>
    {
        private readonly IDataDependencies Dependencies;

        public MembershipCourseListingWidgetController(IDataDependencies dataDependencies) { Dependencies = dataDependencies; }

        public ActionResult Index()
        {
            var props = GetProperties();

            List<TreeNode> items = new List<TreeNode>();
            if (!string.IsNullOrEmpty(props.MembershipCategory))
            {
                items = Dependencies.NavigationRepo.GetPagesByCategory(new string[1] { props.MembershipCategory }).OrderBy(x => x.DocumentName).ToList();
            }

            var model = new MembershipCourseListingWidgetViewModel
            {
                MembershipClasses = items,
                ColumnCount = int.Parse(props.ColumnCount)
            };

            return PartialView(ComponentIdentifiers.MEMBERSHIP_COURSE_lISTING_WIDGET_VIEW, model);
        }
    }
}