﻿using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using Kentico.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.ChangePasswordWidget;

[assembly: RegisterWidget(
    ComponentIdentifiers.CHANGE_PASSWORD_WIDGET_IDENTIFIER,
    typeof(ChangePasswordWidgetController),
    "Change Password Widget",
    Description = "A widget used to allow the user to change their password",
    IconClass = "icon-rotate-left")]

namespace Pedagogy.Controllers.Widgets
{
    public class ChangePasswordWidgetController : WidgetController<ChangePasswordWidgetProperties>
    {
        public UserManager UserManager => HttpContext.GetOwinContext().Get<UserManager>();

        public ActionResult Index()
        {
            return PartialView(ComponentIdentifiers.CHANGE_PASSWORD_WIDGET_VIEW, ChangePasswordWidgetViewModel.GetViewModel(GetProperties().CardHeading));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RequestPasswordReset(ChangePasswordWidgetViewModel model)
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var msgs = new List<string>();

            var resetResult = UserManager.ChangePassword(user.UserID, model.CurrentPassword, model.NewPassword);

            if (!resetResult.Succeeded)
            {
                foreach (string error in resetResult.Errors)
                {
                    msgs.Add(error);
                }
                return Json(msgs, JsonRequestBehavior.AllowGet);
            }

            var data = user.UserSettings.UserCustomData.GetValue(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
            if (data != null)
            {
                user.UserSettings.UserCustomData.Remove(ContentIdentifiers.IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN);
                user.Update();
            }

            msgs.Add(ResHelper.GetString("ChangePasswordWidget.SuccessMsg", LocalizationContext.CurrentCulture.CultureCode));
            return Json(msgs, JsonRequestBehavior.AllowGet);
        }
    }
}