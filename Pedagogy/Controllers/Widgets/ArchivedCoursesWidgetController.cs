﻿using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.ClassBuilder.DependencyInjection;
using Pedagogy.ClassBuilder.Entities.Class;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.StudentClass;
using Pedagogy.ClassMembers.Entities.StudentTestAttempt;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.ArchivedCoursesWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.ARCHIVED_COURSES_WIDGET_IDENTIFIER,
    typeof(ArchivedCoursesWidgetController),
    "Archived Courses Widget",
    Description = "A widget used to display all of the current users archived courses",
    IconClass = "icon-folder-opened")]

namespace Pedagogy.Controllers.Widgets
{
    public class ArchivedCoursesWidgetController : WidgetController<ArchivedCoursesWidgetProperties>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;
        private readonly IClassBuilderDependencies ClassBuilderDependencies;

        public ArchivedCoursesWidgetController(IClassMemberDependencies classMemberDependencies, IClassBuilderDependencies classBuilderDependencies) 
        {
            ClassMemberDependencies = classMemberDependencies; 
            ClassBuilderDependencies = classBuilderDependencies;
        }

        public ActionResult Index()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });

            var courses = new List<StudentClassViewModel>();
            if (student != null)
            {
                var archivedWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.ARCHIVED_WF_CODE_NAME);
                var archived = ClassMemberDependencies.StudentClassRepo.GetStudentClassesByStudentID(student.ContactID, x => x.ClassWorkflowID == archivedWF, x => new StudentClassDto(x));

                foreach(var item in archived)
                {
                    var attemptUrls = new Dictionary<int,string>();
                    if (item.TestAttemptID != null)
                    {
                        var attempts = ClassMemberDependencies.StudentTestAttemptRepo.GetStudentTestAttempts(item.StudentClassID, 
                            x => new StudentTestAttemptDto { StudentClassID = x.StudentClassID, TestAttemptID = x.TestAttemptID });

                        if (attempts.Count() > 0)
                        {
                            foreach (var i in attempts)
                            {
                                attemptUrls.Add(attempts.IndexOf(i) + 1, ClassMembersHelper.BuildTestAttemptUrl(i.StudentClassID, i.TestAttemptID));
                            }
                        }
                    }

                    var courseData = ClassBuilderDependencies.ClassRepo.GetClass(item.ClassID, x => new ClassDto { ClassName = x.ClassName, CEUValue = x.CEUValue });

                    courses.Add(new StudentClassViewModel { 
                        ClassName = courseData.ClassName,
                        ContactHours = courseData.CEUValue.Value,
                        ClassPageUrl = ClassMembersHelper.BuildCourseUrl(item.StudentClassID, 1, 1),
                        StudentClassID = item.StudentClassID,
                        StudentID = item.StudentID,
                        TestScore = Math.Round(item.TestScore.Value, 2),
                        DateCompletedOn = item.TestCompletedOn.Value.ToString("MM/dd/yyyy"),
                        PrintCertUrl = ClassMembersHelper.BuildPrintCertUrl(item.StudentClassID, item.CertID.Value),
                        AttemptUrls = attemptUrls
                    });
                }
            }

            return PartialView(ComponentIdentifiers.ARCHIVED_COURSES_WIDGET_VIEW, ArchivedCoursesWidgetViewModel.GetViewModel(courses, GetProperties().CardHeading));
        }
    }
}