﻿using Kentico.PageBuilder.Web.Mvc;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.DocumentEngine;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.SliderWidget;

using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;

[assembly: RegisterWidget(
    ComponentIdentifiers.SLIDER_WIDGET_IDENTIFIER,
    typeof(SliderWidgetController),
    "Slider Widget",
    Description = "This displays a slider widget",
    IconClass = "icon-carousel"
    )]

namespace Pedagogy.Controllers.Widgets
{
    public class SliderWidgetController : WidgetController<SliderWidgetProperties>
    {
        public ActionResult Index()
        {
            var props = GetProperties();

            List<Slide> slides = new List<Slide>();
            if(props.SliderItem != null)
            {
                slides = SlideProvider.GetSlides()
                                          .Path(props.SliderItem?.FirstOrDefault()?.NodeAliasPath, PathTypeEnum.Children)
                                          .OnCurrentSite()
                                          .Published()
                                          .PublishedVersion()
                                          .ToList();
            }

            var model = new SliderWidgetViewModel
            {
                Slides = slides,
                SliderType = props.SliderType,
                CardGroupHeading = props.CardGroupHeading
            };

            
            return PartialView(ComponentIdentifiers.SLIDER_WIDGET_VIEW, model);
        }
    }
}