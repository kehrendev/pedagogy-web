﻿using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.ContactFacility;
using Pedagogy.ClassMembers.Entities.Facility;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.StudentInfoWidget;

using System.Collections.Generic;
using System.Web.Mvc;

[assembly: RegisterWidget(
    ComponentIdentifiers.STUDENT_INFO_WIDGET_IDENTIFIER,
    typeof(StudentInfoWidgetController),
    "Student Info Widget",
    Description = "A widget used to allow the current user to update their student information",
    IconClass = "icon-picture")]

namespace Pedagogy.Controllers.Widgets
{
    public class StudentInfoWidgetController : WidgetController<StudentInfoWidgetProperties>
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public StudentInfoWidgetController(IClassMemberDependencies classMemberDependencies) { ClassMemberDependencies = classMemberDependencies; }

        public ActionResult Index()
        {
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            if(user != null)
            {
                int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
                var contact = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto(x));

                if (contact != null)
                {
                    var contactFacilities = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByContactID(contact.ContactID, x => new ContactFacilityDto(x));
                    var facilities = new List<FacilityDto>();

                    foreach(var item in contactFacilities)
                    {
                        var facility = ClassMemberDependencies.FacilityRepo.GetFacility(item.FacilityID, x => new FacilityDto(x));
                        if (facility != null) facilities.Add(facility);
                    }

                    return PartialView(ComponentIdentifiers.STUDENT_INFO_WIDGET_VIEW, StudentInfoWidgetViewModel.GetViewModel(GetProperties().CardHeading, contact));
                }
                else
                {
                    return PartialView(ComponentIdentifiers.STUDENT_INFO_WIDGET_VIEW, 
                        StudentInfoWidgetViewModel.GetViewModel(GetProperties().CardHeading, ResHelper.GetString("Dashboard.NotAStudentWarningMsg.UpdateStudentInfo", LocalizationContext.CurrentCulture.CultureCode)));
                }
            }

            return Redirect(ContentIdentifiers.LOGIN_PAGE);
        }

        [HttpPost]
        public void SaveStudentInfo(StudentInfoWidgetViewModel model)
        {
            ClassMemberDependencies.ContactRepo.UpdateContactFirstLastName(model.ContactID, model.FirstName, model.LastName);
        }
    }
}