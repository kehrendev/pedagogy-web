﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.InfoSectionWidget;

using System.Linq;
using System.Web.Mvc;


[assembly: RegisterWidget(
    ComponentIdentifiers.INFO_SECTION_WIDGET_IDENTIFIER,
    typeof(InfoSectionWidgetController),
    "Info Section Widget",
    Description = "Displays a widget containing an infomation section with an image either in a tabbed layout or one section",
    IconClass = "icon-l-text-col"
    )]

namespace Pedagogy.Controllers.Widgets
{
    public class InfoSectionWidgetController : WidgetController<InfoSectionWidgetProperties>
    {
        public ActionResult Index()
        {
            var props = GetProperties();
            var infoSections = InfoSectionProvider.GetInfoSections()
                                                .Path(props.InfoSectionItems?.FirstOrDefault()?.NodeAliasPath, PathTypeEnum.Children)
                                                .OnCurrentSite()
                                                .Published()
                                                .PublishedVersion()
                                                .ToList();
            
            var model = new InfoSectionWidgetViewModel 
            { 
                DisplayIdentifier = props.DisplayIdentifier,
                InfoSections = infoSections,
                IsTabbedContent = props.IsTabbedContent
            };

            return PartialView(ComponentIdentifiers.INFO_SECTION_WIDGET_VIEW, model);
        }
    }
}