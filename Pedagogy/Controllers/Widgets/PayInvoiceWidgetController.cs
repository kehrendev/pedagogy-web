﻿using CMS.Ecommerce;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using Kentico.PageBuilder.Web.Mvc;

using Pedagogy;
using Pedagogy.Controllers.Widgets;
using Pedagogy.Models.Widgets.PayInvoiceWidget;

using System;
using System.Linq;
using System.Web.Mvc;

[assembly:RegisterWidget(
    ComponentIdentifiers.PAY_INVOICE_WIDGET_IDENTIFIER,
    typeof(PayInvoiceWidgetController),
    "Pay Invoice Widget",
    Description = "A widget where facility admins can pay for an invoice",
    IconClass = "icon-money-bill")]

namespace Pedagogy.Controllers.Widgets
{
    public class PayInvoiceWidgetController : WidgetController<PayInvoiceWidgetProperties>
    {
        public ActionResult Index()
        {
            var cardHeading = GetProperties().CardHeading;

            var guid = Request.QueryString["orderGUID"];
            var orderGuid = !string.IsNullOrEmpty(guid) ? Guid.Parse(guid) : Guid.Empty;

            if (orderGuid == new Guid())
            {
                return PartialView(ComponentIdentifiers.PAY_INVOICE_WIDGET_VIEW,
                    PayInvoiceWidgetViewModel.GetViewModel(cardHeading, ResHelper.GetString("Dashboard.PayInvoiceWidget.MissingUrlParams", LocalizationContext.CurrentCulture.CultureCode), true, 0));
            }
            else
            {
                var order = OrderInfoProvider.GetOrders()
                    .WhereEquals(nameof(OrderInfo.OrderGUID), orderGuid)
                    .FirstOrDefault();

                if (order == null)
                {
                    return PartialView(ComponentIdentifiers.PAY_INVOICE_WIDGET_VIEW,
                        PayInvoiceWidgetViewModel.GetViewModel(cardHeading, ResHelper.GetString("Dashboard.PayInvoiceWidget.InvoiceNotFound", LocalizationContext.CurrentCulture.CultureCode), true, 0));
                }

                if (order.OrderIsPaid)
                {
                    return PartialView(ComponentIdentifiers.PAY_INVOICE_WIDGET_VIEW,
                        PayInvoiceWidgetViewModel.GetViewModel(cardHeading, ResHelper.GetString("Dashboard.PayInvoiceWidget.OrderAlreadyPaidFor", LocalizationContext.CurrentCulture.CultureCode), true, 0));
                }

                return PartialView(ComponentIdentifiers.PAY_INVOICE_WIDGET_VIEW,
                    PayInvoiceWidgetViewModel.GetViewModel(cardHeading, order.OrderInvoice, false, order.OrderID));
            }
        }

        //[HttpPost]
        //public ActionResult LookUpOrder(PayInvoiceWidgetViewModel model)
        //{
        //    var order = OrderInfoProvider.GetOrders()
        //        .WhereEquals(nameof(OrderInfo.OrderInvoiceNumber), model.InvoiceNumber)
        //        .FirstOrDefault();

        //    if (order == null)
        //    {
        //        return Json(ResHelper.GetString("Dashboard.PayInvoiceWidget.InvoiceNotFound", LocalizationContext.CurrentCulture.CultureCode), JsonRequestBehavior.AllowGet);
        //    }

        //    if (order.OrderIsPaid) return Json(new { invoice = order.OrderInvoice, paidForMsg = ResHelper.GetString("Dashboard.PayInvoiceWidget.OrderAlreadyPaidFor", LocalizationContext.CurrentCulture.CultureCode), orderID = order.OrderID }, JsonRequestBehavior.AllowGet);

        //    return Json(new { invoice = order.OrderInvoice, paidForMsg = "", orderID = order.OrderID }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult SendCardData(PayInvoiceWidgetViewModel model)
        {
            var order = OrderInfoProvider.GetOrderInfo(model.PaymentInfo.OrderId);
            var ccPaymentOptionID = PaymentOptionInfoProvider.GetPaymentOptionInfo(ContentIdentifiers.CREDIT_CARD_PAYMENT_METHOD, SiteContext.CurrentSiteName).PaymentOptionID;

            order.OrderPaymentOptionID = ccPaymentOptionID;
            order.Update();

            var expDate = $"{model.PaymentInfo.CardData.Month}/{model.PaymentInfo.CardData.Year}";
            model.PaymentInfo.CardData.ExpDate = expDate;

            TempData["cardModel"] = model.PaymentInfo;
            return RedirectToAction("ProcessPayment", "Payment", new { isFromWidget = true });
        }
    }
}