﻿using CMS.Taxonomy;
using Kentico.Forms.Web.Mvc;

using Pedagogy.Controllers.FormComponents;
using Pedagogy.Models.FormComponents.FacilityMembershipCategoryDropDownComponent;

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterFormComponent(
    FacilityMembershipCategoryDropDownComponent.IDENTIFIER,
    typeof(FacilityMembershipCategoryDropDownComponent), 
    "Drop-down with facility membership categories",
    IconClass = "icon-menu")]
namespace Pedagogy.Controllers.FormComponents
{
    public class FacilityMembershipCategoryDropDownComponent : SelectorFormComponent<FacilityMembershipCategoryDropDownComponentProperites>
    {
        public const string IDENTIFIER = "FacilityMembershipCategoryDropDownComponent";

        protected override IEnumerable<SelectListItem> GetItems()
        {
            var categories = CategoryInfoProvider.GetCategories()
                .Where(x => x.CategoryName.StartsWith("facilityMems."))
                .Select(x => new { x.CategoryName, x.CategoryDisplayName })
                .ToList();

            foreach(var item in categories)
            {
                var listItem = new SelectListItem()
                {
                    Value = item.CategoryName,
                    Text = item.CategoryDisplayName
                };

                yield return listItem;
            }
        }
    }
}