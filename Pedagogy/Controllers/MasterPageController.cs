﻿using CMS.Membership;

using Pedagogy.DataAccess.DependencyInjection;

using System.Web.Mvc;

namespace Pedagogy.Controllers
{
    public class MasterPageController : Controller
    {
        protected IDataDependencies Dependencies { get; }
        public MasterPageController(IDataDependencies dependencies)
        {
            Dependencies = dependencies;
        }

        public ActionResult Header()
        {
            var name = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var model = new Models.MasterPage.HeaderViewModel
            {
                MainNav = Dependencies.NavigationRepo.GetCategoryNavigation(ContentIdentifiers.NAV_MAIN_CATEGORY),
                AccountActionPages = Dependencies.NavigationRepo.GetCategoryNavigation(ContentIdentifiers.NAV_ACCOUNT_ACTIONS_CATEGORY),
                SocialLinks = Dependencies.SocialMediaRepo.GetSocialMediaItemsByPath(ContentIdentifiers.SOCIAL_MEDIA_ITEMS_FOLDER),
                CurrentUserFirstName =  name != null ? name.FirstName : ""
            };
            return View(model);
        }

        public ActionResult Footer()
        {
            var model = new Models.MasterPage.FooterViewModel
            {
                FooterNav = Dependencies.NavigationRepo.GetCategoryNavigation(ContentIdentifiers.NAV_FOOTER_CATEGORY),
                SocialLinks = Dependencies.SocialMediaRepo.GetSocialMediaItemsByPath(ContentIdentifiers.SOCIAL_MEDIA_ITEMS_FOLDER)
            };
            return View(model);
        }
    }
}