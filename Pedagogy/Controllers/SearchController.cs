﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using CMS.Search;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Dto;
using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.DataAccess.Dto.News;
using Pedagogy.DataAccess.Dto.Resources;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.Search;

namespace Pedagogy.Controllers
{
    public class SearchController : BaseController
    {
        public SearchController(IDataDependencies dependencies) : base(dependencies) {  }

        // Adds the smart search indexes that will be used to perform searches
        public static readonly string[] courseSearchIndex = new string[] { "coursesindex" };
        public static readonly string[] resourceSearchIndex = new string[] { "resourcesindex" };
        public static readonly string[] entireWebsiteIndex = new string[] { "entirewebsiteindex" };
        public static readonly string[] newsSearchIndex = new string[] { "newsindex" };

        // Sets the limit of items per page of search results
        private const int PAGE_SIZE = 500;

        [ValidateInput(false)]
        public ActionResult CourseSearch(string searchText)
        {
            // Displays the search page without any search results if the query is empty
            if (string.IsNullOrWhiteSpace(searchText))
            {
                // Creates a model representing empty search results
                SearchViewModel emptyModel = new SearchViewModel
                {
                    Items = new List<SearchResultItem>(),
                    Query = string.Empty
                };

                TempData["courseSearchModel"] = emptyModel;
                return RedirectToAction("Index", "Product");
            }

            // Searches the specified index and gets the matching results
            SearchParameters searchParameters = SearchParameters.PrepareForPages(searchText, courseSearchIndex, 1, PAGE_SIZE, MembershipContext.AuthenticatedUser, LocalizationContext.CurrentCulture.CultureCode, true);
            SearchResult searchResult = SearchHelper.Search(searchParameters);

            List<CourseItemDto> courses = new List<CourseItemDto>();

            foreach(var item in searchResult.Items)
            {
                var course = Dependencies.CourseRepo.GetCourseByTitle(item.Title);

                if(course != null) courses.Add(course);
            }

            // Creates a model with the search result items
            SearchViewModel model = new SearchViewModel
            {
                Items = searchResult.Items.OrderByDescending(x => x.Score),
                Query = searchText,
                CourseData = courses
            };

            TempData["courseSearchModel"] = model;
            return RedirectToAction("Index", "Product");
        }

        [ValidateInput(false)]
        public ActionResult ResourceSearch(string searchText)
        {
            // Displays the search page without any search results if the query is empty
            if (string.IsNullOrWhiteSpace(searchText))
            {
                // Creates a model representing empty search results
                SearchViewModel emptyModel = new SearchViewModel
                {
                    Items = new List<SearchResultItem>(),
                    Query = string.Empty
                };

                TempData["resourceSearchModel"] = emptyModel;
                return RedirectToAction("Index", "Resource");
            }

            // Searches the specified index and gets the matching results
            SearchParameters searchParameters = SearchParameters.PrepareForPages(searchText, resourceSearchIndex, 1, PAGE_SIZE, MembershipContext.AuthenticatedUser, LocalizationContext.CurrentCulture.CultureCode, true);
            SearchResult searchResult = SearchHelper.Search(searchParameters);

            List<ResourceItemDto> resources = new List<ResourceItemDto>();

            foreach (var item in searchResult.Items)
            {
                resources.Add(Dependencies.ResourceRepo.GetResourceByTitle(item.Title));
            }

            // Creates a model with the search result items
            SearchViewModel model = new SearchViewModel
            {
                Items = searchResult.Items.OrderByDescending(x => x.Score),
                Query = searchText,
                ResourceData = resources
            };

            TempData["resourceSearchModel"] = model;
            return RedirectToAction("Index", "Resource");
        }

        [ValidateInput(false)]
        public ActionResult NewsSearch(string searchText)
        {
            // Displays the search page without any search results if the query is empty
            if (string.IsNullOrWhiteSpace(searchText))
            {
                // Creates a model representing empty search results
                SearchViewModel emptyModel = new SearchViewModel
                {
                    Items = new List<SearchResultItem>(),
                    Query = string.Empty
                };

                TempData["newsSerachModel"] = emptyModel;
                return RedirectToAction("Index", "News");
            }

            // Searches the specified index and gets the matching results
            SearchParameters searchParameters = SearchParameters.PrepareForPages(searchText, newsSearchIndex, 1, PAGE_SIZE, MembershipContext.AuthenticatedUser, LocalizationContext.CurrentCulture.CultureCode, true);
            SearchResult searchResult = SearchHelper.Search(searchParameters);

            List<NewsItemDto> newsArticles = new List<NewsItemDto>();

            foreach (var item in searchResult.Items)
            {
                newsArticles.Add(Dependencies.NewsRepo.GetNewsArticleByTitle(item.Title));
            }

            // Creates a model with the search result items
            SearchViewModel model = new SearchViewModel
            {
                Items = searchResult.Items.OrderByDescending(x => x.Score),
                Query = searchText,
                NewsArticleData = newsArticles
            };

            TempData["newsSearchModel"] = model;
            return RedirectToAction("Index", "News");
        }

        [ValidateInput(false)]
        public ActionResult Index(string searchText)
        {
            SearchViewModel model = new SearchViewModel
            {
                Items = new List<SearchResultItem>(),
                Query = searchText
            };

            return View(GetBaseViewModel(new BaseDto 
            { 
                MetaTitle = ResHelper.GetString("Search.MainSiteSearch.MetaData.Title", LocalizationContext.CurrentCulture.CultureCode), 
                MetaDescription = ResHelper.GetString("Search.MainSiteSearch.MetaData.Description", LocalizationContext.CurrentCulture.CultureCode),
                MetaKeywords = ResHelper.GetString("Search.MainSiteSearch.MetaData.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                InfoBarIsVisible = true 
            }, 
            model));
        }

        [HttpGet]
        public ActionResult GetPagedData(int pageNumber = 1, int pageSize = 12, string searchText = "")
        {
            if (string.IsNullOrEmpty(searchText)) return Json("", JsonRequestBehavior.AllowGet);

            SearchParameters searchParameters = SearchParameters.PrepareForPages(searchText, entireWebsiteIndex, 1, PAGE_SIZE, MembershipContext.AuthenticatedUser, LocalizationContext.CurrentCulture.CultureCode, true);

            SearchResult searchResult = SearchHelper.Search(searchParameters);

            var pagedData = PagingHelper.PagedResult(
                searchResult.Items.Select(x => new
                {
                    x.Title,
                    Content = x.Content.Length > 500 ? string.Concat(x.Content.Substring(0, 500).Replace("<br />", "").Replace("<br/>", ""), ".....") : x.Content.Replace("<br />", "").Replace("<br/>", ""),
                    URL = x.Data.GetStringValue(nameof(TreeNode.NodeAliasPath), ""),
                    x.Score
                })
                .OrderByDescending(x => x.Score)
                .ToList(),
                pageNumber,
                pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }
    }
}