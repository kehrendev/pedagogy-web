﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Generic;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using Kentico.Membership;
using Kentico.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using CMS.EventLog;
using CMS.Ecommerce;
using CMS.Membership;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using CMS.DataEngine;
using CMS.CustomTables;
using CMS.CustomTables.Types.Pedagogy;
using CMS.DocumentEngine;

using DynamicRouting.Interfaces;

using Pedagogy.Models.LoginManager;
using Pedagogy.DataAccess.Dto;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Models;
using Pedagogy.Lib.Helpers;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;

using Newtonsoft.Json;
using Pedagogy.ClassMembers.Entities.Facility;

namespace Pedagogy.Controllers
{
    public class LoginManagerController : BaseController
    {
        private IDynamicRouteHelper mDynamicRouteHelper;
        private IShoppingService mShoppingService;
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public LoginManagerController(IDataDependencies dependencies, IShoppingService iss, IDynamicRouteHelper drh, IClassMemberDependencies classMemberDependencies) 
            : base(dependencies) 
        { 
            mShoppingService = iss; 
            mDynamicRouteHelper = drh;
            ClassMemberDependencies = classMemberDependencies;
        }

        //Provides access to the Kentico.Membership.SignInManager instance
        public SignInManager SignInManager => HttpContext.GetOwinContext().Get<SignInManager>();

        //Provides access to the Microsoft.Owin.Security.IAuthenticationManager instance.
        public IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        //Provides access to the Kentico.Membership.UserManager instance.
        public UserManager UserManager => HttpContext.GetOwinContext().Get<UserManager>();

        //Basic aciton that displays the sign-in form.
        public ActionResult Login()
        {
            string checkEmailMsg = (string)TempData["confirmEmailMsg"];

            var page = mDynamicRouteHelper.GetPage() as TreeNode;

            var model = GetBaseViewModel(new BaseDto(page),
            new LoginViewModel
            {
                CheckEmailMsg = !string.IsNullOrEmpty(checkEmailMsg) ? checkEmailMsg : ""
            });

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Login(BaseViewModel<LoginViewModel> model, string baseNodeJson, string returnUrl)
        {
            model.BaseNodeData = JsonConvert.DeserializeObject<BaseDto>(baseNodeJson);

            //Validates the received user credentials based on the view model
            if (!ModelState.IsValid)
            {
                //Displays the sign-in form if the user credentials are invalid
                return View(model);
            }

            //Attemps to authenticate the user against the Kentico database
            SignInStatus logInResult = SignInStatus.Failure;
            try
            {
                logInResult = await SignInManager.PasswordSignInAsync(model.ViewModel.UserName, model.ViewModel.Password, model.ViewModel.RememberMe, false);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("LogInManagerController", "ERROR", ex);
            }

            //If the authentication was not successful, displays the sign-in form with an "Authentication failed" message
            if (logInResult != SignInStatus.Success)
            {
                if (logInResult == SignInStatus.LockedOut)
                {
                    ModelState.AddModelError(String.Empty, ResHelper.GetString("LoginManager.WaitingForApprovalMsg", LocalizationContext.CurrentCulture.CultureCode, true));
                }
                if (logInResult == SignInStatus.Failure)
                {
                    ModelState.AddModelError(String.Empty, ResHelper.GetString("LoginManager.LoginFailureMsg", LocalizationContext.CurrentCulture.CultureCode, true));
                }
                return View(model);
            }

            var user = UserInfoProvider.GetUserInfo(model.ViewModel.UserName);
            user.LastLogon = DateTime.Now;
            user.Update();

            var customer = CustomerInfoProvider.GetCustomerInfoByUserID(user.UserID);
            if (customer == null)
            {
                customer = CustomerHelper.MapToCustomer(user);
            }
            mShoppingService.SetCustomer(customer);

            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });

            if (student != null)
            {
                if (CookieHelper.GetExistingCookie(ContentIdentifiers.STUDENT_ID_COOKIE) != null) CookieHelper.Remove(ContentIdentifiers.STUDENT_ID_COOKIE);

                HttpCookie cookie = new HttpCookie(ContentIdentifiers.STUDENT_ID_COOKIE)
                {
                    Value = student.ContactID.ToString()
                };

                Response.Cookies.Add(cookie);
            }

            //If authentication was successful, redirects to the return URL when possible or to a different default action
            var decodedReturnUrl = Server.UrlDecode(returnUrl);
            if (!string.IsNullOrEmpty(decodedReturnUrl) && Url.IsLocalUrl(decodedReturnUrl))
            {
                return Redirect(decodedReturnUrl);
            }

            return Redirect(ContentIdentifiers.DASHBOARD);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignOut()
        {
            // remove all custom data columns and cookies
            if (CookieHelper.GetValue(ContentIdentifiers.STUDENT_ID_COOKIE) != null) CookieHelper.Remove(ContentIdentifiers.STUDENT_ID_COOKIE);
            if (CookieHelper.GetValue(ContentIdentifiers.FACILITY_ID_COOKIE) != null) CookieHelper.Remove(ContentIdentifiers.FACILITY_ID_COOKIE);
            if (CookieHelper.GetValue(ContentIdentifiers.CURRENT_USERNAME_COOKIE) != null) CookieHelper.Remove(ContentIdentifiers.CURRENT_USERNAME_COOKIE);
            if (CookieHelper.GetValue(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE) != null) CookieHelper.Remove(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);

            var currentCart = mShoppingService.GetCurrentShoppingCart();
            currentCart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, out object mem);

            if (mem != null)
            {
                currentCart.ShoppingCartCustomData.Remove(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN);
                currentCart.Update();
            }

            currentCart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_QTY_CUSTOM_DATA_COLUMN, out object memPurchase);
            if (memPurchase != null)
            {
                currentCart.ShoppingCartCustomData.Remove(ContentIdentifiers.MEMBERSHIP_QTY_CUSTOM_DATA_COLUMN);
                currentCart.Update();
            }

            mShoppingService.RemoveAllItemsFromCart();

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        
            return Redirect(ContentIdentifiers.LOGIN_PAGE);
        }

        public ActionResult Register()
        {
            var page = mDynamicRouteHelper.GetPage() as TreeNode;

            HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

            var model = GetBaseViewModel(new BaseDto(page), new RegisterViewModel { });

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Register(BaseViewModel<RegisterViewModel> model, string baseNodeJson)
        {
            model.BaseNodeData = JsonConvert.DeserializeObject<BaseDto>(baseNodeJson);

            var page = mDynamicRouteHelper.GetPage();

            //Validates the received user data based on the view model
            if (!ModelState.IsValid)
            {
                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                return View(model);
            }

            var isCaptchaValid = await IsCaptchaValid(model.ViewModel.GoogleCaptchaToken);
            if (!isCaptchaValid)
            {
                ModelState.AddModelError(String.Empty, "The captcha is not valid");

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);
                return View(model);
            }

            //Prepares a new user entity using the posted registration data
            var user = new User
            {
                UserName = model.ViewModel.Email,
                Email = model.ViewModel.Email,
                FirstName = model.ViewModel.FirstName,
                LastName = model.ViewModel.LastName,
                Enabled = true
            };

            // Attempts to create the user in the Kentico database
            IdentityResult registerResult = IdentityResult.Failed();
            try
            {
                registerResult = await UserManager.CreateAsync(user, model.ViewModel.Password);
            }
            catch (Exception ex)
            {
                // Logs an error into the Kentico event log if the creation of the user fails
                EventLogProvider.LogException("LoginManagerController", "UserRegistration", ex);
                ModelState.AddModelError(String.Empty, "Unable to create this user");
            }

            //If the registration was not successful, displays the registration form with an error message
            if (!registerResult.Succeeded)
            {
                foreach (string error in registerResult.Errors)
                {
                    ModelState.AddModelError(String.Empty, error);
                }

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                return View(model);
            }

            var userInfo = UserInfoProvider.GetUserInfo(user.Id);
            userInfo.LastLogon = DateTime.Now;
            userInfo.Update();

            UserInfoProvider.AddUserToRole(user.UserName, ContentIdentifiers.STUDENT_ROLE, SiteContext.CurrentSiteName);

            var contact = ClassMemberDependencies.ContactRepo.MapUserToStudent(userInfo);

            ClassMemberDependencies.ContactRepo.AddHowToCourseToContact(contact.ContactID);

            HttpCookie cookie = new HttpCookie(ContentIdentifiers.STUDENT_ID_COOKIE)
            {
                Value = contact.ContactID.ToString()
            };

            Response.Cookies.Add(cookie);

            SignInManager.PasswordSignIn(user.Email, model.ViewModel.Password, false, false);

            return Redirect(ContentIdentifiers.DASHBOARD);
        }

        public ActionResult RegisterAsFacilityAdmin()
        {
            var page = mDynamicRouteHelper.GetPage() as TreeNode;

            HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

            var model = GetBaseViewModel(new BaseDto(page), new RegisterViewModel { });

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> RegisterAsFacilityAdmin(BaseViewModel<RegisterViewModel> model, string baseNodeJson)
        {
            try
            {
                model.BaseNodeData = JsonConvert.DeserializeObject<BaseDto>(baseNodeJson);

                var page = mDynamicRouteHelper.GetPage();

                //Validates the received user data based on the view model
                if (!ModelState.IsValid)
                {
                    HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);
                    return View(model);
                }

                var isCaptchaValid = await IsCaptchaValid(model.ViewModel.GoogleCaptchaToken);
                if (!isCaptchaValid)
                {
                    ModelState.AddModelError(string.Empty, "The captcha is not valid");

                    HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);
                    return View(model);
                }

                var state = StateInfoProvider.GetStateInfo(model.ViewModel.StateID).StateCode;

                var facility = ClassMemberDependencies.FacilityRepo.GetFacilityByAddress(model.ViewModel.CorporateInfo.AddressLine1, model.ViewModel.CorporateInfo.AddressLine2,
                    model.ViewModel.CorporateInfo.City, state, model.ViewModel.CorporateInfo.Zip, x => new FacilityDto(x));

                if (facility != null)
                {
                    ModelState.AddModelError(String.Empty, ResHelper.GetString("Registration.FacilityCorporationAlreadyExistsMsg", LocalizationContext.CurrentCulture.CultureCode));

                    HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);
                    return View(model);
                }

                //Prepares a new user entity using the posted registration data
                var user = new User
                {
                    UserName = model.ViewModel.Email,
                    Email = model.ViewModel.Email,
                    FirstName = model.ViewModel.FirstName,
                    LastName = model.ViewModel.LastName,
                    Enabled = false
                };

                // Attempts to create the user in the Kentico database
                IdentityResult registerResult = IdentityResult.Failed();
                try
                {
                    registerResult = await UserManager.CreateAsync(user, model.ViewModel.Password);
                }
                catch (Exception ex)
                {
                    // Logs an error into the Kentico event log if the creation of the user fails
                    EventLogProvider.LogException("LoginManagerController", "UserRegistration", ex);
                    ModelState.AddModelError(String.Empty, "Unable to create this user");
                }

                //If the registration was not successful, displays the registration form with an error message
                if (!registerResult.Succeeded)
                {
                    foreach (string error in registerResult.Errors)
                    {
                        ModelState.AddModelError(String.Empty, error);
                    }

                    HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);
                    return View(model);
                }

                //var newItem = CustomTableItem.New(nameof(FacilityWaitListItem.CLASS_NAME));
                var newItem = CustomTableItem.New("Pedagogy.FacilityWaitList");
                newItem.SetValue(nameof(FacilityWaitListItem.AdminUserID), user.Id);
                newItem.SetValue(nameof(FacilityWaitListItem.AdminFirstName), user.FirstName);
                newItem.SetValue(nameof(FacilityWaitListItem.AdminLastName), user.LastName);
                newItem.SetValue(nameof(FacilityWaitListItem.AdminEmail), user.Email);
                newItem.SetValue(nameof(FacilityWaitListItem.IsFacility), model.ViewModel.RegistrationType.Contains("facility"));
                newItem.SetValue(nameof(FacilityWaitListItem.FacilityName), model.ViewModel.CorporateInfo.Name);
                //newItem.SetValue(nameof(FacilityWaitListItem.FacilityAcctingCode), model.ViewModel.CorporateInfo.AcctingCode);
                newItem.SetValue(nameof(FacilityWaitListItem.FacilityAddress1), model.ViewModel.CorporateInfo.AddressLine1);
                newItem.SetValue(nameof(FacilityWaitListItem.FacilityAddress2), model.ViewModel.CorporateInfo.AddressLine2);
                newItem.SetValue(nameof(FacilityWaitListItem.FacilityCity), model.ViewModel.CorporateInfo.City);
                newItem.SetValue(nameof(FacilityWaitListItem.FacilityState), state);
                newItem.SetValue(nameof(FacilityWaitListItem.FacilityZip), model.ViewModel.CorporateInfo.Zip);
                newItem.SetValue(nameof(FacilityWaitListItem.ApprovalStatus), ContentIdentifiers.PENDING_STATUS);
                newItem.SetValue(nameof(FacilityWaitListItem.NoApplicableParentCorporation), model.ViewModel.NoApplicableCorporation);
                newItem.SetValue(nameof(FacilityWaitListItem.ParentCorporationName), model.ViewModel.ParentCorporationName);
                newItem.SetValue(nameof(FacilityWaitListItem.ParentCorporationWebsiteURL), model.ViewModel.WebsiteURL);

                newItem.Insert();

                Dictionary<string, object> data = new Dictionary<string, object>
            {
                { "CorporateInfo", model.ViewModel.CorporateInfo },
                { "User", user },
                { "RegistrationType", model.ViewModel.RegistrationType },
                { "UserID", user.Id },
                { "FirstName", user.FirstName },
                { "LastName", user.LastName },
                { "Email", user.Email },
                { "CorpName", model.ViewModel.CorporateInfo.Name },
                { "Address1", model.ViewModel.CorporateInfo.AddressLine1 },
                { "Address2", model.ViewModel.CorporateInfo.AddressLine2 },
                { "City", model.ViewModel.CorporateInfo.City },
                { "State", state },
                { "Zip", model.ViewModel.CorporateInfo.Zip },
                { "NoApplicableParentCorp", model.ViewModel.NoApplicableCorporation },
                { "ParentCorpName", model.ViewModel.ParentCorporationName },
                { "WebsiteURL", model.ViewModel.WebsiteURL }
            };

                EmailHelper.SendEmail(ContentIdentifiers.SUPPORT_EMAIL, ContentIdentifiers.FACILITY_REGISTRATION_DATA_ET_IDENTIFIER, data);

                Dictionary<string, object> data2 = new Dictionary<string, object>
            {
                { "RegistrationType", model.ViewModel.RegistrationType }
            };

                EmailHelper.SendEmail(user.Email, ContentIdentifiers.FACILITY_REGISTRATION_CONFIRMATION_ET_IDENTIFIER, data2);

                return RedirectToAction("FacilityPendingApproval");
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("LoginMangerController", "ERROR", e);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }

        public ActionResult FacilityPendingApproval()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage(ContentIdentifiers.FACILITY_PENDING_APPROVAL_PAGE) as TreeNode;

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                return View(GetBaseViewModel(new BaseDto(page)));
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("LoginMangerController", "ERROR", e);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }

        //Allows visitors to submit their email address and request a password reset.
        public ActionResult RequestPasswordReset()
        {
            var page = mDynamicRouteHelper.GetPage() as TreeNode;

            var model = GetBaseViewModel(new BaseDto(page),
            new RequestPasswordResetViewModel
            {

            });

            return View(model);
        }

        //Generates a password reset request for the specified email address.
        //Accepts the email address posted via the RequestPasswordResetViewModel.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RequestPasswordReset(BaseViewModel<RequestPasswordResetViewModel> model, string baseNodeJson, string returnUrl = "")
        {
            model.BaseNodeData = JsonConvert.DeserializeObject<BaseDto>(baseNodeJson);

            //Validates the recieved email address based on the view model
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //Gets the user entity for the specified email address
            User user = UserManager.FindByEmail(model.ViewModel.Email);

            if (user != null)
            {
                //Generates a password reset token for the user
                string token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                //Prepares the URL of the password to reset link (targets the "ResetPassword" action)
                //Fill in the name of your controller
                string resetUrl = Url.Action("ResetPassword", "LoginManager", new { userId = user.Id, token }, Request.Url.Scheme);

                //Creates and sends the password reset email to the user's address
                await UserManager.SendEmailAsync(user.Id, "Password Reset Request", String.Format("To reset your account's password, click <a href=\"{0}\">here</a>", resetUrl));

                if (returnUrl != "")
                {
                    return Redirect(returnUrl);
                }

                model.ViewModel.CheckEmailMsg = ResHelper.GetString("LoginManager.ResetPassword.CheckEmailMsg", SiteContext.CurrentSiteName);
                //Displays a view asking the visitor to check their email and click the password reset link
                return View(model);
            }

            if (returnUrl != "")
            {
                return Redirect(returnUrl);
            }

            model.ViewModel.UserNotFoundErrMsg = ResHelper.GetString("LoginManager.ResetPassword.UserNotFoundMsg", SiteContext.CurrentSiteName);
            //If the password request is invalid, returns a view informing the user
            return View(model);
        }

        //Handles the links that users click in password reset emails.
        //If the request parameters are valid, displays a form where users can reset their password.
        public ActionResult ResetPassword(int? userId, string token)
        {
            var baseModel = GetBaseViewModel(new BaseDto
            {
                MetaTitle = "Reset Password Invalid",
                UsePageBuilder = false
            });

            try
            {
                //Verifies the parameters of the password reset request
                //True if the token is valid for the specified user, false if the token is invalid or has expired
                //By default, the generated tokens are single-use and expire in 1 day
                if (UserManager.VerifyUserToken(userId.Value, "ResetPassword", token))
                {
                    //If the password request is valid, displays the password reset form
                    var model = GetBaseViewModel(new BaseDto
                    {
                        MetaTitle = "Update Password"
                    },
                    new ResetPasswordViewModel
                    {
                        UserId = userId.Value,
                        Token = token
                    });

                    return View(model);
                }

                //If the password request is invalid, returns a view informing the user
                return View("ResetPasswordInvalid", baseModel);
            }
            catch (InvalidOperationException)
            {
                //An InvalidOperationException occurs if a user with a given ID is not found
                //Returns a view informing the user that the password reset request is not valid
                return View("ResetPasswordInvalid", baseModel);
            }
        }

        //Resets the user's password based on the posted data.
        //Accepts the user ID, password reset token and the new password via the ResetPasswordViewModel.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ResetPassword(BaseViewModel<ResetPasswordViewModel> model, string baseNodeJson)
        {
            model.BaseNodeData = JsonConvert.DeserializeObject<BaseDto>(baseNodeJson);

            //Validates the received password data based on the view model
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                //Changes the user's password if the provided reset token is valid
                if (UserManager.ResetPassword(model.ViewModel.UserId, model.ViewModel.Token, model.ViewModel.Password).Succeeded)
                {
                    return RedirectToAction("Login");
                }

                //Occurs if the reset token is invalid
                //Returns a view informing the user that the password reset failed
                return View("ResetPasswordInvalid", model);
            }
            catch (InvalidOperationException)
            {
                //An InvalidOperationException occurs if a user with the given ID is not found
                //Returns a view informing the user that the password reset failed
                return View("ResetPasswordInvalid", model);
            }
        }

        [HttpPost]
        public JsonResult ImpersonateUser(int userID)
        {
            try
            {
                var user = UserInfoProvider.GetUserInfo(userID);

                if (user == null) return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = "The selected user doesn't exist.", ReturnUrl = "" }), JsonRequestBehavior.AllowGet);

                var cookie = CookieHelper.GetExistingCookie(ContentIdentifiers.CURRENT_USERNAME_COOKIE);
                if (cookie != null) CookieHelper.Remove(ContentIdentifiers.CURRENT_USERNAME_COOKIE);

                HttpCookie c = new HttpCookie(ContentIdentifiers.CURRENT_USERNAME_COOKIE)
                {
                    Value = User.Identity.Name
                };

                Response.Cookies.Add(c);
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                var studentIDCookie = CookieHelper.GetExistingCookie(ContentIdentifiers.STUDENT_ID_COOKIE);
                if (studentIDCookie != null) CookieHelper.Remove(ContentIdentifiers.STUDENT_ID_COOKIE);

                int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
                var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });

                if (student.ContactID == 0) ClassMemberDependencies.ContactRepo.MapUserToStudent(user);

                HttpCookie studentCookie = new HttpCookie(ContentIdentifiers.STUDENT_ID_COOKIE)
                {
                    Value = student.ContactID.ToString()
                };

                Response.Cookies.Add(studentCookie);

                var u = new User();
                u.MapFromUserInfo(user);
                SignInManager.SignIn(u, false, false);

                return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "", ReturnUrl = ContentIdentifiers.DASHBOARD }), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("LoginMangerController", "ERROR", ex);
                return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = ex.Message, ReturnUrl = "" }), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CancelImpersonation()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            var currentUserName = CookieHelper.GetExistingCookie(ContentIdentifiers.CURRENT_USERNAME_COOKIE).Value;
            var user = UserInfoProvider.GetUserInfo(currentUserName);

            CookieHelper.Remove(ContentIdentifiers.STUDENT_ID_COOKIE);

            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID });

            if(student != null)
            {
                HttpCookie studentCookie = new HttpCookie(ContentIdentifiers.STUDENT_ID_COOKIE)
                {
                    Value = student.ContactID.ToString()
                };

                Response.Cookies.Add(studentCookie);
            }

            var u = new User();
            u.MapFromUserInfo(user);
            SignInManager.SignIn(u, false, false);
            CookieHelper.Remove(ContentIdentifiers.CURRENT_USERNAME_COOKIE);

            return Json(ContentIdentifiers.DASHBOARD, JsonRequestBehavior.AllowGet);
        }

        public void CreateNewUserAccount()
        {

        }

        private async Task<bool> IsCaptchaValid(string response)
        {
            try
            {
                var secret = SettingsKeyInfoProvider.GetValue("CMSReCaptchaPrivateKey");
                using var client = new HttpClient();
                var values = new Dictionary<string, string>
                    {
                        { "secret", secret },
                        { "response", response },
                        { "remoteip", Request.UserHostAddress }
                    };

                var content = new FormUrlEncodedContent(values);
                var verify = await client.PostAsync("https://www.google.com/recaptcha/api/siteverify", content);
                var captchaResponseJson = await verify.Content.ReadAsStringAsync();
                var captchaResult = JsonConvert.DeserializeObject<CaptchaResponseViewModel>(captchaResponseJson);
                return captchaResult.Success
                    && captchaResult.Action == "register"
                    && captchaResult.Score > 0.5;
            }
            catch
            {
                return false;
            }
        }
    }
}