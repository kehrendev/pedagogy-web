﻿using CMS.Core;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Ecommerce;
using CMS.Taxonomy;
using CMS.DataEngine;
using CMS.CustomTables;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;
using Kentico.Content.Web.Mvc;
using Kentico.Membership;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Dto.Authors;
using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.Controllers;
using Pedagogy.Models;
using Pedagogy.Models.Products;
using Pedagogy.Models.Search;

using Microsoft.AspNet.Identity.Owin;

using Newtonsoft.Json;

using Castle.Core.Internal;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.SiteProvider;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.ClassMembers.DependencyInjection;

[assembly: DynamicRouting(typeof(ProductController), new string[] { Course.CLASS_NAME }, "Detail")]
[assembly: DynamicRouting(typeof(ProductController), new string[] { CourseMembership.CLASS_NAME }, "Detail")]
namespace Pedagogy.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IDynamicRouteHelper mDynamicRouteHelper;
        private readonly IShoppingService mShoppingService;
        private readonly ICatalogPriceCalculatorFactory mCalculatorFactory;
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public UserManager UserManager => HttpContext.GetOwinContext().Get<UserManager>();

        public ProductController(IDataDependencies dependencies, IDynamicRouteHelper drh, IClassMemberDependencies classMemberDependencies) : base(dependencies)
        {
            mDynamicRouteHelper = drh;
            mShoppingService = Service.Resolve<IShoppingService>();
            mCalculatorFactory = Service.Resolve<ICatalogPriceCalculatorFactory>();
            ClassMemberDependencies = classMemberDependencies;
        }

        public ActionResult CustomRedirect(int? Class)
        {
            if (!Class.HasValue) return Redirect(ContentIdentifiers.NOT_FOUND);

            var page = Dependencies.CourseRepo.GetCourseBySKUNumber(Class.Value.ToString(), x => x);

            if (page == null) return Redirect(ContentIdentifiers.NOT_FOUND);

            return Redirect(page.Url);
        }

        public ActionResult Index()
        {
            SearchViewModel model = (SearchViewModel)TempData["courseSearchModel"] ?? new SearchViewModel { };
            IEnumerable<ProductListItemViewModel> p = (IEnumerable<ProductListItemViewModel>)TempData["filteredItems"];
            ProductExtraFiltersViewModel currentExtraFilters = (ProductExtraFiltersViewModel)TempData["currentExtraFilters"];
            IEnumerable<ProductListItemViewModel> facilityAdminProducts = (IEnumerable<ProductListItemViewModel>)TempData["facilityAdminProducts"];
            var cart = mShoppingService.GetCurrentShoppingCart();

            var currentPage = mDynamicRouteHelper.GetPage(ContentIdentifiers.COURSE_CATALOG);

            //needed for workflow to be enabled.
            bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

            HttpContext.Kentico().PageBuilder().Initialize(currentPage.DocumentID);

            var node = Dependencies.PedagogyPageRepo.GetPedagogyPageByNodeGuid(currentPage.NodeGUID, isPublished);

            var categories = new ProductFilterCheckboxViewModel[] { };

            var items = GetFilteredCourses(categories);
            var filter = new ProductFilterViewModel();
            filter.Load();

            List<SelectListItem> sortByList = new List<SelectListItem>();
            sortByList.Add(new SelectListItem { Value = "minPrice", Text = "Min Price" });
            sortByList.Add(new SelectListItem { Value = "maxPrice", Text = "Max Price" });

            List<SelectListItem> contactHoursList = new List<SelectListItem>();
            contactHoursList.Add(new SelectListItem { Value = "0-1", Text = "0-1" });
            contactHoursList.Add(new SelectListItem { Value = "2-3", Text = "2-3" });
            contactHoursList.Add(new SelectListItem { Value = "4-5", Text = "4-5" });
            contactHoursList.Add(new SelectListItem { Value = "6+", Text = "6+" });


            var searchData = new List<ProductListItemViewModel>();
            if (model.CourseData != null)
            {
                foreach (var i in model.CourseData)
                {
                    ProductCatalogPrices price = mCalculatorFactory
                            .GetCalculator(cart.ShoppingCartSiteID)
                            .GetPrices(i.SKU, Enumerable.Empty<SKUInfo>(), cart);
                    searchData.Add(new ProductListItemViewModel(i.Course, price, i.PublicStatus) { Course = i, UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null });
                }
            }

            var stateCategories = CategoryInfoProvider.GetCategories()
                .Where(x => x.CategoryName.StartsWith("requiredState."))
                .ToList();

            var extraFilters = new ProductExtraFiltersViewModel { };
            if (currentExtraFilters != null)
            {
                extraFilters = currentExtraFilters;
                currentExtraFilters.ContactHoursList = contactHoursList;
                currentExtraFilters.SortByList = sortByList;
                currentExtraFilters.StateCategories = new SelectList(stateCategories, nameof(CategoryInfo.CategoryName), nameof(CategoryInfo.CategoryDisplayName));
            }
            else
            {
                extraFilters.SortByList = sortByList;
                extraFilters.ContactHoursList = contactHoursList;
                extraFilters.StateCategories = new SelectList(stateCategories, nameof(CategoryInfo.CategoryName), nameof(CategoryInfo.CategoryDisplayName));
            }

            if (model.CourseData != null)
            {
                model.CourseData.OrderBy(x => !string.IsNullOrEmpty(x.CourseShortName) ? x.CourseShortName : x.CourseTitle);
            }

            var currentUser = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var cookie = CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE);
            var facilities = new List<FacilityViewModel>();

            if(currentUser != null)
            {
                var isSiteAdmin = currentUser.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
                var isCorpAdmin = currentUser.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);
                facilities = ClassMembersHelper.GetFacilitiesForCurrentUser(ClassMemberDependencies, isSiteAdmin, isCorpAdmin, currentUser.UserID, "");
            }

            var viewModel = GetBaseViewModel(node, new ProductListViewModel
            {
                Filter = filter,
                SearchResults = model,
                ExtraFilters = extraFilters,
                PagingInfo = new PagingInfo<ProductListItemViewModel>
                {
                    Data = searchData.Count > 0 ? searchData : null ?? facilityAdminProducts ?? p ?? items,
                    CurrentPage = 1,
                    ItemsPerPage = 15,
                    SearchQuery = model.Query ?? "",
                    ExtraFilters = extraFilters
                },
                IsFacilityPurchaser = currentUser != null && currentUser.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName),
                FacilityID = cookie != null ? int.Parse(cookie.Value) : 0,
                Facilities = new SelectList(facilities, nameof(FacilityViewModel.FacilityID), nameof(FacilityViewModel.Name))
            });
            viewModel.ViewModel.PagingInfo.TotalItems = viewModel.ViewModel.PagingInfo.Data.Count();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(List<string> currentList = null, int page = 1, string searchQuery = null, string jsonFilter = null, string jsonExtraFilters = null)
        {
            var data = new List<ProductListItemViewModel>();
            if (currentList != null)
            {
                var cart = mShoppingService.GetCurrentShoppingCart();
                foreach (var item in currentList)
                {
                    var course = Dependencies.CourseRepo.GetCourseByTitle(item);
                    ProductCatalogPrices price = mCalculatorFactory
                        .GetCalculator(cart.ShoppingCartSiteID)
                        .GetPrices(course.SKU, Enumerable.Empty<SKUInfo>(), cart);

                    var newModel = new ProductListItemViewModel(course.Course, price, course.PublicStatus) { Course = course, UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null };

                    data.Add(newModel);
                }
            }

            var currentPage = mDynamicRouteHelper.GetPage(ContentIdentifiers.COURSE_CATALOG);

            //needed for workflow to be enabled.
            bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

            HttpContext.Kentico().PageBuilder().Initialize(currentPage.DocumentID);

            var node = Dependencies.PedagogyPageRepo.GetPedagogyPageByNodeGuid(currentPage.NodeGUID, isPublished);

            var filter = new ProductFilterViewModel();
            if (jsonFilter != null)
            {
                filter = JsonConvert.DeserializeObject<ProductFilterViewModel>(jsonFilter);
            }

            if (filter.Categories == null)
            {
                filter = new ProductFilterViewModel();
                filter.Load();
            }

            var extraFilters = new ProductExtraFiltersViewModel { };
            if (jsonExtraFilters != null)
            {
                extraFilters = JsonConvert.DeserializeObject<ProductExtraFiltersViewModel>(jsonExtraFilters);
            }
            else
            {
                var stateCategories = CategoryInfoProvider.GetCategories()
                    .Where(x => x.CategoryName.StartsWith("requiredState."))
                    .ToList();

                extraFilters.StateCategories = new SelectList(stateCategories, nameof(CategoryInfo.CategoryName), nameof(CategoryInfo.CategoryDisplayName));

                extraFilters.SortByList = new List<SelectListItem>()
                {
                    new SelectListItem { Value = "minPrice", Text = "Min Price" },
                    new SelectListItem { Value = "maxPrice", Text = "Max Price" }
                };

                extraFilters.ContactHoursList = new List<SelectListItem>
                {
                    new SelectListItem { Value = "0-1", Text = "0-1" },
                    new SelectListItem { Value = "2-3", Text = "2-3" },
                    new SelectListItem { Value = "4-5", Text = "4-5" },
                    new SelectListItem { Value = "6+", Text = "6+" }
                };
            }



            var baseModel = GetBaseViewModel(node, new ProductListViewModel
            {
                Filter = filter ?? new ProductFilterViewModel(),
                ExtraFilters = extraFilters,
                PagingInfo = new PagingInfo<ProductListItemViewModel>
                {
                    ProductFilter = filter ?? new ProductFilterViewModel(),
                    Data = data ?? new List<ProductListItemViewModel>(),
                    ItemsPerPage = 15,
                    CurrentPage = page,
                    SearchQuery = searchQuery,
                    ExtraFilters = extraFilters
                }
            });
            baseModel.ViewModel.PagingInfo.TotalItems = baseModel.ViewModel.PagingInfo.Data.Count();

            return View(baseModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Filter(ProductFilterViewModel filter)
        {
            if (!Request.IsAjaxRequest())
            {
                return HttpNotFound();
            }

            var courses = filter.Categories;
            var items = GetFilteredCourses(courses);

            var model = new PagingInfo<ProductListItemViewModel>
            {
                Data = items,
                SearchQuery = "",
                ItemsPerPage = 15,
                CurrentPage = 1,
                ProductFilter = filter
            };
            model.TotalItems = model.Data.Count();

            return PartialView("_CourseList", model);
        }

        private IEnumerable<ProductListItemViewModel> GetFilteredCourses(ProductFilterCheckboxViewModel[] categories)
        {
            ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();
            List<string> categoryNames = new List<string>();
            if (!categories.IsNullOrEmpty())
            {
                foreach (var value in categories)
                {
                    if (value.IsChecked)
                    {
                        categoryNames.Add(value.Value);
                    }
                }
            }

            List<Course> products = CourseProvider.GetCourses()
                .LatestVersion(false)
                .Published(true)
                .OnCurrentSite()
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .CombineWithDefaultCulture()
                .Columns(
                    nameof(Course.NodeSKUID),
                    nameof(Course.NodeGUID),
                    nameof(Course.SKU.SKUPrice),
                    nameof(Course.SKU.SKUNumber),
                    nameof(Course.SKU.SKUNeedsShipping)
                )
                .InCategories(categoryNames.ToArray())
                .WhereTrue("SKUEnabled")
                .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                .Where(x => !x.SKU.SKUNeedsShipping)
                .ToList();

            var items = products.Select(
                course => new ProductListItemViewModel(
                    course,
                    GetPrice(course.SKU, cart),
                    course.Product.PublicStatus?.PublicStatusDisplayName)
                {
                    Course = Dependencies.CourseRepo.GetCourseByNodeGuid(course.NodeGUID),
                    SKUID = course.NodeSKUID,
                    UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null
                });

            var membershipProducts = CourseMembershipProvider.GetCourseMemberships()
                .LatestVersion(false)
                .Published(true)
                .OnCurrentSite()
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .CombineWithDefaultCulture()
                .Columns(
                    nameof(Course.NodeSKUID),
                    nameof(Course.NodeGUID),
                    nameof(Course.SKU.SKUPrice),
                    nameof(Course.SKU.SKUNumber),
                    nameof(Course.SKU.SKUNeedsShipping)
                )
                .InCategories(categoryNames.ToArray())
                .WhereTrue("SKUEnabled")
                .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                .Where(x => !x.SKU.SKUNeedsShipping)
                .ToList();

            var memItems = membershipProducts.Select(
                course => new ProductListItemViewModel(
                    course,
                    GetPrice(course.SKU, cart),
                    course.Product.PublicStatus?.PublicStatusDisplayName)
                {
                    Course = Dependencies.CourseRepo.GetCourseMembershipByNodeGuid(course.NodeGUID),
                    SKUID = course.NodeSKUID,
                    UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null
                });

            if (items.Count() > 0)
            {
                foreach (var i in memItems)
                {
                    items.ToList().Add(i);
                }
            }
            else
            {
                items = memItems;
            }

            return items.OrderBy(x => x.Course.CourseShortName);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExtraFilters(ProductExtraFiltersViewModel model)
        {
            var cart = mShoppingService.GetCurrentShoppingCart();

            var allProductListItems = new List<ProductListItemViewModel>();
            var allCourses = CourseProvider.GetCourses()
                                .LatestVersion(false)
                                .Published(true)
                                .OnCurrentSite()
                                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                                .CombineWithDefaultCulture()
                                .Columns(
                                    nameof(Course.NodeSKUID),
                                    nameof(Course.NodeGUID),
                                    nameof(Course.SKU.SKUPrice),
                                    nameof(Course.SKU.SKUNumber),
                                    nameof(Course.SKU.SKUNeedsShipping)
                                )
                                .WhereTrue("SKUEnabled")
                                .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                                .Where(x => !x.SKU.SKUNeedsShipping)
                                .ToList();

            allProductListItems = allCourses.Select(
                course => new ProductListItemViewModel(
                    course,
                    GetPrice(course.SKU, cart),
                    course.Product.PublicStatus?.PublicStatusDisplayName)
                {
                    Course = Dependencies.CourseRepo.GetCourseByNodeGuid(course.NodeGUID),
                    SKUID = course.NodeSKUID,
                    UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null
                })
                    .ToList();

            var stateProductListItems = new List<ProductListItemViewModel>();
            if (!string.IsNullOrEmpty(model.StateCategory))
            {
                List<Course> products = CourseProvider.GetCourses()
                        .LatestVersion(false)
                        .Published(true)
                        .OnCurrentSite()
                        .Culture(LocalizationContext.CurrentCulture.CultureCode)
                        .CombineWithDefaultCulture()
                        .Columns(
                                nameof(Course.NodeSKUID),
                                nameof(Course.NodeGUID),
                                nameof(Course.SKU.SKUPrice),
                                nameof(Course.CourseTitle),
                                nameof(Course.SKU.SKUNumber),
                                nameof(Course.SKU.SKUNeedsShipping)
                            )
                        .InCategories(model.StateCategory)
                        .WhereTrue("SKUEnabled")
                        .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                        .Where(x => !x.SKU.SKUNeedsShipping)
                        .ToList();

                stateProductListItems = products.Select(
                    course => new ProductListItemViewModel(
                        course,
                        GetPrice(course.SKU, cart),
                        course.Product.PublicStatus?.PublicStatusDisplayName)
                    {
                        Course = Dependencies.CourseRepo.GetCourseByNodeGuid(course.NodeGUID),
                        SKUID = course.NodeSKUID,
                        UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null
                    })
                        .ToList();
            }

            var hours = model.ContactHourData;
            var contactHourCourses = new List<Course>();
            var contactHourProductListItems = new List<ProductListItemViewModel>();
            var firstGroup = new List<Course>();
            var secondGroup = new List<Course>();
            var thirdGroup = new List<Course>();
            var fourthGroup = new List<Course>();

            if (model.ContactHourData != null)
            {
                firstGroup = DocumentHelper.GetDocuments<Course>()
                        .Published(true)
                        .PublishedVersion()
                        .OnCurrentSite()
                        .Culture(LocalizationContext.CurrentCulture.CultureCode)
                        .CombineWithDefaultCulture()
                        .WhereTrue("SKUEnabled")
                        .And().WhereGreaterOrEquals(nameof(Course.CourseContactHours), 0)
                        .And().WhereLessThan(nameof(Course.CourseContactHours), 2)
                        .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                        .Where(x => !x.SKU.SKUNeedsShipping)
                        .ToList();

                secondGroup = DocumentHelper.GetDocuments<Course>()
                            .Published(true)
                            .PublishedVersion()
                            .OnCurrentSite()
                            .Culture(LocalizationContext.CurrentCulture.CultureCode)
                            .CombineWithDefaultCulture()
                            .WhereTrue("SKUEnabled")
                            .And().WhereGreaterOrEquals(nameof(Course.CourseContactHours), 2)
                            .And().WhereLessThan(nameof(Course.CourseContactHours), 4)
                            .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                            .Where(x => !x.SKU.SKUNeedsShipping)
                            .ToList();

                thirdGroup = DocumentHelper.GetDocuments<Course>()
                            .Published(true)
                            .PublishedVersion()
                            .OnCurrentSite()
                            .Culture(LocalizationContext.CurrentCulture.CultureCode)
                            .CombineWithDefaultCulture()
                            .WhereTrue("SKUEnabled")
                            .And().WhereGreaterOrEquals(nameof(Course.CourseContactHours), 4)
                            .And().WhereLessThan(nameof(Course.CourseContactHours), 6)
                            .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                            .Where(x => !x.SKU.SKUNeedsShipping)
                            .ToList();

                fourthGroup = DocumentHelper.GetDocuments<Course>()
                            .Published(true)
                            .PublishedVersion()
                            .OnCurrentSite()
                            .Culture(LocalizationContext.CurrentCulture.CultureCode)
                            .CombineWithDefaultCulture()
                            .WhereTrue("SKUEnabled")
                            .And().WhereGreaterOrEquals(nameof(Course.CourseContactHours), 6)
                            .Where(x => !string.IsNullOrEmpty(x.SKU.SKUNumber))
                            .Where(x => !x.SKU.SKUNeedsShipping)
                            .ToList();

                if (hours == "0-1")
                {
                    contactHourCourses = firstGroup;
                }
                else if (hours == "2-3")
                {
                    contactHourCourses = secondGroup;
                }
                else if (hours == "4-5")
                {
                    contactHourCourses = thirdGroup;
                }
                else if (hours == "6+")
                {
                    contactHourCourses = fourthGroup;
                }

                contactHourProductListItems = contactHourCourses.Select(
                    course => new ProductListItemViewModel(
                        course,
                        GetPrice(course.SKU, cart),
                        course.Product.PublicStatus?.PublicStatusDisplayName)
                        {
                            Course = Dependencies.CourseRepo.GetCourseByNodeGuid(course.NodeGUID),
                            SKUID = course.NodeSKUID,
                            UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null
                        })
                        .ToList();
            }

            if (!string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData != null)
            {
                if (hours == "0-1")
                {
                    var filteredState = new List<Course>();
                    foreach (var g in firstGroup)
                    {
                        IEnumerable<BaseInfo> categories = ((IEnumerable<BaseInfo>)g.Categories).ToList();
                        foreach (var category in categories)
                        {
                            if (category.GetValue("CategoryName").ToString().ToLowerInvariant().Equals(model.StateCategory.ToLowerInvariant()))
                            {
                                filteredState.Add(g);
                            }
                        }
                    }
                    contactHourCourses = filteredState;
                }
                else if (hours == "2-3")
                {
                    var filteredState = new List<Course>();
                    foreach (var h in secondGroup)
                    {
                        IEnumerable<BaseInfo> categories = ((IEnumerable<BaseInfo>)h.Categories).ToList();
                        foreach (var category in categories)
                        {
                            if (category.GetValue("CategoryName").ToString().ToLowerInvariant().Equals(model.StateCategory.ToLowerInvariant()))
                            {
                                filteredState.Add(h);
                            }
                        }
                    }
                    contactHourCourses = filteredState;
                }
                else if (hours == "4-5")
                {
                    var filteredState = new List<Course>();
                    foreach (var z in thirdGroup)
                    {
                        IEnumerable<BaseInfo> categories = ((IEnumerable<BaseInfo>)z.Categories).ToList();
                        foreach (var category in categories)
                        {
                            if (category.GetValue("CategoryName").ToString().ToLowerInvariant().Equals(model.StateCategory.ToLowerInvariant()))
                            {
                                filteredState.Add(z);
                            }
                        }
                    }
                    contactHourCourses = filteredState;
                }
                else if (hours == "6+")
                {
                    var filteredState = new List<Course>();
                    foreach (var e in fourthGroup)
                    {
                        IEnumerable<BaseInfo> categories = ((IEnumerable<BaseInfo>)e.Categories).ToList();
                        foreach (var category in categories)
                        {
                            var name = category.GetValue("CategoryName");
                            if (name.ToString().ToLowerInvariant().Equals(model.StateCategory.ToLowerInvariant()))
                            {
                                filteredState.Add(e);
                            }
                        }
                    }
                    contactHourCourses = filteredState;
                }

                contactHourProductListItems = contactHourCourses.Select(
                    course => new ProductListItemViewModel(
                        course,
                        GetPrice(course.SKU, cart),
                        course.Product.PublicStatus?.PublicStatusDisplayName)
                    {
                        Course = Dependencies.CourseRepo.GetCourseByNodeGuid(course.NodeGUID),
                        SKUID = course.NodeSKUID,
                        UserLoggedIn = UserInfoProvider.GetUserInfo(User.Identity.Name) != null
                    })
                        .ToList();
            }

            if (model.SortByData == "minPrice")
            {
                if (string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData == null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = allProductListItems.OrderBy(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
                else if (string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData != null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = contactHourProductListItems.OrderBy(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
                else if (!string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData == null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = stateProductListItems.OrderBy(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
                else if (!string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData != null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = contactHourProductListItems.OrderBy(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
            }
            else if (model.SortByData == "maxPrice")
            {
                if (string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData == null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = allProductListItems.OrderByDescending(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
                else if (string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData != null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = contactHourProductListItems.OrderByDescending(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
                else if (!string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData == null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = stateProductListItems.OrderByDescending(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
                else if (!string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData != null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = contactHourProductListItems.OrderByDescending(m => m.Course.CoursePrice);
                    return RedirectToAction("Index");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData == null)
                {
                    return RedirectToAction("Index");
                }
                else if (string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData != null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = contactHourProductListItems.OrderBy(x => x.Course.CourseShortName);
                    return RedirectToAction("Index");
                }
                else if (!string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData == null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = stateProductListItems.OrderBy(x => x.Course.CourseShortName);
                    return RedirectToAction("Index");
                }
                else if (!string.IsNullOrEmpty(model.StateCategory) && model.ContactHourData != null)
                {
                    TempData["currentExtraFilters"] = model;
                    TempData["filteredItems"] = contactHourProductListItems.OrderBy(x => x.Course.CourseShortName);
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult AddToWaitList(Guid courseGUID)
        {
            var usr = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var customer = CustomerInfoProvider.GetCustomerInfoByUserID(usr.UserID);
            if(customer == null)
            {
                customer = CustomerHelper.MapToCustomer(usr);
            }

            var newItem = CustomTableItem.New("Pedagogy.CourseWaitList");
            newItem.SetValue("CustomerEmail", customer.CustomerEmail);
            newItem.SetValue("CourseGUID", courseGUID.ToString());


            var item = CustomTableItemProvider.GetItems("Pedagogy.CourseWaitList")
                .WhereStartsWith("CustomerEmail", customer.CustomerEmail)
                .WhereStartsWith("CourseGUID", courseGUID.ToString())
                .FirstOrDefault();

            if(item == null)
            {
                newItem.Insert();
            }

            return Json(ResHelper.GetString("CourseCatalog.CourseWaitListMsg", LocalizationContext.CurrentCulture.CultureCode), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail()
        {
            var page = mDynamicRouteHelper.GetPage();

            SKUTreeNode product = GetProduct(page.NodeGUID);

            bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

            HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

            if ((product == null) || (product.SKU == null) || !product.SKU.SKUEnabled)
            {
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }

            ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();

            ProductCatalogPrices price = mCalculatorFactory
                .GetCalculator(cart.ShoppingCartSiteID)
                .GetPrices(product.SKU, Enumerable.Empty<SKUInfo>(), cart);

            AuthorItemDto author = new AuthorItemDto();
            BaseViewModel<ProductViewModel> viewModel;

            if (page.ClassName == Course.CLASS_NAME)
            {
                CourseItemDto course = Dependencies.CourseRepo.GetCourseByNodeGuid(page.NodeGUID);
                IEnumerable<CourseItemDto> relatedCourses = Dependencies.CourseRepo.GetRelatedCourses(course);
                if (course.CourseAuthor != "")
                {
                    author = Dependencies.AuthorRepo.GetAuthorByPath(course.CourseAuthor, isPublished);
                }

                viewModel = GetBaseViewModel(course,
                new ProductViewModel(product, price, course, author)
                {
                    RelatedCourses = relatedCourses,
                    currentCart = cart.CartItems
                });
            }
            else
            {
                CourseItemDto course = Dependencies.CourseRepo.GetCourseMembershipByNodeGuid(page.NodeGUID);
                viewModel = GetBaseViewModel(course,
                new ProductViewModel(product, price, course, author)
                {
                    RelatedCourses = new List<CourseItemDto>(),
                    currentCart = cart.CartItems
                });
            }

            return View(viewModel);
        }

        private ProductCatalogPrices GetPrice(SKUInfo product, ShoppingCartInfo cart)
        {
            return mCalculatorFactory
                .GetCalculator(cart.ShoppingCartSiteID)
                .GetPrices(product, Enumerable.Empty<SKUInfo>(), cart);
        }

        private SKUTreeNode GetProduct(Guid nodeGuid)
        {
            TreeNode node = DocumentHelper.GetDocuments()
                .LatestVersion(false)
                .Published(true)
                .OnCurrentSite()
                .Culture(LocalizationContext.CurrentCulture.CultureCode)
                .CombineWithDefaultCulture()
                .WhereEquals("NodeGUID", nodeGuid)
                .FirstOrDefault();

            if (node == null || !node.IsProduct())
            {
                return null;
            }

            node.MakeComplete(true);

            return node as SKUTreeNode;
        }
    }
}