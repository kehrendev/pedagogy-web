﻿using CMS.DocumentEngine.Types.Pedagogy;
using CMS.EventLog;
using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Controllers;
using Pedagogy.Models.HowTo;

using System;
using System.Web.Mvc;

[assembly: DynamicRouting(typeof(HowToController), new string[] { HowTo.CLASS_NAME }, "Index")]

namespace Pedagogy.Controllers
{
    public class HowToController : BaseController
    {
        private IDynamicRouteHelper mDynamicRouteHelper;
        public HowToController(IDataDependencies dependencies, IDynamicRouteHelper drh) : base(dependencies) { mDynamicRouteHelper = drh; }

        public ActionResult Index()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();

                //needed for workflow to be enabled.
                bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

                if (page == null)
                {
                    throw new Exception("DynamicRouting: GetPage returned null");
                }

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                var node = Dependencies.HowToRepo.GetHowToItemByNodeGuid(page.NodeGUID, isPublished);

                var model = GetBaseViewModel(node, new HowToViewModel()
                {
                    HowToData = node
                });

                return View(model);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("HowToController", "ERROR", ex);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }
    }
}