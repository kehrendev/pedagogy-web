﻿using Castle.Core.Internal;

using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.EventLog;
using CMS.SiteProvider;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Dto.Resources;
using Pedagogy.Controllers;
using Pedagogy.Models;
using Pedagogy.Models.Resource;
using Pedagogy.Models.Search;

using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;



[assembly: DynamicRouting(typeof(ResourceController), new string[] { Resource.CLASS_NAME }, "Details")]
namespace Pedagogy.Controllers
{
    public class ResourceController : BaseController
    {
        private IDynamicRouteHelper mDynamicRouteHelper;
        public ResourceController(IDataDependencies dependencies, IDynamicRouteHelper drh) : base(dependencies) { mDynamicRouteHelper = drh; }

        public ActionResult Index()
        {
            SearchViewModel searchResults = (SearchViewModel)TempData["resourceSearchModel"] ?? new SearchViewModel { };

            var currentPage = mDynamicRouteHelper.GetPage(ContentIdentifiers.RESOURCE_PAGE);

            bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

            HttpContext.Kentico().PageBuilder().Initialize(currentPage.DocumentID);

            var node = Dependencies.PedagogyPageRepo.GetPedagogyPageByNodeGuid(currentPage.NodeGUID, isPublished);

            var categories = new ResourceFilterCheckboxViewModel[] { };

            var items = GetFilteredResources(categories);
            var filter = new ResourceFilterViewModel();
            filter.Load();

            var model = GetBaseViewModel(node, new ResourceViewModel()
            {
                Filter = filter,
                Maps = Dependencies.MapRepo.GetMapsByPath(ContentIdentifiers.RESOURCE_PAGE),
                SearchResults = searchResults,
                PagingInfo = new PagingInfo<ResourceItemDto>
                {
                    Data = searchResults.ResourceData ?? items,
                    ItemsPerPage = 12,
                    CurrentPage = 1
                }
            });
            model.ViewModel.PagingInfo.TotalItems = model.ViewModel.PagingInfo.Data.Count();

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(List<string> currentList = null, int page = 1, string searchQuery = null, string jsonFilter = null)
        {
            var data = new List<ResourceItemDto>();
            if(currentList != null)
            {
                foreach (var item in currentList)
                {
                    data.Add(Dependencies.ResourceRepo.GetResourceByTitle(item));
                }
            }

            var currentPage = mDynamicRouteHelper.GetPage(ContentIdentifiers.RESOURCE_PAGE);

            //needed for workflow to be enabled.
            bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

            HttpContext.Kentico().PageBuilder().Initialize(currentPage.DocumentID);

            var node = Dependencies.PedagogyPageRepo.GetPedagogyPageByNodeGuid(currentPage.NodeGUID, isPublished);

            var filter = new ResourceFilterViewModel();
            if(jsonFilter != null)
            {
                filter = JsonConvert.DeserializeObject<ResourceFilterViewModel>(jsonFilter);
            }

            if(filter.Categories == null)
            {
                filter = new ResourceFilterViewModel();
                filter.Load();
            }

            var baseModel = GetBaseViewModel(node, new ResourceViewModel
            {
                Filter = filter ?? new ResourceFilterViewModel(),
                Maps = Dependencies.MapRepo.GetMapsByPath(ContentIdentifiers.RESOURCE_PAGE),
                PagingInfo = new PagingInfo<ResourceItemDto>
                {
                    Filter = filter ?? new ResourceFilterViewModel(),
                    Data = data ?? new List<ResourceItemDto>(),
                    ItemsPerPage = 12,
                    CurrentPage = page,
                    SearchQuery = searchQuery
                }
            });
            baseModel.ViewModel.PagingInfo.TotalItems = baseModel.ViewModel.PagingInfo.Data.Count();

            return View(baseModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Filter(ResourceFilterViewModel filter)
        {
            if (!Request.IsAjaxRequest())
            {
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }

            var resources = filter.Categories;
            var items = GetFilteredResources(resources);

            var model = new PagingInfo<ResourceItemDto>
            {
                Data = items,
                SearchQuery = "",
                ItemsPerPage = 12,
                CurrentPage = 1,
                Filter = filter
            };
            model.TotalItems = model.Data.Count();

            return PartialView("_ResourceList", model);
        }

        private IEnumerable<ResourceItemDto> GetFilteredResources(ResourceFilterCheckboxViewModel[] categories)
        {
            List<string> categoryNames = new List<string>();

            if (!categories.IsNullOrEmpty())
            {
                foreach (var value in categories)
                {
                    if (value.IsChecked)
                    {
                        categoryNames.Add(value.Value);
                    }
                }
            }

            List<Resource> resources = ResourceProvider.GetResources()
            .LatestVersion(false)
            .Published(true)
            .OnSite(SiteContext.CurrentSiteName)
            .Culture(CMS.Localization.LocalizationContext.CurrentCulture.CultureCode)
            .CombineWithDefaultCulture()
            .Columns(
                nameof(Resource.ResourceName),
                nameof(Resource.ResourceSummary),
                nameof(Resource.ResourceImg),
                nameof(Resource.ResourceImgAltText),
                nameof(Resource.NodeAliasPath)
             )
            .InCategories(categoryNames.ToArray())
            .WhereNotLike(nameof(Resource.NodeAliasPath), "/Resources/State-Board%")
            .ToList();

            List<ResourceItemDto> resourceDtos = new List<ResourceItemDto>();
            foreach(var resource in resources)
            {
                resourceDtos.Add(new ResourceItemDto 
                {
                    ResourceName = resource.ResourceName,
                    ResourceSummary = resource.ResourceSummary,
                    ResourceImg = resource.ResourceImg,
                    ResourceImgAltText = resource.ResourceImgAltText,
                    Url = resource.NodeAliasPath
                });
            }

            return resourceDtos;
        }

        public ActionResult Details()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();

                //needed for workflow to be enabled.
                bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

                if (page == null)
                {
                    throw new Exception("DynamicRouting: GetPage returned null");
                }

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                var node = Dependencies.ResourceRepo.GetResourceByNodeGuid(page.NodeGUID, isPublished);

                var model = GetBaseViewModel(node, new ResourceDetailsViewModel()
                {
                    ResourceData = node,
                    Files = Dependencies.FileRepo.GetFilesByPath(node.Url),
                    RelatedCourses = Dependencies.ResourceRepo.GetRelatedCourses(node)
                });

                return View(model);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("ResourceController", "ERROR", ex);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }
    }
}