﻿using System.Web.Mvc;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Dto;
using Pedagogy.Models;

namespace Pedagogy.Controllers
{
    public class BaseController : Controller
    {
        protected IDataDependencies Dependencies { get; }
        protected BaseController(IDataDependencies dependencies)
        {
            Dependencies = dependencies;
        }

        protected BaseViewModel GetBaseViewModel(BaseDto baseNode)
        {
            return new BaseViewModel()
            {
                BaseNodeData = baseNode,
                InfoBoxItem = Dependencies.InfoBoxRepo.GetInfoBoxItems()
            };
        }

        protected BaseViewModel<TViewModel> GetBaseViewModel<TViewModel>(BaseDto baseNode, TViewModel viewModel) where TViewModel : IViewModel
        {
            return new BaseViewModel<TViewModel>()
            {
                BaseNodeData = baseNode,
                ViewModel = viewModel,
                InfoBoxItem = Dependencies.InfoBoxRepo.GetInfoBoxItems()
            };
        }
    }
}