﻿using System.Web.Mvc;

namespace Pedagogy.Controllers
{
    public class MainfestController : Controller
    {
        // GET: Mainfest
        public FileResult Index()
        {
            var manifestPath = Server.MapPath("~/manifest.json");
            return File(manifestPath, "application/json", "manifest.json");
        }
    }
}