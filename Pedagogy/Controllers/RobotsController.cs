﻿using CMS.Helpers;

using System.Text;
using System.Web.Mvc;

namespace Pedagogy.Controllers
{
    public class RobotsController : Controller
    {
        // GET: Robots
        public ContentResult Index()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("user-agent: *");
            stringBuilder.AppendLine("disallow: /Error");
            stringBuilder.AppendLine("disallow: /Social");
            stringBuilder.AppendLine(string.Format("sitemap: {0}", URLHelper.GetAbsoluteUrl("/sitemap.xml")));

            return this.Content(stringBuilder.ToString(), "text/plain", Encoding.UTF8);
        }
    }
}