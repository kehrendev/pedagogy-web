﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.SiteProvider;

using SimpleMvcSitemap;


namespace Pedagogy.Controllers
{
    public class SitemapController : Controller
    {
        // GET: Sitemap
        public ActionResult Index()
        {
            List<SitemapNode> nodes = new List<SitemapNode>();

            foreach (var doc in GetXMLSitemapDocuments())
            {
                nodes.Add(new SitemapNode(doc.NodeAliasPath)
                {
                    LastModificationDate = new DateTime(doc.DocumentModifiedWhen.Year, doc.DocumentModifiedWhen.Month, doc.DocumentModifiedWhen.Day, doc.DocumentModifiedWhen.Hour, doc.DocumentModifiedWhen.Minute, doc.DocumentModifiedWhen.Second, DateTimeKind.Local),
                });
            }
            return (ActionResult)new SitemapProvider().CreateSitemap(new SitemapModel(nodes));
        }

        private MultiDocumentQuery GetXMLSitemapDocuments()
        {
            //Define sitewide params
            var culture = "en-us";
            var siteName = SiteContext.CurrentSiteName;

            //Define required params for data call
            var defaultPath = "/%";
            var defaultWhere = "(DocumentShowInSiteMap = 1) AND (DocumentMenuItemHideInNavigation = 0) AND (NodeLinkedNodeID IS NULL)";
            var defaultOrderBy = "NodeLevel, NodeOrder, DocumentName";
            var defaultColumns = "DocumentModifiedWhen, DocumentUrlPath, NodeAliasPath, NodeID, NodeParentID, NodeSiteID, NodeACLID";

            Func<MultiDocumentQuery> dataLoadMethod = () => DocumentHelper.GetDocuments()
                                                                        .PublishedVersion(true)
                                                                        .Types() // FILL WITH SITE SPECIFIC TYPES
                                                                        .Path(defaultPath)
                                                                        .Culture(culture)
                                                                        .CombineWithDefaultCulture(true)
                                                                        .Where(defaultWhere)
                                                                        .OrderBy(defaultOrderBy)
                                                                        .Published(true)
                                                                        .Columns(defaultColumns);

            //Cache settings set to 4 hours, but with dependency's on if any page changes in the tree
            var cacheSettings = new CacheSettings(240, "data|xmlsitemap", culture)
            {
                GetCacheDependency = () =>
                {
                    //Creates cache dependencies. This example makes the cache clear data when any node is modified, deleted, or created
                    string dependencyCacheKey = $"node|{siteName}|/|childnodes";
                    return CacheHelper.GetCacheDependency(dependencyCacheKey);
                }
            };

            return CacheHelper.Cache(dataLoadMethod, cacheSettings);
        }
    }
}