﻿using CMS.DocumentEngine.Types.Pedagogy;
using CMS.EventLog;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;
using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Controllers;
using Pedagogy.Models.Solution;

using System;
using System.Web.Mvc;



[assembly: DynamicRouting(typeof(SolutionController), new string[] { Solution.CLASS_NAME }, "Index")]
namespace Pedagogy.Controllers
{
    public class SolutionController : BaseController
    {
        private IDynamicRouteHelper mDynamicRouteHelper;
        public SolutionController(IDataDependencies dependencies, IDynamicRouteHelper drh) : base(dependencies) { mDynamicRouteHelper = drh; }

        public ActionResult Index()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();

                //needed for workflow to be enabled.
                bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

                if (page == null)
                {
                    throw new Exception("DynamicRouting: GetPage returned null");
                }

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                var node = Dependencies.SolutionRepo.GetSolutionItemByNodeGuid(page.NodeGUID, isPublished);

                var model = GetBaseViewModel(node, new SolutionViewModel()
                {
                    SolutionData = node
                });

                return View(model);
            }
            catch(Exception ex)
            {
                EventLogProvider.LogException("SolutionController", "ERROR", ex);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }
    }
}