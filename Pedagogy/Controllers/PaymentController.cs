﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;

using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Ecommerce;
using CMS.Globalization;
using CMS.SiteProvider;
using CMS.Membership;
using CMS.Helpers;
using CMS.DataEngine;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Models.Checkout;
using Pedagogy.ClassMembers.DependencyInjection;

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System;
using System.Web;

using Newtonsoft.Json;
using Pedagogy.ClassMembers.Entities.Contact;

namespace Pedagogy.Controllers
{
    public class PaymentController : BaseController
    {
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public PaymentController(IDataDependencies dependencies, IClassMemberDependencies classMemberDependencies) : base(dependencies) 
        {
            ClassMemberDependencies = classMemberDependencies;
        }

        public ActionResult ProcessPayment(bool isFromWidget = false)
        {
            var model = (PaymentProcessViewModel)TempData["cardModel"];

            OrderInfo order = OrderInfoProvider.GetOrderInfo(model.OrderId);
            ShoppingCartInfo cart = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(order.OrderID);

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = SettingsKeyInfoProvider.GetValue("CMSAuthorizeNETAPILogin", SiteContext.CurrentSiteID),
                ItemElementName = ItemChoiceType.transactionKey,
                Item = SettingsKeyInfoProvider.GetValue("CMSAuthorizeNETTransactionKey", SiteContext.CurrentSiteID)
            };

            //Get card details from the view and assign them to a new card object
            var creditCard = new creditCardType
            {
                cardNumber = model.CardData.CardNo,
                expirationDate = model.CardData.ExpDate,
                cardCode = model.CardData.CVVCode
            };

            var customerData = CustomerInfoProvider.GetCustomerInfo(order.OrderCustomerID);

            string billingState = "";
            if (order.OrderBillingAddress.AddressStateID > 0)
            {
                billingState = StateInfoProvider.GetStateInfo(order.OrderBillingAddress.AddressStateID).StateDisplayName;
            }
            var billingCountry = CountryInfoProvider.GetCountryInfo(order.OrderBillingAddress.AddressCountryID).CountryDisplayName;

            //Get the billing address from the order
            var billingAddress = new customerAddressType
            {
                firstName = customerData.CustomerFirstName,
                lastName = customerData.CustomerLastName,
                city = order.OrderBillingAddress.AddressCity,
                address = order.OrderBillingAddress.AddressLine1 + ", " + order.OrderBillingAddress.AddressLine2,
                zip = order.OrderBillingAddress.AddressZip,
                phoneNumber = order.OrderBillingAddress.AddressPhone,
                state = billingState,
                country = billingCountry
            };

            var paymentType = new paymentType { Item = creditCard };

            //get the list of products from the cart
            var orderedItems = cart.CartItems;
            ShoppingCartItemInfo[] items = new ShoppingCartItemInfo[orderedItems.Count];
            orderedItems.CopyTo(items, 0);
            var lineItems = new List<lineItemType>();

            for (int i = 0; i < items.Length; i++)
            {
                string lineName;
                if (!items[i].IsProductOption && !items[i].IsBundleItem)
                {
                    var skuNum = items[i].SKU.SKUNumber.ToString();

                    if (items[i].SKU.SKUName.Length > 30)
                    {
                        lineName = items[i].SKU.SKUName.Remove(20, items[i].SKU.SKUName.Length - 30);
                        lineItems.Add(new lineItemType { itemId = items[i].SKUID.ToString(), name = lineName, quantity = items[i].CartItemUnits, unitPrice = items[i].UnitPrice });
                    }
                    else
                    {
                        lineName = string.Concat(items[i].SKU.SKUName);
                        lineItems.Add(new lineItemType { itemId = items[i].SKUID.ToString(), name = lineName, quantity = items[i].CartItemUnits, unitPrice = items[i].UnitPrice });
                    }

                    foreach (var x in cart.CartItems)
                    {

                        if (x.IsProductOption && x.ParentProduct.SKUID == items[i].SKUID && x.TotalPrice != 0)
                        {
                            string parentSkuName = items[i].SKU.SKUName;

                            lineName = "Print Option For - " + parentSkuName;
                            if (lineName.Length > 10)
                            {
                                lineName = lineName.Remove(10, parentSkuName.Length - 10);
                            }
                            lineItems.Add(new lineItemType { itemId = x.SKUID.ToString(), name = lineName, quantity = items[i].CartItemUnits, unitPrice = x.UnitPrice });
                        }
                    }
                }
            }

            var orderType = new orderType
            {
                invoiceNumber = order.OrderInvoiceNumber,
                description = order.OrderNote
            };

            var transactionRequest = new transactionRequestType
            {
                order = orderType,
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),
                amount = order.OrderGrandTotal,
                payment = paymentType,
                billTo = billingAddress,
                lineItems = lineItems.ToArray()
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the controller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse(); 
            var responseCode = response.transactionResponse.responseCode;
            var paymentMethod = cart.PaymentOption.PaymentOptionDisplayName;

            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            string[] expDate = model.CardData.ExpDate.Split('/');

            if (response.messages.resultCode == messageTypeEnum.Error)
            {
                var msg = response.transactionResponse.errors;
                var cardErrors = new List<string>();
                var failedResponse = new PaymentResultInfo();
                var failed = OrderStatusInfoProvider.GetOrderStatuses().WhereEquals(nameof(OrderStatusInfo.StatusName), ContentIdentifiers.PAYMENT_FAILED_PAYMENT_STATUS).First();

                foreach (var i in msg)
                {
                    failedResponse = new PaymentResultInfo()
                    {
                        PaymentStatusName = failed.StatusDisplayName,
                        PaymentIsCompleted = false,
                        PaymentDate = DateTime.Now,
                        PaymentDescription = i.errorText,
                        PaymentIsFailed = true
                    };
                    cardErrors.Add(i.errorText);
                }
                order.UpdateOrderStatus(failedResponse);

                if (isFromWidget)
                {
                    var errors = "";
                    foreach (var x in cardErrors)
                    {
                        errors += $"<div class='ms-5'>{x}</div>";
                    }
                    return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = errors }), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<PaymentOptionInfo> enabledPaymentMethods = PaymentOptionInfoProvider.GetPaymentOptions(SiteContext.CurrentSiteID, true).ToList();
                    var paymentMethods = enabledPaymentMethods.Where(paymentMethod => PaymentOptionInfoProvider.IsPaymentOptionApplicable(cart, paymentMethod));

                    var previewAndPayModel = new PreviewAndPayViewModel
                    {
                        DeliveryDetails = new DeliveryDetailsViewModel
                        {
                            Customer = new CustomerViewModel
                            {
                                Email = cart.Customer.CustomerEmail,
                                FirstName = cart.Customer.CustomerFirstName,
                                LastName = cart.Customer.CustomerLastName,
                                PhoneNumber = cart.Customer.CustomerPhone
                            },
                            BillingAddress = new BillingAddressViewModel
                            {
                                Line1 = cart.ShoppingCartBillingAddress.AddressLine1,
                                Line2 = cart.ShoppingCartBillingAddress.AddressLine2,
                                City = cart.ShoppingCartBillingAddress.AddressCity,
                                PostalCode = cart.ShoppingCartBillingAddress.AddressZip,
                                CountryID = cart.ShoppingCartBillingAddress.AddressCountryID,
                                StateID = cart.ShoppingCartBillingAddress.AddressStateID
                            },
                            CurrentCart = cart
                        },
                        Cart = new ShoppingCartViewModel(cart, user)
                        {
                            OrderId = order.OrderID
                        },
                        PaymentMethod = new PaymentMethodViewModel
                        {
                            PaymentMethods = new SelectList(paymentMethods, nameof(PaymentOptionInfo.PaymentOptionID), nameof(PaymentOptionInfo.PaymentOptionDisplayName))
                        },
                        CardInfo = new CreditCardViewModel
                        {
                            CardNo = model.CardData.CardNo,
                            CVVCode = model.CardData.CVVCode,
                            Month = expDate[0],
                            Year = expDate[1]
                        },
                        CardErrors = cardErrors,
                        CouponCodes = cart.CouponCodes.AllAppliedCodes.Select(x => x.Code)
                    };

                    TempData["model"] = previewAndPayModel;
                    return RedirectToAction("PreviewAndPay", "Checkout");
                }
            }
            else
            {
                var orderStatusString = ContentIdentifiers.PAYMENT_FAILED_PAYMENT_STATUS;
                bool paymentIsFailed = true;
                switch (responseCode)
                {
                    case "1":
                        // Approved
                        orderStatusString = ContentIdentifiers.PAID_PAYMENT_STATUS;
                        paymentIsFailed = false;
                        break;
                    case "2":
                        // Declined

                        break;
                    case "3":
                        // Error

                        break;
                    case "4":
                        // Held for review

                        break;
                }

                var paidCreditStatus = OrderStatusInfoProvider.GetOrderStatuses().WhereEquals(nameof(OrderStatusInfo.StatusName), orderStatusString).FirstOrDefault();
                transactionResponseMessage[] successMessages = response.transactionResponse.messages;
                transactionResponseError[] errorMessages = response.transactionResponse.errors;

                var returnMessage = "";
                var errors = new List<string>();
                if (successMessages != null && !paymentIsFailed)
                {
                    var coursesDroppedStatus = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.COURSES_DROPPED_PAYMENT_STATUS, SiteContext.CurrentSiteName);

                    if (order.OrderStatusID != coursesDroppedStatus.StatusID)
                    {
                        if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null && user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName))
                        {
                            CookieHelper.Remove(ContentIdentifiers.FACILITY_ID_COOKIE);
                        }

                        //Custom facility classes purchase
                        if (order.OrderCustomData.ContainsColumn(ContentIdentifiers.FACILITY_ID_CUSTOM_DATA_COLUMN))
                        {
                            var facilityID = int.Parse(order.OrderCustomData.GetValue(ContentIdentifiers.FACILITY_ID_CUSTOM_DATA_COLUMN).ToString());

                            //Facility membership purchase
                            if (order.OrderCustomData.ContainsColumn(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN))
                            {
                                var amount = int.Parse(order.OrderCustomData.GetValue(ContentIdentifiers.MEMBERSHIP_COUNT_CUSTOM_DATA_COLUMN).ToString());
                                var exDate = DateTime.Today.AddYears(1).AddDays(1).AddSeconds(-1);

                                for (var i = 1; i <= amount; i++)
                                {
                                    var memName = order.OrderCustomData.GetValue($"Membership-{i}-Name").ToString();
                                    var qty = order.OrderCustomData.GetValue($"Membership-{i}-Qty").ToString();

                                    var mem = ClassMemberDependencies.FacilityMembershipRepo.CreateFacilityMembership(facilityID, exDate, memName);

                                    var classIDs = order.OrderCustomData.GetValue($"Membership-{i}-ClassIDs").ToString();
                                    var ids = classIDs.Split(',');

                                    foreach (var item in ids)
                                    {
                                        foreach (var cartItem in cart.CartItems)
                                        {
                                            //var discounts = MultiBuyDiscountSKUInfoProvider.GetMultiBuyDiscountSKUs()
                                            //    .Where(x => x.SKUID == cartItem.SKUID)
                                            //    .ToList();

                                            //foreach (var d in discounts)
                                            //{
                                            //    var discount = MultiBuyDiscountInfoProvider.GetMultiBuyDiscountInfo(d.MultiBuyDiscountID);
                                            //    discount.MultiBuyDiscountEnabled = true;
                                            //    discount.Update();
                                            //}

                                            if (cartItem.SKU.SKUNumber == item)
                                            {
                                                cart.CartItems.Remove(cartItem);
                                                break;
                                            }
                                        }

                                        ClassMemberDependencies.FacilityClassRepo.CreateFacilityMembershipClass(facilityID, int.Parse(item), order.OrderInvoiceNumber, int.Parse(qty),
                                            mem.FacilityMembershipID);
                                    }
                                }

                                cart.ShoppingCartCustomData.Remove(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN);

                                if (CookieHelper.GetExistingCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE) != null)
                                {
                                    CookieHelper.Remove(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);
                                }
                            }
                            //Facility regular course purchase
                            else
                            {
                                foreach (var x in cart.CartItems)
                                {
                                    if (!x.IsProductOption)
                                    {
                                        bool canPrint = CanPrintClass(cart.CartItems, x);

                                        if (x.SKU.SKUProductType == SKUProductTypeEnum.Product && !x.IsBundleItem)
                                        {
                                            ClassMemberDependencies.FacilityClassRepo.CreateRegularFacilityClass(facilityID, int.Parse(x.SKU.SKUNumber), order.OrderInvoiceNumber,
                                                x.CartItemUnits, canPrint);

                                        }
                                        else if (x.SKU.SKUProductType == SKUProductTypeEnum.Bundle)
                                        {
                                            var bundleItems = SKUInfoProvider.GetBundleItems(x.SKUID);

                                            foreach (var i in bundleItems)
                                            {
                                                if (!i.IsProductOption)
                                                {
                                                    ClassMemberDependencies.FacilityClassRepo.CreateRegularFacilityClass(facilityID, int.Parse(i.SKUNumber), order.OrderInvoiceNumber,
                                                        x.CartItemUnits, canPrint);
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        // regular student purchase
                        else
                        {
                            var startClassWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
                            int? studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
                            var student = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto(x));

                            if (student == null)
                            {
                                student = ClassMemberDependencies.ContactRepo.MapUserToStudent(user);

                                var c = CookieHelper.GetExistingCookie(ContentIdentifiers.STUDENT_ID_COOKIE);
                                if (c == null)
                                {
                                    HttpCookie cookie = new HttpCookie(ContentIdentifiers.STUDENT_ID_COOKIE)
                                    {
                                        Value = student.ContactID.ToString()
                                    };

                                    Response.Cookies.Add(cookie);
                                }
                            }

                            foreach (var x in cart.CartItems)
                            {
                                if (!x.IsProductOption)
                                {
                                    if (x.SKU.SKUProductType == SKUProductTypeEnum.Product && !x.IsBundleItem)
                                    {
                                        bool canPrintClass = CanPrintClass(cart.CartItems, x);

                                        for (var i = 1; i <= x.CartItemUnits; i++)
                                        {
                                            ClassMemberDependencies.StudentClassRepo.CreateStudentClass(student.ContactID, int.Parse(x.SKU.SKUNumber), order.OrderInvoiceNumber, canPrintClass);
                                        }
                                    }
                                    else if (x.SKU.SKUProductType == SKUProductTypeEnum.Bundle)
                                    {
                                        CourseMembership courseMem = CourseMembershipProvider.GetCourseMemberships().WhereEquals(nameof(CourseMembership.SKU.SKUID), x.SKUID);

                                        int? memID = null;
                                        DateTime? eDate = null;

                                        if (courseMem != null)
                                        {
                                            var exDate = DateTime.Today.AddYears(1).AddDays(1).AddSeconds(-1);

                                            var mem = ClassMemberDependencies.StudentMembershipRepo.CreateStudentMembership(student.ContactID, courseMem.SKU.SKUName, exDate);

                                            memID = mem.StudentMembershipID;
                                            eDate = exDate;
                                        }

                                        bool canPrintClass = CanPrintClass(cart.CartItems, x);

                                        var bundleItems = SKUInfoProvider.GetBundleItems(x.SKUID);

                                        foreach (var item in bundleItems)
                                        {
                                            if (!item.IsProductOption)
                                            {
                                                ClassMemberDependencies.StudentClassRepo.CreateStudentClass(student.ContactID, int.Parse(item.SKUNumber), order.OrderInvoiceNumber, 
                                                    canPrintClass, memID, eDate);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    order.OrderDate = DateTime.Now;
                    order.Update();

                    // succeeded
                    foreach (var sm in successMessages)
                    {
                        var allResponse = new PaymentResultInfo()
                        {
                            PaymentDate = DateTime.Now,
                            PaymentAuthorizationID = response.transactionResponse.transId,
                            PaymentStatusName = paidCreditStatus.StatusDisplayName,
                            PaymentIsCompleted = paidCreditStatus.StatusOrderIsPaid,
                            PaymentDescription = sm.description,
                            PaymentIsFailed = paymentIsFailed
                        };
                        order.OrderPaymentResult = allResponse;
                        order.UpdateOrderStatus(allResponse);
                    }
                }
                else if (errorMessages != null && paymentIsFailed)
                {
                    // errored 
                    foreach (var em in errorMessages)
                    {
                        var allResponse = new PaymentResultInfo()
                        {
                            PaymentDate = DateTime.Now,
                            PaymentAuthorizationID = response.transactionResponse.transId,
                            PaymentStatusName = paidCreditStatus.StatusDisplayName,
                            PaymentIsCompleted = paidCreditStatus.StatusOrderIsPaid,
                            PaymentDescription = em.errorText,
                            PaymentIsFailed = paymentIsFailed
                        };

                        returnMessage += $"<div class='ms-5'>{em.errorText}</div>";
                        errors.Add(em.errorText);

                        order.OrderPaymentResult = allResponse;
                        order.UpdateOrderStatus(allResponse);
                    }
                }

                if (isFromWidget) return Json(JsonConvert.SerializeObject(new { Err = paymentIsFailed, ErrMsg = returnMessage }), JsonRequestBehavior.AllowGet);

                if (paymentIsFailed)
                {
                    IEnumerable<PaymentOptionInfo> enabledPaymentMethods = PaymentOptionInfoProvider.GetPaymentOptions(SiteContext.CurrentSiteID, true).ToList();
                    var paymentMethods = enabledPaymentMethods.Where(paymentMethod => PaymentOptionInfoProvider.IsPaymentOptionApplicable(cart, paymentMethod));

                    var previewAndPayModel = new PreviewAndPayViewModel
                    {
                        DeliveryDetails = new DeliveryDetailsViewModel
                        {
                            Customer = new CustomerViewModel
                            {
                                Email = cart.Customer.CustomerEmail,
                                FirstName = cart.Customer.CustomerFirstName,
                                LastName = cart.Customer.CustomerLastName,
                                PhoneNumber = cart.Customer.CustomerPhone
                            },
                            BillingAddress = new BillingAddressViewModel
                            {
                                Line1 = cart.ShoppingCartBillingAddress.AddressLine1,
                                Line2 = cart.ShoppingCartBillingAddress.AddressLine2,
                                City = cart.ShoppingCartBillingAddress.AddressCity,
                                PostalCode = cart.ShoppingCartBillingAddress.AddressZip,
                                CountryID = cart.ShoppingCartBillingAddress.AddressCountryID,
                                StateID = cart.ShoppingCartBillingAddress.AddressStateID
                            },
                            CurrentCart = cart
                        },
                        Cart = new ShoppingCartViewModel(cart, user)
                        {
                            OrderId = order.OrderID
                        },
                        PaymentMethod = new PaymentMethodViewModel
                        {
                            PaymentMethods = new SelectList(paymentMethods, nameof(PaymentOptionInfo.PaymentOptionID), nameof(PaymentOptionInfo.PaymentOptionDisplayName))
                        },
                        CardInfo = new CreditCardViewModel
                        {
                            CardNo = model.CardData.CardNo,
                            CVVCode = model.CardData.CVVCode,
                            Month = expDate[0],
                            Year = expDate[1]
                        },
                        CardErrors = errors,
                        CouponCodes = cart.CouponCodes.AllAppliedCodes.Select(x => x.Code)
                    };

                    TempData["model"] = previewAndPayModel;
                    return RedirectToAction("PreviewAndPay", "Checkout");
                }

                return RedirectToAction("ThankYou", "Checkout", new { orderID = order.OrderID });
            }
        }

        private bool CanPrintClass(IEnumerable<ShoppingCartItemInfo> cartItems, ShoppingCartItemInfo currentItem)
        {
            foreach (var y in cartItems)
            {
                if (y.IsProductOption && y.ParentProduct.SKUID == currentItem.SKUID && y.TotalPrice != 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}

//4111111111111111