﻿using CMS.DocumentEngine.Types.Pedagogy;
using CMS.EventLog;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;
using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Controllers;
using Pedagogy.Models.Map;

using System;
using System.Web.Mvc;


[assembly: DynamicRouting(typeof(MapController), new string[] { Map.CLASS_NAME }, "Index")]
namespace Pedagogy.Controllers
{
    public class MapController : BaseController
    {
        private IDynamicRouteHelper mDynamicRouteHelper;
        public MapController(IDataDependencies dependencies, IDynamicRouteHelper drh) : base(dependencies) { mDynamicRouteHelper = drh; }


        public ActionResult Index()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();

                //needed for workflow to be enabled.
                bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

                if (page == null)
                {
                    throw new Exception("DynamicRouting: GetPage returned null");
                }

                var node = Dependencies.MapRepo.GetMapByNodeGuid(page.NodeGUID, isPublished);

                if(node.Url.Contains(ContentIdentifiers.STATE_BOARD_OF_NURSING_MAP))
                {
                    var model = GetBaseViewModel(node, new MapViewModel()
                    {
                        MapData = node,
                        MapResources = Dependencies.ResourceRepo.GetAllResourcesByPath(ContentIdentifiers.STATE_BOARD_OF_NURSING_MAP, 3)
                    });

                    return View(model);
                }
                else if(node.Url.Contains(ContentIdentifiers.STATE_BOARD_OF_SOCIAL_WORK_MAP))
                {
                    var model = GetBaseViewModel(node, new MapViewModel()
                    {
                        MapData = node,
                        MapResources = Dependencies.ResourceRepo.GetAllResourcesByPath(ContentIdentifiers.STATE_BOARD_OF_SOCIAL_WORK_MAP, 3)
                    });

                    return View(model);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("MapController", "ERROR", ex);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }
    }
}