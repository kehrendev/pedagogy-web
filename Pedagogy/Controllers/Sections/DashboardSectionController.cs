﻿using Kentico.PageBuilder.Web.Mvc;
using Pedagogy.Controllers.Sections;
using Pedagogy.Models.Sections.DashboardSection;
using System.Web.Mvc;

[assembly: RegisterSection("Pedagogy.DashboardSection", 
    typeof(DashboardSectionController),
    "Dashboard Section", 
    Description = "A section used for the dashboard pages",
    IconClass = "icon-layout")]

namespace Pedagogy.Controllers.Sections
{
    public class DashboardSectionController : SectionController<DashboardSectionProperties>
    {
        public ActionResult Index()
        {
            var props = GetProperties();

            var model = new DashboardSectionViewModel
            {
                DisplayHeading = props.DisplayHeading
            };

            return PartialView("Sections/_Pedagogy.DashboardSection", model);
        }
    }
}