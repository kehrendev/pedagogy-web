﻿using Pedagogy.Controllers;
using Pedagogy.Models.PedagogyPage;
using Pedagogy.DataAccess.DependencyInjection;

using System;
using System.Web.Mvc;

using CMS.Membership;
using CMS.SiteProvider;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.EventLog;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;

[assembly: DynamicRouting(typeof(PedagogyPageController), new string[] { "CMS.Root" }, "Index")]
[assembly: DynamicRouting(typeof(PedagogyPageController), new string[] { PedagogyPage.CLASS_NAME }, "Index")]
[assembly: DynamicRouting(typeof(PedagogyPageController), new string[] { Folder.CLASS_NAME }, "AdminManagePages")]
namespace Pedagogy.Controllers
{
    public class PedagogyPageController : BaseController
    {
        private readonly IDynamicRouteHelper mDynamicRouteHelper;
        public PedagogyPageController(IDataDependencies dependencies, IDynamicRouteHelper drh) : base(dependencies) { mDynamicRouteHelper = drh; }

        public ActionResult Index()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();
                if (page.ClassName == "CMS.Root") page = mDynamicRouteHelper.GetPage(ContentIdentifiers.HOME_PAGE);

                //needed for workflow to be enabled.
                bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

                if (page == null)
                {
                    throw new Exception("DynamicRouting: GetPage returned null");
                }

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                var node = Dependencies.PedagogyPageRepo.GetPedagogyPageByNodeGuid(page.NodeGUID, isPublished);

                if (node == null) return Redirect($"{ContentIdentifiers.LOGIN_PAGE}?returnUrl={page.NodeAliasPath}");

                var model = GetBaseViewModel(node, new PedagogyPageViewModel()
                {
                    PageData = node
                });

                return View(model);
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("PedagogyPageController", "ERROR", e);
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }
        }

        public ActionResult AdminManagePages()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();

                if (page.NodeAliasPath == ContentIdentifiers.ADMIN_MANAGE_FUNCTIONS_FOLDER)
                {
                    var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

                    if(user != null)
                    {
                        if (user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName) || user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName)) return Redirect(ContentIdentifiers.MANAGE_CORPORATION_PAGE);
                        if (user.IsInRole(ContentIdentifiers.FACILITY_ADMIN_ROLE, SiteContext.CurrentSiteName)) return Redirect(ContentIdentifiers.MANAGE_FACILITY_PAGE);
                    }

                    return Redirect(ContentIdentifiers.NOT_FOUND);
                }
                else
                {
                    return Redirect(ContentIdentifiers.NOT_FOUND);
                }
            }
            catch(Exception e)
            {
                EventLogProvider.LogException("PedagogyPageController", "ERROR", e);
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }
        }
    }
}