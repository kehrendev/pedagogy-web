﻿using CMS.DocumentEngine.Types.Pedagogy;
using CMS.EventLog;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;
using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.Controllers;
using Pedagogy.Models.Author;

using System;
using System.Web.Mvc;


[assembly: DynamicRouting(typeof(AuthorController), new string[] { Author.CLASS_NAME }, "Index")]
namespace Pedagogy.Controllers
{
    public class AuthorController : BaseController
    {
        private IDynamicRouteHelper mDynamicRouteHelper;
        public AuthorController(IDataDependencies dependencies, IDynamicRouteHelper drh) : base(dependencies) { mDynamicRouteHelper = drh; }

        public ActionResult Index()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();

                //needed for workflow to be enabled.
                bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

                if (page == null)
                {
                    throw new Exception("DynamicRouting: GetPage returned null");
                }

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                var node = Dependencies.AuthorRepo.GetAuthorByNodeGuid(page.NodeGUID, isPublished);

                var model = GetBaseViewModel(node, new AuthorViewModel()
                {
                    AuthorData = node,
                    SocialLinks = Dependencies.SocialMediaRepo.GetSocialMediaItemsByPath($"{page.NodeAliasPath}/%"),
                    AuthoredCourses = Dependencies.CourseRepo.GetCoursesByAuthor(node.Url)
                });

                return View(model);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("AuthorController", "ERROR", ex);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }
    }
}