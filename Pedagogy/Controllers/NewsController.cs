﻿using CMS.DocumentEngine.Types.Pedagogy;
using CMS.EventLog;
using DynamicRouting.Interfaces;
using DynamicRouting.Kentico.MVC;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Dto.News;
using Pedagogy.Controllers;
using Pedagogy.Models;
using Pedagogy.Models.News;
using Pedagogy.Models.Search;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


[assembly: DynamicRouting(typeof(NewsController), new string[] { News.CLASS_NAME }, "Details")]
namespace Pedagogy.Controllers
{
    public class NewsController : BaseController
    {
        private IDynamicRouteHelper mDynamicRouteHelper;
        public NewsController(IDataDependencies dependencies, IDynamicRouteHelper drh) : base(dependencies) { mDynamicRouteHelper = drh; }

        public ActionResult Index()
        {
            SearchViewModel searchResults = (SearchViewModel)TempData["newsSearchModel"] ?? new SearchViewModel { };
            IEnumerable<NewsItemDto> newsArticlesByCategory = (IEnumerable<NewsItemDto>)TempData["articles"];

            var currentPage = mDynamicRouteHelper.GetPage(ContentIdentifiers.NEWS_ARTICLES);

            //needed for workflow to be enabled.
            bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

            HttpContext.Kentico().PageBuilder().Initialize(currentPage.DocumentID);

            var node = Dependencies.PedagogyPageRepo.GetPedagogyPageByNodeGuid(currentPage.NodeGUID, isPublished);

            var categories = Dependencies.NewsRepo.GetAllNewsArticleCategories();
            var c = new List<CategoryViewModel>();
            foreach(var item in categories)
            {
                c.Add(new CategoryViewModel
                {
                    Name = item.CategoryDisplayName,
                    Value = item.CategoryName
                });
            }

            var baseModel = GetBaseViewModel(node, new NewsViewModel()
            {
                Categories = c,
                SearchData = searchResults,
                PagingInfo = new PagingInfo<NewsItemDto>
                {
                    Data = searchResults.NewsArticleData ?? newsArticlesByCategory ?? Dependencies.NewsRepo.GetAllNewsArticles() ?? new List<NewsItemDto>(),
                    ItemsPerPage = 9,
                    CurrentPage = 1
                }
            });
            baseModel.ViewModel.PagingInfo.TotalItems = baseModel.ViewModel.PagingInfo.Data.Count();

            return View(baseModel);
        }

        [HttpPost]
        public ActionResult Index(List<string> currentList, int page = 1, string searchQuery = null)
        {
            var data = new List<NewsItemDto>();
            if(currentList.Count() > 0 && currentList != null)
            {
                foreach (var item in currentList)
                {
                    data.Add(Dependencies.NewsRepo.GetNewsArticleByTitle(item));
                }
            }

            var currentPage = mDynamicRouteHelper.GetPage(ContentIdentifiers.NEWS_ARTICLES);

            //needed for workflow to be enabled.
            bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

            HttpContext.Kentico().PageBuilder().Initialize(currentPage.DocumentID);

            var node = Dependencies.PedagogyPageRepo.GetPedagogyPageByNodeGuid(currentPage.NodeGUID, isPublished);

            var categories = Dependencies.NewsRepo.GetAllNewsArticleCategories();
            var c = new List<CategoryViewModel>();
            foreach (var item in categories)
            {
                c.Add(new CategoryViewModel
                {
                    Name = item.CategoryDisplayName,
                    Value = item.CategoryName
                });
            }

            var baseModel = GetBaseViewModel(node, new NewsViewModel()
            {
                Categories = c,
                SearchData = new SearchViewModel { Query = searchQuery },
                PagingInfo = new PagingInfo<NewsItemDto>
                {
                    Data = data ?? new List<NewsItemDto>(),
                    ItemsPerPage = 9,
                    CurrentPage = page
                }
            });
            baseModel.ViewModel.PagingInfo.TotalItems = baseModel.ViewModel.PagingInfo.Data.Count();

            return View(baseModel);
        }

        public ActionResult GetArticlesByCategory(string category)
        {
            var newsArticlesByCategory = Dependencies.NewsRepo.GetNewsArticlesByCategory(category);

            TempData["articles"] = newsArticlesByCategory;
            return RedirectToAction("Index");
        }

        public ActionResult Details()
        {
            try
            {
                var page = mDynamicRouteHelper.GetPage();

                //needed for workflow to be enabled.
                bool isPublished = HttpContext.Kentico().PageBuilder().EditMode || HttpContext.Kentico().Preview().Enabled;

                if (page == null)
                {
                    throw new Exception("DynamicRouting: GetPage returned null");
                }

                HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

                var node = Dependencies.NewsRepo.GetNewsItemByNodeGuid(page.NodeGUID, isPublished);

                var categories = Dependencies.NewsRepo.GetAllNewsArticleCategories();
                var c = new List<CategoryViewModel>();
                foreach (var item in categories)
                {
                    c.Add(new CategoryViewModel
                    {
                        Name = item.CategoryDisplayName,
                        Value = item.CategoryName
                    });
                }

                var model = GetBaseViewModel(node, new NewsDetailsViewModel()
                {
                    NewsData = node,
                    Categories = c
                });

                return View(model);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("NewsController", "ERROR", ex);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }
    }
}