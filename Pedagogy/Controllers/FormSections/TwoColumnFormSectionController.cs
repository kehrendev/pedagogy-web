﻿using Kentico.Forms.Web.Mvc;
using Pedagogy.Controllers.Widgets;
using System.Web.Mvc;

[assembly: RegisterFormSection("Pedagogy.FormSections.TwoColumns", typeof(TwoColumnFormSectionController), "Two columns",
    Description = "Organizes fields into two equal-width columns side-by-side.", IconClass = "icon-l-cols-2")]
namespace Pedagogy.Controllers.Widgets
{
    public class TwoColumnFormSectionController : Controller
    {
        public ActionResult Index()
        {
            return PartialView("FormSections/_Pedagogy.TwoColumnFormSection");
        }
    }
}