﻿using CMS.Ecommerce;
using CMS.Globalization;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;
using CMS.EmailEngine;
using CMS.Taxonomy;
using CMS.Localization;
using CMS.EventLog;
using CMS.CustomTables.Types.Pedagogy;
using CMS.CustomTables;

using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Dto;
using Pedagogy.Models.Checkout;
using Pedagogy.Models;
using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.Facility;
using Pedagogy.ClassMembers.Entities.ContactFacility;

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System;
using System.Web;

using Newtonsoft.Json;
using CMS.DocumentEngine.Types.Pedagogy;

namespace Pedagogy.Controllers
{
    public class CheckoutController : BaseController
    {
        #region Dependency Injection

        private readonly IShoppingService mShoppingService;
        private readonly IDataDependencies Dependencies;
        private readonly IAddressConverter addressConverter;
        private readonly IClassMemberDependencies ClassMemberDependencies;

        public CheckoutController(IDataDependencies dependencies, IShoppingService iss, IAddressConverter addressConverter, IClassMemberDependencies classMemberDependencies) : base(dependencies)
        {
            mShoppingService = iss;
            Dependencies = dependencies;
            this.addressConverter = addressConverter;
            ClassMemberDependencies = classMemberDependencies;
        }

        #endregion

        #region STEP 1 Cart

        // setting up shopping cart page
        public ActionResult ShoppingCart()
        {
            // get the current cart and the current logged in user
            ShoppingCartInfo currentCart = mShoppingService.GetCurrentShoppingCart();
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            // set the currency
            currentCart.ShoppingCartCurrencyID = CurrencyInfoProvider.GetMainCurrency(SiteContext.CurrentSiteID).CurrencyID;

            // check if they were creating a membership
            currentCart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, out object mem);
            if (mem != null)
            {
                // send the user to the membership page
                return Redirect(nameof(CheckoutController.CreateMembership));
            }

            // TODO: create a page in the content tree?
            var model = GetBaseViewModel(new BaseDto
            {
                MetaTitle = ResHelper.GetString("Checkout.ShoppingCart.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                MetaDescription = ResHelper.GetString("Checkout.ShoppingCart.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                MetaKeywords = ResHelper.GetString("Checkout.ShoppingCart.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                UsePageBuilder = false
            },
            new ShoppingCartViewModel(currentCart, user) { });

            return View(model);
        }

        // checking if the cart is valid and the user is logged in before sending them to the delivery details page
        [HttpPost]
        public ActionResult ShoppingCartCheckout()
        {
            try
            {
                // get the current cart
                ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();

                // validate the cart
                var cartValidator = ShoppingCartInfoProvider.ValidateShoppingCart(cart);

                // the cart is valid
                if (!cartValidator.Any())
                {
                    // save the cart
                    mShoppingService.SaveCart();

                    // check if the user is logged in
                    UserInfo userInfo = cart.User;
                    if (userInfo == null)
                    {
                        // send them to the log in page
                        return RedirectToAction("Login", "LoginManager", new { returnUrl = ContentIdentifiers.DELIVERY_DETAILS });
                    }

                    // send them to the delivery details page
                    return RedirectToAction(nameof(CheckoutController.DeliveryDetails));
                }

                // the cart isn't valid send them back to the shopping cart page
                return RedirectToAction(nameof(CheckoutController.ShoppingCart));
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("CheckoutController", "ERROR", e);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }

        // updates the quantity of an item in the cart
        [HttpPost]
        public ActionResult UpdateItem(int itemId, int itemUnits)
        {
            var currentCart = mShoppingService.GetCurrentShoppingCart();
            mShoppingService.UpdateItemQuantity(itemId, itemUnits);
            var items = mShoppingService.GetCurrentShoppingCart().CartItems;

            var list = new List<ShoppingCartItemViewModel>();
            foreach (var i in items)
            {
                List<OptionCategoryInfo> optionCategories = OptionCategoryInfoProvider.GetProductOptionCategories(i.SKUID, true, OptionCategoryTypeEnum.Products).ToList();
                var skuInfo = new List<SKUInfo>();
                foreach (var option in optionCategories)
                {
                    skuInfo = SKUInfoProvider.GetSKUOptionsForProduct(i.SKUID, option.CategoryID, true).ToList();
                }

                list.Add(new ShoppingCartItemViewModel
                {
                    CartItemID = i.CartItemID,
                    SKUID = i.SKUID,
                    CartItemUnits = i.CartItemUnits,
                    TotalPrice = i.TotalPrice,
                    SKUName = i.SKU.SKUName,
                    CurrentCart = currentCart.CartItems,
                    CurrencyFormatString = currentCart.Currency.CurrencyFormatString,
                    Options = skuInfo
                });
            }

            return RedirectToAction(nameof(CheckoutController.ShoppingCart));
        }

        // adds or removes the print option from the course
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateOption(int itemSkuid, int itemUnits, int optionSkuid = 0)
        {
            var currentCart = mShoppingService.GetCurrentShoppingCart();
            List<ShoppingCartItemInfo> items = currentCart.CartItems.ToList();
            var currencyString = currentCart.Currency.CurrencyFormatString;
            string updatedPrice;

            if (optionSkuid == 0)
            {
                foreach (var i in items)
                {
                    if (i.IsProductOption && i.ParentProduct.SKUID == itemSkuid)
                    {
                        mShoppingService.RemoveItemFromCart(i.CartItemID);
                    }
                }

                updatedPrice = string.Format(currencyString, currentCart.GrandTotal);

                return Json(updatedPrice, JsonRequestBehavior.AllowGet);
            }

            foreach (var i in items)
            {
                if (i.SKUID == itemSkuid)
                {
                    mShoppingService.RemoveItemFromCart(i.CartItemID);

                    var optsSkuid = new List<int>
                    {
                        optionSkuid
                    };

                    var cartItemParams = new ShoppingCartItemParameters(itemSkuid, itemUnits, optsSkuid);
                    mShoppingService.AddItemToCart(cartItemParams);
                }
                else if (i.TotalPrice == 0)
                {
                    mShoppingService.RemoveItemFromCart(i.CartItemID);
                }
            }

            updatedPrice = string.Format(currencyString, currentCart.GrandTotal);

            return Json(updatedPrice, JsonRequestBehavior.AllowGet);
        }

        // adds a new item to the cart
        [HttpPost]
        public JsonResult AddItem(int itemSkuId, int itemUnits)
        {
            var currentItem = SKUInfoProvider.GetSKUInfo(itemSkuId);
            var cartItems = mShoppingService.GetCurrentShoppingCart().CartItems;

            var alreadyInCartText = ResHelper.GetString("Checkout.AddItem.AlreadyInCartText", LocalizationContext.CurrentCulture.CultureCode);

            if (currentItem.SKUProductType == SKUProductTypeEnum.Bundle)
            {
                var bundleItems = SKUInfoProvider.GetBundleItems(itemSkuId);

                foreach (var item in bundleItems)
                {
                    foreach (var i in cartItems)
                    {
                        if (i.SKUID == item.SKUID)
                        {
                            return Json(new { warningText = alreadyInCartText, warning = true, skuid = itemSkuId, qty = itemUnits }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else if (currentItem.SKUProductType == SKUProductTypeEnum.Product)
            {
                foreach (var item in cartItems)
                {
                    if (item.SKUID == itemSkuId)
                    {
                        return Json(new { warningText = alreadyInCartText, warning = true, skuid = itemSkuId, qty = itemUnits }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            var optsSkuid = GetProductOptions(itemSkuId);

            var cartItemParams = new ShoppingCartItemParameters(itemSkuId, itemUnits, optsSkuid);

            mShoppingService.AddItemToCart(cartItemParams);

            return Json(new { warningText = "", warning = false, skuid = itemSkuId, qty = itemUnits }, JsonRequestBehavior.AllowGet);
        }

        // if the item is already in the cart but the user selects add anyway it adds the course to the cart
        public ActionResult ForceAddItem(int skuid, int units)
        {
            var optsSkuid = GetProductOptions(skuid);

            var cartItemParams = new ShoppingCartItemParameters(skuid, units, optsSkuid);

            mShoppingService.AddItemToCart(cartItemParams);

            return RedirectToAction(nameof(CheckoutController.ShoppingCart));
        }

        // removes the item from the cart
        [HttpPost]
        public ActionResult RemoveItem(int itemId, int itemUnits)
        {
            mShoppingService.UpdateItemQuantity(itemId, itemUnits);

            return RedirectToAction(nameof(CheckoutController.ShoppingCart));
        }

        // removes all items from the cart
        public ActionResult RemoveAllItems()
        {
            mShoppingService.RemoveAllItemsFromCart();

            return RedirectToAction(nameof(CheckoutController.ShoppingCart));
        }

        #endregion

        #region STEP 1 Create Membership

        // setting up the create membership page
        public ActionResult CreateMembership()
        {
            var memDetails = new CreateMembershipViewModel { };

            ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();
            cart.ShoppingCartCurrencyID = CurrencyInfoProvider.GetMainCurrency(SiteContext.CurrentSiteID).CurrencyID;
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            if (cart.ShoppingCartID != 0)
            {
                cart.ShoppingCartCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, "True");
                cart.Update();
            }
            else
            {
                cart = new ShoppingCartInfo
                {
                    ShoppingCartSiteID = SiteContext.CurrentSiteID,
                    ShoppingCartLastUpdate = DateTime.Now,
                    User = user
                };

                cart.ShoppingCartCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, "True");
                ShoppingCartInfoProvider.SetShoppingCartInfo(cart);
            }

            var isSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName);
            var isCorpAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName);

            memDetails.Facilities = new SelectList(ClassMembersHelper.GetFacilitiesForCurrentUser(ClassMemberDependencies, isSiteAdmin, isCorpAdmin, user.UserID, ""), nameof(ClassMembers.Facility.FacilityID), nameof(ClassMembers.Facility.Name));
            memDetails.MembershipCategories = new SelectList(CategoryInfoProvider.GetCategories().WhereStartsWith(nameof(CategoryInfo.CategoryName), "facilityMems."), nameof(CategoryInfo.CategoryName), nameof(CategoryInfo.CategoryDisplayName));

            memDetails.FacilityID = CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null ? int.Parse(CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE).Value) : 0;

            var err = (string)TempData["ClassesError"];
            if (!string.IsNullOrEmpty(err))
            {
                memDetails.ErrMsg = err;
            }

            var c = new List<SelectListItem>();
            var mems = new List<MembershipViewModel>();
            var memNames = new List<string>();
            foreach (var i in cart.CartItems)
            {
                if (!i.IsProductOption)
                {
                    if (i.CartItemCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, out object memName))
                    {
                        if (!memNames.Contains(memName.ToString())) memNames.Add(memName.ToString());
                    }
                }
            }

            if (memNames.Count > 0)
            {
                foreach (var x in memNames)
                {
                    var mem = new MembershipViewModel { MembershipName = x };
                    var classes = new Dictionary<int, List<string>>();
                    foreach (var item in cart.CartItems)
                    {
                        if (!item.IsProductOption)
                        {
                            if (item.CartItemCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, out object memName))
                            {
                                if (x == memName.ToString())
                                {
                                    mem.Quantity = item.CartItemUnits;
                                    var classData = Dependencies.CourseRepo.GetCourseBySKUNumber(item.SKU.SKUNumber, x => new Course { CourseContactHours = x.CourseContactHours });
                                    classes.Add(int.Parse(item.SKU.SKUNumber), new List<string> { item.SKU.SKUName.ToString(), classData.CourseContactHours.ToString(), item.SKUID.ToString() });
                                }
                            }
                            else
                            {
                                if (!item.IsBundleItem && item.BundleItems.Count == 0)
                                {
                                    var id = int.Parse(item.SKU.SKUNumber);
                                    var classData = Dependencies.CourseRepo.GetCourseBySKUNumber(item.SKU.SKUNumber, x => new Course { CourseTitle = x.CourseTitle });

                                    c.Add(new SelectListItem
                                    {
                                        Text = classData.CourseTitle,
                                        Value = id.ToString()
                                    });
                                }
                            }
                        }
                    }
                    mem.MemClassData = classes;
                    mems.Add(mem);
                }
            }
            else
            {
                foreach (var item in cart.CartItems)
                {
                    if (!item.IsProductOption)
                    {
                        if (!item.IsBundleItem && item.BundleItems.Count == 0 && !string.IsNullOrEmpty(item.SKU.SKUNumber))
                        {
                            var id = int.Parse(item.SKU.SKUNumber);
                            var classData = Dependencies.CourseRepo.GetCourseBySKUNumber(item.SKU.SKUNumber, x => new Course { CourseTitle = x.CourseTitle });

                            c.Add(new SelectListItem
                            {
                                Text = classData.CourseTitle,
                                Value = id.ToString()
                            });
                        }
                    }
                }
            }


            if (mems.Count > 0)
            {
                memDetails.Memberships = mems.ToArray();
            }

            memDetails.Classes = c;
            memDetails.PageSize = 5;

            var memCategory = CookieHelper.GetExistingCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);
            if (memCategory != null) memDetails.MembershipCategory = memCategory.Value;

            var model = GetBaseViewModel(new BaseDto
            {
                MetaTitle = ResHelper.GetString("Checkout.CreateMembership.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                MetaDescription = ResHelper.GetString("Checkout.CreateMembership.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                MetaKeywords = ResHelper.GetString("Checkout.CreateMembership.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                UsePageBuilder = false
            }, memDetails);

            return View(model);
        }

        // checks to make sure the membership is valid and sends them to the delivery details page
        [HttpPost]
        public ActionResult CreateMembership(BaseViewModel<CreateMembershipViewModel> model)
        {
            try
            {
                ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();

                var mem = cart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, out object membershipPurchase);
                if (!mem)
                {
                    cart.ShoppingCartCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, true);
                    cart.Update();
                }

                for (var x = 0; x < model.ViewModel.Memberships.Length; x++)
                {
                    foreach (var item in cart.CartItems)
                    {
                        if (model.ViewModel.Memberships[x].ClassIDs == null)
                        {
                            var errMsg = ResHelper.GetString("Checkout.BuildMembership.NoClassesAddedValidationMsg", LocalizationContext.CurrentCulture.CultureCode);
                            item.CartItemCustomData.Clear();
                            return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = errMsg, Url = "" }), JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            foreach (var i in model.ViewModel.Memberships[x].ClassIDs)
                            {
                                if (item.SKU.SKUNumber == i.ToString())
                                {
                                    item.CartItemCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, model.ViewModel.Memberships[x].MembershipName);
                                    item.CartItemUnits = model.ViewModel.Memberships[x].Quantity;
                                    item.Update();
                                }
                                if (item.IsProductOption && i.ToString() == item.ParentProduct.SKU.SKUNumber)
                                {
                                    item.CartItemCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, model.ViewModel.Memberships[x].MembershipName);
                                    item.CartItemUnits = model.ViewModel.Memberships[x].Quantity;
                                    item.Update();
                                }
                            }
                        }
                    }
                }

                cart.ShoppingCartCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_QTY_CUSTOM_DATA_COLUMN, model.ViewModel.Memberships[0].Quantity);
                cart.Update();

                double contactHours = 0;
                foreach (var item in cart.CartItems)
                {
                    if (!item.IsProductOption && !item.IsBundleItem)
                    {
                        var course = Dependencies.CourseRepo.GetCourseBySKUNumber(item.SKU.SKUNumber, x => new Course { CourseContactHours = x.CourseContactHours });
                        contactHours += course.CourseContactHours;
                    }
                }

                var categoryName = CookieHelper.GetExistingCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);

                if (categoryName != null)
                {
                    var categoryID = Dependencies.FacilityMembershipPriceRepo.GetPriceCategory(categoryName.Value);

                    var tooLow = Dependencies.FacilityMembershipPriceRepo.GetFacilityMembershipPrice(categoryID.ToString(), ContentIdentifiers.TOO_LOW);

                    if (contactHours <= tooLow)
                    {
                        var errMsg = ResHelper.GetString("Checkout.BuildMembership.ContactHoursTooLowWarningMsg", LocalizationContext.CurrentCulture.CultureCode);
                        return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = errMsg, Url = "" }), JsonRequestBehavior.AllowGet);
                    }

                    Func<double> tooHighDataLoadMethod = () => CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
                        .Where(x => x.FacilityMembershipCategory == categoryID.ToString())
                        .Where(x => x.FacilityMembershipValidationStatus == ContentIdentifiers.TOO_HIGH)
                        .Select(x => x.FacilityMembershipStartingContactHours)
                        .FirstOrDefault();

                    var cacheSettings2 = new CacheSettings(10, nameof(CheckoutController), nameof(AddItemToMembershipCart), SiteContext.CurrentSiteName, LocalizationContext.CurrentCulture.CultureCode, "GetTooHighValue", categoryID.ToString())
                    {
                        GetCacheDependency = () =>
                        {
                            string dependencyCacheKey = $"{nameof(CheckoutController)}|{nameof(AddItemToMembershipCart)}|{SiteContext.CurrentSiteName}|{LocalizationContext.CurrentCulture.CultureCode}|GetTooHighValue|{categoryID}";
                            return CacheHelper.GetCacheDependency(dependencyCacheKey);
                        }
                    };

                    var tooHigh = CacheHelper.Cache(tooHighDataLoadMethod, cacheSettings2);

                    if (contactHours >= tooHigh)
                    {
                        var errMsg = ResHelper.GetString("Checkout.BuildMembership.ContactHoursTooHighWarningMsg", LocalizationContext.CurrentCulture.CultureCode);
                        return Json(JsonConvert.SerializeObject(new { Err = true, ErrMsg = errMsg, Url = "" }), JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "", Url = "/Checkout/DeliveryDetails" }), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("CheckoutController", "ERROR", e);
                return Json(JsonConvert.SerializeObject(new { Err = false, ErrMsg = "", Url = ContentIdentifiers.ERROR }));
            }
        }

        // adds the facility ID cookie used for the checkout process for autopopulating fields
        [HttpPost]
        public JsonResult AddFacilityID(int facilityID = 0)
        {
            if (facilityID != 0)
            {
                if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) == null)
                {
                    HttpCookie cookie = new HttpCookie(ContentIdentifiers.FACILITY_ID_COOKIE)
                    {
                        Value = facilityID.ToString()
                    };

                    Response.Cookies.Add(cookie);
                }
                else
                {
                    Response.Cookies.Set(new HttpCookie(ContentIdentifiers.FACILITY_ID_COOKIE) { Value = facilityID.ToString() });
                }
            }
            else
            {
                if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null)
                {
                    CookieHelper.Remove(ContentIdentifiers.FACILITY_ID_COOKIE);
                }
            }

            return Json(facilityID, JsonRequestBehavior.AllowGet);
        }

        // gets courses in the selected membership course category
        [HttpGet]
        public JsonResult GetClassesInMembership(string category, int pageNumber = 1, int pageSize = 6)
        {
            var classes = new List<CourseItemDto>();
            if (!string.IsNullOrEmpty(category))
            {
                var memCategory = CookieHelper.GetExistingCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);
                if (memCategory == null)
                {
                    mShoppingService.RemoveAllItemsFromCart();
                    HttpCookie cookie = new HttpCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE)
                    {
                        Value = category
                    };

                    Response.Cookies.Add(cookie);
                }
                else if (memCategory.Value != category)
                {
                    mShoppingService.RemoveAllItemsFromCart();
                    Response.Cookies.Set(new HttpCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE) { Value = category });
                }

                classes = Dependencies.CourseRepo.GetCoursesByCategory(new string[] { category }).OrderBy(x => x.CourseTitle).ToList();
            }
            else
            {
                CookieHelper.Remove(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);
            }

            var pagedData = PagingHelper.PagedResult(classes, pageNumber, pageSize);

            return Json(pagedData, JsonRequestBehavior.AllowGet);
        }

        // cancels the membership transaction and removes membership custom columns/cookies
        public ActionResult CancelMembershipTransaction()
        {
            mShoppingService.RemoveAllItemsFromCart();

            var currentCart = mShoppingService.GetCurrentShoppingCart();

            currentCart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, out object mem);
            if (mem != null)
            {
                currentCart.ShoppingCartCustomData.Remove(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN);
            }

            var memCategory = CookieHelper.GetExistingCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);
            if (memCategory != null)
            {
                CookieHelper.Remove(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);
            }

            var facilityID = CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE);
            if (facilityID != null)
            {
                //var id = int.Parse(facilityID.Value);
                //var facility = ClassMemberDependencies.FacilityRepo.GetFacility(id, x => new FacilityDto { FacilityName = x.Name });

                //var discount = DiscountInfoProvider.GetDiscountInfo(ContentIdentifiers.TEMP_FACILITY_MEMBERSHIP_DISCOUNT_NAME + facility.FacilityName.Replace(" ", ""), SiteContext.CurrentSiteName);
                //if (discount != null)
                //{
                //    if (discount.HasCoupons)
                //    {
                //        var coupons = CouponCodeInfoProvider.GetCouponCodes()
                //            .Where(x => x.CouponCodeDiscountID == discount.DiscountID)
                //            .ToList();

                //        foreach (var item in coupons)
                //        {
                //            var coupon = CouponCodeInfoProvider.GetCouponCodeInfo(item.CouponCodeID);
                //            CouponCodeInfoProvider.DeleteCouponCodeInfo(coupon);
                //        }
                //    }
                //    DiscountInfoProvider.DeleteDiscountInfo(discount);
                //}
                CookieHelper.Remove(ContentIdentifiers.FACILITY_ID_COOKIE);
            }

            return Redirect(nameof(CheckoutController.ShoppingCart));
        }

        // adds the course to the cart that the user dropped into the membership
        [HttpPost]
        public JsonResult AddItemToMembershipCart(int skuid, int units = 1)
        {
            var optsSkuid = GetProductOptions(skuid);

            var cartItemParams = new ShoppingCartItemParameters(skuid, units, optsSkuid);

            mShoppingService.AddItemToCart(cartItemParams);

            //var cartItems = mShoppingService.GetCurrentShoppingCart().CartItems;

            //double contactHours = 0;
            //foreach (var item in cartItems)
            //{
            //    if (!item.IsProductOption && !item.IsBundleItem)
            //    {
            //        var course = Dependencies.CourseRepo.GetCourseBySKUNumber(item.SKU.SKUNumber);
            //        contactHours += course.CourseContactHours;
            //    }
            //}

            //var categoryName = CookieHelper.GetExistingCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);

            //if (categoryName != null)
            //{
            //    Func<int> categoryIDDataLoadMethod = () => CategoryInfoProvider.GetCategories()
            //        .Where(x => x.CategoryName == categoryName.Value)
            //        .Select(x => x.CategoryID)
            //        .FirstOrDefault();

            //    var cacheSettings = new CacheSettings(10, nameof(CheckoutController), nameof(AddItemToMembershipCart), SiteContext.CurrentSiteName, LocalizationContext.CurrentCulture.CultureCode, "GetMembershipCategoryID", categoryName.Value)
            //    {
            //        GetCacheDependency = () =>
            //        {
            //            string dependencyCacheKey = $"{nameof(CheckoutController)}|{nameof(AddItemToMembershipCart)}|{SiteContext.CurrentSiteName}|{LocalizationContext.CurrentCulture.CultureCode}|GetMembershipCategoryID|{categoryName.Value}";
            //            return CacheHelper.GetCacheDependency(dependencyCacheKey);
            //        }
            //    };

            //    var categoryID = CacheHelper.Cache(categoryIDDataLoadMethod, cacheSettings);

            //    Func<double> tooHighDataLoadMethod = () => CustomTableItemProvider.GetItems<FacilityMembershipPricesItem>()
            //        .Where(x => x.FacilityMembershipCategory == categoryID.ToString())
            //        .Where(x => x.FacilityMembershipValidationStatus == ContentIdentifiers.TOO_HIGH)
            //        .Select(x => x.FacilityMembershipStartingContactHours)
            //        .FirstOrDefault();

            //    var cacheSettings2 = new CacheSettings(10, nameof(CheckoutController), nameof(AddItemToMembershipCart), SiteContext.CurrentSiteName, LocalizationContext.CurrentCulture.CultureCode, "GetTooHighValue", categoryID.ToString())
            //    {
            //        GetCacheDependency = () =>
            //        {
            //            string dependencyCacheKey = $"{nameof(CheckoutController)}|{nameof(AddItemToMembershipCart)}|{SiteContext.CurrentSiteName}|{LocalizationContext.CurrentCulture.CultureCode}|GetTooHighValue|{categoryID}";
            //            return CacheHelper.GetCacheDependency(dependencyCacheKey);
            //        }
            //    };

            //    var tooHigh = CacheHelper.Cache(tooHighDataLoadMethod, cacheSettings2);

            //    if (contactHours >= tooHigh)
            //    {
            //        var i = cartItems.Where(x => x.SKUID == itemSkuId).FirstOrDefault();
            //        if (i != null) mShoppingService.RemoveItemFromCart(i);

            //        return Json(ResHelper.GetString("Checkout.BuildMembership.ContactHoursTooHighWarningMsg", LocalizationContext.CurrentCulture.CultureCode), JsonRequestBehavior.AllowGet);
            //    }
            //}

            return Json(JsonRequestBehavior.AllowGet);
        }

        // removes the course from the membership
        [HttpPost]
        public void RemoveItemFromMembership(int itemSkuId)
        {
            var cart = mShoppingService.GetCurrentShoppingCart();

            //var skuid = int.Parse(itemSkuId);
            var item = cart.CartItems.Where(x => x.SKUID == itemSkuId).FirstOrDefault();

            if (item != null)
            {
                mShoppingService.RemoveItemFromCart(item.CartItemID);
            }
        }

        #endregion

        #region STEP 2 Delivery Details

        // notes:
        //  - need to differentiate between a facility purchaser/site admin and a regular student
        //      - site admin/facility purchaser
        //          - need to check for a AP person associated with the facility
        //              - if no facility is selected, leave all fields blank
        //              - populate the contact info with the AP person or the current logged in user if there's no applicable AP person
        //              - populate the address fields with the facilities address
        //      - student
        //          - no need to select an address just populate the address dropdown
        //          - add in their customer info

        // setting up the delivery details page
        public ActionResult DeliveryDetails()
        {
            try
            {
                // get the current cart
                ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();

                // TODO: add an orderID check
                if (cart.IsEmpty)
                {
                    return RedirectToAction(nameof(CheckoutController.ShoppingCart));
                }

                // get the current customer and the current logged in user
                CustomerInfo customer = mShoppingService.GetCurrentCustomer();
                var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

                // check if a facility has been selected
                if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null)
                {
                    // get the selected facility
                    var id = int.Parse(CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE).Value);
                    var facility = ClassMemberDependencies.FacilityRepo.GetFacility(id, x => new FacilityDto(x));

                    // if the user is a site admin or facility purchaser switch the current customer to the accounts payable person
                    if (user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName) || user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName))
                    {
                        customer = ApplyAPDataToCart(facility, user, cart);
                    }
                }
                // regular purchase
                else
                {
                    // set the customer details to the current logged in user
                    mShoppingService.SetCustomer(customer);
                }

                if (customer == null)
                {
                    customer = CustomerHelper.MapToCustomer(user);
                    mShoppingService.SetCustomer(customer);
                }

                // Gets all customer billing addresses for the address selector
                IEnumerable<AddressInfo> customerAddresses = Enumerable.Empty<AddressInfo>();
                if (customer != null) customerAddresses = AddressInfoProvider.GetAddresses(customer.CustomerID).ToList();

                //Prepares address selector options
                var addressList = new List<SelectListItem>
                {
                    new SelectListItem { Value = "0", Text = "(new)" }
                };

                foreach (var address in customerAddresses)
                {
                    addressList.Add(new SelectListItem { Value = address.AddressID.ToString(), Text = address.AddressName });
                }

                // populate the country dropdown
                SelectList countries = new SelectList(CountryInfoProvider.GetCountries(), nameof(CountryInfo.CountryID), nameof(CountryInfo.CountryDisplayName));

                // TODO: add this page to the content tree?
                var model = GetBaseViewModel(new BaseDto
                {
                    MetaTitle = ResHelper.GetString("Checkout.DeliveryDetails.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                    MetaDescription = ResHelper.GetString("Checkout.DeliveryDetails.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                    MetaKeywords = ResHelper.GetString("Checkout.DeliveryDetails.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                    UsePageBuilder = false
                },
                new DeliveryDetailsViewModel
                {
                    Customer = new CustomerViewModel(customer),
                    BillingAddress = new BillingAddressViewModel(mShoppingService.GetBillingAddress(), countries, new SelectList(addressList, "Value", "Text")),
                    CurrentCart = cart,
                    ShowCancelFacilityTransactionBtn = CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null && user != null && user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName)
                });

                // check if this cart is for a membership
                model.ViewModel.CreateMembership = false;
                var value = cart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, out object mem);
                if (value)
                {
                    model.ViewModel.CreateMembership = true;
                    model.ViewModel.ShowCancelFacilityTransactionBtn = false;
                }

                return View(model);
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("CheckoutController", "ERROR", e);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }

        // validates the delivery details and sends them to the review and pay page
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult DeliveryDetails(BaseViewModel<DeliveryDetailsViewModel> model, string baseNodeJson)
        {
            try
            {
                model.BaseNodeData = JsonConvert.DeserializeObject<BaseDto>(baseNodeJson);

                var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

                // set the customer
                var customer = CustomerInfoProvider.GetCustomers()
                    .Where(x => x.CustomerFirstName == model.ViewModel.Customer.FirstName)
                    .Where(x => x.CustomerLastName == model.ViewModel.Customer.LastName)
                    .Where(x => x.CustomerEmail == model.ViewModel.Customer.Email)
                    .FirstOrDefault();

                if (customer == null)
                {
                    // just a regular student that needs to be mapped to a customer
                    if (!user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName) && !user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName))
                    {
                        customer = mShoppingService.GetCurrentCustomer();

                        if (customer == null) customer = CustomerHelper.MapToCustomer(user);
                    }
                    // a site admin or facility purchaser needs to create a new customer
                    else
                    {
                        customer = new CustomerInfo
                        {
                            CustomerSiteID = SiteContext.CurrentSiteID,
                            CustomerFirstName = model.ViewModel.Customer.FirstName,
                            CustomerLastName = model.ViewModel.Customer.LastName,
                            CustomerEmail = model.ViewModel.Customer.Email,
                            //CustomerPhone = model.ViewModel.Customer.PhoneNumber,
                            CustomerCreated = DateTime.Now
                        };

                        CustomerInfoProvider.SetCustomerInfo(customer);
                    }
                }

                mShoppingService.SetCustomer(customer);

                // set the billing address
                AddressInfo address = null;
                if (model.ViewModel.BillingAddress.AddressID != 0)
                {
                    address = AddressInfoProvider.GetAddressInfo(model.ViewModel.BillingAddress.AddressID);
                }
                else
                {
                    address = new AddressInfo
                    {
                        AddressLine1 = model.ViewModel.BillingAddress.Line1,
                        AddressLine2 = model.ViewModel.BillingAddress.Line2,
                        AddressCity = model.ViewModel.BillingAddress.City,
                        AddressZip = model.ViewModel.BillingAddress.PostalCode,
                        AddressStateID = model.ViewModel.BillingAddress.StateID,
                        AddressCountryID = model.ViewModel.BillingAddress.CountryID,
                        AddressCustomerID = customer.CustomerID,
                        AddressPersonalName = $"{customer.CustomerFirstName} {customer.CustomerLastName}"
                    };

                    address.AddressName = $"{address.AddressLine1},{(!string.IsNullOrEmpty(address.AddressLine2) ? $" {address.AddressLine2}," : "")} {CountryInfoProvider.GetCountryInfo(address.AddressCountryID).CountryName}, {address.AddressCity}, {(address.AddressStateID != 0 ? StateInfoProvider.GetStateInfo(address.AddressStateID).StateCode : "")}, {address.AddressZip}";

                    AddressInfoProvider.SetAddressInfo(address);
                }

                mShoppingService.SetBillingAddress(address);

                // Redirects to the next step of the checkout process
                return RedirectToAction(nameof(CheckoutController.PreviewAndPay));
            }
            catch (Exception e)
            {
                EventLogProvider.LogException("CheckoutController", "ERROR", e);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }

        // gets the saved address and populates the address section
        [HttpPost]
        public JsonResult CustomerAddress(int addressID)
        {
            AddressInfo address = AddressInfoProvider.GetAddressInfo(addressID);

            if (address == null)
            {
                return null;
            }

            var responseModel = new
            {
                Line1 = address.AddressLine1,
                Line2 = address.AddressLine2,
                City = address.AddressCity,
                PostalCode = address.AddressZip,
                CountryID = address.AddressCountryID,
                StateID = address.AddressStateID,
                PersonalName = address.AddressPersonalName
            };

            return Json(responseModel);
        }

        // populates the state dropdown if USA is selected
        [HttpPost]
        public JsonResult CountryStates(int countryId)
        {
            // Gets the display names of the country's states
            var responseModel = StateInfoProvider.GetStates().WhereEquals(nameof(StateInfo.CountryID), countryId)
                .Select(s => new
                {
                    id = s.StateID,
                    name = HTMLHelper.HTMLEncode(s.StateDisplayName)
                });

            // Returns serialized display names of the states
            return Json(responseModel);
        }

        #endregion

        #region STEP 3 Preview

        public ActionResult PreviewAndPay()
        {
            PreviewAndPayViewModel errorModel = (PreviewAndPayViewModel)TempData["model"];
            var facilityPurchaser = UserInfoProvider.IsUserInRole(User.Identity.Name, ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var creditCardPaymentOption = PaymentOptionInfoProvider.GetPaymentOptionInfo(ContentIdentifiers.CREDIT_CARD_PAYMENT_METHOD, SiteContext.CurrentSiteName);

            var cart = mShoppingService.GetCurrentShoppingCart();
            var freeCourse = false;

            if (cart.GrandTotal == 0 && !cart.IsEmpty)
            {
                freeCourse = true;
            }

            if (errorModel == null)
            {
                // TODO: add logic to check for an orderID
                if (cart.IsEmpty)
                {
                    return RedirectToAction(nameof(CheckoutController.ShoppingCart));
                }

                PreviewAndPayViewModel previewAndPayViewModel = PreparePreviewViewModel();
                previewAndPayViewModel.FacilityPurchaser = facilityPurchaser;
                previewAndPayViewModel.IsFacilityPurchase = facilityPurchaser;
                previewAndPayViewModel.IsMembershipPurchase = false;
                previewAndPayViewModel.FreeCourse = freeCourse;
                previewAndPayViewModel.ShowCancelFacilityTransactionBtn = false;
                previewAndPayViewModel.ShowOver200StaffPopup = false;
                previewAndPayViewModel.IsStudentPurchase = false;

                if (!facilityPurchaser)
                {
                    previewAndPayViewModel.IsStudentPurchase = true;
                    previewAndPayViewModel.PaymentMethod.PaymentMethodID = creditCardPaymentOption.PaymentOptionID;
                }

                if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null && user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName))
                {
                    var id = int.Parse(CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE).Value);
                    var facility = ClassMemberDependencies.FacilityRepo.GetFacility(id, x => new FacilityDto { FacilityCode = x.FacilityCode, FacilityName = x.Name });

                    previewAndPayViewModel.FacilityPODetails.FacilityName = facility.FacilityName;
                    previewAndPayViewModel.FacilityPODetails.FacilityCode = facility.FacilityCode;
                    previewAndPayViewModel.ShowCancelFacilityTransactionBtn = true;
                }

                var membershipNames = new List<string>();
                var memberships = new List<MembershipViewModel>();
                var regCourses = new List<ShoppingCartItemViewModel>();
                var value = cart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, out object memPurchase);
                if (value)
                {
                    if (memPurchase.ToString() == "True")
                    {
                        previewAndPayViewModel.IsMembershipPurchase = true;
                        previewAndPayViewModel.ShowCancelFacilityTransactionBtn = false;

                        //get memberships
                        foreach (var item in cart.CartItems)
                        {
                            if (!item.IsProductOption)
                            {
                                var name = item.CartItemCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, out object memName);
                                if (name)
                                {
                                    if (!membershipNames.Contains(memName.ToString())) membershipNames.Add(memName.ToString());
                                }
                                else
                                {
                                    if (!item.IsBundleItem)
                                    {
                                        var cartItem = new ShoppingCartItemViewModel
                                        {
                                            CartItemID = item.CartItemID,
                                            SKUID = item.SKUID,
                                            SKUName = item.SKU.SKUName,
                                            CartItemUnits = item.CartItemUnits,
                                            CurrentCart = cart.CartItems,
                                            TotalPrice = item.TotalPrice,
                                            CurrencyFormatString = cart.Currency.CurrencyFormatString
                                        };

                                        regCourses.Add(cartItem);
                                    }
                                }
                            }
                        }

                        //add classes to the memberships
                        foreach (var item in membershipNames)
                        {
                            var items = new List<ShoppingCartItemViewModel>();
                            var model = new MembershipViewModel { };
                            foreach (var i in cart.CartItems)
                            {
                                if (!i.IsProductOption)
                                {
                                    var memNameValue = i.CartItemCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, out object memName);
                                    if (memNameValue)
                                    {
                                        if (item == memName.ToString())
                                        {
                                            var cartItem = new ShoppingCartItemViewModel
                                            {
                                                CartItemID = i.CartItemID,
                                                SKUID = i.SKUID,
                                                SKUName = i.SKU.SKUName,
                                                CartItemUnits = i.CartItemUnits,
                                                CurrentCart = cart.CartItems,
                                                TotalPrice = i.TotalPrice,
                                                CurrencyFormatString = cart.Currency.CurrencyFormatString
                                            };

                                            items.Add(cartItem);

                                            model.Quantity = i.CartItemUnits;
                                        }
                                    }
                                }
                            }
                            model.CartItemData = items;
                            model.MembershipName = item;
                            memberships.Add(model);
                        }
                    }
                }

                previewAndPayViewModel.RegularCourses = regCourses;
                previewAndPayViewModel.Memberships = memberships;

                return View(GetBaseViewModel(new BaseDto
                {
                    MetaTitle = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                    MetaDescription = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                    MetaKeywords = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                    UsePageBuilder = false
                }, previewAndPayViewModel));
            }

            errorModel.ShowCancelFacilityTransactionBtn = CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null && user != null && user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
            errorModel.FacilityPurchaser = facilityPurchaser;

            errorModel.IsStudentPurchase = false;
            if (!facilityPurchaser)
            {
                errorModel.IsStudentPurchase = true;
                errorModel.PaymentMethod.PaymentMethodID = creditCardPaymentOption.PaymentOptionID;
            }

            errorModel.FreeCourse = freeCourse;
            if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null && user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName))
            {
                var id = int.Parse(CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE).Value);
                var facility = ClassMemberDependencies.FacilityRepo.GetFacility(id, x => new FacilityDto { FacilityName = x.Name, FacilityCode = x.FacilityCode });

                errorModel.FacilityPODetails.FacilityName = facility.FacilityName;
                errorModel.FacilityPODetails.FacilityCode = facility.FacilityCode;
            }

            return View(GetBaseViewModel(new BaseDto
            {
                MetaTitle = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                MetaDescription = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                MetaKeywords = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                UsePageBuilder = false
            }, errorModel));
        }

        [HttpPost]
        public ActionResult PreviewAndPay(BaseViewModel<PreviewAndPayViewModel> baseModel, string baseNodeJson, int orderId = 0)
        {
            try
            {
                baseModel.BaseNodeData = JsonConvert.DeserializeObject<BaseDto>(baseNodeJson);

                ShoppingCartInfo cart;
                OrderInfo order;

                var paymentData = new CreditCardViewModel
                {
                    CardNo = baseModel.ViewModel.CardInfo.CardNo,
                    CVVCode = baseModel.ViewModel.CardInfo.CVVCode,
                    ExpDate = string.Concat(baseModel.ViewModel.CardInfo.Month, "/", baseModel.ViewModel.CardInfo.Year)
                };

                if (orderId != 0)
                {
                    cart = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(orderId);

                    var attemptedCard = new PaymentProcessViewModel
                    {
                        CardData = paymentData,
                        OrderId = cart.OrderId
                    };

                    TempData["cardModel"] = attemptedCard;
                    return RedirectToAction("ProcessPayment", "Payment");
                }
                cart = mShoppingService.GetCurrentShoppingCart();

                var validationErrors = ShoppingCartInfoProvider.ValidateShoppingCart(cart);

                if (validationErrors.Any())
                {
                    PreviewAndPayViewModel viewModel = PreparePreviewViewModel();
                    viewModel.FacilityPODetails = baseModel.ViewModel.FacilityPODetails;

                    var model = GetBaseViewModel(new BaseDto
                    {
                        MetaTitle = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                        MetaDescription = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                        MetaKeywords = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                        UsePageBuilder = false
                    }, viewModel);

                    return View(nameof(CheckoutController.PreviewAndPay), model);
                }

                mShoppingService.SetPaymentOption(baseModel.ViewModel.PaymentMethod.PaymentMethodID);

                if (baseModel.ViewModel.IsFacilityPurchase)
                {
                    var payOffType = PaymentOptionInfoProvider.GetPaymentOptionInfo(ContentIdentifiers.PAYOFF_PAYMENT_METHOD, SiteContext.CurrentSiteName);
                    var facility = ClassMemberDependencies.FacilityRepo.GetFacility(baseModel.ViewModel.FacilityPODetails.FacilityCode, baseModel.ViewModel.FacilityPODetails.FacilityName,
                        x => new FacilityDto { FacilityName = x.Name, FacilityCode = x.FacilityCode, FacilityID = x.FacilityID });

                    if (facility != null)
                    {
                        var cUser = UserInfoProvider.GetUserInfo(User.Identity.Name);
                        var note = $"Facility Name: {baseModel.ViewModel.FacilityPODetails.FacilityName}, Facility Code: {baseModel.ViewModel.FacilityPODetails.FacilityCode}, Purchaser Name: {baseModel.ViewModel.DeliveryDetails.Customer.FirstName} {baseModel.ViewModel.DeliveryDetails.Customer.LastName}";

                        var inProgress = OrderStatusInfoProvider.GetOrderStatuses().WhereEquals(nameof(OrderStatusInfo.StatusName), ContentIdentifiers.IN_PROGRESS_PAYMENT_STATUS).First();

                        var cCustomer = mShoppingService.GetCurrentCustomer();
                        var customer = CustomerInfoProvider.GetCustomerInfo(cart.ShoppingCartCustomerID);

                        if (cart.ShoppingCartCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, out object mem))
                        {
                            var membershipNames = new List<string>();
                            var memberships = new List<MembershipViewModel>();

                            foreach (var item in cart.CartItems)
                            {
                                if (!item.IsProductOption)
                                {
                                    if (item.CartItemCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, out object memName))
                                    {
                                        if (!membershipNames.Contains(memName.ToString())) membershipNames.Add(memName.ToString());
                                    }
                                }
                            }

                            //add classes to the memberships
                            foreach (var item in membershipNames)
                            {
                                var items = "";
                                var model = new MembershipViewModel { };
                                foreach (var i in cart.CartItems)
                                {
                                    if (!i.IsProductOption)
                                    {
                                        if (i.CartItemCustomData.TryGetValue(ContentIdentifiers.MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN, out object memName))
                                        {
                                            if (item == memName.ToString())
                                            {
                                                if (string.IsNullOrEmpty(items)) items += i.SKU.SKUNumber;
                                                else items += $",{i.SKU.SKUNumber}";

                                                model.Quantity = i.CartItemUnits;
                                            }
                                        }
                                    }
                                }
                                model.ClassIDStringList = items;
                                model.MembershipName = item;
                                memberships.Add(model);
                            }

                            if (cCustomer != customer)
                            {
                                order = new OrderInfo
                                {
                                    OrderNote = note,
                                    OrderSiteID = SiteContext.CurrentSiteID,
                                    OrderTotalPrice = cart.TotalPrice,
                                    OrderGrandTotal = cart.GrandTotal,
                                    OrderDate = DateTime.Now,
                                    OrderStatusID = inProgress.StatusID,
                                    OrderCustomerID = customer.CustomerID,
                                    OrderPaymentOptionID = baseModel.ViewModel.PaymentMethod.PaymentMethodID,
                                    OrderTotalTax = cart.TotalTax,
                                    OrderTotalShipping = 0,
                                    OrderCurrencyID = CurrencyInfoProvider.GetMainCurrency(SiteContext.CurrentSiteID).CurrencyID,
                                    OrderCompletedByUserID = cUser.UserID,
                                    OrderTotalPriceInMainCurrency = cart.TotalPrice,
                                    OrderCulture = LocalizationContext.CurrentCulture.CultureCode,
                                    OrderGrandTotalInMainCurrency = cart.GrandTotal,
                                    OrderCouponCodes = cart.CouponCodes.Serialize()
                                };

                                OrderInfoProvider.SetOrderInfo(order);

                                order.OrderInvoiceNumber = order.OrderID.ToString();
                                order.Update();

                                var address = mShoppingService.GetBillingAddress();
                                var orderBillingAddress = addressConverter.Convert(address, order.OrderID, AddressType.Billing);
                                OrderAddressInfoProvider.SetAddressInfo(orderBillingAddress);

                                // add the cart items to the order
                                foreach (var item in cart.CartItems)
                                {
                                    OrderItemInfoProvider.SetOrderItemInfo(new OrderItemInfo
                                    {
                                        OrderItemOrderID = order.OrderID,
                                        OrderItemSKUID = item.SKUID,
                                        OrderItemSKUName = item.SKU.SKUName,
                                        OrderItemUnitPrice = item.UnitPrice,
                                        OrderItemUnitCount = item.CartItemUnits,
                                        OrderItemParentGUID = item.CartItemParentGUID,
                                        OrderItemBundleGUID = item.CartItemBundleGUID,
                                        OrderItemTotalPriceInMainCurrency = item.TotalPrice,
                                        OrderItemProductDiscounts = "",
                                        OrderItemDiscountSummary = "",
                                        OrderItemTotalPrice = item.TotalPrice
                                    });
                                }

                                var orderCart = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(order.OrderID);

                                order.OrderInvoice = ShoppingCartInfoProvider.GetOrderInvoice(orderCart);
                                order.Update();

                                if (baseModel.ViewModel.PaymentMethod.PaymentMethodID != payOffType.PaymentOptionID)
                                {
                                    var cartFromOrder = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(order.OrderID);
                                    cartFromOrder.ShoppingCartCustomData.SetValue("IsFacilityPurchase", true);
                                    cartFromOrder.ShoppingCartCustomData.SetValue("FacilityName", facility.FacilityName);

                                    OrderInfoProvider.SendOrderNotificationToCustomer(cartFromOrder, "Ecommerce.OrderNotificationToCustomer");
                                }
                            }
                            else
                            {
                                order = mShoppingService.CreateOrder();
                            }

                            order.SetValue("OrderIsDemo", false);
                            order.SetValue("OrderSalesCommissionProcessed", false);

                            order.OrderCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN, true);
                            order.OrderCustomData.SetValue(ContentIdentifiers.FACILITY_ID_CUSTOM_DATA_COLUMN, facility.FacilityID);
                            order.OrderCustomData.SetValue(ContentIdentifiers.FACILITY_NAME_CUSTOM_DATA_COLUMN, facility.FacilityName);
                            order.OrderCustomData.SetValue(ContentIdentifiers.FACILITY_CODE_CUSTOM_DATA_COLUMN, facility.FacilityCode);

                            var category = CookieHelper.GetExistingCookie(ContentIdentifiers.MEMBERSHIP_CATEGORY_COOKIE);
                            if (category != null) order.OrderCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_CATEGORY_CUSTOM_DATA_COLUMN, category.Value);

                            order.OrderCustomData.SetValue(ContentIdentifiers.MEMBERSHIP_COUNT_CUSTOM_DATA_COLUMN, memberships.Count());

                            var count = 1;
                            foreach (var item in memberships)
                            {
                                order.OrderCustomData.SetValue($"Membership-{count}-Name", item.MembershipName);
                                order.OrderCustomData.SetValue($"Membership-{count}-ClassIDs", item.ClassIDStringList);
                                order.OrderCustomData.SetValue($"Membership-{count}-Qty", item.Quantity);

                                count++;
                            }

                            order.Update();

                            cart.ShoppingCartCustomData.Remove(ContentIdentifiers.MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN);
                            cart.Update();
                        }
                        else
                        {
                            // manually create the order for the accounts payable person
                            if (cUser.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName) || cUser.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName))
                            {
                                order = new OrderInfo
                                {
                                    OrderNote = note,
                                    OrderSiteID = SiteContext.CurrentSiteID,
                                    OrderTotalPrice = cart.TotalPrice,
                                    OrderGrandTotal = cart.GrandTotal,
                                    OrderDate = DateTime.Now,
                                    OrderStatusID = inProgress.StatusID,
                                    OrderCustomerID = customer.CustomerID,
                                    OrderPaymentOptionID = baseModel.ViewModel.PaymentMethod.PaymentMethodID,
                                    OrderTotalTax = cart.TotalTax,
                                    OrderTotalShipping = 0,
                                    OrderCurrencyID = CurrencyInfoProvider.GetMainCurrency(SiteContext.CurrentSiteID).CurrencyID,
                                    OrderCompletedByUserID = cUser.UserID,
                                    OrderTotalPriceInMainCurrency = cart.TotalPrice,
                                    OrderCulture = LocalizationContext.CurrentCulture.CultureCode,
                                    OrderGrandTotalInMainCurrency = cart.GrandTotal,
                                    OrderCouponCodes = cart.CouponCodes.Serialize(),
                                };

                                OrderInfoProvider.SetOrderInfo(order);

                                order.OrderInvoiceNumber = order.OrderID.ToString();
                                order.Update();

                                var address = mShoppingService.GetBillingAddress();
                                var orderBillingAddress = addressConverter.Convert(address, order.OrderID, AddressType.Billing);
                                OrderAddressInfoProvider.SetAddressInfo(orderBillingAddress);

                                // add the cart items to the order
                                foreach (var item in cart.CartItems)
                                {
                                    OrderItemInfoProvider.SetOrderItemInfo(new OrderItemInfo
                                    {
                                        OrderItemOrderID = order.OrderID,
                                        OrderItemSKUID = item.SKUID,
                                        OrderItemSKUName = item.SKU.SKUName,
                                        OrderItemUnitPrice = item.UnitPrice,
                                        OrderItemUnitCount = item.CartItemUnits,
                                        OrderItemParentGUID = item.CartItemParentGUID,
                                        OrderItemBundleGUID = item.CartItemBundleGUID,
                                        OrderItemTotalPriceInMainCurrency = item.TotalPrice,
                                        OrderItemProductDiscounts = "",
                                        OrderItemDiscountSummary = "",
                                        OrderItemTotalPrice = item.TotalPrice
                                    });
                                }

                                var orderCart = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(order.OrderID);

                                order.OrderInvoice = ShoppingCartInfoProvider.GetOrderInvoice(orderCart);
                                order.Update();

                                if (baseModel.ViewModel.PaymentMethod.PaymentMethodID != payOffType.PaymentOptionID)
                                {
                                    var cartFromOrder = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(order.OrderID);
                                    cartFromOrder.ShoppingCartCustomData.SetValue("IsFacilityPurchase", true);
                                    cartFromOrder.ShoppingCartCustomData.SetValue("FacilityName", facility.FacilityName);

                                    OrderInfoProvider.SendOrderNotificationToCustomer(cartFromOrder, "Ecommerce.OrderNotificationToCustomer");
                                }
                            }
                            else
                            {
                                order = mShoppingService.CreateOrder();
                            }

                            order.OrderCustomData.SetValue(ContentIdentifiers.FACILITY_ID_CUSTOM_DATA_COLUMN, facility.FacilityID);
                            order.OrderCustomData.SetValue(ContentIdentifiers.FACILITY_NAME_CUSTOM_DATA_COLUMN, facility.FacilityName);
                            order.OrderCustomData.SetValue(ContentIdentifiers.FACILITY_CODE_CUSTOM_DATA_COLUMN, facility.FacilityCode);
                            order.Update();
                        }

                        if (baseModel.ViewModel.PaymentMethod.PaymentMethodID == payOffType.PaymentOptionID)
                        {
                            var payInvoiceURL = ContentIdentifiers.PAY_INVOICE + "?orderGUID=" + order.OrderGUID;
                            order.SetValue("OrderPayInvoiceURL", payInvoiceURL);

                            order.Update();

                            var data = new Dictionary<string, object>
                            {
                                { "InvoiceNumber", order.OrderInvoiceNumber }
                            };

                            Lib.Helpers.EmailHelper.SendEmail(ContentIdentifiers.SUPPORT_EMAIL, ContentIdentifiers.FACILITY_PO_ORDER_SUBMITTED_ET_IDENTIFIER, data);

                            var customerEmail = CustomerInfoProvider.GetCustomerInfo(order.OrderCustomerID).CustomerEmail;
                            var poData = new Dictionary<string, object>
                            {
                                { "InoviceNum", order.OrderInvoiceNumber },
                                { "PayURL", payInvoiceURL }
                            };

                            Lib.Helpers.EmailHelper.SendEmail(customerEmail, ContentIdentifiers.FACILITY_PO_ORDER_CONFIRMATION_ET_IDENTIFER, poData);

                            mShoppingService.RemoveAllItemsFromCart();

                            return RedirectToAction(nameof(CheckoutController.ThankYou), new { orderID = order.OrderID });
                        }
                    }
                    else
                    {
                        PreviewAndPayViewModel viewModel = PreparePreviewViewModel();
                        viewModel.FacilityPODetails = baseModel.ViewModel.FacilityPODetails;
                        viewModel.IsFacilityPurchase = baseModel.ViewModel.IsFacilityPurchase;
                        viewModel.CardErrors = new List<string>
                        {
                            ResHelper.GetString("Checkout.PreviewAndPay.FacilityValidationErrMsg", LocalizationContext.CurrentCulture.CultureCode)
                        };

                        var model = GetBaseViewModel(new BaseDto
                        {
                            MetaTitle = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                            MetaDescription = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                            MetaKeywords = ResHelper.GetString("Checkout.PreviewAndPay.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                            UsePageBuilder = false
                        }, viewModel);

                        return View(nameof(CheckoutController.PreviewAndPay), model);
                    }
                }
                else
                {
                    order = mShoppingService.CreateOrder();
                }

                order.SetValue("OrderIsDemo", false);
                order.SetValue("OrderSalesCommissionProcessed", false);
                order.Update();

                mShoppingService.RemoveAllItemsFromCart();

                var cardModel = new PaymentProcessViewModel
                {
                    CardData = paymentData,
                    OrderId = order.OrderID
                };

                TempData["cardModel"] = cardModel;
                return RedirectToAction("ProcessPayment", "Payment");
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("CheckoutController", "ERROR", ex);
                return Redirect(ContentIdentifiers.ERROR);
            }
        }

        [HttpPost]
        public ActionResult AddCouponCode(string couponCode)
        {
            if ((couponCode == "") || !mShoppingService.AddCouponCode(couponCode))
            {
                ModelState.AddModelError("CouponCodeError", "The entered coupon code is not valid.");
            }
            else
            {
                mShoppingService.AddCouponCode(couponCode);
            }

            return RedirectToAction(nameof(CheckoutController.PreviewAndPay));
        }

        [HttpPost]
        public ActionResult RemoveCouponCode(string couponCode)
        {
            mShoppingService.RemoveCouponCode(couponCode);

            return RedirectToAction(nameof(CheckoutController.PreviewAndPay));
        }

        private PreviewAndPayViewModel PreparePreviewViewModel()
        {
            ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            var customer = CustomerInfoProvider.GetCustomerInfo(cart.ShoppingCartCustomerID);

            //if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null &&
            //    (user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName)
            //    || user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName)))
            //{
            //    var id = int.Parse(CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE).Value);
            //    var facility = ClassMembersHelper.GetFacility(id, x => x);

            //    customer = ApplyAPDataToCart(facility);
            //}

            DeliveryDetailsViewModel deliveryDetailsModel = new DeliveryDetailsViewModel
            {
                Customer = new CustomerViewModel(customer),
                BillingAddress = new BillingAddressViewModel(mShoppingService.GetBillingAddress(), null, null),
                CurrentCart = cart
            };

            PaymentMethodViewModel paymentViewModel = new PaymentMethodViewModel
            {
                PaymentMethods = new SelectList(GetApplicablePaymentMethods(cart), nameof(PaymentOptionInfo.PaymentOptionID), nameof(PaymentOptionInfo.PaymentOptionDisplayName))
            };

            PaymentOptionInfo paymentMethod = cart.PaymentOption;
            if (paymentMethod != null)
            {
                paymentViewModel.PaymentMethodID = paymentMethod.PaymentOptionID;
            }

            CreditCardViewModel cardViewModel = new CreditCardViewModel
            {
                CardNo = "",
                CVVCode = ""
            };

            FacilityPODetailsViewModel facilityPODetailsViewModel = new FacilityPODetailsViewModel
            {
                FacilityCode = "",
                FacilityName = ""
            };

            PreviewAndPayViewModel previewAndPayModel = new PreviewAndPayViewModel
            {
                DeliveryDetails = deliveryDetailsModel,
                Cart = new ShoppingCartViewModel(cart, user) { OrderId = cart.Order == null ? 0 : cart.Order.OrderID },
                PaymentMethod = paymentViewModel,
                CardInfo = cardViewModel,
                CardErrors = new List<string>(),
                CouponCodes = cart.CouponCodes.AllAppliedCodes.Select(x => x.Code),
                FacilityPODetails = facilityPODetailsViewModel
            };

            return previewAndPayModel;
        }

        #endregion

        #region STEP 4 Thank You

        // setting up the thank you page
        public ActionResult ThankYou()
        {
            string msg = (string)TempData["successMsg"];

            var model = new ThankYouViewModel
            {
                SuccessMessage = !string.IsNullOrEmpty(msg) ? msg : "",
                InvoiceNumber = Request.QueryString["orderId"]
            };

            return View(GetBaseViewModel(new BaseDto
            {
                MetaTitle = ResHelper.GetString("Checkout.ThankYou.Metadata.Title", LocalizationContext.CurrentCulture.CultureCode),
                MetaDescription = ResHelper.GetString("Checkout.ThankYou.Metadata.Description", LocalizationContext.CurrentCulture.CultureCode),
                MetaKeywords = ResHelper.GetString("Checkout.ThankYou.Metadata.Keywords", LocalizationContext.CurrentCulture.CultureCode),
                UsePageBuilder = false
            }, model));
        }

        // sends the invoice to all the submitted emails
        public ActionResult EmailExtraPeople(IEnumerable<string> emails, int orderID)
        {
            var cart = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(orderID);

            //redirect to an error page
            if (cart == null)
            {
                return Redirect(ContentIdentifiers.NOT_FOUND);
            }

            foreach (var item in emails)
            {
                var e = new EmailMessage
                {
                    From = ResHelper.GetString("SiteInfo.NoReplyEmail", LocalizationContext.CurrentCulture.CultureCode),
                    Body = ShoppingCartInfoProvider.GetOrderInvoice(cart),
                    Subject = ResHelper.GetString("Checkout.OrderInvoiceEmailSubject", LocalizationContext.CurrentCulture.CultureCode),
                    Recipients = item
                };

                EmailSender.SendEmail(SiteContext.CurrentSiteName, e, true);
            }

            TempData["successMsg"] = "Emails Sent!";
            return RedirectToAction(nameof(CheckoutController.ThankYou), new { orderID });
        }

        #endregion

        #region Global Checkout Methods

        private CustomerInfo ApplyAPDataToCart(FacilityDto facility, UserInfo cUser, ShoppingCartInfo cCart)
        {
            CustomerInfo customer = null;

            int? APType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.ACCTS_PAYABLE_CONTACT_CODE_NAME);
            var cf = ClassMemberDependencies.ContactFacilityRepo.GetContactFacilitiesByFacilityID(facility.FacilityID, x => new ContactFacilityDto { ContactID = x.ContactID });

            // if the facility has an AP person, they will be set as the customer
            foreach (var x in cf)
            {
                var contact = ClassMemberDependencies.ContactRepo.GetContact(x.ContactID, APType, x => new ContactDto(x));
                if (contact != null)
                {
                    // if they have a user account set that as the user
                    if (contact.ContactUserID.HasValue)
                    {
                        var usr = UserInfoProvider.GetUserInfo(contact.ContactUserID.Value);
                        customer = CustomerInfoProvider.GetCustomerInfoByUserID(contact.ContactUserID.Value);

                        if (customer == null) customer = CustomerHelper.MapToCustomer(usr);

                        break;
                    }
                    // need to just create an anonymous customer with no user
                    else
                    {
                        var c = CustomerInfoProvider.GetCustomers()
                            .Where(x => x.CustomerFirstName == contact.ContactFirstName)
                            .Where(x => x.CustomerLastName == contact.ContactLastName)
                            .Where(x => x.CustomerPhone == (contact.ContactPhone ?? ""))
                            .Where(x => x.CustomerEmail == (contact.ContactEmail ?? ""))
                            .FirstOrDefault();

                        if (c == null)
                        {
                            customer = new CustomerInfo
                            {
                                CustomerFirstName = contact.ContactFirstName,
                                CustomerLastName = contact.ContactLastName,
                                CustomerEmail = contact.ContactEmail ?? "",
                                CustomerPhone = contact.ContactPhone ?? "",
                                CustomerSiteID = SiteContext.CurrentSiteID,
                                CustomerCreated = DateTime.Now
                            };
                        }
                        else
                        {
                            customer = c;
                        }

                        break;
                    }
                }
            }

            // if no AP person was found then the current logged in user will be used as the customer
            if (customer == null)
            {
                customer = CustomerInfoProvider.GetCustomerInfoByUserID(cUser.UserID);

                if (customer == null) customer = CustomerHelper.MapToCustomer(cUser);
            }

            mShoppingService.SetCustomer(customer);

            // set billing info
            var facilityAddresses = AddressInfoProvider.GetAddresses(customer.CustomerID)
                                .Where(x => x.AddressName.Equals("Facility Address"))
                                .ToList();

            if (facilityAddresses.Count() == 0)
            {
                var facilityAddress = new AddressInfo()
                {
                    AddressName = "Facility Address",
                    AddressLine1 = facility.FacilityAddress1,
                    AddressLine2 = facility.FacilityAddress2,
                    AddressCity = facility.FacilityCity,
                    AddressZip = facility.FacilityZip,
                    AddressCountryID = CountryInfoProvider.GetCountryInfo("USA").CountryID,
                    AddressStateID = StateInfoProvider.GetStateInfoByCode(facility.FacilityState).StateID,
                    AddressPersonalName = $"{facility.FacilityName} Address",
                    AddressCustomerID = customer.CustomerID
                };

                AddressInfoProvider.SetAddressInfo(facilityAddress);

                mShoppingService.SetBillingAddress(facilityAddress);
            }
            else
            {
                mShoppingService.SetBillingAddress(facilityAddresses.FirstOrDefault());
            }

            return customer;
        }

        public static List<int> GetProductOptions(int itemSKUID)
        {
            SKUInfo item = SKUInfoProvider.GetSKUInfo(itemSKUID);
            List<OptionCategoryInfo> optionCategories = OptionCategoryInfoProvider.GetProductOptionCategories(item.SKUID, true, OptionCategoryTypeEnum.Products).ToList();

            var optsSkuid = new List<int>();
            if (optionCategories.Count != 0)
            {
                foreach (var option in optionCategories)
                {
                    var skuOpts = SKUInfoProvider.GetSKUOptionsForProduct(item.SKUID, option.CategoryID, true).ToList();
                    foreach (var x in skuOpts)
                    {
                        if (x.SKUPrice == 0)
                        {
                            optsSkuid.Add(x.SKUID);
                        }
                    }
                }
            }

            return optsSkuid;
        }

        private bool IsPaymentMethodValid(int paymentMethodID)
        {
            ShoppingCartInfo cart = mShoppingService.GetCurrentShoppingCart();

            List<PaymentOptionInfo> paymentMethods = GetApplicablePaymentMethods(cart).ToList();

            return paymentMethods.Exists(p => p.PaymentOptionID == paymentMethodID);
        }

        private IEnumerable<PaymentOptionInfo> GetApplicablePaymentMethods(ShoppingCartInfo cart)
        {
            IEnumerable<PaymentOptionInfo> enabledPaymentMethods = PaymentOptionInfoProvider.GetPaymentOptions(SiteContext.CurrentSiteID, true).ToList();

            var poType = PaymentOptionInfoProvider.GetPaymentOptionInfo(ContentIdentifiers.PAYOFF_PAYMENT_METHOD, SiteContext.CurrentSiteName);
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
            if (user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName))
            {
                poType.PaymentOptionEnabled = true;
                poType.Update();
            }
            else
            {
                poType.PaymentOptionEnabled = false;
                poType.Update();
            }

            return enabledPaymentMethods.Where(paymentMethod => PaymentOptionInfoProvider.IsPaymentOptionApplicable(cart, paymentMethod));
        }

        public ActionResult AddFreeCourse(int itemSKUID)
        {
            mShoppingService.RemoveAllItemsFromCart();
            mShoppingService.AddItemToCart(itemSKUID, 1);

            int.TryParse(SKUInfoProvider.GetSKUInfo(itemSKUID).SKUNumber, out int classID);

            var paymentOption = PaymentOptionInfoProvider.GetPaymentOptionInfo(ContentIdentifiers.CREDIT_CARD_PAYMENT_METHOD, SiteContext.CurrentSiteName);
            var studentType = ClassMemberDependencies.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);
            var user = UserInfoProvider.GetUserInfo(User.Identity.Name);

            var startWF = ClassMemberDependencies.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            var studentID = ClassMemberDependencies.ContactRepo.GetContact(user?.UserID, studentType, x => new ContactDto { ContactID = x.ContactID }).ContactID;

            var customer = CustomerInfoProvider.GetCustomerInfoByUserID(user.UserID);
            mShoppingService.SetCustomer(customer);

            if (studentID == 0) studentID = ClassMemberDependencies.ContactRepo.MapUserToStudent(user).ContactID;

            var optsSkuid = GetProductOptions(itemSKUID);
            var cartItemParams = new ShoppingCartItemParameters(itemSKUID, 1, optsSkuid);
            mShoppingService.AddItemToCart(cartItemParams);

            mShoppingService.SetPaymentOption(paymentOption.PaymentOptionID);

            var billing = AddressInfoProvider.GetAddresses(customer.CustomerID).FirstOrDefault();
            if (billing == null)
            {
                billing = new AddressInfo()
                {
                    AddressName = "FREE COURSE ADDRESS",
                    AddressLine1 = "FREE",
                    AddressCity = "FREE",
                    AddressZip = "FREE",
                    AddressCustomerID = customer.CustomerID,
                    AddressCountryID = 271,
                    AddressPersonalName = "FREE COURSE ADDRESS"
                };
            }
            mShoppingService.SetBillingAddress(billing);

            var order = mShoppingService.CreateOrder();
            var orderPaidStatus = OrderStatusInfoProvider.GetOrderStatusInfo(ContentIdentifiers.PAID_PAYMENT_STATUS, SiteContext.CurrentSiteName);
            order.OrderStatusID = orderPaidStatus.StatusID;
            order.OrderNote = ResHelper.GetString("Checkout.OrderNote.FreeCourse", LocalizationContext.CurrentCulture.CultureCode);
            order.Update();

            if (studentID == 0) studentID = ClassMemberDependencies.ContactRepo.MapUserToStudent(user).ContactID;

            ClassMemberDependencies.StudentClassRepo.CreateStudentClass(studentID, classID, "", order.OrderInvoiceNumber, true);

            return Redirect(ContentIdentifiers.DASHBOARD);
        }

        public ActionResult CancelFacilityTransaction()
        {
            if (CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null)
            {
                CookieHelper.Remove(ContentIdentifiers.FACILITY_ID_COOKIE);
                mShoppingService.RemoveAllItemsFromCart();

                var user = UserInfoProvider.GetUserInfo(User.Identity.Name);
                var customer = CustomerInfoProvider.GetCustomerInfoByUserID(user.UserID);

                if (customer == null) CustomerHelper.MapToCustomer(user);

                mShoppingService.SetCustomer(customer);
            }

            return RedirectToAction(nameof(CheckoutController.ShoppingCart));
        }

        #endregion
    }
}