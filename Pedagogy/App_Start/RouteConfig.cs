﻿using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;
using System.Web.Routing;
using DynamicRouting.Kentico.MVC;
using Kentico.Web.Mvc;
using Pedagogy.App_Start;

namespace Pedagogy
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //route for sitemap
            routes.MapRoute(
               name: "Sitemap",
               url: "sitemap.xml",
               defaults: new { controller = "Sitemap", action = "Index" }
           );

            // route for robots
            routes.MapRoute(
                name: "Robots",
                url: "robots.txt",
                defaults: new { controller = "Robots", action = "Index" }
            );

            // route for manifest.json
            routes.MapRoute(
                name: "Manifest",
                url: "manifest.json",
                defaults: new { controller = "Manifest", action = "Index" }
            );

            // Map routes to Kentico HTTP handlers first as some Kentico URLs might be matched by the default ASP.NET MVC route resulting in displaying pages without images
            routes.Kentico().MapRoutes();

            // This will honor Attribute Routing in MVC [Route("")] and [RoutePrefix("")] over Dynamic Routing
            //<see href="https://devblogs.microsoft.com/aspnet/attribute-routing-in-asp-net-mvc-5/">See Attribute Routing</see>
            routes.MapMvcAttributeRoutes();

            // Redirect to administration site if the path is "admin"
            //Can also replace this with the[Route("Admin")] on your AdminRedirectController's Index Method
            routes.MapRoute(
                name: "Admin",
                url: "admin",
                defaults: new { controller = "AdminRedirect", action = "Index" }
            );

            routes.MapRoute(
                name: "Courses",
                url: "Courses",
                defaults: new { controller = "Product", action = "Index" }
                );

            routes.MapRoute(
                name: "Resources",
                url: "Resources",
                defaults: new { controller = "Resource", action = "Index" }
                );

            routes.MapRoute(
                name: "News",
                url: "News",
                defaults: new { controller = "News", action = "Index" }
                );

            routes.MapRoute(
                name: "Login",
                url: "Account/Login",
                defaults: new { controller = "LoginManager", action = "Login" }
                );

            routes.MapRoute(
                name: "Forgot-Password",
                url: "Account/Forgot-Password",
                defaults: new { controller = "LoginManager", action = "RequestPasswordReset" }
                );

            routes.MapRoute(
                name: "Register",
                url: "Account/Register",
                defaults: new { controller = "LoginManager", action = "Register" }
                );

            routes.MapRoute(
                name: "Register-Facility",
                url: "Account/Register-Facility",
                defaults: new { controller = "LoginManager", action = "RegisterAsFacilityAdmin" }
                );

            routes.MapRoute(
                name: "Facility-Pending-Approval",
                url: "Account/Facility-Pending-Approval",
                defaults: new { controller = "LoginManager", action = "FacilityPendingApproval" }
                );

            routes.MapRoute(
                name: "Old Course Route",
                url: "{*url}",
                defaults: new { controller = "Product", action = "CustomRedirect" },
                constraints: new { url = new URLContainsRouteConstraint("Class.aspx") });

            // If the Page is found, will handle the routing dynamically
            var route = routes.MapRoute(
                name: "DynamicRouting",
                url: "{*url}",
                defaults: new { defaultcontroller = "Error", defaultaction = "NotFound" },
                constraints: new { PageFound = new DynamicRouteConstraint() }
            );
            route.RouteHandler = new DynamicRouteHandler();

            // default route
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "PedagogyPage", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}