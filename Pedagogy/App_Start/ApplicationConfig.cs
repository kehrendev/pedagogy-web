﻿using DynamicRouting.Kentico.MVC;
using Kentico.Content.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

namespace Pedagogy
{
    public class ApplicationConfig
    {
        public static void RegisterFeatures(IApplicationBuilder builder)
        {
            //Enable required Kentico features
            builder.UsePreview();

            // Enables the alternative URLs feature
            builder.UsePageRouting(new PageRoutingOptions
            {
                EnableAlternativeUrls = true
            });

            builder.UsePageBuilder(new PageBuilderOptions()
                {
                    DefaultSectionIdentifier = "Pedagogy.DefaultSection",
                    RegisterDefaultSection = false
                });

            builder.UseResourceSharingWithAdministration();

            RegisterPageTemplateFilters();
        }

        private static void RegisterPageTemplateFilters()
        {
            //Enabled, This must be last
            //PageBuilderFilters.PageTemplates.Add(new EmptyPageTemplateFilter());

            //Disabled
            PageBuilderFilters.PageTemplates.Add(new NoEmptyPageTemplateFilter());
        }
    }
}