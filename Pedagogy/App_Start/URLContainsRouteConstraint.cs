﻿using System.Web;
using System.Web.Routing;

namespace Pedagogy.App_Start
{
    public class URLContainsRouteConstraint : IRouteConstraint
    {
        private string phrase;

        public URLContainsRouteConstraint(string phrase)
        {
            this.phrase = phrase;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return values[parameterName] != null && values[parameterName].ToString().Contains(phrase);
        }
    }
}