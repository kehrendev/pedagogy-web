﻿using Autofac;
using Autofac.Integration.Mvc;

using Pedagogy.Config;
using Pedagogy.Utils;
using Pedagogy.DataAccess.DependencyInjection;
using Pedagogy.DataAccess.Repos;
using Pedagogy.DataAccess.Services;
using Pedagogy.DataAccess.Services.Context;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassBuilder.DependencyInjection;

using DynamicRouting.Implementations;
using DynamicRouting.Interfaces;

using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web;

using Microsoft.Owin.Security;

namespace Pedagogy.App_Start
{
    public class AutofacConfig
    {
        public static void ContainerConfig()
        {
            // Initializes the Autofac builder instance
            var builder = new ContainerBuilder();

            // DataAccess Registration
            builder.RegisterControllers(typeof(MvcApplication).Assembly); // Allows the IoC container to resolve dependencies in the apps controllers
            builder.RegisterType<DataDependencies>().As<IDataDependencies>().InstancePerRequest();
            builder.RegisterType<ClassMemberDependencies>().As<IClassMemberDependencies>().InstancePerRequest();
            builder.RegisterType<ClassBuilderDependencies>().As<IClassBuilderDependencies>().InstancePerRequest();

            // Used to register services provided by the CMS, allows on the fly registration for all services that are part
            // of the Kentico API. (ShoppingService, etc)
            // CmsRegistrationSource is a custom class in Business/DependencyInjection
            builder.RegisterSource(new CmsRegistrationSource());

            // Registers all services that implement the IService interface
            builder.RegisterAssemblyTypes(typeof(IService).Assembly)
                .Where(x => x.IsClass && !x.IsAbstract && typeof(IService).IsAssignableFrom(x))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            // Registers all repos that implement the IRepo interface
            builder.RegisterAssemblyTypes(typeof(IRepo).Assembly)
                .Where(x => x.IsClass && !x.IsAbstract && typeof(IRepo).IsAssignableFrom(x))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            // Registers all repos that implement the IRepo interface from the class members class library project
            builder.RegisterAssemblyTypes(typeof(ClassMembers.Entities.IRepo).Assembly)
                .Where(x => x.IsClass && !x.IsAbstract && typeof(ClassMembers.Entities.IRepo).IsAssignableFrom(x))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            // Registers all repos that implement the IRepo interface from the class builder class library project
            builder.RegisterAssemblyTypes(typeof(ClassBuilder.Entities.IRepo).Assembly)
                .Where(x => x.IsClass && !x.IsAbstract && typeof(ClassBuilder.Entities.IRepo).IsAssignableFrom(x))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            // Register the SiteContext service
            builder.RegisterType<SiteContextService>().As<ISiteContextService>()
                .WithParameter((parameter, context) => parameter.Name == "currentCulture",
                    (parameter, context) => CultureInfo.CurrentUICulture.Name)
                .WithParameter((parameter, context) => parameter.Name == "siteName",
                  (parameter, context) => AppConfig.SiteName)
                .InstancePerRequest();

            // Dynamic Routing Registration
            builder.RegisterType(typeof(BaseDynamicRouteHelper)).As(typeof(IDynamicRouteHelper));

            // Registers the authentication manager of the OWIN context for DI retrieval.
            builder.Register(context =>
                HttpContext.Current.GetOwinContext().Authentication)
                .As<IAuthenticationManager>();

            // Autowire Property Injection for controllers (can't have constructor injection)
            var allControllers = Assembly.GetExecutingAssembly().GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type));
            foreach (var controller in allControllers)
            {
                builder.RegisterType(controller).PropertiesAutowired();
            }

            //Resolves the dependencies
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}