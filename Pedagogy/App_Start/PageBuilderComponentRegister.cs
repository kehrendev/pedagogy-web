﻿using Kentico.PageBuilder.Web.Mvc;
using Pedagogy.Models.Sections;
using Pedagogy.Models.Widgets.AdWidget;
using Pedagogy.Models.Widgets.HowToWidget;
using Pedagogy.Models.Widgets.InfoWidget;

//Widgets
[assembly:RegisterWidget("Pedagogy.InfoWidget",
    "Information Widget",
    typeof(InfoWidgetProperties),
    customViewName: "Widgets/_Pedagogy.Widgets.InfoWidget",
    Description = "Displays an informational widget used in the three column section",
    IconClass = "icon-arrow-right-top-square")]

[assembly:RegisterWidget("Pedagogy.AuthorWidget",
    "Author Widget",
    null,
    customViewName: "Widgets/_Pedagogy.Widgets.AuthorWidget",
    Description = "Used to display a list of cards for each author",
    IconClass = "icon-id-cards")]

[assembly: RegisterWidget("Pedagogy.NewsWidget",
    "News Widget",
    null,
    customViewName: "Widgets/_Pedagogy.Widgets.NewsWidget",
    Description = "Used to display a list of news articles",
    IconClass = "icon-newspaper")]

[assembly: RegisterWidget("Pedagogy.AdWidget",
    "Ad Widget",
    typeof(AdWidgetProperties),
    customViewName: "Widgets/_Pedagogy.Widgets.AdWidget",
    Description = "Used to display an image and a link for an ad",
    IconClass = "icon-dollar-sign")]


//Sections
[assembly:RegisterSection("Pedagogy.DefaultSection",
    "Default Section",
    typeof(DefualtSectionProperties),
    Description = "Default section",
    IconClass = "icon-square-dashed")]

[assembly:RegisterSection("Pedagogy.ThreeColumnSection",
    "Three Column Section",
    typeof(ThreeColumnSectionProperties),
    Description = "Three column section",
    IconClass = "icon-l-header-cols-3-footer")]

[assembly:RegisterSection("Pedagogy.InfoSection",
    "Info Section",
    typeof(InfoSectionProperties),
    Description = "A section to place a rich text editor in",
    IconClass = "icon-i")]

[assembly: RegisterSection("Pedagogy.ContactFormSection",
    "Contact Section",
    typeof(ContactSectionProperties),
    Description = "A section used for the contact form",
    IconClass = "icon-l-cols-70-30")]