﻿using System.Web.Optimization;

namespace Pedagogy
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr")
                .Include("~/Scripts/Vendor/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/vendor/js")
                .Include("~/Scripts/Vendor/jQuery/jquery-3.4.1.min.js")
                .Include("~/Scripts/Vendor/jQuery/jquery-ui.min.js")
                .Include("~/Scripts/Vendor/jQuery/jquery.ui.touch-punch.min.js")
                .Include("~/Scripts/Vendor/jQuery/jquery.validate.min.js")
                .Include("~/Scripts/Vendor/jQuery/jquery.validate.unobtrusive.min.js")
                .Include("~/Scripts/Vendor/jQuery/jquery.slicknav.min.js")
                .Include("~/Scripts/Vendor/jQuery/jquery.magnific-popup.min.js")
                .Include("~/Scripts/Vendor/jQuery/jquery.counterup.min.js")
                .Include("~/Scripts/components/countryStateSelector.js")
                .Include("~/Scripts/Vendor/OwlCarousel/owl.carousel.min.js")
                .Include("~/Scripts/Vendor/slick/slick.min.js")
                .Include("~/Scripts/Vendor/wow.min.js")
                .Include("~/Scripts/Vendor/isotope.min.js")
                .Include("~/Scripts/Vendor/scrollIt.min.js")
                .Include("~/Scripts/Vendor/FontAwesome/fontawesome.min.js")
                .Include("~/Scripts/Vendor/MediaElement/mediaelement-and-player.min.js")
                .Include("~/Scripts/Vendor/MediaElement/speed.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/js")
                .Include("~/Scripts/main.js")
                .Include("~/Scripts/pages/*.js")
                .Include("~/Scripts/components/*.js")
                .Include("~/Scripts/formAutoPost.js"));

            bundles.Add(new StyleBundle("~/bundles/vendor/css")
                .Include("~/Content/Vendor/Bootstrap/bootstrap.min.css")
                .Include("~/Content/Vendor/slick/slick.min.css")
                .Include("~/Content/Vendor/slick/slick-theme.min.css")
                .Include("~/Content/Vendor/OwlCarousel/owl.carousel.min.css")
                .Include("~/Content/Vendor/OwlCarousel/owl.theme.default.css")
                .Include("~/Content/Vendor/magnific-popup.css")
                .Include("~/Content/Vendor/nice-select.css")
                .Include("~/Content/Vendor/gijgo.css")
                .Include("~/Content/Vendor/font-awesome.min.css")
                .Include("~/Content/Vendor/animate.css")
                .Include("~/Content/Vendor/themify-icons.css")
                .Include("~/Content/Vendor/flaticon.css"));

            bundles.Add(new StyleBundle("~/bundles/css")
                .Include("~/Content/css/styles.min.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}