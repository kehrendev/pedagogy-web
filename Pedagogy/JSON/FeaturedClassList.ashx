﻿<%@ WebHandler Language="C#" Class="FeaturedClassList" %>

using System;
using System.Web;
using CMS.DocumentEngine;
using CMS.Helpers;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;

public class FeaturedClassList : IHttpHandler
{

    [DataContract]
    public class PedagogyClass
    {
        [DataMember(Name = "CourseName")]
        public String CourseName { get; set; }
        [DataMember(Name = "Cost")]
        public String Cost { get; set; }
        [DataMember(Name = "ShortDescription")]
        public String ShortDescription { get; set; }
        [DataMember(Name = "ClassURL")]
        public String ClassURL { get; set; }
    }

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json";
        context.Response.ContentEncoding = System.Text.Encoding.UTF8;

        //context.Response.AddHeader("content-disposition", "attachment;filename=ClassList.json");

        System.Collections.Generic.List<PedagogyClass> listClasses = new List<PedagogyClass>();

        PedagogyClass tempClass;

        IEnumerable<TreeNode> classes = DocumentHelper.GetDocuments()
                .Type("Pedagogy.Course")
                .OnCurrentSite()
                .Path("/Courses", PathTypeEnum.Children)
                .Culture(CMS.Localization.LocalizationContext.CurrentCulture.CultureCode)
                .WhereTrue("CourseFeatured")
                .Published()
                .PublishedVersion()
                .OrderBy("NodeOrder")
                .ToList();

        foreach (var course in classes)
        {
            tempClass = new PedagogyClass();
            tempClass.CourseName = course.GetStringValue("CourseTitle", string.Empty);
            tempClass.Cost = string.Format("${0:C2}", course.GetStringValue("CoursePrice", string.Empty));
            tempClass.ShortDescription = TextHelper.LimitLength(course.GetStringValue("CourseSummary", string.Empty), 100);
            tempClass.ClassURL = course.GetStringValue("NodeAliasPath", string.Empty);

            listClasses.Add(tempClass);
        }

        System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        WriteCallback(context, jsSerializer.Serialize(listClasses));
        /**context.Response.Write(
            jsSerializer.Serialize(listCategories)
            );**/
    }

    void WriteCallback(HttpContext context, string json)
    {
        context.Response.Write(string.Format("{0}({1})", context.Request["jsoncallback"], json));
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}