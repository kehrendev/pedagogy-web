﻿<%@ WebHandler Language="C#" Class="ClassList" %>

using System;
using System.Web;
using CMS.DocumentEngine;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;

public class ClassList : IHttpHandler
{

    [DataContract]
    public class Category
    {
        [DataMember(Name = "CategoryName")]
        public String CategoryName { get; set; }

        [DataMember(Name = "Classes")]
        public List<PedagogyClass> Classes { get; set; }
    }

    [DataContract]
    public class PedagogyClass
    {
        [DataMember(Name = "ClassName")]
        public String ClassName { get; set; }

        [DataMember(Name = "ClassURL")]
        public String ClassURL { get; set; }
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/json";
        context.Response.ContentEncoding = System.Text.Encoding.UTF8;

        List<PedagogyClass> listClasses = new List<PedagogyClass>();

        IEnumerable<TreeNode> classes = DocumentHelper.GetDocuments()
                .Type("Pedagogy.Course")
                .OnCurrentSite()
                .Published()
                .PublishedVersion()
                .Culture(CMS.Localization.LocalizationContext.CurrentCulture.CultureCode)
                .Path("/Courses", PathTypeEnum.Children)
                .OrderBy(nameof(CMS.DocumentEngine.Types.Pedagogy.Course.NodeOrder))
                .ToList();

        foreach (var course in classes)
        {
            var tempClass = new PedagogyClass();
            tempClass.ClassName = course.GetStringValue("CourseTitle", string.Empty);
            tempClass.ClassURL = course.GetStringValue("NodeAliasPath", string.Empty);

            listClasses.Add(tempClass);
        }

        System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        WriteCallback(context, jsSerializer.Serialize(listClasses));
    }

    void WriteCallback(HttpContext context, string json)
    {
        context.Response.Write(string.Format("{0}({1})", context.Request["jsoncallback"].ToString(), json));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}