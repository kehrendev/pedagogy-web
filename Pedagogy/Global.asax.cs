﻿using Pedagogy.App_Start;
using Pedagogy.Lib.Helpers;
using Pedagogy.Lib.Events;

using Kentico.Web.Mvc;

using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Mvc;

namespace Pedagogy
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            // Enables and configures selected Kentico ASP.NET MVC integration features
            ApplicationConfig.RegisterFeatures(ApplicationBuilder.Current);

            AreaRegistration.RegisterAllAreas();
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Register dependency injection container
            AutofacConfig.ContainerConfig();

            FormBuilderCustomizations.SetGlobalRenderingConfigurations();

            //Register the form customizations
            FormFieldMarkupInjection.RegisterEventHandlers();
        }
    }
}