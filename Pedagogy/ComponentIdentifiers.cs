﻿namespace Pedagogy
{
    public class ComponentIdentifiers
    {
        #region Base Strings

        public const string BASE_WIDGET_IDENTIFIER = "Pedagogy.";
        public const string BASE_WIDGET_VIEW_PATH = "Widgets/";
        public const string BASE_PARTIAL_VIEW_PATH = "Partials/";

        #endregion

        #region Component Identifers

        public const string ACCOUNT_DETAILS_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "AccountDetailsWidget";
        public const string ADMIN_FUNCTIONS_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "AdminFunctionsWidget";
        public const string APPROVE_FACILITIES_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ApproveFacilitiesWidget";
        public const string ARCHIVED_COURSES_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ArchivedCoursesWidget";
        public const string BANNER_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "BannerWidget";
        public const string CHANGE_PASSWORD_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ChangePasswordWidget";
        public const string COURSE_INFO_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "CourseInfoWidget";
        public const string EVENT_LOG_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "EventLogWidget";
        public const string HOW_TO_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "HowToWidget";
        public const string INFO_SECTION_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "InfoSectionWidget";
        public const string LICENSING_INFO_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "LicensingInfoWidget";
        public const string MANAGE_CORPORATIONS_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ManageCorporationsWidget";
        public const string MANAGE_EDUCATION_PLANS_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ManageEducationPlansWidget";
        public const string MANAGE_ENROLLMENT_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ManageEnrollmentWidget";
        public const string MANAGE_FACILITIES_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ManageFacilitiesWidget";
        public const string MANAGE_FACILITY_CLASSES_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ManageFacilityClassesWidget";
        public const string MANAGE_STAFF_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ManageStaffWidget";
        public const string MANAGE_STUDENT_CLASSES_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ManageStudentClassesWidget";
        public const string MEMBERSHIP_COURSE_lISTING_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "MembershipCourseListingWidget";
        public const string PAY_INVOICE_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "PayInvoiceWidget";
        public const string PEDAGOGY_CLASS_WIDGETS_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "PedagogyClassWidgetsWidget";
        public const string PERSONAL_INFO_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "PersonalInfoWidget";
        public const string PRINT_STUDENT_CERTIFICATES_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "PrintStudentCertificatesWidget";
        public const string REGISTER_NEW_COURSE_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "RegisterNewCourse";
        public const string REPORTS_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "ReportsWidget";
        public const string SLIDER_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "SliderWidget";
        public const string SOLUTIONS_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "SolutionsWidget";
        public const string SPEECH_PREFERENCES_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "SpeechPreferencesWidget";
        public const string STUDENT_INFO_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "StudentInfoWidget";
        public const string UPDATE_STAFF_INFO_WIDGET_IDENTIFIER = BASE_WIDGET_IDENTIFIER + "UpdateStaffInfoWidget";

        #endregion

        #region Component View Paths

        public const string ACCOUNT_DETAILS_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.AccountDetailsWidget";
        public const string ADMIN_FUNCTIONS_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.AdminFunctionsWidget";
        public const string APPROVE_FACILITIES_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ApproveFacilitiesWidget";
        public const string ARCHIVED_COURSES_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ArchivedCoursesWidget";
        public const string BANNER_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.BannerWidget";
        public const string CHANGE_PASSWORD_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ChangePasswordWidget";
        public const string COURSE_INFO_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.CourseInfoWidget";
        public const string EVENT_LOG_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.EventLogWidget";
        public const string HOW_TO_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.HowToWidget";
        public const string INFO_SECTION_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.InfoSectionWidget";
        public const string LICENSING_INFO_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.LicensingInfoWidget";
        public const string MANAGE_CORPORATIONS_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ManageCorporationsWidget";
        public const string MANAGE_EDUCATION_PLANS_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ManageEducationPlansWidget";
        public const string MANAGE_ENROLLMENT_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ManageEnrollmentWidget";
        public const string MANAGE_FACILITIES_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ManageFacilitiesWidget";
        public const string MANAGE_FACILITY_CLASSES_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ManageFacilityClassesWidget";
        public const string MANAGE_STAFF_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ManageStaffWidget";
        public const string MANAGE_STUDENT_CLASSES_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ManageStudentClassesWidget";
        public const string MEMBERSHIP_COURSE_lISTING_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.MembershipCourseListingWidget";
        public const string PAY_INVOICE_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.PayInvoiceWidget";
        public const string PEDAGOGY_CLASS_WIDGETS_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.PedagogyClassWidgetsWidget";
        public const string PERSONAL_INFO_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.PersonalInfoWidget";
        public const string PRINT_STUDENT_CERTIFICATES_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.PrintStudentCertificatesWidget";
        public const string REGISTER_NEW_COURSE_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.RegisterNewCourseWidget";
        public const string REPORTS_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.ReportsWidget";
        public const string SLIDER_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.SliderWidget";
        public const string SOLUTIONS_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.SolutionsWidget";
        public const string SPEECH_PREFERENCES_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.SpeechPreferencesWidget";
        public const string STUDENT_INFO_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.StudentInfoWidget";
        public const string UPDATE_STAFF_INFO_WIDGET_VIEW = BASE_WIDGET_VIEW_PATH + "_Pedagogy.Widgets.UpdateStaffInfoWidget";

        #endregion

        #region Partial View Paths

        //Widgets
        //  Manage Corporations
        public const string NEW_CORPORATION_FORM_PARTIAL_VIEW = BASE_PARTIAL_VIEW_PATH + "_NewCorporationForm";

        //  Manage Facilities
        public const string NEW_FACILITY_FORM_PARTIAL_VIEW = BASE_PARTIAL_VIEW_PATH + "_NewFacilityForm";

        //  Manage Staff
        public const string STUDENT_TABLE_PARTIAL_VIEW = BASE_PARTIAL_VIEW_PATH + "_StudentTable";

        #endregion
    }
}