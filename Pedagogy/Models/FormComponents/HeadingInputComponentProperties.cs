﻿using CMS.DataEngine;
using Kentico.Forms.Web.Mvc;

namespace Pedagogy.Models.FormComponents
{
    public class HeadingInputComponentProperties : FormComponentProperties<string>
    {
        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Heading text", DefaultValue = "", Tooltip = "", Order = 1)]
        public string HeadingText { get; set; } = "";

        [DefaultValueEditingComponent(TextInputComponent.IDENTIFIER)]
        public override string DefaultValue { get; set; }

        public HeadingInputComponentProperties() : base(FieldDataType.Text, 1000) { }
    }
}