﻿using CMS.DataEngine;
using Kentico.Forms.Web.Mvc;

namespace Pedagogy.Models.FormComponents
{
    public class SubHeadingInputComponentProperties : FormComponentProperties<string>
    {
        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Heading text", DefaultValue = "", Tooltip = "", Order = 1)]
        public string SubHeadingText { get; set; } = "";

        [DefaultValueEditingComponent(TextInputComponent.IDENTIFIER)]
        public override string DefaultValue { get; set; }

        public SubHeadingInputComponentProperties() : base(FieldDataType.Text, 1000) { }
    }
}