﻿using CMS.DataEngine;
using Kentico.Forms.Web.Mvc;

namespace Pedagogy.Models.FormComponents
{
    public class reCAPTCHAInputComponentProperties : FormComponentProperties<string>
    {
        //[EditingComponent(TextInputComponent.IDENTIFIER, Label = "reCAPTCHA Token", DefaultValue = "", Tooltip = "", Order = 0)]
        //public string reCAPTCHAToken { get; set; } = "";

        [DefaultValueEditingComponent("Pedagogy.reCAPTCHAInputComponent")]
        public override string DefaultValue { get; set; }

        public reCAPTCHAInputComponentProperties() : base(FieldDataType.Text, 300) { }
    }
}