﻿using Kentico.Forms.Web.Mvc;
using Pedagogy.Models.FormComponents;

[assembly: RegisterFormComponent(HeadingInputComponent.IDENTIFIER, typeof(HeadingInputComponent), "Heading Text",
    Description = "Allows the user to enter heading text", IconClass = "icon-l-header-text")]
namespace Pedagogy.Models.FormComponents
{
    public class HeadingInputComponent : FormComponent<HeadingInputComponentProperties, string>
    {
        public const string IDENTIFIER = "Pedagogy.HeadingInputComponent";

        //Disables automatic server-side evaluation for the component
        public override bool CustomAutopostHandling => true;

        [BindableProperty]
        public string Value { get; set; } = "";

        public override string GetValue()
        {
            return Value;
        }

        public override void SetValue(string value)
        {
            Value = value;
        }
    }
}