﻿using Kentico.Forms.Web.Mvc;

using Pedagogy.Models.FormComponents;

[assembly: RegisterFormComponent(SubHeadingInputComponent.IDENTIFIER, typeof(SubHeadingInputComponent), "Sub Heading Text",
    Description = "Allows the user to enter sub heading text", IconClass = "icon-l-header-text")]
namespace Pedagogy.Models.FormComponents
{
    public class SubHeadingInputComponent : FormComponent<SubHeadingInputComponentProperties, string>
    {
        public const string IDENTIFIER = "Pedagogy.SubHeadingInputComponent";

        //Disables automatic server-side evaluation for the component
        public override bool CustomAutopostHandling => true;

        [BindableProperty]
        public string Value { get; set; } = "";

        public override string GetValue()
        {
            return Value;
        }

        public override void SetValue(string value)
        {
            Value = value;
        }
    }
}