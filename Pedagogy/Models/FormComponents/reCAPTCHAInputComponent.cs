﻿using Kentico.Forms.Web.Mvc;
using Pedagogy.Models.FormComponents;

[assembly: RegisterFormComponent(reCAPTCHAInputComponent.IDENTIFIER, typeof(reCAPTCHAInputComponent), "reCAPTCHA V3",
    Description = "Allows the user to enter heading text", IconClass = "icon-recaptcha")]
namespace Pedagogy.Models.FormComponents
{
    public class reCAPTCHAInputComponent : FormComponent<HeadingInputComponentProperties, string>
    {
        public const string IDENTIFIER = "Pedagogy.reCAPTCHAInputComponent";

        [BindableProperty]
        public string reCAPTCHAToken { get; set; }

        // Disables automatic server-side evaluation for the component
        public override bool CustomAutopostHandling => true;

        public override string GetValue()
        {
            return reCAPTCHAToken;
        }

        public override void SetValue(string value)
        {
            reCAPTCHAToken = value;
        }
    }
}