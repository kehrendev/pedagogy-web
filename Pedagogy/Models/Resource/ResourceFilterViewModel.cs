﻿using CMS.SiteProvider;
using CMS.Taxonomy;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Pedagogy.Models.Resource
{
    public class ResourceFilterViewModel
    {
        [UIHint("ResourceFilter")]
        public ResourceFilterCheckboxViewModel[] Categories { get; set; }

        public void Load()
        {
            Categories = GetCategories()
                .Select(GetFilterCheckboxViewModel)
                .ToArray();
        }

        public List<string> GetSelectedCategories()
        {
            return Categories
                .Where(x => x.IsChecked)
                .Select(x => x.Value)
                .ToList()
                ?? new List<string>();
        }

        private IEnumerable<string> GetCategories()
        {
            return CategoryInfoProvider.GetCategories()
                .OnSite(SiteContext.CurrentSiteName)
                .OrderBy(x => x.CategoryOrder)
                .Where(x => x.CategoryName.StartsWith("resourcetype."))
                .Select(x => x.CategoryDisplayName);

        }

        private static ResourceFilterCheckboxViewModel GetFilterCheckboxViewModel(string category)
        {
            var formattedCategoryName = category.ToLower().Replace(" & ", "").Replace(", ", "").Replace(" ", "");

            return new ResourceFilterCheckboxViewModel
            {
                DisplayName = category,
                Value = "resourcetype." + formattedCategoryName
            };
        }
    }
}