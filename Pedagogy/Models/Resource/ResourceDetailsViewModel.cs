﻿using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.DataAccess.Dto.Files;
using Pedagogy.DataAccess.Dto.Resources;
using System.Collections.Generic;

namespace Pedagogy.Models.Resource
{
    public class ResourceDetailsViewModel : IViewModel
    {
        public ResourceItemDto ResourceData { get; set; }
        public IEnumerable<FileItemDto> Files { get; set; }
        public IEnumerable<CourseItemDto> RelatedCourses { get; set; }
    }
}