﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Resource
{
    public class ResourceFilterCheckboxViewModel : IViewModel
    {
        public string Value { get; set; }

        public bool IsChecked { get; set; }

        public string DisplayName { get; set; }
    }
}