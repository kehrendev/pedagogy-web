﻿using Pedagogy.DataAccess.Dto.Maps;
using Pedagogy.DataAccess.Dto.Resources;
using Pedagogy.Lib.DataModels;
using Pedagogy.Models.Search;
using System.Collections.Generic;

namespace Pedagogy.Models.Resource
{
    public class ResourceViewModel : IViewModel
    {
        public IEnumerable<ResourceItemDto> Resources { get; set; }
        public IEnumerable<MapItemDto> Maps { get; set; }
        public SearchViewModel SearchResults { get; set; }
        public ResourceFilterViewModel Filter { get; set; }
        public ResourceFilterViewModel FilterOptions { get; set; }
        public PagingInfo<ResourceItemDto> PagingInfo { get; set; }
    }
}