﻿using Pedagogy.DataAccess.Dto.News;
using System.Collections.Generic;

namespace Pedagogy.Models.News
{
    public class NewsDetailsViewModel : IViewModel
    {
        public NewsItemDto NewsData { get; set; }
        public IEnumerable<CategoryViewModel> Categories { get; set; }
    }
}