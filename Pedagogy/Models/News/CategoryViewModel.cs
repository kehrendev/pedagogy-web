﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.News
{
    public class CategoryViewModel : IViewModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}