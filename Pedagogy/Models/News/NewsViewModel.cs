﻿using Pedagogy.DataAccess.Dto.News;
using Pedagogy.Lib.DataModels;
using Pedagogy.Models.Search;
using System.Collections.Generic;

namespace Pedagogy.Models.News
{
    public class NewsViewModel : IViewModel
    {
        public PagingInfo<NewsItemDto> NewsArticlesByYear { get; set; }
        public SearchViewModel SearchData { get; set; }
        public PagingInfo<NewsItemDto> SearchResults { get; set; }
        public IEnumerable<CategoryViewModel> Categories { get; set; }
        public PagingInfo<NewsItemDto> PagingInfo { get; set; }
    }
}