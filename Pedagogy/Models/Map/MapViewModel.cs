﻿using Pedagogy.DataAccess.Dto.Maps;
using Pedagogy.DataAccess.Dto.Resources;
using System.Collections.Generic;

namespace Pedagogy.Models.Map
{
    public class MapViewModel : IViewModel
    {
        public MapItemDto MapData { get; set; }
        public IEnumerable<ResourceItemDto> MapResources { get; set; }
    }
}