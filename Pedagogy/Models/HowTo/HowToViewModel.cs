﻿using Pedagogy.DataAccess.Dto.HowToFiles;

namespace Pedagogy.Models.HowTo
{
    public class HowToViewModel : IViewModel
    {
        public HowToItemDto HowToData { get; set; }
    }
}