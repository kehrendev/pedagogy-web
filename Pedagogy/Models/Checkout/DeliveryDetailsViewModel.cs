﻿using CMS.Ecommerce;

namespace Pedagogy.Models.Checkout
{
    public class DeliveryDetailsViewModel : IViewModel
    {
        public CustomerViewModel Customer { get; set; }
        public BillingAddressViewModel BillingAddress { get; set; }
        public ShoppingCartInfo CurrentCart { get; set; }
        public bool CreateMembership { get; set; }
        public bool ShowCancelFacilityTransactionBtn { get; set; }
    }
}