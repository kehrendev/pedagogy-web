﻿using CMS.Ecommerce;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;

using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.Models.Checkout
{
    public class ShoppingCartViewModel : IViewModel
    {
        public IEnumerable<ShoppingCartItemViewModel> CartItems { get; set; }
        public string CurrencyFormatString { get; set; }
        public IEnumerable<string> CouponCodes { get; set; }
        public IEnumerable<decimal?> CouponValues { get; set; }
        public decimal CouponValueTotals { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public bool IsEmpty { get; set; }
        public int OrderId { get; set; }
        public bool CreateMemership { get; set; }
        public bool ShowCancelFacilityTransactionBtn { get; set; }

        public ShoppingCartViewModel(ShoppingCartInfo cart, UserInfo currentUser)
        {
            CartItems = cart.CartProducts.Select((cartItemInfo) =>
            {
                List<OptionCategoryInfo> optionCategories = OptionCategoryInfoProvider.GetProductOptionCategories(cartItemInfo.SKUID, true, OptionCategoryTypeEnum.Products).ToList();
                var skuInfo = new List<SKUInfo>();
                foreach (var option in optionCategories)
                {
                    skuInfo = SKUInfoProvider.GetSKUOptionsForProduct(cartItemInfo.SKUID, option.CategoryID, true).ToList();
                }

                return new ShoppingCartItemViewModel()
                {
                    CartItemUnits = cartItemInfo.CartItemUnits,
                    SKUName = cartItemInfo.SKU.SKUName,
                    TotalPrice = cartItemInfo.TotalPrice,
                    CartItemID = cartItemInfo.CartItemID,
                    SKUID = cartItemInfo.SKUID,
                    Options = skuInfo,
                    CurrencyFormatString = cart.Currency.CurrencyFormatString,
                    CurrentCart = cart.CartItems
                };
            });

            CurrencyFormatString = cart.Currency.CurrencyFormatString;
            CouponCodes = cart.CouponCodes.AllAppliedCodes.Select(x => x.Code);
            CouponValues = cart.CouponCodes.AllAppliedCodes.Select(y => y.ValueInMainCurrency);
            CouponValueTotals = cart.ItemsDiscount;
            TotalTax = cart.TotalTax;
            GrandTotal = cart.GrandTotal;
            IsEmpty = cart.IsEmpty;
            ShowCancelFacilityTransactionBtn = CookieHelper.GetExistingCookie(ContentIdentifiers.FACILITY_ID_COOKIE) != null && currentUser != null && currentUser.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
        }
    }
}