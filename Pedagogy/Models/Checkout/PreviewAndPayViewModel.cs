﻿using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;

namespace Pedagogy.Models.Checkout
{
    public class PreviewAndPayViewModel : IViewModel
    {
        public DeliveryDetailsViewModel DeliveryDetails { get; set; }
        public ShoppingCartViewModel Cart { get; set; }
        public PaymentMethodViewModel PaymentMethod { get; set; }
        public CreditCardViewModel CardInfo { get; set; }
        public List<string> CardErrors { get; set; }
        public IEnumerable<string> CouponCodes { get; set; }
        public FacilityPODetailsViewModel FacilityPODetails { get; set; }
        public bool IsFacilityPurchase { get; set; }
        public bool FacilityPurchaser { get; set; }
        public bool IsMembershipPurchase { get; set; }
        public IEnumerable<MembershipViewModel> Memberships { get; set; }
        public IEnumerable<ShoppingCartItemViewModel> RegularCourses { get; set; }
        public bool FreeCourse { get; set; }
        public bool ShowCancelFacilityTransactionBtn { get; set; }
        public bool ShowOver200StaffPopup { get; set; }
        public bool IsStudentPurchase { get; set; }
    }
}