﻿using CMS.Ecommerce;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pedagogy.Models.Checkout
{
    public class PaymentMethodViewModel : IViewModel
    {
        [DisplayName("Payment method")]
        public int PaymentMethodID { get; set; }
        public SelectList PaymentMethods { get; set; }

        public PaymentMethodViewModel(PaymentOptionInfo paymentMethod, SelectList paymentMethods)
        {
            PaymentMethods = paymentMethods;

            if(paymentMethod != null)
            {
                PaymentMethodID = paymentMethod.PaymentOptionID;
            }
        }

        public PaymentMethodViewModel()
        {

        }
    }
}