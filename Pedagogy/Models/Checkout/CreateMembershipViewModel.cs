﻿using Pedagogy.Models.ClassMembers;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Pedagogy.Models.Checkout
{
    public class CreateMembershipViewModel : IViewModel
    {
        public MembershipViewModel[] Memberships { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }
        public string ErrMsg { get; set; }
        public SelectList MembershipCategories { get; set; }
        public string MembershipCategory { get; set; }
        public int PageSize { get; set; }
        public SelectList Facilities { get; set; }
        public int FacilityID { get; set; }
    }
}