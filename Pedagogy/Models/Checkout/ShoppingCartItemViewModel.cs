﻿using CMS.Ecommerce;
using System.Collections.Generic;

namespace Pedagogy.Models.Checkout
{
    public class ShoppingCartItemViewModel : IViewModel
    {
        public string SKUName { get; set; }
        public int SKUID { get; set; }
        public int CartItemUnits { get; set; }
        public decimal TotalPrice { get; set; }
        public int CartItemID { get; set; }
        public string CurrencyFormatString { get; set; }
        public IEnumerable<ShoppingCartItemInfo> CurrentCart { get; set; }
        public List<SKUInfo> Options { get; set; }
    }
}