﻿using System.Collections.Generic;

namespace Pedagogy.Models.Checkout
{
    public class OptionsViewModel : IViewModel
    {
        public int ParentSku { get; set; }
        public IEnumerable<int> OptionSkuValues { get; set; }
        public bool IsSelected { get; set; }
    }
}