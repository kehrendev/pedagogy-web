﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.Checkout
{
    public class CreditCardViewModel : IViewModel
    {
        [CreditCard]
        [Required(ErrorMessage = "Card number is required.")]
        [DisplayName("Card Number")]
        public string CardNo { get; set; }

        [Required(ErrorMessage = "CVV code is required.")]
        [DisplayName("CVV")]
        [MaxLength(4, ErrorMessage = "The CVV code can only be 4 numbers max")]
        public string CVVCode { get; set; }

        public string Month { get; set; }
        public string Year { get; set; }
        public string ExpDate { get; set; }
    }
}