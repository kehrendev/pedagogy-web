﻿namespace Pedagogy.Models.Checkout
{
    public class PaymentProcessViewModel : IViewModel
    {
        public CreditCardViewModel CardData { get; set; }
        public int OrderId { get; set; }
    }
}