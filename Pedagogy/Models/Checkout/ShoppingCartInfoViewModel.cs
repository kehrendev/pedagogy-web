﻿using CMS.Ecommerce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Checkout
{
    public class ShoppingCartInfoViewModel : IViewModel
    {
        public IEnumerable<ShoppingCartItemViewModel> CartItems { get; set; }
        public string CurrencyFormatString { get; set; }
        public IEnumerable<string> CouponCodes { get; set; }
        public IEnumerable<decimal?> CouponValues { get; set; }
        public decimal CouponValueTotals { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public bool IsEmpty { get; set; }

        public ShoppingCartInfoViewModel(ShoppingCartInfo cart)
        {
            CartItems = cart.CartProducts.Select((cartItemInfo) =>
            {
                List<OptionCategoryInfo> optionCategories = OptionCategoryInfoProvider.GetProductOptionCategories(cartItemInfo.SKUID, true, OptionCategoryTypeEnum.Products).ToList();
                var skuInfo = new List<SKUInfo>();
                foreach (var option in optionCategories)
                {
                    skuInfo = SKUInfoProvider.GetSKUOptionsForProduct(cartItemInfo.SKUID, option.CategoryID, true).ToList();
                }

                return new ShoppingCartItemViewModel()
                {
                    CartItemUnits = cartItemInfo.CartItemUnits,
                    SKUName = cartItemInfo.SKU.SKUName,
                    TotalPrice = cartItemInfo.TotalPrice,
                    CartItemID = cartItemInfo.CartItemID,
                    SKUID = cartItemInfo.SKUID,
                    Options = skuInfo,
                    CurrencyFormatString = cart.Currency.CurrencyFormatString
                };
            });
            CurrencyFormatString = cart.Currency.CurrencyFormatString;
            CouponCodes = cart.CouponCodes.AllAppliedCodes.Select(x => x.Code);
            CouponValues = cart.CouponCodes.AllAppliedCodes.Select(y => y.ValueInMainCurrency);
            CouponValueTotals = cart.ItemsDiscount;
            TotalTax = cart.TotalTax;
            GrandTotal = cart.GrandTotal;
            IsEmpty = cart.IsEmpty;
        }
    }
}