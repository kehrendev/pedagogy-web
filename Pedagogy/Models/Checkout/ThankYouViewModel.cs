﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.Checkout
{
    public class ThankYouViewModel : IViewModel
    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "The Email address cannot be empty.")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        [MaxLength(320, ErrorMessage = "The Email address cannot be longer than 320 characters.")]
        public string Email { get; set; }
        public IEnumerable<string> Emails { get; set; }
        public string SuccessMessage { get; set; }
        public string InvoiceNumber { get; set; }
    }
}