﻿using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.Checkout
{
    public class FacilityPODetailsViewModel : IViewModel
    {
        [Required(ErrorMessage = "Facility name is required")]
        [Display(Name = "Facility Name")]
        public string FacilityName { get; set; }

        [Required(ErrorMessage = "Facility code is required")]
        [Display(Name = "Facility Code")]
        public string FacilityCode { get; set; }
    }
}