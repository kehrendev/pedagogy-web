﻿using Pedagogy.Models.Checkout;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.ClassMembers
{
    public class MembershipViewModel
    {
        [Required(ErrorMessage = "Membership Name is required")]
        public string MembershipName { get; set; }

        public IEnumerable<int> ClassIDs { get; set; }

        [Required(ErrorMessage = "Quantity is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Enter a valid number that is at least 1 or more")]
        public int Quantity { get; set; }
        public string ClassIDStringList { get; set; }

        public IEnumerable<ShoppingCartItemViewModel> CartItemData { get; set; }
        public IEnumerable<StudentClassViewModel> Classes { get; set; }
        public Dictionary<int, List<string>> MemClassData { get; set; }
    }
}