﻿namespace Pedagogy.Models.ClassMembers
{
    public class LicenseTypeModel
    {
        public int LicenseTypeID { get; set; }
        public string Name { get; set; }
        public string LicenseCodeName { get; set; }

        public static LicenseTypeModel SetModel(int licenseID, string licenseName, string licenseCodeName)
        {
            return new LicenseTypeModel
            {
                LicenseTypeID = licenseID,
                Name = licenseName,
                LicenseCodeName = licenseCodeName
            };
        }
    }
}