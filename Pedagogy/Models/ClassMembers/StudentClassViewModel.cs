﻿using System.Collections.Generic;

namespace Pedagogy.Models.ClassMembers
{
    public class StudentClassViewModel : IViewModel
    {
        public int StudentID { get; set; }
        public int StudentClassID { get; set; }
        public string ClassName { get; set; }
        public decimal ContactHours { get; set; }
        public string ClassPageUrl { get; set; }
        public Dictionary<int, string> AttemptUrls { get; set; }
        public string WorkflowStep { get; set; }
        public decimal TestScore { get; set; }
        public string DateCompletedOn { get; set; }
        public string PrintCertUrl { get; set; }
        public string ExpDate { get; set; }
        public bool OnStart { get; set; }
        public string MemName { get; set; }
        public string InvoiceNum { get; set; }
        public bool InPackage { get; set; }
    }
}