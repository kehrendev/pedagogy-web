﻿using CMS.Membership;
using CMS.SiteProvider;

using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.Lib.Helpers;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pedagogy.Models.ClassMembers
{
    public class ContactViewModel : IViewModel
    {
        public ContactViewModel() { }

        public ContactViewModel(ContactDto c)
        {
            UserInfo user = null;
            if (c.ContactUserID.HasValue) user = UserInfoProvider.GetUserInfo(c.ContactUserID.Value);

            ContactID = c.ContactID;
            ContactUserID = c.ContactUserID;
            FirstName = user != null ? user.FirstName : c.ContactFirstName;
            LastName = user != null ? user.LastName : c.ContactLastName;
            CreatedOn = c.CreatedOn;
            UpdatedOn = c.UpdatedOn;
            SpeechVoiceID = c.SpeechVoiceID;
            SpeechRateID = c.SpeechRateID;
            TypeID = c.ContactTypeID;
            MemberSince = user != null ? user.UserCreated.ToString("MM/dd/yyyy") : "";
            Enabled = user != null && user.Enabled;
            UserName = user != null ? user.UserName : "";
            Phone = user != null ? user.UserSettings.UserPhone : c.ContactPhone ?? "";
            Email = user != null ? user.Email : c.ContactEmail ?? "";
            CertFirstName = c.ContactFirstName;
            CertLastName = c.ContactLastName;
            HasUserAcct = user != null;
            AllowOnlinePurchases = user != null && user.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName);
        }

        public int FacilityID { get; set; }
        public int ContactID { get; set; }
        public int? ContactUserID { get; set; }


        [Required(ErrorMessage = "The First Name cannot be empty.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The Last Name cannot be empty.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public bool ExisitingUser { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [DisplayName("User Name")]
        public string UserName { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? SpeechVoiceID { get; set; }
        public int? SpeechRateID { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please select a type.")]
        public int? TypeID { get; set; }
        public string TypeName { get; set; }
        //public SelectList TypesForAdmin { get; set; }
        public SelectList ContactTypes { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        //[Required(ErrorMessage = "The Phone Number cannot be empty.")]
        public string Phone { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        [MaxLength(320, ErrorMessage = "The Email address cannot be longer than 320 characters.")]
        public string Email { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Username & Password Email Recipient")]
        [Required(ErrorMessage = "Email cannont be empty.")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        [MaxLength(320, ErrorMessage = "The Email address cannot be longer than 320 characters.")]
        public string UserPassEmailRecipient { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "The Password cannot be empty.")]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [RegularExpression(@"^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$", ErrorMessage = "Password must include..<ul class='password-validation-err'><li>at least 8 characters</li><li>at least 1 numeric digit</li><li>at least 1 special character</li><li>1 uppercase letter</li><li>at least 1 lowercase letter</li></ul>")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The entered passwords do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Username is required")]
        public string TemporaryUserName { get; set; }
        public bool NeedsUserAcct { get; set; }

        public bool HasUserAcct { get; set; }

        public bool IsVerified { get; set; }

        public string FacilityName { get; set; }

        public string ReturnUrl { get; set; }

        public bool AllowOnlinePurchases { get; set; }

        public string MemberSince { get; set; }

        public bool Enabled { get; set; }

        [DisplayName("First Name")]
        [Required(ErrorMessage = "The First Name cannot be empty.")]
        public string CertFirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "The Last Name cannot be empty.")]
        public string CertLastName { get; set; }

        public IEnumerable<StudentLicenseViewModel> StudentLicenses { get; set; }
        public bool NALicense { get; set; }

        public bool IsAdmin { get; set; }

        public static ContactViewModel GetViewModel(int contactID, string firstName, string lastName, string phone)
        {
            return new ContactViewModel
            {
                ContactID = contactID,
                FirstName = firstName,
                LastName = lastName,
                Phone = phone
            };
        }

        public static ContactViewModel GetViewModel(dynamic contactData, bool hasUserAcct, string userName, bool allowOnlinePurchase, SelectList contactTypes)
        {
            return new ContactViewModel
            {
                ContactID = contactData.ContactID,
                ContactUserID = contactData.ContactUserID,
                TypeID = contactData.ContactTypeID,
                FirstName = contactData.ContactFirstName,
                LastName = contactData.ContactLastName,
                Phone = contactData.ContactPhone,
                Email = contactData.ContactEmail,
                HasUserAcct = hasUserAcct,
                UserName = userName,
                AllowOnlinePurchases = allowOnlinePurchase,
                ContactTypes = contactTypes
            };
        }
    }
}