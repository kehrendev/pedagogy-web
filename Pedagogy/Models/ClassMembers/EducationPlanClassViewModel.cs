﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.ClassMembers
{
    public class EducationPlanClassViewModel
    {
        public string DateAddedOn { get; set; }
        public int QtyAvailable { get; set; }
        public string ClassName { get; set; }
        public int ClassID { get; set; }
    }
}