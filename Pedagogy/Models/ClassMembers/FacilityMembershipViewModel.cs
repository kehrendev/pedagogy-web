﻿using System.Collections.Generic;

namespace Pedagogy.Models.ClassMembers
{
    public class FacilityMembershipViewModel
    {
        public int MembershipID { get; set; }
        public string MembershipName { get; set; }
        public string MembershipExpDate { get; set; }
        public int MembershipQty { get; set; }

        public int FacilityID { get; set; }
        public IEnumerable<FacilityClassViewModel> FacilityClasses { get; set; }
        public string InvoiceNumber { get; set; }
    }
}