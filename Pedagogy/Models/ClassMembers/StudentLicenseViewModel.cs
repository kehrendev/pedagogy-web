﻿using CMS.Globalization;

using Pedagogy.ClassMembers.Entities.StudentLicense;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pedagogy.Models.ClassMembers
{
    public class StudentLicenseViewModel
    {
        public StudentLicenseViewModel () { }

        public StudentLicenseViewModel(Pedagogy.ClassMembers.StudentLicense s, string licenseTypeName = "")
        {
            StudentID = s.StudentID;
            LicenseID = s.StudentLicenseID;
            LicenseTypeName = licenseTypeName;
            LicenseNumber = s.LicenseNumber ?? "";
            LicenseState = s.State ?? "";
            LicenseTypeID = s.LicenseTypeID;
            StateID = !string.IsNullOrEmpty(s.State) ? StateInfoProvider.GetStateInfoByCode(s.State).StateID : 0;
        }
        public StudentLicenseViewModel(StudentLicenseDto s, string licenseTypeName = "")
        {
            StudentID = s.StudentID;
            LicenseID = s.StudentLicenseID;
            LicenseTypeName = licenseTypeName;
            LicenseNumber = s.LicenseNumber ?? "";
            LicenseState = s.State ?? "";
            LicenseTypeID = s.LicenseTypeID;
            StateID = !string.IsNullOrEmpty(s.State) ? StateInfoProvider.GetStateInfoByCode(s.State).StateID : 0;
        }

        public int LicenseID { get; set; }
        public string LicenseTypeName { get; set; }
        [Display(Name = "License Number")]
        public string LicenseNumber { get; set; }
        public string LicenseState { get; set; }

        public bool LicenseNA { get; set; }
        public int StudentID { get; set; }

        [Display(Name = "License Type")]
        public int LicenseTypeID { get; set; }
        public SelectList LicenseTypes { get; set; }

        [Display(Name = "State")]
        public int StateID { get; set; }
    }
}