﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pedagogy.Models.ClassMembers
{
    public class EducationPlanViewModel
    {
        [Required(ErrorMessage = "Please enter an education plan name.")]
        [DisplayName("Education Plan Name")]
        public string PlanName { get; set; }

        [Required(ErrorMessage = "Please select a facility.")]
        [DisplayName("Select Facility")]
        public int FacilityID { get; set; }
        public IEnumerable<SelectListItem> Facilities { get; set; }

        [DisplayName("Auto-Upgrade to New Class Versions?")]
        public bool AutoUpgradeClasses { get; set; }

        [DisplayName("Auto-Assign Based on License Type?")]
        public bool AssignClassesBasedOnLicense { get; set; }

        [DisplayName("Plan Status")]
        public bool PlanStatus { get; set; }

        public IEnumerable<EducationPlanClassViewModel> Classes { get; set; }
    }
}