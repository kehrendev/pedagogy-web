//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pedagogy.Models.ClassMembers
{
    using System;
    using System.Collections.Generic;
    
    public partial class Contact
    {
        public int ContactID { get; set; }
        public Nullable<int> ContactUserID { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> SpeechVoiceID { get; set; }
        public Nullable<int> SpeechRateID { get; set; }
        public Nullable<int> ContactTypeID { get; set; }
    
        public virtual ContactType ContactType { get; set; }
        public virtual SpeechRate SpeechRate { get; set; }
    }
}
