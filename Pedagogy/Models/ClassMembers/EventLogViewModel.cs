﻿namespace Pedagogy.Models.ClassMembers
{
    public class EventLogViewModel
    {
        public EventLogViewModel(Pedagogy.ClassMembers.EventLog e)
        {
            EventLogID = e.EventLogID;
            UsersName = e.UsersName;
            UsersUserName = e.UsersUserName;
            StudentsName = e.StudentName;
            StudentsUserName = e.StudentUserName;
            CourseName = e.CourseName;
            CreatedOn = e.EventCreatedOn.ToString();
        }

        public int EventLogID { get; set; }
        public string UsersName { get; set; }
        public string UsersUserName { get; set; }
        public string StudentsName { get; set; }
        public string StudentsUserName { get; set; }
        public string CourseName { get; set; }
        public string LogType { get; set; }
        public string CreatedOn { get; set; }
    }
}