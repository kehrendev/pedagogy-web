﻿using CMS.Globalization;

using Pedagogy.ClassMembers.Entities.Facility;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pedagogy.Models.ClassMembers
{
    public class FacilityViewModel : IViewModel
    {
        public FacilityViewModel() { }

        public FacilityViewModel(Pedagogy.ClassMembers.Facility f)
        {
            FacilityID = f.FacilityID;
            Name = f.Name;
            FacilityCode = f.FacilityCode;
            AddressLine1 = f.Address1;
            AddressLine2 = f.Address2;
            City = f.City ?? "";
            Zip = f.PostalCode;
            Enabled = f.Enabled;
            PartnerCode = f.PartnerCode;
            AcctingCode = f.AcctingCode ?? "";
            FacilityParentID = f.FacilityParentID;
            IsCorporation = f.FacilityIsCorporation;
            State = f.State;
            StateID = !string.IsNullOrEmpty(f.State) ? StateInfoProvider.GetStateInfoByCode(f.State) == null ? 0 : StateInfoProvider.GetStateInfoByCode(f.State).StateID : 0;
            FacilityPreferenceID = f.FacilityPreferenceID;
        }

        public FacilityViewModel(FacilityDto f)
        {
            FacilityID = f.FacilityID;
            Name = f.FacilityName;
            FacilityCode = f.FacilityCode;
            AddressLine1 = f.FacilityAddress1;
            AddressLine2 = f.FacilityAddress2;
            City = f.FacilityCity ?? "";
            Zip = f.FacilityZip;
            Enabled = f.Enabled;
            AcctingCode = f.AccountingCode ?? "";
            FacilityParentID = f.FacilityParentID;
            IsCorporation = f.FacilityIsCorporation;
            State = f.FacilityState;
            StateID = !string.IsNullOrEmpty(f.FacilityState) ? StateInfoProvider.GetStateInfoByCode(f.FacilityState) == null ? 0 : StateInfoProvider.GetStateInfoByCode(f.FacilityState).StateID : 0;
            FacilityPreferenceID = f.FacilityPreferenceID;
        }

        public int FacilityID { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        public string FacilityCode { get; set; }

        [Required(ErrorMessage = "Address line 1 is required")]
        [Display(Name = "Line 1")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Line 2")]
        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        public string State { get; set; }

        [Required(ErrorMessage = "State is requried")]
        [Display(Name = "State")]
        public int StateID { get; set; }

        [Required(ErrorMessage = "Zip code is required")]
        [DataType(DataType.PostalCode)]
        public string Zip { get; set; }

        public bool Enabled { get; set; }

        public int PartnerCode { get; set; }

        public string AcctingCode { get; set; }

        [Required(ErrorMessage = "Facility Parent is required")]
        [Display(Name = "Parent Corporation")]
        public int? FacilityParentID { get; set; }

        public bool? IsCorporation { get; set; }

        public string AdminName { get; set; }
        public string AdminPhone { get; set; }

        public List<ContactViewModel> Contacts { get; set; }

        public IEnumerable<ContactViewModel> Students { get; set; }

        public bool IsSiteAdmin { get; set; }
        public bool IsCorporateAdmin { get; set; }
        public string ReturnUrl { get; set; }

        public bool EmailMonthlyProgress { get; set; }
        public bool AssignHowToPedagogy { get; set; }

        public SelectList ContactTypes { get; set; }
        public ContactViewModel ContactModel { get; set; }

        public bool HasChildFacilities { get; set; } = false;

        public int? FacilityPreferenceID { get; set; }

        public static FacilityViewModel GetViewModel(string name, int id)
        {
            return new FacilityViewModel
            {
                FacilityID = id,
                Name = name
            };
        }

        public static FacilityViewModel GetViewModel(string returnUrl, int facilityID, string name, string city, string state, bool isSiteAdmin, int? facilityParentID,
            string facilityCode)
        {
            return new FacilityViewModel
            {
                ReturnUrl = returnUrl,
                FacilityID = facilityID,
                Name = name,
                City = city ?? "",
                State = state,
                IsSiteAdmin = isSiteAdmin,
                FacilityParentID = facilityParentID,
                FacilityCode = facilityCode
            };
        }

        public static FacilityViewModel GetViewModel(string returnUrl, int facilityID, string name, string facilityCode, bool isSiteAdmin, bool hasChildFacilities)
        {
            return new FacilityViewModel
            {
                ReturnUrl = returnUrl,
                FacilityID = facilityID,
                Name = name,
                FacilityCode = facilityCode,
                IsSiteAdmin = isSiteAdmin,
                HasChildFacilities = hasChildFacilities
            };
        }
    }
}