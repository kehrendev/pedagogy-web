﻿namespace Pedagogy.Models.ClassMembers
{
    public class FacilityClassViewModel
    {
        public int FacilityClassID { get; set; }
        public int ClassID { get; set; }
        public int FacilityID { get; set; }
        public string ClassName { get; set; }
        public string Status { get; set; }
        public bool Unassignable { get; set; }
        public bool LimitReached { get; set; }
        public int QuantityPurchased { get; set; }
        public int AmountUsed { get; set; }
        public int? FacilityMembershipID { get; set; }
        public string ExpDate { get; set; }
        public string InvoiceNumber { get; set; }
        public int AmountNotStarted { get; set; }
        public int AmountInProgress { get; set; }
        public int AmountCompleted { get; set; }
        public string StudentName { get; set; }
        public int StudentID { get; set; }
        public bool InPackage { get; set; }
    }
}