﻿namespace Pedagogy.Models.ClassMembers
{
    public class ContactTypeModel
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
        public string TypeCodeName { get; set; }

        public static ContactTypeModel SetModel(int typeID, string typeName, string typeCodeName)
        {
            return new ContactTypeModel
            {
                TypeID = typeID,
                TypeName = typeName,
                TypeCodeName = typeCodeName
            };
        }
    }
}