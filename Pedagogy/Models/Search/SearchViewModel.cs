﻿using CMS.Search;
using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.DataAccess.Dto.News;
using Pedagogy.DataAccess.Dto.Resources;
using System.Collections.Generic;

namespace Pedagogy.Models.Search
{
    public class SearchViewModel : IViewModel
    {
        public string Query { get; set; }
        public IEnumerable<SearchResultItem> Items { get; set; }
        public IEnumerable<CourseItemDto> CourseData { get; set; }
        public IEnumerable<ResourceItemDto> ResourceData { get; set; }
        public IEnumerable<NewsItemDto> NewsArticleData { get; set; }
    }
}