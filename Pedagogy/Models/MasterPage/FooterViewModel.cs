﻿using CMS.DocumentEngine;
using Pedagogy.DataAccess.Dto.Navigation;
using System.Collections.Generic;

namespace Pedagogy.Models.MasterPage
{
    public class FooterViewModel
    {
        public IEnumerable<NavigationItemDto> FooterNav { get; set; }
        public IEnumerable<SocialMediaItemDto> SocialLinks { get; set; }
    }
}