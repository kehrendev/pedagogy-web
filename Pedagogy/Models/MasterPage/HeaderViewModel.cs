﻿using CMS.DocumentEngine;
using Pedagogy.DataAccess.Dto.Navigation;
using System.Collections.Generic;

namespace Pedagogy.Models.MasterPage
{
    public class HeaderViewModel
    {
        public string CurrentUserFirstName { get; set; }
        public IEnumerable<NavigationItemDto> MainNav { get; set; }
        public IEnumerable<NavigationItemDto> AccountActionPages { get; set; }
        public IEnumerable<SocialMediaItemDto> SocialLinks { get; set; }
    }
}