﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.LoginManager
{
    public class LoginViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "The User name cannot be empty.")]
        [DisplayName("User name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "The Password cannot be empty.")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Remember Me")]
        public bool RememberMe { get; set; }

        public string CheckEmailMsg { get; set; }
    }
}