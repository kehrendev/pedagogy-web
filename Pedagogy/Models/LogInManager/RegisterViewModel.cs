﻿using Pedagogy.Models.ClassMembers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pedagogy.Models.LoginManager
{
    public class RegisterViewModel : BaseViewModel
    {
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "The Password cannot be empty.")]
        [DisplayName("Password")]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [RegularExpression(@"^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$", ErrorMessage = "The password doesn't meet the requirements.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Password Confirmation")]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The entered passwords do not match.")]
        public string PasswordConfirmation { get; set; }

        [Required(ErrorMessage = "The First name cannot be empty.")]
        [DisplayName("First Name")]
        [MaxLength(25, ErrorMessage = "The First name cannot be longer than 25 characters.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The Last name cannot be empty.")]
        [DisplayName("Last Name")]
        [MaxLength(25, ErrorMessage = "The Last name cannot be longer than 25 characters.")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "The Email address cannot be empty.")]
        [DisplayName("Email Address")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        [MaxLength(320, ErrorMessage = "The Email address cannot be longer than 320 characters.")]
        public string Email { get; set; }

        [DataType(DataType.EmailAddress)]
        [DisplayName("Confirm Email")]
        [Required(ErrorMessage = "The confirm email cannot be empty.")]
        [System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "The entered emails do not match.")]
        public string ConfirmEmail { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }

        public FacilityViewModel CorporateInfo { get; set; }

        public Pedagogy.ClassMembers.Contact ContactInfo { get; set; }

        [DisplayName("State")]
        public int StateID { get; set; }

        public SelectList States { get; set; }

        public string GoogleCaptchaToken { get; set; }

        [Required(ErrorMessage = "Please select Facility or Corporation")]
        public string RegistrationType { get; set; }

        [Display(Name = "Website URL")]
        public string WebsiteURL { get; set; }
        public bool NoApplicableCorporation { get; set; }

        [Display(Name = "Parent Corporation Name")]
        public string ParentCorporationName { get; set; }

        public bool DefaultPassword { get; set; }
    }
}