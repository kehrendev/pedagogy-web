﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.LoginManager
{
    public class RequestPasswordResetViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "The Email address cannot be empty.")]
        [DisplayName("Email")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        [MaxLength(200, ErrorMessage = "The Email cannot be longer than 200 characters.")]
        public string Email { get; set; }

        public string UserNotFoundErrMsg { get; set; }
        public string CheckEmailMsg { get; set; }
    }
}