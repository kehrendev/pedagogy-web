﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.LoginManager
{
    public class ResetPasswordViewModel : BaseViewModel
    {
        public int UserId { get; set; }
        public string Token { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "The Password cannot be empty.")]
        [DisplayName("Password")]
        [MaxLength(100, ErrorMessage = "The Password cannot be longer than 100 characters.")]
        [RegularExpression(@"^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$", ErrorMessage = "Password must include..<ul class='password-validation-err'><li>at least 8 characters</li><li>at least 1 numeric digit</li><li>at least 1 special character</li><li>1 uppercase letter</li><li>at least 1 lowercase letter</li></ul>")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Password confirmation")]
        [MaxLength(100, ErrorMessage = "The Password cannot be longer than 100 characters.")]
        [Compare("Password", ErrorMessage = "The entered passwords do not match.")]
        public string PasswordConfirmation { get; set; }
    }
}