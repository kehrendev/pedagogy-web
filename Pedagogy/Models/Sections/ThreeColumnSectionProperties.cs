﻿using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace Pedagogy.Models.Sections
{
    public class ThreeColumnSectionProperties : ISectionProperties
    {
        [EditingComponent(CheckBoxComponent.IDENTIFIER, DefaultValue = true, Label = "Orange background color?", Order = 0)]
        public bool OrangeBackground { get; set; }
    }
}