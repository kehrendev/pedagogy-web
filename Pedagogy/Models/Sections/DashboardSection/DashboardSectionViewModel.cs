﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Sections.DashboardSection
{
    public class DashboardSectionViewModel
    {
        public bool DisplayHeading { get; set; }
    }
}