﻿using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace Pedagogy.Models.Sections.DashboardSection
{
    public class DashboardSectionProperties : ISectionProperties
    {
        [EditingComponent(CheckBoxComponent.IDENTIFIER, DefaultValue = false, Order = 0, Label = "Display Heading?")]
        public bool DisplayHeading { get; set; }
    }
}