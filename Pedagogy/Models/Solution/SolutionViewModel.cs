﻿using Pedagogy.DataAccess.Dto.Solutions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Solution
{
    public class SolutionViewModel : IViewModel
    {
        public SolutionItemDto SolutionData { get; set; }
    }
}