﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Products
{
    public class ProductFilterCheckboxViewModel
    {
        public string Value { get; set; }

        public bool IsChecked { get; set; }

        public string DisplayName { get; set; }
    }
}