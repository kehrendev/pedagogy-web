﻿using CMS.Ecommerce;
using Pedagogy.DataAccess.Dto.Authors;
using Pedagogy.DataAccess.Dto.Courses;
using System;
using System.Collections.Generic;

namespace Pedagogy.Models.Products
{
    public class ProductViewModel : IViewModel
    {
        public CourseItemDto Course;
        public AuthorItemDto Author;
        public IEnumerable<ShoppingCartItemInfo> currentCart;
        public IEnumerable<CourseItemDto> RelatedCourses { get; set; }
        public readonly PriceDetailViewModel PriceDetail;
        public readonly string Name;
        public readonly string Description;
        public readonly string ShortDescription;
        public readonly int SKUID;
        public readonly string ImagePath;
        public readonly Guid ProductPageGuid;
        public readonly string ProductPageAlias;
        public readonly bool IsInStock;
        public bool IsFree;


        public ProductViewModel(SKUTreeNode productPage, ProductCatalogPrices priceDetail, CourseItemDto course, AuthorItemDto author)
        {
            //Used to get extra course info
            Course = course;
            Author = author;

            // Fills the page information   
            Name = productPage.DocumentName;
            Description = productPage.DocumentSKUDescription;
            ShortDescription = productPage.DocumentSKUShortDescription;
            ProductPageGuid = productPage.NodeGUID;
            ProductPageAlias = productPage.NodeAlias;

            // Fills the SKU information
            SKUInfo sku = productPage.SKU;
            SKUID = sku.SKUID;
            ImagePath = sku.SKUImagePath;
            IsInStock = sku.SKUTrackInventory == TrackInventoryTypeEnum.Disabled ||
                        sku.SKUAvailableItems > 0;

            PriceDetail = new PriceDetailViewModel()
            {
                Price = priceDetail.Price,
                ListPrice = priceDetail.ListPrice,
                CurrencyFormatString = priceDetail.Currency.CurrencyFormatString
            };

            if (priceDetail.Price == 0) IsFree = true;
            else IsFree = false;
        }
    }
}