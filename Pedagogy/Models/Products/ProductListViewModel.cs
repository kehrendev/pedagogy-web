﻿using Pedagogy.Models.Search;

using System.Collections.Generic;
using System.Web.Mvc;

namespace Pedagogy.Models.Products
{
    public class ProductListViewModel : IViewModel
    {
        public ProductFilterViewModel Filter { get; set; }
        public IEnumerable<ProductListItemViewModel> Items { get; set; }
        public SearchViewModel SearchResults { get; set; }
        public ProductExtraFiltersViewModel ExtraFilters { get; set; }
        public PagingInfo<ProductListItemViewModel> PagingInfo { get; set; }

        public bool IsFacilityPurchaser { get; set; }
        public int FacilityID { get; set; }
        public SelectList Facilities { get; set; }
    }
}