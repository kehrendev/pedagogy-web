﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Products
{
    public class SortByFilterViewModel
    {
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
    }
}