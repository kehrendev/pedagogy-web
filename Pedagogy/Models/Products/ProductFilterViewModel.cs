﻿using CMS.SiteProvider;
using CMS.Taxonomy;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Pedagogy.Models.Products
{
    public class ProductFilterViewModel
    {
        [UIHint("CourseProductFilter")]
        public ProductFilterCheckboxViewModel[] Categories { get; set; }

        public void Load()
        {
            Categories = GetCategories()
                .Select(GetFilterCheckboxViewModel)
                .ToArray();
        }

        public List<string> GetSelectedCategories()
        {
            return Categories
                .Where(x => x.IsChecked)
                .Select(x => x.Value)
                .ToList()
                ?? new List<string>();
        }

        private IEnumerable<string> GetCategories()
        {
            return CategoryInfoProvider.GetCategories()
                .OnSite(SiteContext.CurrentSiteName)
                .OrderBy(x => x.CategoryOrder)
                .Where(x => x.CategoryName.StartsWith("coursecategory."))
                .Select(x => x.CategoryDisplayName);
            
        }

        private static ProductFilterCheckboxViewModel GetFilterCheckboxViewModel(string category)
        {
            var value = CategoryInfoProvider.GetCategories()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereEquals(nameof(CategoryInfo.CategoryDisplayName), category)
                .Where(x => x.CategoryName.StartsWith("coursecategory."))
                .Select(c => c.CategoryName)
                .FirstOrDefault();

            return new ProductFilterCheckboxViewModel
            {
                DisplayName = category,
                Value = value
            };
        }
    }
}