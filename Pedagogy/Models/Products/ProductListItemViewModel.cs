﻿using System;
using CMS.Ecommerce;
using CMS.Membership;
using Pedagogy.DataAccess.Dto.Courses;

namespace Pedagogy.Models.Products
{
    public class ProductListItemViewModel : IViewModel
    {
        public CourseItemDto Course;
        public readonly PriceDetailViewModel PriceModel;
        public string Name;
        public string ImagePath;
        public string PublicStatusName;
        public bool Available;
        public Guid ProductPageGuid;
        public string ProductPageAlias;
        public int SKUID;
        public bool IsFree;
        public bool UserLoggedIn;


        public ProductListItemViewModel(SKUTreeNode productPage, ProductCatalogPrices priceDetail, string publicStatusName)
        {
            //Extra Course Info
            //Course = course;

            // Set page information
            Name = productPage.DocumentName;
            ProductPageGuid = productPage.NodeGUID;
            ProductPageAlias = productPage.NodeAlias;

            // Set SKU information
            SKUID = productPage.NodeSKUID;
            ImagePath = productPage.SKU.SKUImagePath;
            Available = !productPage.SKU.SKUSellOnlyAvailable || productPage.SKU.SKUAvailableItems > 0;
            PublicStatusName = publicStatusName;

            // Set additional info
            PriceModel = new PriceDetailViewModel
            {
                Price = priceDetail.Price,
                ListPrice = priceDetail.ListPrice,
                CurrencyFormatString = priceDetail.Currency.CurrencyFormatString
            };

            if(priceDetail.Price == 0) IsFree = true;
            else IsFree = false;
        }
    }
}