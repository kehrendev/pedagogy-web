﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Products
{
    public class ContactHourFilterViewModel
    {
        public string StartNum { get; set; }
        public string EndNum { get; set; }
    }
}