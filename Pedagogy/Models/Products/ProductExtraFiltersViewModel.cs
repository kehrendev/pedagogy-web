﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Pedagogy.Models.Products
{
    public class ProductExtraFiltersViewModel : IViewModel
    {
        public string StateCategory { get; set; }
        public SelectList StateCategories { get; set; }
        public string ContactHourData { get; set; }
        public List<SelectListItem> ContactHoursList { get; set; }
        public List<SelectListItem> SortByList { get; set; }
        public string SortByData { get; set; }
    }
}