﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Products
{
    public class StateFilterViewModel
    {
        public string StateName { get; set; }
    }
}