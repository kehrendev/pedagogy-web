﻿using Pedagogy.DataAccess.Dto;
using Pedagogy.DataAccess.Dto.InfoBox;
using Pedagogy.DataAccess.Dto.Navigation;
using System.Collections.Generic;

namespace Pedagogy.Models
{
    public class BaseViewModel : IViewModel
    {
        public BaseDto BaseNodeData { get; set; }
        public IEnumerable<InfoBoxItemDto> InfoBoxItem { get; set; }
    }

    public class BaseViewModel<TViewModel> : BaseViewModel where TViewModel : IViewModel
    {
        public TViewModel ViewModel { get; set; }
    }
}