﻿using Pedagogy.DataAccess.Dto.pedagogyPage;

namespace Pedagogy.Models.PedagogyPage
{
    public class PedagogyPageViewModel : BaseViewModel
    {
        public PedagogyPageDto PageData { get; set; }
    }
}