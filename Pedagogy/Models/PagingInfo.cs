﻿using Pedagogy.Models.Products;
using Pedagogy.Models.Resource;
using System;
using System.Collections.Generic;

namespace Pedagogy.Models
{
    public class PagingInfo<T> where T : class
    {
        public int TotalItems { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public string SearchQuery { get; set; }
        public ResourceFilterViewModel Filter { get; set; }
        public ProductFilterViewModel ProductFilter { get; set; }
        public ProductExtraFiltersViewModel ExtraFilters { get; set; }

        public int TotalPages =>
            (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);

        public IEnumerable<T> Data { get; set; }
    }
}