﻿using Pedagogy.DataAccess.Dto.Authors;
using Pedagogy.DataAccess.Dto.Courses;
using Pedagogy.DataAccess.Dto.Navigation;
using System.Collections.Generic;

namespace Pedagogy.Models.Author
{
    public class AuthorViewModel : IViewModel
    {
        public AuthorItemDto AuthorData { get; set; }
        public IEnumerable<SocialMediaItemDto> SocialLinks { get; set; }
        public IEnumerable<CourseItemDto> AuthoredCourses { get; set; }
    }
}