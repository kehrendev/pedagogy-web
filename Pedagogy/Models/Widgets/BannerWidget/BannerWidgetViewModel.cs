﻿using CMS.DocumentEngine;
using Pedagogy.Lib.DataModels;
using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.BannerWidget
{
    public class BannerWidgetViewModel
    {
        public TreeNode BannerItem { get; set; }
        public IEnumerable<Breadcrumb> Breadcrumbs { get; set; }
    }
}