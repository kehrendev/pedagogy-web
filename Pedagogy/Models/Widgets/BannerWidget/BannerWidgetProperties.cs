﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.BannerWidget
{
    public class BannerWidgetProperties : IWidgetProperties
    {
        [EditingComponent(PageSelector.IDENTIFIER, Label = "Banner Item", Order = 0)]
        public IList<PageSelectorItem> Banner { get; set; }
    }
}