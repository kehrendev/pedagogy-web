﻿using CMS.Globalization;
using CMS.Membership;
using CMS.SiteProvider;

using Pedagogy.Models.ClassMembers;
using Pedagogy.ClassMembers.Entities.ContactType;

using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.ManageCorporationsWidget
{
    public class ManageCorporationsWidgetViewModel
    {
        public bool IsSiteAdmin { get; set; }
        public bool IsCorpAdmin { get; set; }
        public int CurrentUserID { get; set; }
        public int CurrentCorpID { get; set; }
        public FacilityViewModel CorporationData { get; set; }
        public ContactViewModel ContactModel { get; set; }
        public SelectList States { get; set; }
        public string ReturnUrl { get; set; }
        public string ErrorMsg { get; set; }
        public string CardHeading { get; set; }

        public static ManageCorporationsWidgetViewModel GetViewModel(string userName, string returnUrl, string cardHeading, int corpID, List<ContactTypeDto> contactTypes)
        {
            var usr = UserInfoProvider.GetUserInfo(userName);

            return new ManageCorporationsWidgetViewModel
            {
                IsSiteAdmin = usr.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName),
                IsCorpAdmin = usr.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName),
                CurrentUserID = usr.UserID,
                CurrentCorpID = corpID,
                CorporationData = new FacilityViewModel(),
                ContactModel = new ContactViewModel()
                {
                    ContactTypes = new SelectList(contactTypes, nameof(ContactTypeDto.ContactTypeID), nameof(ContactTypeDto.ContactTypeDisplayName))
                },
                States = new SelectList(StateInfoProvider.GetStates().Select(x => new { x.StateID, x.StateDisplayName }), nameof(StateInfo.StateID).ToString(), nameof(StateInfo.StateDisplayName)),
                ReturnUrl = returnUrl,
                CardHeading = cardHeading
            };
        }
    }
}