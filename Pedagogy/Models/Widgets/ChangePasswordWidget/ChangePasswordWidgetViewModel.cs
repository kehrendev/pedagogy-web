﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.Widgets.ChangePasswordWidget
{
    public class ChangePasswordWidgetViewModel
    {
        [Required(ErrorMessage = "Your current password is required")]
        [DataType(DataType.Password)]
        [DisplayName("Current Password:")]
        public string CurrentPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "The Password cannot be empty.")]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [DisplayName("New Password:")]
        [RegularExpression(@"^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$", ErrorMessage = "The password doesn't meet the requirements.")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Please confirm your new password")]
        [DataType(DataType.Password)]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [Compare(nameof(NewPassword), ErrorMessage = "The entered passwords do not match.")]
        [DisplayName("Confirm New Password:")]
        public string ConfirmPassword { get; set; }

        public string CardHeading { get; set; }

        public static ChangePasswordWidgetViewModel GetViewModel(string cardHeading)
        {
            return new ChangePasswordWidgetViewModel
            {
                CardHeading = cardHeading
            };
        }
    }
}