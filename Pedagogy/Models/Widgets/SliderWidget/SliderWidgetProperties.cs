﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.SliderWidget
{
    public class SliderWidgetProperties : IWidgetProperties
    {
        [EditingComponent(DropDownComponent.IDENTIFIER, Label = "Slider Type", Order = 0, DefaultValue = "top")]
        [EditingComponentProperty(nameof(DropDownProperties.DataSource), "regular;Regular\r\ntestimonial;Testimonial\r\ncard;Card\r\nimage-slideshow;Image Slide Show")]
        public string SliderType { get; set; }

        [EditingComponent(PathSelector.IDENTIFIER, 
            ExplanationText = "Select the folder containing the slides you would like displayed.", 
            Label = "Slider Items", 
            Order = 1)]
        public IList<PathSelectorItem> SliderItem { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, 
            ExplanationText = "**Used only for when you want to display a card slider** Displays a heading above the cards.", 
            Label = "Card Group Heading", 
            Order = 2)]
        public string CardGroupHeading { get; set; }
    }
}