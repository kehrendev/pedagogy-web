﻿using CMS.DocumentEngine.Types.Pedagogy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Widgets.SliderWidget
{
    public class SliderWidgetViewModel
    {
        public IEnumerable<Slide> Slides { get; set; }
        public string SliderType { get; set; }
        public string CardGroupHeading { get; set; }
    }
}