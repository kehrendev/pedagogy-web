﻿using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.ManageStudentClassesWidget
{
    public class ManageStudentClassesWidgetViewModel
    {
        public int FacilityID { get; set; }
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public IEnumerable<FacilityClassViewModel> FacilityClasses { get; set; }
        public IEnumerable<FacilityClassViewModel> FacilityMembershipClasses { get; set; }
        public string CardHeading { get; set; }

        public static ManageStudentClassesWidgetViewModel GetViewModel(string cardHeading)
        {
            return new ManageStudentClassesWidgetViewModel
            {
                FacilityClasses = new List<FacilityClassViewModel>(),
                FacilityMembershipClasses = new List<FacilityClassViewModel>(),
                CardHeading = cardHeading
            };
        }

        public static ManageStudentClassesWidgetViewModel GetViewModel(int facilityID, int studentID, string cardHeading, string studentName,
            IEnumerable<FacilityClassViewModel> facilityClasses, IEnumerable<FacilityClassViewModel> facilityMemClasses)
        {
            return new ManageStudentClassesWidgetViewModel
            {
                FacilityClasses = facilityClasses,
                FacilityMembershipClasses = facilityMemClasses,
                CardHeading = cardHeading,
                FacilityID = facilityID,
                StudentID = studentID,
                StudentName = studentName
            };
        }
    }
}