﻿using CMS.Globalization;

using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.LicensingInfoWidget;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.UpdateStaffInfoWidget
{
    public class UpdateStaffInfoWidgetViewModel
    {
        public ContactViewModel ContactModel { get; set; }
        public FacilityViewModel FacilityData { get; set; }
        public LicensingInfoWidgetViewModel LicensingData { get; set; }
        public string ReturnUrl { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "The Password cannot be empty.")]
        [DisplayName("New Password:")]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [RegularExpression(@"^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$", ErrorMessage = "The password doesn't meet the requirements.")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [MaxLength(50, ErrorMessage = "The Password cannot be longer than 50 characters.")]
        [System.ComponentModel.DataAnnotations.Compare(nameof(NewPassword), ErrorMessage = "The entered passwords do not match.")]
        [DisplayName("Confirm New Password:")]
        public string ConfirmPassword { get; set; }

        [DisplayName("Facility")]
        [Required(ErrorMessage = "Please select a facility.")]
        public int FacilityID { get; set; }
        public SelectList Facilities { get; set; }

        public string CardHeading { get; set; }

        public static UpdateStaffInfoWidgetViewModel GetViewModel(string cardHeading)
        {
            return new UpdateStaffInfoWidgetViewModel
            {
                CardHeading = cardHeading,
                FacilityData = new FacilityViewModel(),
                LicensingData = new LicensingInfoWidgetViewModel
                {
                    CurrentLicenses = new List<StudentLicense>(),
                    NotApplicable = true,
                    LicenseTypes = new SelectList(new List<LicenseType>(), nameof(LicenseType.LicenseTypeID), nameof(LicenseType.Name)),
                    States = new SelectList(StateInfoProvider.GetStates(), nameof(StateInfo.StateID), nameof(StateInfo.StateDisplayName))
                },
                ContactModel = new ContactViewModel(),
                FacilityID = 0,
                Facilities = new SelectList(new List<FacilityViewModel>(), nameof(Facility.FacilityID), nameof(Facility.Name))
            };
        }

        public static UpdateStaffInfoWidgetViewModel GetViewModel(string cardHeading, Facility facility, ClassMembers.Contact student, List<FacilityViewModel> facilities,
            string returnUrl, LicensingInfoWidgetViewModel licensingInfo)
        {
            return new UpdateStaffInfoWidgetViewModel
            {
                CardHeading = cardHeading,
                FacilityData = new FacilityViewModel(facility),
                ContactModel = new ContactViewModel(student)
                {
                    FacilityID = facility.FacilityID
                },
                Facilities = new SelectList(facilities, nameof(Facility.FacilityID), nameof(Facility.Name)),
                FacilityID = facility.FacilityID,
                ReturnUrl = returnUrl,
                LicensingData = licensingInfo
            };
        }
    }
}