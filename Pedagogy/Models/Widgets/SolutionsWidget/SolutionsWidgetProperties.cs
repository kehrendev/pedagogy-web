﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.SolutionsWidget
{
    public class SolutionsWidgetProperties : IWidgetProperties
    {
        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Title", Order = 1)]
        public string SolutionTitle { get; set; }

        [EditingComponent(TextAreaComponent.IDENTIFIER, Label = "Text", Order = 2)]
        public string SolutionText { get; set; }
    }
}