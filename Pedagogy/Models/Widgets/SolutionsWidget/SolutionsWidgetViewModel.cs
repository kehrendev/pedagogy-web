﻿using CMS.DocumentEngine;
using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.SolutionsWidget
{
    public class SolutionsWidgetViewModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public IEnumerable<TreeNode> Solutions { get; set; }
    }
}