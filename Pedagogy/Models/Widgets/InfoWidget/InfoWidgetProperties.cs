﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.InfoWidget
{
    public class InfoWidgetProperties : IWidgetProperties
    {
        [EditingComponent(DropDownComponent.IDENTIFIER, Label = "Icon", Order = 0, DefaultValue = "None")]
        [EditingComponentProperty(nameof(DropDownProperties.DataSource), "\"\";None\r\nflaticon-electrocardiogram;Electrocardiogram\r\nflaticon-emergency-call;Phone\r\nflaticon-first-aid-kit;First Aid Kit")]
        public string Icon { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Title", Order = 1)]
        public string Title { get; set; }

        [EditingComponent(TextAreaComponent.IDENTIFIER, Label = "Text", Order = 2)]
        public string Text { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Button Text", Order = 3)]
        public string BtnText { get; set; }
 
        [EditingComponent(PageSelector.IDENTIFIER, Label = "Button URL", Order = 4)]
        public IList<PageSelectorItem> BtnUrl { get; set; }
    }
}