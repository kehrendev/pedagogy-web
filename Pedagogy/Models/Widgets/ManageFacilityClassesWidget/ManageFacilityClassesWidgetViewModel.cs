﻿using CMS.Membership;
using CMS.SiteProvider;

using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.ManageFacilityClassesWidget
{
    public class ManageFacilityClassesWidgetViewModel
    {
        public IEnumerable<FacilityClassViewModel> FacilityClasses { get; set; }
        public IEnumerable<FacilityClassViewModel> FacilityMembershipClasses { get; set; }
        public string FacilityName { get; set; }
        public int FacilityID { get; set; }
        public bool IsSiteAdmin { get; set; }
        public string CardHeading { get; set; }

        public static ManageFacilityClassesWidgetViewModel GetViewModel(string cardHeading)
        {
            return new ManageFacilityClassesWidgetViewModel
            {
                FacilityClasses = new List<FacilityClassViewModel>(),
                FacilityMembershipClasses = new List<FacilityClassViewModel>(),
                CardHeading = cardHeading
            };
        }

        public static ManageFacilityClassesWidgetViewModel GetViewModel(string facilityName, int facilityID, string cardHeading, string userName,
            IEnumerable<FacilityClassViewModel> facilityClasses, IEnumerable<FacilityClassViewModel> facilityMemClasses)
        {
            return new ManageFacilityClassesWidgetViewModel
            {
                FacilityName = facilityName,
                FacilityID = facilityID,
                CardHeading = cardHeading,
                IsSiteAdmin = UserInfoProvider.IsUserInRole(userName, ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName),
                FacilityClasses = facilityClasses,
                FacilityMembershipClasses = facilityMemClasses
            };
        }
    }
}