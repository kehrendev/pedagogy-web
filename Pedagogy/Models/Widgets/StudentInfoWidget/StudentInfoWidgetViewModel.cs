﻿using Pedagogy.ClassMembers.Entities.Contact;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.Widgets.StudentInfoWidget
{
    public class StudentInfoWidgetViewModel
    {
        [DisplayName("First Name")]
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        public int ContactID { get; set; }
        public string NonStudentErrMsg { get; set; }
        public string CardHeading { get; set; }

        public static StudentInfoWidgetViewModel GetViewModel(string cardHeading, ContactDto contact)
        {
            return new StudentInfoWidgetViewModel
            {
                CardHeading = cardHeading,
                FirstName = contact.ContactFirstName,
                LastName = contact.ContactLastName,
                ContactID = contact.ContactID
            };
        }

        public static StudentInfoWidgetViewModel GetViewModel(string cardHeading, string errMsg)
        {
            return new StudentInfoWidgetViewModel
            {
                CardHeading = cardHeading,
                NonStudentErrMsg = errMsg
            };
        }
    }
}