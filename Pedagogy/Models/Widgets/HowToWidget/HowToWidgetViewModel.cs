﻿using Pedagogy.DataAccess.Dto.HowToFiles;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.HowToWidget
{
    public class HowToWidgetViewModel
    {
        public IEnumerable<HowToItemDto> HowToItems { get; set; }
        public IEnumerable<string> HowToCategory { get; set; }
        public string CardHeading { get; set; }

        public static HowToWidgetViewModel GetViewModel(IEnumerable<HowToItemDto> howToItems, IEnumerable<string> howToCategory, string cardHeading)
        {
            return new HowToWidgetViewModel
            {
                HowToItems = howToItems,
                HowToCategory = howToCategory,
                CardHeading = cardHeading
            };
        }
    }
}