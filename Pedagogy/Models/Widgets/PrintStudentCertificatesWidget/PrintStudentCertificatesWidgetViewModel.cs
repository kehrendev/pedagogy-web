﻿using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.PrintStudentCertificatesWidget
{
    public class PrintStudentCertificatesWidgetViewModel
    {
        public int FacilityID { get; set; }
        public Dictionary<int, string> StudentData { get; set; }
        public string CardHeading { get; set; }

        public static PrintStudentCertificatesWidgetViewModel GetViewModel(string cardHeading, int facilityID, Dictionary<int, string> studentData)
        {
            return new PrintStudentCertificatesWidgetViewModel
            {
                CardHeading = cardHeading,
                FacilityID = facilityID,
                StudentData = studentData
            };
        }
    }
}