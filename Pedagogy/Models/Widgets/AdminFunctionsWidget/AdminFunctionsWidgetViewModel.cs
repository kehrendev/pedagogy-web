﻿using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;

using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.Models.Widgets.AdminFunctionsWidget
{
    public class AdminFunctionsWidgetViewModel
    {
        public IEnumerable<TreeNode> AdminFunctionPages { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsSiteAdmin { get; set; }
        public IEnumerable<UserInfo> Users { get; set; }
        public UserFilterViewModel FilterModel { get; set; }
        public bool Impersonating { get; set; }
        public bool IsFacilityPurchaser { get; set; }
        public string CardHeading { get; set; }

        public static AdminFunctionsWidgetViewModel GetViewModel(IEnumerable<TreeNode> adminFunctionPages, UserInfo usr, string cardHeading)
        {
            return new AdminFunctionsWidgetViewModel
            {
                AdminFunctionPages= adminFunctionPages,
                IsAdmin = usr.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName) || usr.IsInRole(ContentIdentifiers.FACILITY_ADMIN_ROLE, SiteContext.CurrentSiteName),
                IsSiteAdmin = usr != null && usr.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName) || CookieHelper.GetExistingCookie(ContentIdentifiers.CURRENT_USERNAME_COOKIE) != null,
                Users = UserInfoProvider.GetUsers().Where(x => x.Enabled).ToList(),
                FilterModel = new UserFilterViewModel(),
                Impersonating = CookieHelper.GetExistingCookie(ContentIdentifiers.CURRENT_USERNAME_COOKIE) != null,
                IsFacilityPurchaser = usr.IsInRole(ContentIdentifiers.FACILITY_PURCHASER_ROLE, SiteContext.CurrentSiteName),
                CardHeading = cardHeading
            };
        }
    }
}