﻿using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.ManageStaffWidget
{
    public class ManageStaffWidgetViewModel
    {
        public FacilityViewModel FacilityData { get; set; }
        public ContactViewModel ContactModel { get; set; }
        public List<ContactViewModel> Contacts { get; set; }
        public string ReturnUrl { get; set; }
        public string CardHeading { get; set; }

        public static ManageStaffWidgetViewModel GetViewModel(string cardHeading)
        {
            return new ManageStaffWidgetViewModel
            {
                ContactModel = new ContactViewModel(),
                Contacts = new List<ContactViewModel>()
                {
                    new ContactViewModel()
                },
                FacilityData = new FacilityViewModel
                {
                    Students = new List<ContactViewModel>()
                },
                CardHeading = cardHeading
            };
        }

        public static ManageStaffWidgetViewModel GetViewModel(string cardHeading, FacilityViewModel facilityData, string returnUrl)
        {
            return new ManageStaffWidgetViewModel
            {
                ContactModel = new ContactViewModel(),
                Contacts = new List<ContactViewModel>()
                {
                    new ContactViewModel()
                },
                FacilityData = facilityData,
                CardHeading = cardHeading,
                ReturnUrl = returnUrl
            };
        }
    }
}