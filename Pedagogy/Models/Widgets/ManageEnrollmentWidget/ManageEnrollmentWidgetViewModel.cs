﻿using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.ManageEnrollmentWidget
{
    public class ManageEnrollmentWidgetViewModel
    {
        public string ClassName { get; set; }
        public int FacilityID { get; set; }
        public IEnumerable<FacilityClassViewModel> ClassData { get; set; }
        public bool LimitReached { get; set; }
        public int AmountUnused { get; set; }
        public string CardHeading { get; set; }

        public static ManageEnrollmentWidgetViewModel GetViewModel(int facilityID, string className, bool limitReached, IEnumerable<FacilityClassViewModel> classData,
            int amountUnused, string cardHeading)
        {
            return new ManageEnrollmentWidgetViewModel
            {
                FacilityID = facilityID,
                ClassName = className,
                LimitReached = limitReached,
                ClassData = classData,
                AmountUnused = amountUnused,
                CardHeading = cardHeading
            };
        }
    }
}