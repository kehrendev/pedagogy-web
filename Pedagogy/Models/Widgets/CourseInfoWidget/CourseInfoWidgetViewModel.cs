﻿using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.CourseInfoWidget
{
    public class CourseInfoWidgetViewModel
    {
        public bool IsSiteAdmin { get; set; }
        public bool HasLicenses { get; set; }
        public bool IsDefaultPass { get; set; }
        public IEnumerable<StudentClassViewModel> CurrentCourses { get; set; }
        public string CardHeading { get; set; }

        public static CourseInfoWidgetViewModel GetViewModel(bool isSiteAdmin, IEnumerable<StudentClassViewModel> currentCourses, string cardHeading)
        {
            return new CourseInfoWidgetViewModel
            {
                IsSiteAdmin = isSiteAdmin,
                CurrentCourses = currentCourses,
                CardHeading = cardHeading
            };
        }
    }
}