﻿using CMS.Globalization;
using CMS.Membership;
using CMS.SiteProvider;

using Pedagogy.ClassMembers.Entities.ContactType;
using Pedagogy.ClassMembers.Entities.LicenseTypes;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.LoginManager;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.ManageFacilitiesWidget
{
    public class ManageFacilitiesWidgetViewModel
    {
        public string CardHeading { get; set; }
        public string ReturnUrl { get; set; }

        public bool IsSiteAdmin { get; set; }
        public bool IsCorporateAdmin { get; set; }
        public int CurrentUserID { get; set; }

        public int ParentFacilityID { get; set; }
        public SelectList ParentFacilities { get; set; }
        public SelectList States { get; set; }

        [Display(Name = "Facility")]
        public int FacilityID { get; set; }
        public SelectList Facilities { get; set; }
        public int StudentLicenseID { get; set; }
        public SelectList StudentLicenses { get; set; }

        public FacilityViewModel FacilityData { get; set; }
        public ContactViewModel ContactModel { get; set; }
        public RegisterViewModel RegisterModel { get; set; }
        public StudentLicenseViewModel LicensingData { get; set; }
        public List<ContactViewModel> ContactModels { get; set; }

        public static ManageFacilitiesWidgetViewModel GetViewModel(string userName, int parentFacilityID, string returnUrl, string cardHeading, 
            IEnumerable<FacilityViewModel> parentCorps, IEnumerable<FacilityViewModel> facilities, List<LicenseTypeDto> licenseTypes, List<ContactTypeDto> contactTypes)
        {
            var user = UserInfoProvider.GetUserInfo(userName);

            return new ManageFacilitiesWidgetViewModel
            {
                IsSiteAdmin = user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName),
                IsCorporateAdmin = user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName),
                CurrentUserID = user.UserID,
                ParentFacilityID = parentFacilityID,
                ReturnUrl = returnUrl,
                States = new SelectList(StateInfoProvider.GetStates().Select(x => new { x.StateID, x.StateDisplayName }), nameof(StateInfo.StateID).ToString(), nameof(StateInfo.StateDisplayName)),
                FacilityData = new FacilityViewModel(),
                ContactModel = new ContactViewModel()
                {
                    ContactTypes = new SelectList(contactTypes, nameof(ContactTypeDto.ContactTypeID), nameof(ContactTypeDto.ContactTypeDisplayName))
                },
                CardHeading = cardHeading,
                ParentFacilities = new SelectList(parentCorps.Select(x => new { x.FacilityID, x.Name}), nameof(Pedagogy.ClassMembers.Facility.FacilityID), nameof(Pedagogy.ClassMembers.Facility.Name)),
                Facilities = new SelectList(facilities.Select(x => new { x.FacilityID, x.Name }), nameof(Pedagogy.ClassMembers.Facility.FacilityID), nameof(Pedagogy.ClassMembers.Facility.Name)),
                LicensingData = new StudentLicenseViewModel()
                {
                    LicenseTypes = new SelectList(licenseTypes, nameof(LicenseTypeDto.LicenseTypeID), nameof(LicenseTypeDto.LicenseDisplayName))
                }
            };
        }
    }
}