﻿using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.ManageEducationPlansWidget
{
    public class ManageEducationPlansWidgetViewModel
    {
        public EducationPlanViewModel EducationPlanData { get; set; }
        public IEnumerable<EducationPlanViewModel> EducationPlans { get; set; }
        public Dictionary<string, string> AvailableClasses { get; set; }
        public string CardHeading { get; set; }

        public static ManageEducationPlansWidgetViewModel GetViewModel(IEnumerable<SelectListItem> facilities, Dictionary<string, string> availClasses, string cardHeading)
        {
            return new ManageEducationPlansWidgetViewModel
            {
                EducationPlanData = new EducationPlanViewModel
                {
                    Facilities = facilities
                },
                EducationPlans = new List<EducationPlanViewModel>(),
                AvailableClasses = availClasses,
                CardHeading = cardHeading
            };
        }
    }
}