﻿using Pedagogy.Models.ClassMembers;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.ArchivedCoursesWidget
{
    public class ArchivedCoursesWidgetViewModel
    {
        public IEnumerable<StudentClassViewModel> ArchivedClasses { get; set; }
        public string CardHeading { get; set; }

        public static ArchivedCoursesWidgetViewModel GetViewModel(IEnumerable<StudentClassViewModel> archivedCourses, string cardHeading)
        {
            return new ArchivedCoursesWidgetViewModel
            {
                ArchivedClasses = archivedCourses,
                CardHeading = cardHeading
            };
        }
    }
}