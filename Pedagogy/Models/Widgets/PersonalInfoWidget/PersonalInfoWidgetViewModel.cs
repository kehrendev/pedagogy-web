﻿using CMS.Ecommerce;
using CMS.Globalization;
using CMS.Membership;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.PersonalInfoWidget
{
    public class PersonalInfoWidgetViewModel
    {
        public int CustomerId { get; set; }

        public int UserID { get; set; }

        public string UserName { get; set; }

        [Required(ErrorMessage = "Address line 1 is required")]
        [DisplayName("Address line 1")]
        [MaxLength(100, ErrorMessage = "The maximum length allowed for the field has been exceeded.")]
        public string Line1 { get; set; }

        [DisplayName("Address line 2")]
        [MaxLength(100, ErrorMessage = "The maximum length allowed for the field has been exceeded.")]
        public string Line2 { get; set; }

        [Required(ErrorMessage = "City is required")]
        [MaxLength(100, ErrorMessage = "The maximum length allowed for the field has been exceeded.")]
        public string City { get; set; }

        [Required(ErrorMessage = "Postal code is required")]
        [DisplayName("Postal code")]
        [MaxLength(20, ErrorMessage = "The maximum length allowed for the field has been exceeded.")]
        [DataType(DataType.PostalCode, ErrorMessage = "Please enter a valid postal code.")]
        public string PostalCode { get; set; }

        [DisplayName("Country")]
        public int CountryID { get; set; }

        [DisplayName("State")]
        public int StateID { get; set; }

        public int AddressID { get; set; }

        public SelectList Countries { get; set; }

        public IEnumerable<AddressInfo> Addresses { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [DisplayName("First Name")]
        [MaxLength(100, ErrorMessage = "The maximum length allowed for the field has been exceeded.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [DisplayName("Last Name")]
        [MaxLength(100, ErrorMessage = "The maximum length allowed for the field has been exceeded.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [DisplayName("Phone Number")]
        [MaxLength(20, ErrorMessage = "The maximum length allowed for the field has been exceeded.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Please enter a valid phone number.")]
        public string PhoneNumber { get; set; }

        public string ReturnUrl { get; set; }

        public string CardHeading { get; set; }

        public static PersonalInfoWidgetViewModel GetViewModel(string cardHeading, CustomerInfo customer, UserInfo user, string returnUrl)
        {
            return new PersonalInfoWidgetViewModel
            {
                CardHeading = cardHeading,
                CustomerId = customer.CustomerID,
                FirstName = customer.CustomerFirstName,
                LastName = customer.CustomerLastName,
                Email = customer.CustomerEmail,
                PhoneNumber = customer.CustomerPhone,
                UserName = user.UserName,
                UserID = user.UserID,
                Countries = new SelectList(CountryInfoProvider.GetCountries(), nameof(CountryInfo.CountryID), nameof(CountryInfo.CountryDisplayName)),
                Addresses = customer != null ? AddressInfoProvider.GetAddresses(customer.CustomerID).ToList() : new List<AddressInfo>(),
                ReturnUrl = returnUrl
            };
        }
    }
}