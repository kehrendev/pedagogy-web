﻿using CMS.Globalization;

using Pedagogy.ClassMembers.Entities.LicenseTypes;
using Pedagogy.ClassMembers.Entities.StudentLicense;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.LicensingInfoWidget
{
    public class LicensingInfoWidgetViewModel
    {
        [Required(ErrorMessage = "License type is required")]
        [DisplayName("License Type")]
        public int LicenseTypeID { get; set; }
        public SelectList LicenseTypes { get; set; }

        [Required(ErrorMessage = "License number is required")]
        [DisplayName("License Number")]
        public string LicenseNum { get; set; }

        [Required(ErrorMessage = "State is required")]
        [DisplayName("State")]
        public int StateID { get; set; }
        public SelectList States { get; set; }

        public List<StudentLicenseDto> CurrentLicenses { get; set; }
        public int StudentID { get; set; }
        public string ReturnUrl { get; set; }
        public string NonStudentErrMsg { get; set; }
        public int StudentLicenseID { get; set; }
        public bool NotApplicable { get; set; }
        public string CardHeading { get; set; }

        public static LicensingInfoWidgetViewModel GetViewModel(bool notApplicable, int studentID, List<LicenseTypeDto> licenseTypes, 
            List<StudentLicenseDto> currentLicenses, string returnURL, string cardHeading)
        {
            return new LicensingInfoWidgetViewModel
            {
                NotApplicable = notApplicable,
                StudentID = studentID,
                LicenseTypes = new SelectList(licenseTypes, nameof(LicenseTypeDto.LicenseTypeID), nameof(LicenseTypeDto.LicenseDisplayName)),
                States = new SelectList(StateInfoProvider.GetStates(), nameof(StateInfo.StateID), nameof(StateInfo.StateDisplayName)),
                CurrentLicenses = currentLicenses,
                ReturnUrl = returnURL,
                CardHeading = cardHeading
            };
        }

        public static LicensingInfoWidgetViewModel GetViewModel(string nonStudentErrMsg)
        {
            return new LicensingInfoWidgetViewModel
            {
                NonStudentErrMsg = nonStudentErrMsg
            };
        }
    }
}