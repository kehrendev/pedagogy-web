﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedagogy.Models.Widgets.AdWidget
{
    public class AdWidgetProperties : IWidgetProperties
    {
        [EditingComponent(MediaFilesSelector.IDENTIFIER, Order = 0, Label = "Ad Image")]
        public IEnumerable<MediaFilesSelectorItem> AdImage { get; set; }

        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Url", Order = 1)]
        public string AdUrl { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Label = "Open In New Tab?", Order = 2, DefaultValue = false)]
        public bool NewTab { get; set; }
    }
}