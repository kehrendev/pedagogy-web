﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.PedagogyClassWidgetsWidget
{
    public class PedagogyClassWidgetsWidgetViewModel
    {
        public string WidgetType { get; set; }
        public IEnumerable<SelectListItem> WidgetTypes { get; set; }

        public string AffiliateCode { get; set; }

        public string ColorStyle { get; set; }
    }
}