﻿using Pedagogy.Models.Checkout;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Pedagogy.Models.Widgets.PayInvoiceWidget
{
    public class PayInvoiceWidgetViewModel
    {
        public string CardHeading { get; set; } = "";

        //[Required(ErrorMessage = "Invoice number is required")]
        //[DisplayName("Invoice Number")]
        //public string InvoiceNumber { get; set; }

        public string Content { get; set; } = "";
        public bool PaidFor { get; set; } = false;
        public int OrderID { get; set; } = 0;

        public PaymentProcessViewModel PaymentInfo { get; set; }

        public static PayInvoiceWidgetViewModel GetViewModel(string cardHeading, string content, bool paidFor, int orderID)
        {
            return new PayInvoiceWidgetViewModel
            {
                Content = content,
                CardHeading = cardHeading,
                PaidFor = paidFor,
                PaymentInfo = new PaymentProcessViewModel() { OrderId = orderID }
            };
        }
    }
}