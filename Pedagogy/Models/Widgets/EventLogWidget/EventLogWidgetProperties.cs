﻿using Kentico.Forms.Web.Mvc;

namespace Pedagogy.Models.Widgets.EventLogWidget
{
    public class EventLogWidgetProperties : BaseWidgetProperties
    {
        [EditingComponent(IntInputComponent.IDENTIFIER, DefaultValue = 10, Label = "Events Per Page", Order = 0)]
        public int EventsPerPage { get; set; }
    }
}