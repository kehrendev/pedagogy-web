﻿using System;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.EventLogWidget
{
    public class EventLogFilterViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ExceptionID { get; set; }
        public string OrganizationName { get; set; }
        public string CourseName { get; set; }
        public string UsersName { get; set; }
        public string UsersUserName { get; set; }
        public string StudentName { get; set; }
        public string StudentUserName { get; set; }
        public SelectList Exceptions { get; set; }
    }
}