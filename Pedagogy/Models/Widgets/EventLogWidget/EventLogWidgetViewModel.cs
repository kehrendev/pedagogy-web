﻿using Pedagogy.ClassMembers.Entities.EventLog;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.EventLogWidget
{
    public class EventLogWidgetViewModel
    {
        public List<EventLogDto> LogItems { get; set; }
        public int EventsPerPage { get; set; }
        public EventLogFilterViewModel FilterModel { get; set; }
        public string CardHeading { get; set; }

        public static EventLogWidgetViewModel GetViewModel(List<EventLogDto> logItems, int eventsPerPage, EventLogFilterViewModel filterModel, string cardHeading)
        {
            return new EventLogWidgetViewModel
            {
                LogItems = logItems,
                EventsPerPage = eventsPerPage,
                FilterModel = filterModel,
                CardHeading = cardHeading
            };
        }
    }
}