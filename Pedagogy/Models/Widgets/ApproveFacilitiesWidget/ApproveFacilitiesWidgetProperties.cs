﻿using Kentico.Forms.Web.Mvc;

namespace Pedagogy.Models.Widgets.ApproveFacilitiesWidget
{
    public class ApproveFacilitiesWidgetProperties : BaseWidgetProperties
    {
        [EditingComponent(IntInputComponent.IDENTIFIER, DefaultValue = 6, Label = "Facilities/Corporations Per Page", Order = 0)]
        public int FacilitiesPerPage { get; set; }
    }
}