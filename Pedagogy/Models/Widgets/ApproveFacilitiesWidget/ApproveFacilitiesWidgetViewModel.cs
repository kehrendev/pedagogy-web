﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.ApproveFacilitiesWidget
{
    public class ApproveFacilitiesWidgetViewModel
    {
        public int FacilitiesPerPage { get; set; }

        [Required(ErrorMessage = "You must select a corporation")]
        public int CorpID { get; set; }

        public IEnumerable<SelectListItem> Corporations { get; set; }

        public string CorporationName { get; set; }

        public string CardHeading { get; set; }

        public string ErrMsg { get; set; }

        public static ApproveFacilitiesWidgetViewModel GetViewModel(int facilitiesPerPage, IEnumerable<SelectListItem> corporations, string cardHeading, string errMsg)
        {
            return new ApproveFacilitiesWidgetViewModel
            {
                FacilitiesPerPage = facilitiesPerPage,
                Corporations = corporations,
                CardHeading = cardHeading,
                ErrMsg = errMsg
            };
        }
    }
}