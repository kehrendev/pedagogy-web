﻿using Pedagogy.DataAccess.Dto.Courses;

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.RegsiterNewCourseWidget
{
    public class RegisterNewCourseWidgetViewModel
    {
        public IEnumerable<SelectListItem> Courses { get; set; }
        public IEnumerable<SelectListItem> Facilities { get; set; }
        public IEnumerable<CourseItemDto> ClassData { get; set; }
        public int ClassID { get; set; }
        public int FacilityID { get; set; }
        public bool IsAdmin { get; set; }
        public string CardHeadingText { get; set; }

        public static RegisterNewCourseWidgetViewModel GetViewModel(IEnumerable<SelectListItem> courses, IEnumerable<SelectListItem> facilities, 
            IEnumerable<CourseItemDto> classData, bool isAdmin, string cardHeadingText)
        {
            return new RegisterNewCourseWidgetViewModel
            {
                Courses = courses ?? Enumerable.Empty<SelectListItem>(),
                Facilities = facilities ?? Enumerable.Empty<SelectListItem>(),
                ClassData = classData ?? Enumerable.Empty<CourseItemDto>(),
                IsAdmin = isAdmin,
                CardHeadingText = cardHeadingText
            };
        }
    }
}