﻿using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace Pedagogy.Models.Widgets
{
    public class BaseWidgetProperties : IWidgetProperties
    {
        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Card Heading", Order = 1)]
        public string CardHeading { get; set; } = "";
    }
}