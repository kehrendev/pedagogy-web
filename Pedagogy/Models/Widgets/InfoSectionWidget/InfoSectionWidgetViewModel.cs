﻿using CMS.DocumentEngine.Types.Pedagogy;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.InfoSectionWidget
{
    public class InfoSectionWidgetViewModel
    {
        public bool IsTabbedContent { get; set; }
        public string DisplayIdentifier { get; set; }
        public List<InfoSection> InfoSections { get; set; }
    }
}