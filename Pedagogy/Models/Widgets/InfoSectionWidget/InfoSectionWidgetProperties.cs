﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.InfoSectionWidget
{
    public class InfoSectionWidgetProperties : IWidgetProperties
    {
        [EditingComponent(CheckBoxComponent.IDENTIFIER,
            ExplanationText = "Check this box to show a tabbed layout for the information",
            Label = "Is Tabbed Content",
            Order = 0)]
        public bool IsTabbedContent { get; set; }

        [EditingComponent(PathSelector.IDENTIFIER,
            ExplanationText = "Select the folder containing the info sections you would like displayed.",
            Label = "Info Section",
            Order = 1)]
        public IList<PathSelectorItem> InfoSectionItems { get; set; }

        [EditingComponent(RadioButtonsComponent.IDENTIFIER, Label = "Image Display", DefaultValue = "left", Order = 2)]
        [EditingComponentProperty(nameof(RadioButtonsProperties.DataSource), "left;Left\r\nright;Right")]
        public string DisplayIdentifier { get; set; }
    }
}