﻿using CMS.Taxonomy;

using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Pedagogy.Controllers.FormComponents;
using System.Linq;

namespace Pedagogy.Models.Widgets.MembershipCourseListingWidget
{
    public class MembershipCourseListingWidgetProperties : IWidgetProperties
    {
        [EditingComponent(FacilityMembershipCategoryDropDownComponent.IDENTIFIER, Label = "Membership Category", Order = 1)]
        public string MembershipCategory { get; set; }

        [EditingComponent(DropDownComponent.IDENTIFIER, Label = "Column Count", Order = 2)]
        [EditingComponentProperty(nameof(DropDownProperties.DataSource), "1;1\r\n2;2\r\n3;3\r\n4;4")]
        public string ColumnCount { get; set; } = "1";
    }
}