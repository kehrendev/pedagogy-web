﻿using CMS.DocumentEngine;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.MembershipCourseListingWidget
{
    public class MembershipCourseListingWidgetViewModel
    {
        public IEnumerable<TreeNode> MembershipClasses { get; set; }
        public int ColumnCount { get; set; }
    }
}