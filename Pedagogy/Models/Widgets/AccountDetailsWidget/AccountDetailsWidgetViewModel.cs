﻿using CMS.DocumentEngine;

using System.Collections.Generic;

namespace Pedagogy.Models.Widgets.AccountDetailsWidget
{
    public class AccountDetailsWidgetViewModel
    {
        public IEnumerable<TreeNode> AccountDetailPages { get; set; }
        public string CardHeading { get; set; }

        public static AccountDetailsWidgetViewModel GetViewModel(IEnumerable<TreeNode> accountDetailPages, string cardHeading)
        {
            return new AccountDetailsWidgetViewModel
            {
                AccountDetailPages = accountDetailPages,
                CardHeading = cardHeading
            };
        }
    }
}