﻿using Pedagogy.ClassBuilder.Entities.Class;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.StudentClass;
using Pedagogy.Lib.Helpers;
using Pedagogy.Models.ClassMembers;

using System;

namespace Pedagogy.Models.Widgets.ReportsWidget
{
    public class ProgressReportViewModel
    {
        public int FacilityID { get; set; }
        public string FacilityName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public string Status { get; set; }
        public string TestScore { get; set; }
        public string LastAccessDay { get; set; }
        public string Source { get; set; }
        public string ClassTime { get; set; }
        public string TestTime { get; set; }

        public static ProgressReportViewModel GetViewModel(FacilityViewModel facilityData, ContactDto student, StudentClassDto studentClass, ClassDto classData, string status, 
            string lastAccessDay, string testTime, string classTime)
        {
            return new ProgressReportViewModel
            {
                FacilityID = facilityData.FacilityID,
                FacilityName = facilityData.Name,
                ClassID = studentClass.ClassID,
                ClassName = classData.ClassName,
                StudentID = studentClass.StudentID,
                StudentName = $"{student.ContactFirstName} {student.ContactLastName}",
                Status = status,
                LastAccessDay = lastAccessDay,
                ClassTime = classTime,
                TestTime = testTime,
                TestScore = studentClass.TestScore.HasValue ? (studentClass.TestScore.Value > classData.PassingGrade ? Math.Round(studentClass.TestScore.Value, 2).ToString() : "--") : "--",
                Source = studentClass.FacilityMembershipID.HasValue && studentClass.FacilityMembershipID.Value != 0 ? ClassMembersHelper.FACILITY_MEM_SOURCE : ClassMembersHelper.FACILITY_SOURCE
            };
        }
    }
}