﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.ReportsWidget
{
    public class ReportsWidgetViewModel
    {
        public IEnumerable<ProgressReportViewModel> ProgressReportData { get; set; }
        public ProgressReportFilterViewModel ProgressFilter { get; set; }

        public IEnumerable<CorporateReportViewModel> CorporateReportData { get; set; }

        public IEnumerable<ComplianceReportViewModel> ComplianceReportData { get; set; }

        public bool IsSiteAdmin { get; set; }
        public bool IsCorpAdmin { get; set; }

        public bool HasReportItems { get; set; }

        public int FacilityID { get; set; }
        public SelectList Facilities { get; set; }

        [DisplayName("Education Plan")]
        public int EducationPlanID { get; set; }
        public IEnumerable<SelectListItem> EducationPlans { get; set; }

        public string CardHeading { get; set; }

        public static ReportsWidgetViewModel GetViewModel(IEnumerable<ProgressReportViewModel> progressReportData, ProgressReportFilterViewModel progressFilter,
            IEnumerable<CorporateReportViewModel> corporateReportData, bool isSiteAdmin, bool isCorpAdmin, string cardHeading, SelectList facilities)
        {
            return new ReportsWidgetViewModel
            {
                ProgressReportData = progressReportData,
                ProgressFilter = progressFilter,
                CorporateReportData = corporateReportData,
                IsSiteAdmin = isSiteAdmin,
                IsCorpAdmin = isCorpAdmin,
                CardHeading = cardHeading,
                Facilities = facilities
            };
        }

        public static ReportsWidgetViewModel GetViewModel(IEnumerable<ProgressReportViewModel> progressReportData, ProgressReportFilterViewModel progressFilter,
            IEnumerable<CorporateReportViewModel> corporateReportData)
        {
            return new ReportsWidgetViewModel
            {
                ProgressReportData = progressReportData,
                ProgressFilter = progressFilter,
                CorporateReportData = corporateReportData
            };
        }
    }
}