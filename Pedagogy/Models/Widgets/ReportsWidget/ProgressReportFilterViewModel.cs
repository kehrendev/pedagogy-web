﻿using Pedagogy.Lib.Helpers;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.ReportsWidget
{
    public class ProgressReportFilterViewModel
    {
        public const string NOT_STARTED_STATUS = "Not Started";
        public const string IN_PROGRESS_STATUS = "In Progress";
        public const string COMPLETED_STATUS = "Completed";

        [DisplayName("Facility")]
        public int FacilityID { get; set; }
        public IEnumerable<SelectListItem> Facilities { get; set; }

        [DisplayName("Class")]
        public int ClassID { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }
        public IEnumerable<SelectListItem> Statuses { get; set; }

        [DisplayName("Student")]
        public int StudentID { get; set; }
        public IEnumerable<SelectListItem> Students { get; set; }

        [DisplayName("Last Access Begin")]
        public DateTime LastAccessBegin { get; set; }

        [DisplayName("Last Access End")]
        public DateTime LastAccessEnd { get; set; }

        public static ProgressReportFilterViewModel GetViewModel(IEnumerable<SelectListItem> students, IEnumerable<SelectListItem> facilities, IEnumerable<SelectListItem> classes)
        {
            return new ProgressReportFilterViewModel
            {
                Statuses = new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Value = NOT_STARTED_STATUS,
                        Text = NOT_STARTED_STATUS
                    },
                    new SelectListItem
                    {
                        Value = IN_PROGRESS_STATUS,
                        Text = IN_PROGRESS_STATUS
                    },
                    new SelectListItem
                    {
                        Value = COMPLETED_STATUS,
                        Text = COMPLETED_STATUS
                    }
                },
                Students = students,
                Facilities = facilities,
                Classes = classes,
            };
        }
    }
}