﻿namespace Pedagogy.Models.Widgets.ReportsWidget
{
    public class CorporateReportViewModel
    {
        public string FacilityName { get; set; }
        public string ClassName { get; set; }
        public int QtyNotStarted { get; set; }
        public int QtyInProgress { get; set; }
        public int QtyCompleted { get; set; }
        public int ClassTotal { get; set; }

        public static CorporateReportViewModel GetViewModel(string facilityName, string className, int qtyNotStarted, int qtyInProgress, int qtyCompleted, int classTotal)
        {
            return new CorporateReportViewModel
            {
                FacilityName = facilityName,
                ClassName = className,
                QtyNotStarted = qtyNotStarted,
                QtyInProgress = qtyInProgress,
                QtyCompleted = qtyCompleted,
                ClassTotal = classTotal
            };
        }
    }
}