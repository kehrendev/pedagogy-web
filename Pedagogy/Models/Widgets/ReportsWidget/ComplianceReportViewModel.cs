﻿namespace Pedagogy.Models.Widgets.ReportsWidget
{
    public class ComplianceReportViewModel
    {
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string DateAssigned { get; set; }
        public string DateComplete { get; set; }
        public string Status { get; set; }
        public string Expires { get; set; }
        public string PlanName { get; set; }
    }
}