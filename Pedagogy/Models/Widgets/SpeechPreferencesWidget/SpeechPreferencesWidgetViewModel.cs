﻿using System.ComponentModel;
using System.Web.Mvc;

namespace Pedagogy.Models.Widgets.SpeechPreferencesWidget
{
    public class SpeechPreferencesWidgetViewModel
    {
        [DisplayName("Preferred Voice:")]
        public int SpeechVoiceID { get; set; }
        public SelectList PreferredVoices { get; set; }

        [DisplayName("Preferred Speed: (higher is faster)")]
        public int SpeechRateID { get; set; }
        public SelectList PreferredSpeed { get; set; }

        public bool IsStudent { get; set; }
        public int StudentID { get; set; }
        public string CardHeading { get; set; }

        public static SpeechPreferencesWidgetViewModel GetViewModel(string cardHeading, int speechVoiceID, SelectList preferredVoices, int speechRateID, SelectList preferredSpeed,
            bool isStudent, int studentID)
        {
            return new SpeechPreferencesWidgetViewModel
            {
                CardHeading = cardHeading,
                SpeechVoiceID = speechVoiceID,
                PreferredVoices = preferredVoices,
                SpeechRateID = speechRateID,
                PreferredSpeed = preferredSpeed,
                IsStudent = isStudent,
                StudentID = studentID
            };
        }
    }
}