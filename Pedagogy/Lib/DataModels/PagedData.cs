﻿using System.Collections.Generic;

namespace Pedagogy.Lib.DataModels
{
    public class PagedData<T> where T : class
    {
        public IEnumerable<T> Data { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
    }

    public class PagedData<T, Y> where T : class where Y : class
    {
        public Dictionary<T, Y> Data { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
    }
}