﻿namespace Pedagogy.Lib.DataModels
{
    public class Breadcrumb
    {
        public Breadcrumb()
        {

        }

        public Breadcrumb(string title, string url, bool active = false, bool canNavigate = true)
        {

            this.Title = title;
            this.Url = url;
            this.Active = active;
            this.CanNavigate = canNavigate;
        }

        public string Title { get; set; }
        public string Url { get; set; }
        public bool Active { get; set; }
        public bool CanNavigate { get; set; }
    }
}