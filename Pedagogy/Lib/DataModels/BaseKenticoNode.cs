﻿using CMS.DocumentEngine;
using CMS.SiteProvider;
using System;
using System.Linq;

namespace Pedagogy.Lib.DataModels
{
    public class BaseKenticoNode
    {
        public BaseKenticoNode()
        {

        }

        public BaseKenticoNode(Guid nodeGuid)
        {
            var node = DocumentHelper.GetDocuments()
                .OnSite(SiteContext.CurrentSiteID)
                .Culture("en-us")
                .Published(true)
                .Columns("DocumentPageTitle","DocumentPageDescription","DocumentPageKeywords","NodeIsContentOnly")
                .WhereEquals("NodeGUID", nodeGuid)
                .FirstOrDefault();

            MetaTitle = node.DocumentPageTitle;
            MetaDescription = node.DocumentPageDescription;
            MetaKeywords = node.DocumentPageKeyWords;
            PageIsContentOnly = node.NodeIsContentOnly;
        }

        // Base Required Data
        public string MetaTitle { get; set; } = "";
        public string MetaDescription { get; set; } = "";
        public string MetaKeywords { get; set; } = "";
        public bool? PageIsContentOnly { get; set; } = false;

        // Relationships
        
    }
}