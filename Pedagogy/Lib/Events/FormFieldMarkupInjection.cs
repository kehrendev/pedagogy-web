﻿using Kentico.Forms.Web.Mvc;
using System;

namespace Pedagogy.Lib.Events
{
    public class FormFieldMarkupInjection
    {
        public static void RegisterEventHandlers()
        {
            //Contextually customizes the markup of rendered form fields
            FormFieldRenderingConfiguration.GetConfiguration.Execute += InjectMarkupIntoKenticoComponents;
        }

        private static void InjectMarkupIntoKenticoComponents(object sender, GetFormFieldRenderingConfigurationEventArgs e)
        {
            //Only injects additional markup into default Kentico form components
            if(!e.FormComponent.Definition.Identifier.StartsWith("Kentico", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            //Adds WAI-ARIA and HTML5 accessibility attributes to form fields marked as 'Required' via the Form builder interface
            AddAccessibilityAttributes(e);

            AddFieldSpecificMarkup(e);
        }

        private static void AddAccessibilityAttributes(GetFormFieldRenderingConfigurationEventArgs e)
        {
            if (e.FormComponent.BaseProperties.Required)
            {
                //Adds the 'aria-required' and 'required' attributes to the component's 'input' element
                e.Configuration.EditorHtmlAttributes["aria-required"] = "true";
                e.Configuration.EditorHtmlAttributes["required"] = "";
            }
        }

        private static void AddFieldSpecificMarkup(GetFormFieldRenderingConfigurationEventArgs e)
        {
            var htmlAttribute = e.Configuration.RootConfiguration.HtmlAttributes;

            if(e.FormComponent is TextAreaComponent)
            {
                e.Configuration.EditorHtmlAttributes["rows"] = "9";
                e.Configuration.EditorHtmlAttributes["cols"] = "30";
            }
            else if(e.FormComponent is RadioButtonsComponent)
            {
                if (e.Configuration.EditorHtmlAttributes.ContainsKey("class"))
                {
                    e.Configuration.EditorHtmlAttributes["class"] = "radio-btn";
                }
            }

            if (htmlAttribute.ContainsKey("class"))
            {
                if(htmlAttribute["class"].Equals("form-field"))
                {
                    htmlAttribute["class"] = "form-group";
                }
            }
        }
    }
}