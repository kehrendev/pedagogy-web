﻿namespace Pedagogy.Lib.Helpers
{
    public static class WidgetHelper
    {
        public static string[] SolutionPageWidgets() => new[] 
        {
            "Pedagogy.SolutionsWidget"
        };

        public static string[] SingleAuthorPageWidgets() => new[]
        {
            "Pedagogy.SliderWidget",
            "Pedagogy.BannerWidget"
        };

        public static string[] SingleNewsPageWidgets() => new[]
        {
            "Pedagogy.SliderWidget",
            "Pedagogy.BannerWidget"
        };
    }
}