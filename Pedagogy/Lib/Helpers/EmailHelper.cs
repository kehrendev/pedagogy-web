﻿using CMS.EmailEngine;
using CMS.MacroEngine;
using CMS.SiteProvider;

using System.Collections.Generic;

namespace Pedagogy.Lib.Helpers
{
    public class EmailHelper
    {
        public static void SendEmail(string toEmail, string emailTemplateName, Dictionary<string, object> data = null)
        {
            MacroResolver mr = MacroResolver.GetInstance();
            if (data != null)
                mr.SetNamedSourceData(data, true);

            EmailTemplateInfo emailTemplate = EmailTemplateProvider.GetEmailTemplate(emailTemplateName, SiteContext.CurrentSiteName);
            EmailMessage emailMessage = new EmailMessage
            {
                From = emailTemplate.TemplateFrom,
                EmailFormat = EmailFormatEnum.Html,
                Recipients = toEmail,
                CcRecipients = emailTemplate.TemplateCc,
                BccRecipients = emailTemplate.TemplateBcc,
                Subject = mr.ResolveMacros(emailTemplate.TemplateSubject),
                Body = mr.ResolveMacros(emailTemplate.TemplateText),
                PlainTextBody = mr.ResolveMacros(emailTemplate.TemplatePlainText)
            };

            EmailSender.SendEmailWithTemplateText(SiteContext.CurrentSiteName, emailMessage, emailTemplate, mr, false);
        }
    }
}