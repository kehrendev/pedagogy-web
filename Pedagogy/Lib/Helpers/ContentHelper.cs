﻿using CMS.DocumentEngine;
using CMS.Localization;
using CMS.MediaLibrary;
using CMS.SiteProvider;
using Kentico.Components.Web.Mvc.FormComponents;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.Lib.Helpers
{
    public class ContentHelper
    {
        public static IEnumerable<TreeNode> GetChildPages(TreeNode parentPage, string categoryName = "", int nestingLevel = 1)
        {
            IEnumerable<TreeNode> childPages = null;

            if (string.IsNullOrEmpty(categoryName))
            {
                childPages = DocumentHelper.GetDocuments()
                .Path(parentPage.NodeAliasPath, CMS.DocumentEngine.PathTypeEnum.Children)
                .NestingLevel(nestingLevel)
                .OrderBy("NodeLevel, NodeOrder, NodeName")
                .Published()
                .OnCurrentSite();
            }
            else
            {
                childPages = DocumentHelper.GetDocuments()
                .Path(parentPage.NodeAliasPath, CMS.DocumentEngine.PathTypeEnum.Children)
                .NestingLevel(nestingLevel)
                .InEnabledCategories(categoryName)
                .OrderBy("NodeLevel, NodeOrder, NodeName")
                .Published()
                .OnCurrentSite();
            }

            return childPages;
        }

        public static string GetSelectedMediaFile(MediaFilesSelectorItem selectedItem)
        {
            if (selectedItem == null) return null;

            Guid guid = selectedItem.FileGuid;

            MediaFileInfo mediaFile = MediaFileInfoProvider.GetMediaFileInfo(guid, SiteContext.CurrentSiteName);

            if (mediaFile != null) return MediaLibraryHelper.GetPermanentUrl(mediaFile);

            return null;
        }

        public static string GetSelectedPath(PathSelectorItem selectedPath)
        {
            if (selectedPath == null) return null;

            string path = selectedPath?.NodeAliasPath;
            if (string.IsNullOrEmpty(path)) return null;

            return path;
        }

        public static TreeNode GetSelectedPage(PageSelectorItem selectedPage, string className = "")
        {
            if (selectedPage == null) return TreeNode.New();

            // Retrieves the node GUID of the selected page from the 'Pages' property
            Guid? selectedPageGuid = selectedPage.NodeGuid;
            // Retrieves the page that corresponds to the selected GUID
            TreeNode page;
            if (string.IsNullOrEmpty(className))
            {
                page = DocumentHelper.GetDocuments()
                            .WhereEquals("NodeGUID", selectedPageGuid)
                            .Culture(LocalizationContext.CurrentCulture.CultureCode)
                            .Published()
                            .TopN(1)
                            .OnCurrentSite()
                            .FirstOrDefault();
            }
            else
            {
                page = DocumentHelper.GetDocuments()
                            .Type(className)
                            .WhereEquals("NodeGUID", selectedPageGuid)
                            .Culture(LocalizationContext.CurrentCulture.CultureCode)
                            .Published()
                            .WithCoupledColumns()
                            .TopN(1)
                            .OnCurrentSite()
                            .FirstOrDefault();
            }

            return page ?? TreeNode.New();
        }
    }
}