﻿using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using Pedagogy.Lib.DataModels;
using System;
using System.Collections.Generic;

namespace Pedagogy.Lib.Helpers
{
    public class BreadcrumbProvider
    {
        public static IEnumerable<Breadcrumb> GetBreadcrumbs(TreeNode startingNode)
        {
            if (startingNode == null)
            {
                throw new Exception("GetBreadcrumbs(TreeNode startingNode): startingNode is required and requires type of TreeNode");
            }

            var startingNodeName = startingNode.DocumentName;
            var breadcrumbs = new List<Breadcrumb>();

            if (startingNode.NodeLevel == 0 || startingNode.NodeLevel == 1)
            {
                if(startingNode.ClassName != Banner.CLASS_NAME)
                {
                    breadcrumbs.Add(new Breadcrumb(startingNode.DocumentName, startingNode.NodeAliasPath, true));
                    breadcrumbs.Add(new Breadcrumb("Home", "/", startingNode.DocumentName == "Home" ? true : false));

                    breadcrumbs.Reverse();
                }
                return breadcrumbs;
            }

            var currentNode = startingNode;
            var currentNodeLevel = currentNode.NodeLevel;
            while (currentNodeLevel != 0)
            {
                if(currentNode.ClassName != "Pedagogy.Folder" && currentNode.ClassName != Banner.CLASS_NAME)
                {
                    breadcrumbs.Add(new Breadcrumb(currentNode.DocumentName, currentNode.NodeAliasPath, currentNode.DocumentName == startingNodeName ? true : false, currentNode.GetBooleanValue("CanNavigate", true)));
                }

                currentNode = currentNode.Parent;
                currentNodeLevel--;
            }

            breadcrumbs.Add(new Breadcrumb("Home", "/", currentNode.DocumentName == "Home" ? true : false));

            breadcrumbs.Reverse();
            return breadcrumbs;
        }
    }
}