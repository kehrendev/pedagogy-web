﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pedagogy.Lib.DataModels;

namespace Pedagogy.Lib.Helpers
{
    public static class PagingHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="doPagingInternal">If true (default), it will choose the page from full results, otherwise it will simply pass the list through.</param>
        /// <returns></returns>
        public static PagedData<T> PagedResult<T>(this List<T> list, int pageNumber, int pageSize, bool doPagingInternal = true) where T : class
        {
            var result = new PagedData<T>();
            result.Data = list;
            if (doPagingInternal)
            {
                result.Data = list.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            }
            result.TotalPages = Convert.ToInt32(Math.Ceiling((double)list.Count() / pageSize));
            result.CurrentPage = pageNumber;

            return result;
        }

        public static PagedData<T, Y> PagedResult<T, Y>(this Dictionary<T, Y> dictionary, int pageNumber, int pageSize, bool doPagingInternal = true) where T : class where Y : class
        {
            var result = new PagedData<T, Y>();
            result.Data = dictionary;
            if (doPagingInternal)
            {
                result.Data = dictionary.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToDictionary(pair => pair.Key, pair => pair.Value);
            }
            result.TotalPages = Convert.ToInt32(Math.Ceiling((double)dictionary.Count() / pageSize));
            result.CurrentPage = pageNumber;

            return result;
        }
    }
}