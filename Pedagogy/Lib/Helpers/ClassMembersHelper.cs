﻿using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Ecommerce;

using Pedagogy.ClassBuilder.DependencyInjection;
using Pedagogy.ClassBuilder.Entities.Class;
using Pedagogy.ClassMembers.DependencyInjection;
using Pedagogy.ClassMembers.Entities.ClassTestDetail;
using Pedagogy.ClassMembers.Entities.Contact;
using Pedagogy.ClassMembers.Entities.ContactFacility;
using Pedagogy.ClassMembers.Entities.Facility;
using Pedagogy.ClassMembers.Entities.FacilityClass;
using Pedagogy.ClassMembers.Entities.StudentClass;
using Pedagogy.ClassMembers.Entities.StudentClassDetail;
using Pedagogy.Custom.Generated.ModuleClasses;
using Pedagogy.Models.ClassMembers;
using Pedagogy.Models.Widgets.ReportsWidget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Pedagogy.Lib.Helpers
{
    public class ClassMembersHelper
    {
        #region Constants

        public const string FACILITY_MEM_SOURCE = "Facility Membership";
        public const string FACILITY_SOURCE = "Facility";
        public const string NOT_STARTED_STATUS = "Not Started";
        public const string IN_PROGRESS_STATUS = "In Progress";
        public const string COMPLETED_STATUS = "Completed";

        #endregion

        #region Build Portal URLs
        public static string BuildCourseUrl(int studentClassID, int chapter, int page)
        {
            return $"{ContentIdentifiers.CLASS}?studentClassID={studentClassID}&chapter={chapter}&page={page}";
        }

        public static string BuildTestUrl(int studentClassID, int chapter, int page)
        {
            return $"{ContentIdentifiers.TEST}?studentClassID={studentClassID}&chapter={chapter}&page={page}";
        }

        public static string BuildTestAttemptUrl(int studentClassID, int attemptID)
        {
            return $"{ContentIdentifiers.TEST}?studentClassID={studentClassID}&attempt={attemptID}";
        }

        public static string BuildClassEvalUrl(int studentClassID)
        {
            return $"{ContentIdentifiers.CLASS_EVAL}?studentClassID={studentClassID}";
        }

        public static string BuildPrintCertUrl(int studentClassID, int certID)
        {
            return $"{ContentIdentifiers.PRINT_CERTIFICATE}?studentClassID={studentClassID}&certID={certID}";
        }

        public static string BuildPrintStudentsCertsUrl(IEnumerable<int> ids)
        {
            var idString = "";
            foreach (var item in ids)
            {
                if (ids.ToList().IndexOf(item) == (ids.Count() - 1))
                {
                    idString += item.ToString();
                }
                else
                {
                    idString += $"{item}|";
                }
            }

            return $"{ContentIdentifiers.PRINT_ALL_STUDENT_CERTS}?ids={idString}";
        }

        #endregion

        #region Facility/Corporation Methods

        public static List<FacilityViewModel> GetFacilitiesForCurrentUser(IClassMemberDependencies provider, bool isSiteAdmin, bool isCorporateAdmin, int? userID, string returnUrl)
        {
            var facilities = new List<FacilityViewModel>();

            if (isSiteAdmin)
            {
                var f = provider.FacilityRepo.GetAllFacilities(x => new FacilityDto 
                { 
                    FacilityName = x.Name, 
                    FacilityID = x.FacilityID, 
                    FacilityCity = x.City, 
                    FacilityState = x.State, 
                    FacilityParentID = x.FacilityParentID, 
                    FacilityCode = x.FacilityCode 
                }, true);

                f.ForEach(x => facilities.Add(
                    FacilityViewModel.GetViewModel(returnUrl, x.FacilityID, x.FacilityName, x.FacilityCity, x.FacilityState, isSiteAdmin, x.FacilityParentID, x.FacilityCode)));

                return facilities.OrderBy(x => x.Name).ToList();
            }
            else if (isCorporateAdmin)
            {
                int? corpAdminType = provider.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);
                var contact = provider.ContactRepo.GetContact(userID, corpAdminType, x => new ContactDto { ContactID = x.ContactID });

                if (contact!= null && contact.ContactID != 0)
                {
                    var ids = provider.ContactFacilityRepo.GetContactFacilitiesByContactID(contact.ContactID, x => new ContactFacilityDto { FacilityID = x.FacilityID });

                    foreach (var id in ids)
                    {
                        var f = provider.FacilityRepo.GetChildFacilities(id.FacilityID,
                            x => new FacilityDto { FacilityID = x.FacilityID, FacilityName = x.Name, FacilityCity = x.City, FacilityState = x.State, FacilityParentID = x.FacilityParentID, 
                                FacilityCode = x.FacilityCode });

                        f.ForEach(x => 
                        { 
                            facilities.Add(
                                FacilityViewModel.GetViewModel(returnUrl, x.FacilityID, x.FacilityName, x.FacilityCity, x.FacilityState, isSiteAdmin, x.FacilityParentID, x.FacilityCode)); 
                        });
                    }
                }

                return facilities.OrderBy(x => x.Name).ToList(); ;
            }
            else
            {
                int? facilityAdminType = provider.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.FACILITY_ADMIN_CODE_NAME);
                var contact = provider.ContactRepo.GetContact(userID, facilityAdminType, x => new ContactDto { ContactID = x.ContactID });

                if (contact != null && contact.ContactID != 0)
                {
                    var ids = provider.ContactFacilityRepo.GetContactFacilitiesByContactID(contact.ContactID, x => new ContactFacilityDto { FacilityID = x.FacilityID });

                    foreach (var id in ids)
                    {
                        var facility = provider.FacilityRepo.GetFacility(id.FacilityID, 
                            x => new FacilityDto { FacilityID = x.FacilityID, FacilityName = x.Name, FacilityCity = x.City, FacilityState = x.State, FacilityParentID = x.FacilityParentID, 
                                FacilityCode = x.FacilityCode }, false);

                        if (facility != null)
                        {
                            facilities.Add(FacilityViewModel.GetViewModel(returnUrl, facility.FacilityID, facility.FacilityName, facility.FacilityCity, facility.FacilityState, isSiteAdmin, 
                                facility.FacilityParentID, facility.FacilityCode));
                        }
                    }
                }

                return facilities.OrderBy(x => x.Name).ToList();
            }
        }

        public static List<FacilityViewModel> GetCorporationsForCurrentUser(IClassMemberDependencies provider, bool isSiteAdmin, int? userID, string returnUrl)
        {
            var corps = new List<FacilityViewModel>();

            if (isSiteAdmin)
            {
                var corporations = provider.FacilityRepo.GetAllCorporations(x => new FacilityDto { FacilityName = x.Name, FacilityCode = x.FacilityCode, FacilityID = x.FacilityID }).OrderBy(x => x.FacilityName).ToList();

                corporations.ForEach(x => corps.Add(
                    FacilityViewModel.GetViewModel(returnUrl, x.FacilityID, x.FacilityName, x.FacilityCode, isSiteAdmin, provider.FacilityRepo.CorporationHasChildFacilities(x.FacilityID))));

                return corps;
            }
            else
            {
                int? corpAdminType = provider.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.CORP_ADMIN_CODE_NAME);
                var contact = provider.ContactRepo.GetContact(userID, corpAdminType, x => new ContactDto { ContactID = x.ContactID, ContactTypeID = x.ContactTypeID });

                if (contact != null && contact.ContactID != 0)
                {
                    var facilityIDs = provider.ContactFacilityRepo.GetContactFacilitiesByContactID(contact.ContactID, x => new ContactFacilityDto { FacilityID = x.FacilityID });

                    foreach (var id in facilityIDs)
                    {
                        var corporation = provider.FacilityRepo.GetCorporation(id.FacilityID,
                            x => new FacilityDto { FacilityName = x.Name, FacilityCode = x.FacilityCode, FacilityID = x.FacilityID },
                            false);

                        if (corporation != null)
                        {
                            corps.Add(FacilityViewModel.GetViewModel(returnUrl, corporation.FacilityID, corporation.FacilityName, corporation.FacilityCode, isSiteAdmin, 
                                provider.FacilityRepo.CorporationHasChildFacilities(corporation.FacilityID)));
                        }
                    }
                }

                return corps.OrderBy(x => x.Name).ToList();
            }            
        }

        public static List<FacilityViewModel> SetUpFacilityViewModel(IClassMemberDependencies provider, List<ContactViewModel> contacts, List<FacilityViewModel> facilities)
        {
            var f = new List<FacilityViewModel>();
            foreach (var c in contacts)
            {
                var contactFacility = provider.ContactFacilityRepo.GetContactFacilitiesByContactID(c.ContactID, x => new ContactFacilityDto { FacilityID = x.FacilityID });
                foreach (var cf in contactFacility)
                {
                    var facility = facilities
                        .Where(x => x.FacilityID == cf.FacilityID)
                        .FirstOrDefault();

                    if (facility != null)
                    {
                        f.Add(facility);
                    }
                }
            }

            return f.OrderBy(x => x.Name).ToList();
        }

        public static List<FacilityViewModel> FilterFacilities(FacilityViewModel filter, string state, List<FacilityViewModel> facilities)
        {
            if (!string.IsNullOrEmpty(filter.Name)) facilities = facilities.Where(m => m.Name.ToLower().Contains(filter.Name.ToLower())).ToList();
            if (!string.IsNullOrEmpty(filter.City)) facilities = facilities.Where(m => m.City.ToLower().Contains(filter.City.ToLower())).ToList();
            if (!string.IsNullOrEmpty(state)) facilities = facilities.Where(m => m.State == state).ToList();

            return facilities;
        }

        #endregion

        #region Class Queries

        public static Dictionary<string, string> GetAvailableClasses()
        {
            var availClasses = new Dictionary<string, string>();

            var classes = CourseProvider.GetCourses()
                .OnCurrentSite()
                .Published()
                .PublishedVersion()
                .WhereNull(nameof(Course.CourseDemoClassID))
                .Select(x => new { x.SKU, x.CourseTitle });

            foreach (var i in classes)
            {
                if (i.SKU.SKUProductType != SKUProductTypeEnum.Bundle)
                {
                    availClasses.Add(i.SKU.SKUNumber, i.CourseTitle);
                }
            }

            return availClasses;
        }

        public static bool IsClassInPackage(string invoiceNumber, int classID)
        {
            if (string.IsNullOrEmpty(invoiceNumber)) return false;

            var orderItem = OrderItemInfoProvider.GetOrderItems()
                   .Where(x => x.OrderItemOrderID == int.Parse(invoiceNumber))
                   .Where(x => x.OrderItemSKU.SKUNumber == classID.ToString())
                   .Select(x => new { x.OrderItemBundleGUID })
                   .FirstOrDefault();

            if (orderItem == null) return true;

            if (orderItem.OrderItemBundleGUID != null && orderItem.OrderItemBundleGUID != Guid.Empty) return true;

            return false;
        }

        #endregion

        #region Sales Queries

        public static void RemoveClassFromSalesTable(string invoiceNumber, int classID)
        {
            int? salesItem = SalesInfoProvider.GetSales()
                .Where(x => x.SaleInvoiceNumber == int.Parse(invoiceNumber))
                .Where(x => x.SaleClassID == classID)
                .Select(x => x.SalesID)
                .FirstOrDefault();

            if (salesItem.HasValue) SalesInfoProvider.DeleteSalesInfo(salesItem.Value);
        }

        #endregion

        #region Report Queries

        //public static ReportsWidgetViewModel GetReportData(UserInfo user)
        //{
        //    var f = GetFacilitiesForCurrentUser(
        //        user.IsInRole(ContentIdentifiers.SITE_ADMIN_ROLE, SiteContext.CurrentSiteName), 
        //        user.IsInRole(ContentIdentifiers.CORP_ADMIN_ROLE, SiteContext.CurrentSiteName), 
        //        user.UserID,
        //        "");

        //    var progData = new List<ProgressReportViewModel>();
        //    var students = new List<SelectListItem>();
        //    var facilities = new List<SelectListItem>();
        //    var classes = new List<SelectListItem>();
        //    var corpData = new List<CorporateReportViewModel>();

        //    var studentTypeId = GetContactTypeID(ContentIdentifiers.STUDENT_CODE_NAME);

        //    foreach (var item in f)
        //    {
        //        facilities.Add(new SelectListItem
        //        {
        //            Value = item.FacilityID.ToString(),
        //            Text = item.Name
        //        });

        //        var facilityClasses = db.FacilityClasses
        //            .Where(f => f.FacilityID == item.FacilityID)
        //            .Where(f => f.QuantityUsed.HasValue && f.QuantityUsed.Value != 0)
        //            .Select(x => new { x.ClassID, x.FacilityMembershipID});

        //        foreach (var t in facilityClasses)
        //        {
        //            int notStarted = 0;
        //            int inProgress = 0;
        //            int completed = 0;

        //            var className = ClassBuilderHelper.GetClass(t.ClassID, x => new Pedagogy.ClassBuilder.TU_Class { ClassName = x.ClassName }).ClassName;

        //            classes.Add(new SelectListItem
        //            {
        //                Value = t.ClassID.ToString(),
        //                Text = className
        //            });

        //            if (t.FacilityMembershipID.HasValue)
        //            {
        //                var studentClassMembershipWorkflowIDs = db.StudentClasses
        //                    .Where(s => s.ClassID == t.ClassID)
        //                    .Where(s => s.PaymentSource == item.FacilityCode)
        //                    .Where(s => s.FacilityMembershipID == t.FacilityMembershipID)
        //                    .Select(x => x.ClassWorkflowID);

        //                foreach (var i in studentClassMembershipWorkflowIDs)
        //                {
        //                    if (i == GetWorkflowID(ContentIdentifiers.START_CLASS_WF_CODE_NAME)) notStarted++;
        //                    else if (i == GetWorkflowID(ContentIdentifiers.SCORE_TEST_WF_CODE_NAME) || i == GetWorkflowID(ContentIdentifiers.CONTINUE_WF_CODE_NAME) || i == GetWorkflowID(ContentIdentifiers.COMPLETE_TEST_WF_CODE_NAME) || i == GetWorkflowID(ContentIdentifiers.RETAKE_TEST_WF_CODE_NAME)) inProgress++;
        //                    else completed++;
        //                }
        //            }
        //            else
        //            {
        //                var studentClassWorkflowIDs = db.StudentClasses
        //                    .Where(s => s.ClassID == t.ClassID)
        //                    .Where(s => s.PaymentSource == item.FacilityCode)
        //                    .Where(s => s.FacilityMembershipID == null)
        //                    .Select(x => x.ClassWorkflowID);

        //                foreach (var i in studentClassWorkflowIDs)
        //                {
        //                    if (i == GetWorkflowID(ContentIdentifiers.START_CLASS_WF_CODE_NAME)) notStarted++;
        //                    else if (i == GetWorkflowID(ContentIdentifiers.SCORE_TEST_WF_CODE_NAME) || i == GetWorkflowID(ContentIdentifiers.CONTINUE_WF_CODE_NAME) || i == GetWorkflowID(ContentIdentifiers.COMPLETE_TEST_WF_CODE_NAME) || i == GetWorkflowID(ContentIdentifiers.RETAKE_TEST_WF_CODE_NAME)) inProgress++;
        //                    else completed++;
        //                }
        //            }

        //            var classTotal = completed + inProgress + notStarted;

        //            corpData.Add(CorporateReportViewModel.GetViewModel(item.Name, className, notStarted, inProgress, completed, classTotal));
        //        }

        //        //get all students associated with this facility
        //        var contactIDs = db.ContactFacilities.Where(f => f.FacilityID == item.FacilityID).Where(f => f.Enabled).Select(x => x.ContactID);
        //        foreach (var x in contactIDs)
        //        {
        //            var student = db.Contacts
        //                .Where(c => c.ContactID == x)
        //                .Where(c => c.ContactTypeID == studentTypeId)
        //                .Select(x => new { x.ContactID, x.ContactFirstName, x.ContactLastName })
        //                .FirstOrDefault();

        //            if (student != null)
        //            {
        //                students.Add(new SelectListItem
        //                {
        //                    Value = student.ContactID.ToString(),
        //                    Text = $"{student.ContactFirstName} {student.ContactLastName}"
        //                });

        //                //get all their courses that were purchased through this facility
        //                var studentClasses = db.StudentClasses
        //                    .Where(sc => sc.StudentID == student.ContactID)
        //                    .Where(sc => sc.PaymentSource.Contains(item.FacilityCode))
        //                    .Select(x => new { x.ClassID, x.StudentClassID, x.TestAttemptID, x.ClassWorkflowID, x.UpdatedOn, x.TestScore, x.StudentID, x.FacilityMembershipID });

        //                foreach (var studentClass in studentClasses)
        //                {
        //                    var classData = cb.TU_Class.Where(c => c.ClassID == studentClass.ClassID).Select(x => new { x.ClassName, x.PassingGrade }).FirstOrDefault();
        //                    var status = "";
        //                    var lastDayAccessed = "";
        //                    var classTime = "0:00:00";
        //                    var testTime = "0:00:00";

        //                    var studentClassDetailsViewTime = db.StudentClassDetails
        //                    .Where(s => s.StudentClassID == studentClass.StudentClassID)
        //                    .Select(x => x.TotalPageViewTimeSecs);

        //                    if (studentClassDetailsViewTime.Count() > 0)
        //                    {
        //                        var count = 0;
        //                        foreach (var viewTime in studentClassDetailsViewTime)
        //                        {
        //                            if (viewTime.HasValue) count += (int)viewTime.Value;
        //                        }

        //                        var t = TimeSpan.FromSeconds(count);
        //                        classTime = t.ToString(@"hh\:mm\:ss");
        //                    }

        //                    var studentTestDetailsViewTime = db.ClassTestDetails
        //                        .Where(s => s.StudentClassID == studentClass.StudentClassID)
        //                        //.Where(s => s.TestAttemptID == studentClass.TestAttemptID)
        //                        .Select(x => x.TotalPageViewTimeSecs);

        //                    if (studentTestDetailsViewTime.Count() > 0)
        //                    {
        //                        var count = 0;
        //                        foreach (var viewTime in studentTestDetailsViewTime)
        //                        {
        //                            if (viewTime.HasValue) count += (int)viewTime.Value;
        //                        }

        //                        var t = TimeSpan.FromSeconds(count);
        //                        testTime = t.ToString(@"hh\:mm\:ss");
        //                    }

        //                    if (studentClass.ClassWorkflowID == GetWorkflowID(ContentIdentifiers.START_CLASS_WF_CODE_NAME))
        //                    {
        //                        status = NOT_STARTED_STATUS;
        //                        lastDayAccessed = "--";
        //                    }
        //                    else if (studentClass.ClassWorkflowID == GetWorkflowID(ContentIdentifiers.SCORE_TEST_WF_CODE_NAME) || studentClass.ClassWorkflowID == GetWorkflowID(ContentIdentifiers.CONTINUE_WF_CODE_NAME) || studentClass.ClassWorkflowID == GetWorkflowID(ContentIdentifiers.COMPLETE_TEST_WF_CODE_NAME) || studentClass.ClassWorkflowID == GetWorkflowID(ContentIdentifiers.RETAKE_TEST_WF_CODE_NAME))
        //                    {
        //                        status = IN_PROGRESS_STATUS;
        //                        lastDayAccessed = studentClass.UpdatedOn.Value.ToString("MM/dd/yyyy");
        //                    }
        //                    else
        //                    {
        //                        status = COMPLETED_STATUS;
        //                        lastDayAccessed = studentClass.UpdatedOn.Value.ToString("MM/dd/yyyy");
        //                    }

        //                    progData.Add(ProgressReportViewModel.GetViewModel(item, student, studentClass, classData, status, lastDayAccessed, testTime, classTime));
        //                }
        //            }
        //        }
        //    }

        //    return ReportsWidgetViewModel.GetViewModel(progData, ProgressReportFilterViewModel.GetViewModel(students, facilities, classes), corpData);
        //}

        //public static IEnumerable<SelectListItem> GetProgressFilterStatuses()
        //{
        //    return new List<SelectListItem>
        //        {
        //            new SelectListItem
        //            {
        //                Value = NOT_STARTED_STATUS,
        //                Text = NOT_STARTED_STATUS
        //            },
        //            new SelectListItem
        //            {
        //                Value = IN_PROGRESS_STATUS,
        //                Text = IN_PROGRESS_STATUS
        //            },
        //            new SelectListItem
        //            {
        //                Value = COMPLETED_STATUS,
        //                Text = COMPLETED_STATUS
        //            }
        //        };
        //}

        public static ReportsWidgetViewModel GetProgressReportData(IClassMemberDependencies classMemberProvider, IClassBuilderDependencies classBuilderProvider,
            bool isSiteAdmin, bool isCorpAdmin, int userID, int facilityID = 0)
        {
            List<FacilityViewModel> f = new List<FacilityViewModel>();

            if (facilityID != 0)
            {
                var facility = classMemberProvider.FacilityRepo.GetFacility(facilityID, x => new FacilityDto(x), false);

                if (facility != null) f.Add(new FacilityViewModel(facility));
            }
            else
            {
                f = GetFacilitiesForCurrentUser(classMemberProvider, isSiteAdmin, isCorpAdmin, userID, "");
            }

            var progData = new List<ProgressReportViewModel>();
            var students = new List<SelectListItem>();
            var classes = new List<SelectListItem>();

            // get contact types
            int? studentTypeID = classMemberProvider.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);

            // get workflows
            var startClassWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            var scoreTestWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.SCORE_TEST_WF_CODE_NAME);
            var continueClassWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.CONTINUE_WF_CODE_NAME);
            var completeTestWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.COMPLETE_TEST_WF_CODE_NAME);
            var retakeTestWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.RETAKE_TEST_WF_CODE_NAME);

            foreach (var item in f)
            {
                var facilityClasses = classMemberProvider.FacilityClassRepo.GetFacilityClasses(item.FacilityID,
                    x => new FacilityClassDto { ClassID = x.ClassID, FacilityMembershipID = x.FacilityMembershipID },
                    x => x.QuantityUsed.HasValue && x.QuantityUsed.Value != 0);

                foreach (var t in facilityClasses)
                {
                    classes.Add(new SelectListItem
                    {
                        Value = t.ClassID.ToString(),
                        Text = classBuilderProvider.ClassRepo.GetClass(t.ClassID, x => new ClassDto { ClassName = x.ClassName }).ClassName ?? ""
                    });
                }

                //get all students associated with this facility
                var contactIDs = classMemberProvider.ContactFacilityRepo.GetContactFacilitiesByFacilityID(item.FacilityID, x => new ContactFacilityDto { ContactID = x.ContactID });

                foreach (var x in contactIDs)
                {
                    var student = classMemberProvider.ContactRepo.GetContact(x.ContactID, studentTypeID,
                        x => new ContactDto { ContactID = x.ContactID, ContactFirstName = x.ContactFirstName, ContactLastName = x.ContactLastName });

                    if (student != null)
                    {
                        students.Add(new SelectListItem
                        {
                            Value = student.ContactID.ToString(),
                            Text = $"{student.ContactFirstName} {student.ContactLastName}"
                        });

                        //get all their courses that were purchased through this facility
                        var studentClasses = classMemberProvider.StudentClassRepo.GetAllFacilityStudentClassesByStudentID(student.ContactID, item.FacilityCode, x => true,
                            x => new StudentClassDto
                            {
                                ClassID = x.ClassID,
                                StudentClassID = x.StudentClassID,
                                TestAttemptID = x.TestAttemptID,
                                ClassWorkflowID = x.ClassWorkflowID,
                                UpdatedOn = x.UpdatedOn,
                                TestScore = x.TestScore,
                                StudentID = x.StudentID,
                                FacilityMembershipID = x.FacilityMembershipID
                            });

                        foreach (var studentClass in studentClasses)
                        {
                            var status = "";
                            var lastDayAccessed = "";
                            var classTime = "0:00:00";
                            var testTime = "0:00:00";

                            var studentClassDetailsViewTime = classMemberProvider.StudentClassDetailRepo.GetStudentClassDetails(studentClass.StudentClassID,
                                x => new StudentClassDetailDto { TotalPageViewTimeSecs = x.TotalPageViewTimeSecs });

                            if (studentClassDetailsViewTime.Count() > 0)
                            {
                                var count = 0;
                                foreach (var detail in studentClassDetailsViewTime)
                                {
                                    if (detail.TotalPageViewTimeSecs.HasValue) count += (int)detail.TotalPageViewTimeSecs.Value;
                                }

                                var t = TimeSpan.FromSeconds(count);
                                classTime = t.ToString(@"hh\:mm\:ss");
                            }

                            var studentTestDetailsViewTime = classMemberProvider.ClassTestDetailRepo.GetClassTestDetails(studentClass.StudentClassID,
                                x => new ClassTestDetailDto { TotalPageViewTimeSecs = x.TotalPageViewTimeSecs });

                            if (studentTestDetailsViewTime.Count() > 0)
                            {
                                var count = 0;
                                foreach (var detail in studentTestDetailsViewTime)
                                {
                                    if (detail.TotalPageViewTimeSecs.HasValue) count += (int)detail.TotalPageViewTimeSecs.Value;
                                }

                                var t = TimeSpan.FromSeconds(count);
                                testTime = t.ToString(@"hh\:mm\:ss");
                            }

                            if (studentClass.ClassWorkflowID == startClassWF)
                            {
                                status = NOT_STARTED_STATUS;
                                lastDayAccessed = "--";
                            }
                            else if (studentClass.ClassWorkflowID == scoreTestWF || studentClass.ClassWorkflowID == continueClassWF || studentClass.ClassWorkflowID == completeTestWF || studentClass.ClassWorkflowID == retakeTestWF)
                            {
                                status = IN_PROGRESS_STATUS;
                                lastDayAccessed = studentClass.UpdatedOn.Value.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                status = COMPLETED_STATUS;
                                lastDayAccessed = studentClass.UpdatedOn.Value.ToString("MM/dd/yyyy");
                            }

                            var classData = classBuilderProvider.ClassRepo.GetClass(studentClass.ClassID, x => new ClassDto { ClassName = x.ClassName, PassingGrade = x.PassingGrade });

                            progData.Add(ProgressReportViewModel.GetViewModel(item, student, studentClass, classData, status, lastDayAccessed, testTime, classTime));
                        }
                    }
                }
            }

            var model = new ReportsWidgetViewModel
            {
                ProgressReportData = progData,
                ProgressFilter = new ProgressReportFilterViewModel
                {
                    Students = students,
                    Classes = classes
                },
                HasReportItems = progData.Count() > 0
            };

            return model;
        }

        public static IEnumerable<CorporateReportViewModel> GetCorporateReportData(IClassMemberDependencies classMemberProvider, IClassBuilderDependencies classBuilderProvider,
            bool isSiteAdmin, bool isCorpAdmin, int userID, int facilityID = 0)
        {
            List<FacilityViewModel> f = new List<FacilityViewModel>();

            if (facilityID != 0)
            {
                var facility = classMemberProvider.FacilityRepo.GetFacility(facilityID, x => new FacilityDto(x));

                if (facility != null) f.Add(new FacilityViewModel(facility));
            }
            else
            {
                f = GetFacilitiesForCurrentUser(classMemberProvider, isSiteAdmin, isCorpAdmin, userID, "");
            }

            var facilities = new List<SelectListItem>();
            var classes = new List<SelectListItem>();
            var corpData = new List<CorporateReportViewModel>();

            var studentTypeID = classMemberProvider.ContactTypeRepo.GetContactTypeIDByCodeName(ContentIdentifiers.STUDENT_CODE_NAME);

            // get workflows
            var startClassWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.START_CLASS_WF_CODE_NAME);
            var scoreTestWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.SCORE_TEST_WF_CODE_NAME);
            var continueClassWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.CONTINUE_WF_CODE_NAME);
            var completeTestWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.COMPLETE_TEST_WF_CODE_NAME);
            var retakeTestWF = classMemberProvider.ClassWorkflowRepo.GetWorkflowIDByCodeName(ContentIdentifiers.RETAKE_TEST_WF_CODE_NAME);

            foreach (var item in f)
            {
                facilities.Add(new SelectListItem
                {
                    Value = item.FacilityID.ToString(),
                    Text = item.Name
                });

                var facilityClasses = classMemberProvider.FacilityClassRepo.GetFacilityClasses(item.FacilityID,
                    x => new FacilityClassDto { ClassID = x.ClassID, FacilityMembershipID = x.FacilityMembershipID },
                    x => x.QuantityUsed.HasValue && x.QuantityUsed != 0);

                foreach (var t in facilityClasses)
                {
                    int notStarted = 0;
                    int inProgress = 0;
                    int completed = 0;

                    var classData = classBuilderProvider.ClassRepo.GetClass(t.ClassID, x => new ClassDto { ClassName = x.ClassName });

                    classes.Add(new SelectListItem
                    {
                        Value = t.ClassID.ToString(),
                        Text = classData != null ? classData.ClassName : ""
                    });

                    if (t.FacilityMembershipID.HasValue)
                    {
                        var workflowIDs = classMemberProvider.StudentClassRepo.GetMembershipFacilityStudentClasses(item.FacilityCode, x => true, t.FacilityMembershipID,
                            x => new StudentClassDto { ClassWorkflowID = x.ClassWorkflowID });

                        foreach (var i in workflowIDs)
                        {
                            if (i.ClassWorkflowID == startClassWF) notStarted++;
                            else if (i.ClassWorkflowID == scoreTestWF || i.ClassWorkflowID == continueClassWF || i.ClassWorkflowID == completeTestWF || i.ClassWorkflowID == retakeTestWF) inProgress++;
                            else completed++;
                        }
                    }
                    else
                    {
                        var workflowIDs = classMemberProvider.StudentClassRepo.GetRegularFacilityStudentClassesByClassID(t.ClassID, item.FacilityCode, x => new StudentClassDto { ClassWorkflowID = x.ClassWorkflowID });

                        foreach (var i in workflowIDs)
                        {
                            if (i.ClassWorkflowID == startClassWF) notStarted++;
                            else if (i.ClassWorkflowID == scoreTestWF || i.ClassWorkflowID == continueClassWF || i.ClassWorkflowID == completeTestWF || i.ClassWorkflowID == retakeTestWF) inProgress++;
                            else completed++;
                        }
                    }

                    var classTotal = completed + inProgress + notStarted;

                    corpData.Add(CorporateReportViewModel.GetViewModel(item.Name, classData != null ? classData.ClassName : "", notStarted, inProgress, completed, classTotal));
                }
            }

            return corpData;
        }

        public static ReportsWidgetViewModel FilterProgressReportItems(ProgressReportFilterViewModel progressFilter, int userID, bool isSiteAdmin, bool isCorpAdmin,
            IClassMemberDependencies classMemberProvider, IClassBuilderDependencies classBuilderProvider)
        {
            var data = GetProgressReportData(classMemberProvider, classBuilderProvider, isSiteAdmin, isCorpAdmin, userID, progressFilter.FacilityID);

            var progData = data.ProgressReportData;

            if (progressFilter.LastAccessBegin != new DateTime()) progData = progData.Where(p => DateTime.Parse(p.LastAccessDay) >= progressFilter.LastAccessBegin).ToList();
            if (progressFilter.LastAccessEnd != new DateTime()) progData = progData.Where(p => DateTime.Parse(p.LastAccessDay) <= progressFilter.LastAccessEnd).ToList();
            if (progressFilter.Status != null) progData = progData.Where(p => p.Status.Contains(progressFilter.Status)).ToList();
            if (progressFilter.ClassID > 0) progData = progData.Where(p => p.ClassID == progressFilter.ClassID).ToList();
            if (progressFilter.StudentID > 0) progData = progData.Where(p => p.StudentID == progressFilter.StudentID).ToList();

            var model = new ReportsWidgetViewModel
            {
                ProgressReportData = progData,
                ProgressFilter = data.ProgressFilter,
                HasReportItems = data.HasReportItems
            };

            return model;
        }

        //public static ReportsWidgetViewModel GetComplianceData(UserInfo user)
        //{
        //    var facilities = GetFacilitiesForCurrentUser(user);

        //    var p = new List<SelectListItem>();

        //    foreach(var x in facilities)
        //    {
        //        var plans = db.EducationPlans.Where(e => e.FacilityID == x.FacilityID).Where(e => e.Enabled).ToList();

        //        if(plans.Count > 0)
        //        {
        //            p.Add(new SelectListItem
        //            {
        //                Disabled = true,
        //                Text = x.Name
        //            });

        //            foreach (var y in plans)
        //            {
        //                p.Add(new SelectListItem
        //                {
        //                    Disabled = false,
        //                    Value = y.EducationPlanID.ToString(),
        //                    Text = y.PlanName
        //                });
        //            }
        //        }
        //    }

        //    var model = new ReportsWidgetViewModel
        //    {
        //        EducationPlans = p
        //    };

        //    return model;
        //}

        #endregion
    }
}