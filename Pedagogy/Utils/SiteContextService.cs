﻿using Pedagogy.DataAccess.Services.Context;
using Kentico.Content.Web.Mvc;
using Kentico.Web.Mvc;

namespace Pedagogy.Utils
{
    public class SiteContextService : ISiteContextService
    {
        public string SiteName { get; }

        public string CurrentSiteCulture { get; }

        public string PreviewCulture => System.Web.HttpContext.Current.Kentico().Preview().CultureName;

        public bool IsPreviewEnabled => System.Web.HttpContext.Current.Kentico().Preview().Enabled;

        public SiteContextService(string currentCulture, string siteName)
        {
            CurrentSiteCulture = currentCulture;
            SiteName = siteName;
        }
    }
}