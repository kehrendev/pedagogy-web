﻿const MANAGE_FACILITY_CLASSES = "/Student-Dashboard/Admin-Functions/Manage/Facility-Classes";
const MANAGE_CORPORATION_PAGE = "/Student-Dashboard/Admin-Functions/Manage/Corporations";
const MANAGE_STAFF_PAGE = "/Student-Dashboard/Admin-Functions/Manage/Staff";
const MANAGE_FACILITY_PAGE = "/Student-Dashboard/Admin-Functions/Manage/Facilities";