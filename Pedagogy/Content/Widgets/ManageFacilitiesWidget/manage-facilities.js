﻿$(function () {
    console.log('manage-facilities.js loaded..');

    $(document).ready(function () {
        if ($('#facilityList').length) {
            GetFacilitiesPagedData(1);
        }

        $("#facilityFilterForm").submit(function (e) {
            e.preventDefault();
            GetFacilitiesPagedData(1);
        });

        // displaying the new facility form
        $('#newFacility').click(function () {
            showFacilityForms();
            $('#facilityInfoTab').click();
        });

        // show all of the corporations
        $('#viewAllFacilitiesBtn').click(function () {
            GetFacilitiesPagedData(1);
        });

        // going back from the facility forms to the listing
        $('#backToFacilityListingBtn').click(function () {
            $(this).hide();
            $('.facility-forms').hide();
            $('.facility-listing-objects').show();
            $('#facility-code').show();
            $('#add-facility-btn').hide();
            $('#manageCorpsBtn').show();
            $('#viewAllFacilitiesBtn').show();
            $('#newFacility').show();

            resetAllTabsToDefault();

            GetFacilitiesPagedData(1);
        });

        // switching between the facility tabs
        $('#facilityTabs').on('click', '.nav-link', function (e) {
            e.preventDefault();
            var facilityID = $('.facilityID').val();

            $('#manageEnrollment').hide();

            $('#facilityTabs .nav-link').removeClass('active show');
            $('#facilityTabContent .tab-pane').removeClass('active show');

            $(this).toggleClass('active show');
            $($(this).attr('href')).toggleClass('active show');

            switch ($(this).attr('id')) {
                case 'preferencesTab':
                    // get the facility preferences if any
                    if (facilityID != 0) {
                        $.ajax({
                            url: '/ManageFacilitiesWidget/GetFacilityPreferences',
                            method: 'GET',
                            data: { 'facilityID': facilityID },
                            success: function (obj) {
                                var data = JSON.parse(obj);

                                if (data.emailMonthly) {
                                    $('#facilityPrefEmailMonthlyProgress').attr('checked', 'checked');
                                }

                                if (data.assignHowTo) {
                                    $('#facilityPrefAssignHowToPedagogy').attr('checked', 'checked');
                                }
                            }
                        });
                    }

                    break;

                case 'contactsTab':
                    // get the contacts list if any
                    if (facilityID != 0) {
                        $('#contactListLoading').show();
                        $('#contactsContainer').html('');

                        $.ajax({
                            url: '/ManageFacilitiesWidget/GetFacilityContacts',
                            method: 'GET',
                            data: { 'facilityID': facilityID },
                            success: function (obj) {
                                $('#contactListLoading').hide();
                                updateFacilityContactList(JSON.parse(obj));
                            }
                        });
                    }

                    break;

                case 'staffMembersTab':
                    // get the staff members list if any (similar process as contacts)
                    if (facilityID != 0) {
                        $('#staffMembersFilter').show();
                        $('#staffMembersPanelHeader').show();
                        $('#manageStudentClasses').hide();
                        $('#addExisitingStudent').hide();
                        $('#addNewStudent').hide();

                        GetStaffMembersPagedData(1);
                    }

                    break;

                case 'printCertsTab':
                    // get the student certificates if any
                    if (facilityID != 0) {
                        $('#studentCertsData').hide();
                        $('#staffCertsLoading').show();

                        var studentListContainer = $('#studentCertsList');
                        studentListContainer.html('');

                        $.ajax({
                            url: '/ManageFacilitiesWidget/GetFacilityStaffCerts',
                            method: 'GET',
                            data: { 'facilityID': facilityID },
                            success: function (obj) {
                                var data = JSON.parse(obj);

                                $('#staffCertsLoading').hide();

                                if (Object.keys(data.Students).length > 0) {
                                    $('#studentCertsData').show();
                                    $('#noStudentCertsTxt').hide();

                                    for (var i = 0; i < Object.keys(data.Students).length; i++) {
                                        var rowData = `<li class="m-2">
                                                            <label>
                                                                <input type="checkbox" name="ids" value="${Object.keys(data.Students)[i]}" class="checkboxes" /> ${Object.values(data.Students)[i]}
                                                            </label>
                                                        </li>`;

                                        studentListContainer.append(rowData);
                                    }
                                }
                                else {
                                    $('#noStudentCertsTxt').show();
                                }
                            }
                        });
                    }

                    break;

                case 'membershipsTab':
                    // get the memberships if any
                    if (facilityID != 0) {
                        $('#facilityMembershipLoading').show();
                        $('#facilityMembershipListing').html('');

                        $.ajax({
                            url: '/ManageFacilitiesWidget/GetFacilityMemberships',
                            method: 'GET',
                            data: { 'facilityID': facilityID },
                            success: function (obj) {
                                var data = JSON.parse(obj);

                                $('#facilityMembershipLoading').hide();

                                // populate the listing
                                updateFacilityMembershipList(data);
                            }
                        });
                    }

                    break;

                case 'classesTab':
                    // get the classes if any
                    if (facilityID != 0) {
                        $('#facilityClassLoading').show();

                        $.ajax({
                            url: '/ManageFacilitiesWidget/GetFacilityClasses',
                            method: 'GET',
                            data: { 'facilityID': facilityID },
                            success: function (obj) {
                                var data = JSON.parse(obj);

                                $('#facilityClassLoading').hide();

                                $('#facilityClassListing').show();
                                $('#facilityClassListing').html('');

                                // populate the listing
                                updateFacilityClassList(data);
                            }
                        });
                    }

                    break;

                default:
                    // display the facility info tab by default
                    if (facilityID != 0) {
                        $.ajax({
                            url: '/ManageFacilitiesWidget/GetFacilityData',
                            method: 'GET',
                            data: { 'facilityID': facilityID },
                            success: function (obj) {
                                var data = JSON.parse(obj);

                                $('.facilityID').val(data.FacilityID);

                                $('#facilityCode').html('- ' + data.FacilityCode);
                                $('#facilityName').val(data.Name);
                                $('#facilityParentDropdown').val(data.FacilityParentID);

                                if (data.Enabled) {
                                    $('#facilityEnabled').prop('checked', true);
                                }
                                else {
                                    $('#facilityEnabled').prop('checked', false);
                                }

                                $('#facilityAddressLine1').val(data.AddressLine1);
                                $('#facilityAddressLine2').val(data.AddressLine2);
                                $('#facilityAddressCity').val(data.City);
                                $('#facilityAddressState').val(data.StateID);
                                $('#facilityAddressZip').val(data.Zip);
                            }
                        });
                    }
            }
        });

        // clicking the edit facility button
        $(document).on('click', '.update-facility-btn', function () {
            $('html').scrollTop(0);
            showFacilityForms();

            $('.facilityID').val($(this).attr('data-facilityid'));
            $('#facilityInfoTab').click();
        });

        // updating or creating the facility info
        $('#updateFacilityInfoForm').submit(function (e) {
            e.preventDefault();

            var errs = $(this).find('.field-validation-error');
            if (errs.length == 0) {

                // display loading modal
                $('#facilityProcessLoadingModal').show();

                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        // hide loading modal
                        $('#facilityProcessLoadingModal').hide();

                        $('.facilityID').val(data.FacilityID);
                        $('#saveChangesModal').css('display', 'flex');
                    }
                });
            }
        });

        // updating the facility preferences
        $('#updateFacilityPreferencesForm').submit(function (e) {
            e.preventDefault();

            var errs = $(this).find('.field-validation-error');

            // display loading modal
            $('#facilityProcessLoadingModal').show();

            if (errs.length == 0) {
                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        // hide loading modal
                        $('#facilityProcessLoadingModal').hide();

                        $('.facilityID').val(data.FacilityID);
                        $('#saveChangesModal').css('display', 'flex');
                    }
                });
            }
        });

        // clearing the facility filter form
        $('#clearFacilityFilterBtn').click(function () {
            $('.facility-filter-txt').val('');
            $('.facility-filter-dropdown').val($('.facility-filter-dropdown option:nth-child(1)').val());

            GetFacilitiesPagedData(1);
        });
        
        // showing the new contact form
        $(document).on('click', '#addNewContactBtn', function () {
            $('#contactsContainer').hide();
            $('#updateFacilityContactForm').show();
            $(this).hide();
        });

        // populating the contact form with an existing contacts data
        $(document).on('click', '.edit-contact', function () {
            $.ajax({
                url: '/ManageFacilitiesWidget/GetContactData',
                method: 'GET',
                data: { 'contactID': $(this).attr('data-contactid') },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    $('.contactID').val(data.ContactID);
                    $('#contactUserID').val(data.ContactUserID)

                    $('#facilityContactTypeDropdown').val(data.TypeID)
                    $('#facilityContactFirstName').val(data.FirstName);
                    $('#facilityContactLastName').val(data.LastName);
                    $('#facilityContactEmail').val(data.Email);
                    $('#facilityContactPhone').val(formatPhoneNumber(data.Phone));

                    if (data.AllowOnlinePurchases) {
                        $('#facilityContactAllowPurchases').attr('checked', 'checked');
                    }

                    if (data.HasUserAcct) {
                        $('#facilityContactNeedsUserAcct').hide();
                        $('#facilityContactUserName').show();
                        $('#facilityContactUserName').children('div').html(data.UserName);
                    }

                    $('#addNewContactBtn').hide();
                    $('#contactsContainer').hide();
                    $('#updateFacilityContactForm').show();
                }
            });            
        });

        // canceling out of the contact form
        $('#cancelContactForm').click(function () {
            closeNewFacilityContactForm();
            clearFacilityContactForm();
        });

        // updating an existing contacts info
        $('#updateFacilityContactForm').submit(function (e) {
            e.preventDefault();

            var errs = $(this).find('.field-validation-error');
            if (errs.length == 0) {
                // display loading modal
                $('#facilityProcessLoadingModal').show();

                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        // hide loading modal
                        $('#facilityProcessLoadingModal').hide();


                        if (data.Err) {
                            $('#contactErrMsg').html(data.ErrMsg);
                        }
                        else {
                            //update contact list
                            updateFacilityContactList(data.Contacts);

                            clearFacilityContactForm();
                            closeNewFacilityContactForm();                            
                        }
                    }
                });
            }            
        });

        // showing confirm delete modal for deleting a contact
        $('.manage-facilities').on('click', '.delete-contact', function () {
            var contactID = $(this).attr('data-contactid');
            var contactName = $(this).attr('data-contactname');

            var modal = $('#deleteContactConfirmModal');
            $(modal).find('#deleteContactName').html(contactName);
            $(modal).find('#removeContactBtn').attr('data-contactid', contactID);

            $(modal).show();
        });

        // close delete contact confirm modal
        $('.close-delete-contact').click(function (e) {
            e.preventDefault();

            closeDeleteFacilityContactModal();
        });

        // deleting contact
        $('.manage-facilities').on('click', '#removeContactBtn', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/ManageFacilitiesWidget/RemoveContact',
                method: 'POST',
                data: { 'contactID': $(this).attr('data-contactid'), 'facilityID': $('.facilityID').val() },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    if (data.Err) {
                        $('#contactErrMsg').html(data.ErrMsg);
                    }
                    else {
                        updateFacilityContactList(data.Contacts);
                        closeDeleteFacilityContactModal();                        
                    }
                }
            });
        });

        // displaying an input for a new users user name and password email recipient
        $('#needsUserAcct').click(function () {
            var userPassInput = $('#userPassRecipientForm');
            var emailInput = $('#facilityContactEmail');

            if ($(this).find('input[type=checkbox]').prop('checked') == true && emailInput.val() == "") {
                userPassInput.css('display', 'block');
                emailInput.hide();
                emailInput.siblings('label').hide();
            }
            else {
                userPassInput.css('display', 'none');
                emailInput.show();
                emailInput.siblings('label').show();
            }
        });

        // shows a confirm delete facility modal
        $(document).on('click', '.delete-facility-btn', function (e) {
            e.preventDefault();

            var facilityID = $(this).attr('data-facilityid');
            var facilityName = $(this).attr('data-facilityname');

            var modal = $('#deleteFacilityConfirmModal');

            modal.find('#facilityName').html(facilityName);
            modal.find('#removeFacilityBtn').attr('data-facilityid', facilityID);
            modal.show();
        });

        // closes confirm delete facility modal
        $('.close-delete-facility-modal').click(function (e) {
            e.preventDefault();
            closeDeleteFacilityModal();
        });

        // removes a facility
        $('#removeFacilityBtn').click(function (e) {
            e.preventDefault();

            $('#facilityProcessLoadingModal').show();
            var facilityID = $(this).attr('data-facilityid');
            closeDeleteFacilityModal();
            $.ajax({
                url: '/ManageFacilitiesWidget/RemoveFacility',
                method: 'POST',
                data: { 'facilityID': facilityID },
                success: function () {
                    $('#facilityProcessLoadingModal').hide();
                    GetFacilitiesPagedData(1);
                }
            })
        });

        // takes the user directly to the manage staff tab
        $(document).on('click', '.manage-staff-btn', function (e) {
            e.preventDefault();

            var facilityID = $(this).attr('data-facilityid');
            $('.facilityID').val(facilityID);

            showFacilityForms();
            $('#staffMembersTab').click();
        });

        // takes the user directly to the manage classes tab
        $(document).on('click', '.manage-class-btn', function (e) {
            e.preventDefault();

            var facilityID = $(this).attr('data-facilityid');
            $('.facilityID').val(facilityID);

            showFacilityForms();
            $('#classesTab').click();
        });

        // opens the add a new staff member form
        $('#addNewStaffBtn').click(function () {
            $(this).hide();
            $('#staffMembersList').hide();
            $('#addExistingStaffBtn').hide();
            $('#addExisitingStudent').hide();
            $('#staffMembersPagination').hide();
            $('#staffMembersFilter').hide();
            $('#noStaffMembersTxt').hide();
            $('#addNewStudent').show();
        });

        // opens the add an existing staff member form
        $('#addExistingStaffBtn').click(function () {
            $(this).hide();
            $('#staffMembersList').hide();
            $('#addNewStaffBtn').hide();
            $('#addNewStudent').hide();
            $('#staffMembersPagination').hide();
            $('#staffMembersFilter').hide();
            $('#noStaffMembersTxt').hide();
            $('#addExisitingStudent').show();
        });

        // closes the add an existing staff member form
        $('#cancelAddExistingStudent').click(function () {
            closeAddExistingStudentForm();
        });

        // closes the add a new staff member form
        $('#cancelAddNewStudent').click(function () {
            closeAddNewStudentForm();
        });

        // doesn't allow spaces when create new students or adding an existing student to a facility
        $('.no-spaces-validation').on({
            keydown: function (e) {
                if (e.which === 32) {
                    return false;
                }
            }
        });

        // submits the existing students data
        $('#addExistingStudentForm').submit(function (e) {
            e.preventDefault();

            var facilityID = $('.facilityID').val();
            var errs = $(this).find('.field-validation-error');

            $('#facilityProcessLoadingModal').show();

            if (facilityID != 0 && errs.length == 0) {
                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        $('#facilityProcessLoadingModal').hide();

                        if (data.Err) {
                            $('#addExistingStudentErrMsg').html(data.ErrMsg);
                        }
                        else {
                            closeAddExistingStudentForm();
                            updateFacilityStaffMembersList(data.Students);
                        }
                    }
                });
            }
        });

        // submits the new student(s) data
        $('#addNewStudentForm').submit(function (e) {
            e.preventDefault();

            var facilityID = $('.facilityID').val();
            var errs = $(this).find('.field-validation-error');

            if (facilityID != 0 && errs.length == 0) {
                $('#facilityProcessLoadingModal').show();

                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        $('#facilityProcessLoadingModal').hide();

                        if (data.Err) {
                            $('#addNewStudentErrMsg').html(data.ErrMsg);
                        }
                        else {
                            closeAddNewStudentForm();
                            GetStaffMembersPagedData(1);
                        }
                    }
                });
            }
        });

        // adds a new line for another new staff member
        $('#addStudentInput').click(function () {
            var students = $.makeArray($('.student'));

            $.map(students, function (val, i) {
                var studentCount = $(val).find('.student-count');
                studentCount.html(i + 1);
            });

            var index = students.length;
            var studentsContainer = $('#students');

            var student = `<div class="student row justify-content-start">
                                <div class="col-12 col-md-auto d-flex">
                                    <span class="my-auto student-count">${index + 1}</span>
                                </div>
                                <div class="col-12 col-md-11 row justify-content-evenly">
                                    <div class="col-12 col-md-6 col-lg-3 my-2 my-lg-0">
                                        <div class="mx-md-2 mx-lg-0">
                                            <input class="form-control student-data-txt" placeholder="First Name" data-val="true" data-val-required="The First Name cannot be empty." id="ContactModels_${index}__FirstName" name="ContactModels[${index}].FirstName" type="text" value="">
                                            <span class="text-danger student-data-validation" data-valmsg-for="ContactModels[${index}].FirstName" data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-3 my-2 my-lg-0">
                                        <div class="mx-md-2 mx-lg-0">
                                            <input class="form-control student-data-txt" placeholder="Last Name" data-val="true" data-val-required="The Last Name cannot be empty." id="ContactModels_${index}__LastName" name="ContactModels[${index}].LastName" type="text" value="">
                                            <span class="text-danger student-data-validation" data-valmsg-for="ContactModels[${index}].LastName" data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 my-2 my-lg-0">
                                        <div class="mx-md-2 mx-lg-0">
                                            <input class="form-control student-data-txt" placeholder="Email" data-val="true" data-val-email="Invalid email address." data-val-maxlength="The Email address cannot be longer than 320 characters." data-val-maxlength-max="320" id="ContactModels_${index}__Email" name="ContactModels[${index}].Email" type="text" value="">
                                        </div>
                                    </div>
                                    <div>
                                        <button type="button" class="fa fa-minus-circle remove-student"></button>
                                    </div>
                                </div>
                            </div>`;

            studentsContainer.append(student);
        });

        // removes a new staff member line from the form
        $('#students').on('click', '.remove-student', function () {
            $(this).parents('.student').remove();

            // fix the list indexes
            var students = $.makeArray($('.student'));
            $.map(students, function (val, i) {
                var studentCount = $(val).find('.student-count');
                studentCount.html(i + 1);

                var inputs = $.makeArray($(val).find('input'));

                $.map(inputs, function (input, index) {
                    var name = $(input).attr('name');
                    var nameArr = name.split('.');

                    $(input).attr('name', 'ContactModels[' + i + '].' + nameArr[1])
                });
            });
        });

        // opens up the edit student forms and populates all the fields
        $('#staffMembersList').on('click', '.edit-student', function (e) {
            e.preventDefault();

            var contactID = $(this).attr('data-contactid');

            if (contactID != 0) {
                // get the contacts data
                $.ajax({
                    url: '/ManageFacilitiesWidget/GetStudentData',
                    method: 'GET',
                    data: { 'contactID': contactID },
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        // show/hide the forms
                        $('#staffMembersPanelHeader').hide();
                        $('#staffMembersList').hide();
                        $('#staffMembersPagination').hide();
                        $('#staffMembersFilter').hide();
                        $('#editStudent').show();

                        // populate the form fields
                        $('.contactID').val(data.ContactID);

                        $('.contact-userID').val(data.ContactUserID);
                        $('#studentFirstName').val(data.FirstName);
                        $('#studentLastName').val(data.LastName);
                        $('#studentEnabled').prop('checked', data.Enabled);

                        $('#studentUserID').html(data.ContactUserID);
                        $('#studentMemberSince').html(data.MemberSince);
                        $('#studentUserName').html(data.UserName);

                        $('#studentCertFirstName').val(data.CertFirstName);
                        $('#studentCertLastName').val(data.CertLastName);
                        $('#studentEmail').val(data.Email);

                        if (data.NALicense) {
                            $('#studentLicenseNACheckbox').prop('checked', true);
                            $('#newStudentLicenseForm').hide();
                        }
                        else {
                            $('#studentLicenseNACheckbox').prop('checked', false);
                            $('#newStudentLicenseForm').show();
                            updateStudentLicenseList(data.StudentLicenses);
                        }
                    }
                });
            }            
        });

        // showing confirm delete modal for deleting a student
        $('.manage-facilities').on('click', '.delete-student', function (e) {
            e.preventDefault();

            var contactID = $(this).attr('data-contactid');
            var contactName = $(this).attr('data-contactname');

            var modal = $('#deleteStudentConfirmModal');
            $(modal).find('#deleteStudentName').html(contactName);
            $(modal).find('#removeStudentBtn').attr('data-contactid', contactID);

            $(modal).show();
        });

        // closes delete student modal
        $('.manage-facilities').on('click', '.close-delete-student-modal', function () {
            var modal = $('#deleteStudentConfirmModal');

            modal.find('#deleteStudentName').html('');
            modal.find('#removeStudentBtn').attr('data-contactid', '');

            modal.hide();
        });

        // removes a student from the facility
        $('.manage-facilities').on('click', '#removeStudentBtn', function (e) {
            e.preventDefault();

            var contactID = $(this).attr('data-contactid');

            if (contactID != 0) {
                var modal = $('#deleteStudentConfirmModal');

                modal.find('#deleteStudentName').html('');
                modal.find('#removeStudentBtn').attr('data-contactid', '');

                modal.hide();

                // display loading modal
                $('#facilityProcessLoadingModal').show();

                $.ajax({
                    url: '/ManageFacilitiesWidget/RemoveStudent',
                    method: 'POST',
                    data: { 'contactID': contactID, 'facilityID': $('.facilityID').val() },
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        // close loading modal
                        $('#facilityProcessLoadingModal').hide();

                        updateFacilityStaffMembersList(data);
                    }
                });
            }
        });

        // populates the manage staff classes section
        $('#staffMembersList').on('click', '.manage-student-classes', function (e) {
            e.preventDefault();

            // display loading modal
            $('#facilityProcessLoadingModal').show();

            var facilityID = $('.facilityID').val();
            var contactID = $(this).attr('data-contactid');

            if (contactID != 0 && facilityID != 0) {
            // run an ajax call to get facility class listing
                $.ajax({
                    url: '/ManageFacilitiesWidget/GetManageStudentClassesData',
                    mehtod: 'GET',
                    data: {
                        'contactID': contactID,
                        'facilityID': facilityID
                    },
                    // on success
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        // close loading modal
                        $('#facilityProcessLoadingModal').hide();

                        // close the staff list and header
                        $('#staffMembersList').hide();
                        $('#staffMembersPagination').hide();
                        $('#staffMembersFilter').hide();
                        $('#staffMembersPanelHeader').hide();

                        var container = $('#manageStudentClasses');

                        // display a back to staff member listing button
                        container.show();

                        // display student's name
                        container.find('#studentName').html(data.StudentName);

                        // update class listing
                        updateManageStudentClassListing(data.FacilityClasses, data.StudentID);
                    }
                });
            }
        });

        // populates the student license form with an existing student licenses data
        $(document).on('click', '.edit-student-license', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/ManageFacilitiesWidget/GetStudentLicenseData',
                method: 'GET',
                data: { 'licenseID': $(this).attr('data-studentlicenseid'), 'studentID': $('.contactID').val() },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    //populate form fields
                    $('#studentLicenseID').val(data.LicenseID);
                    $('#studentLicenseTypeDropdown').val(data.LicenseTypeID);
                    $('#studentLicenseNumber').val(data.LicenseNumber);
                    $('#studentLicenseState').val(data.StateID);

                    // display the correct form buttons
                    $('#addStudentLicenseBtn').hide();
                    $('#updateStudentLicenseBtn').show();
                    $('#cancelUpdateStudentLicenseBtn').show();
                }
            });
        });

        // cancels update student license
        $('#cancelUpdateStudentLicenseBtn').click(function () {
            clearNewStudentLicenseForm();
        });

        // submits the updated license to be saved
        $('#newStudentLicenseForm').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (obj) {
                    var data = JSON.parse(obj);

                    // show/hide/clear things
                    clearNewStudentLicenseForm();

                    //update the list
                    updateStudentLicenseList(data);
                }
            });
        });

        // removes an existing student license
        $(document).on('click', '.delete-student-license', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/ManageFacilitiesWidget/RemoveStudentLicense',
                method: 'POST',
                data: { 'licenseID': $(this).attr('data-studentlicenseid'), 'studentID': $('.contactID').val() },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    //update the license list
                    updateStudentLicenseList(data);
                }
            });
        });

        // saves the students user info
        $('#updateStaffUserInfoForm').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function () {
                    //display changes saved modal
                    $('#saveChangesModal').show();
                }
            });
        });

        // saves the students student info
        $('#updateStudentInfoForm').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function () {
                    //display changes saved modal
                    $('#saveChangesModal').show();
                }
            });
        });

        // adds or removes the NA Student License
        $('#studentLicenseNACheckbox').click(function () {
            if ($(this).is(':checked')) {
                $('#newStudentLicenseForm').hide();
                $('#studentLicenseList').hide();

                $.ajax({
                    url: '/ManageFacilitiesWidget/AddNotApplicableLicense',
                    method: 'GET',
                    data: { 'studentID': $('.contactID').val() },
                    success: function () {
                        //display changes saved modal
                        $('#saveChangesModal').show();
                    }
                });
            }
            else {
                $('#newStudentLicenseForm').show();
                $('#studentLicenseList').show();

                $.ajax({
                    url: '/ManageFacilitiesWidget/RemoveNotApplicableLicense',
                    method: 'GET',
                    data: { 'studentID': $('.contactID').val() },
                    success: function () {
                        //display changes saved modal
                        $('#saveChangesModal').show();
                    }
                });
            }
        });

        // displays a modal for changing the users password
        $('#changeUserPass').click(function () {
            $('#studentChangePassModal').show();
        });

        // if default password is selected then hide the form
        $(document).on('click', '#studentDefaultPassword', function () {
            if ($(this).find('input[type="checkbox"]').prop('checked')) {
                $('#studentPasswordInfo').hide();
            }
            else {
                $('#studentPasswordInfo').show();
            }
        });

        // closes the change user pass modal
        $('#closeStudentChangePassModal').click(function () {
            var modal = $('#studentChangePassModal');

            // clear fields
            modal.find('.student-data-checkbox').prop('checked', false);
            modal.find('.student-data-txt').val('');
            modal.find('.student-data-validation').html('');

            $(".characters").removeClass("text-success");
            $(".lowercase").removeClass("text-success");
            $(".uppercase").removeClass("text-success");
            $(".numeric").removeClass("text-success");

            modal.hide();
        });

        // changes users password
        $('#changeStudentPassForm').submit(function (e) {
            e.preventDefault();

            var err = $('.field-validation-error');

            if (err.length == 0) {
                $('#facilityProcessLoadingModal').show();
                $('#studentChangePassModal').hide();
                
                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);

                        $('#facilityProcessLoadingModal').hide();

                        if (data.Err) {
                            $('#studentChangePassModal').show();
                            $('#studentChangePassErrMsg').html(data.ErrMsg);
                        }
                        else {
                            // reset form values
                            $("#characters").removeClass("text-success");
                            $("#lowercase").removeClass("text-success");
                            $("#uppercase").removeClass("text-success");
                            $("#numeric").removeClass("text-success");

                            $('#changeStudentPassForm').find('.student-data-checkbox').prop('checked', false);
                            $('#changeStudentPassForm').find('.student-data-txt').val('');

                            $('#studentPasswordInfo').show();

                            //display changes saved modal
                            $('#saveChangesModal').show();
                        }
                    }
                });
            }
        });

        // checks to see if the users password is valid
        $("#password").keyup(checkPassword);

        // impersonate user action
        $("#impersonateStudentBtn").click(function () {
            $.ajax({
                method: "POST",
                url: "/LoginManager/ImpersonateUser",
                data: { 'userID': $('.contact-userID').val() },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    $('#impersonateErrModal').find('.err-msg').html('');

                    if (data.Err) {
                        $('#impersonateErrModal').show();
                        $('#impersonateErrModal').find('.err-msg').html(data.ErrMsg);
                    }
                    else {
                        $('#impersonateErrModal').hide();

                        window.location.href = data.ReturnUrl;
                    }
                }
            });
        });

        // cancels out of the update forms and goes back to the staff listing
        $('.back-to-staff-listing').click(function () {
            // clear all forms
            clearNewStudentLicenseForm();

            $('.student-data-validation').html('');
            $('.student-data-txt').val('');
            $('.student-data-checkbox').prop('checked', false);
            $('.student-data-dropdown').val($('.student-data-dropdown option:nth-child(1)').val());

            // show/hide things
            $('#staffMembersPanelHeader').show();
            $('#staffMembersList').show();
            $('#staffMembersFilter').show();
            $('#editStudent').hide();

            var manageStudentClassesContainer = $('#manageStudentClasses');
            manageStudentClassesContainer.find('#studentName').html('');
            manageStudentClassesContainer.hide();

            // reload the tab to show any changes
            $('#staffMembersTab').click();
        });

        // setting up the update facility class qty
        $(document).on('click', '.update-facility-class-qty', function (e) {
            e.preventDefault();

            if ($(this).attr('data-inpackage') == 'true') {
                // display warning modal
                var modal = $('#facilityPackageWarningModal');
                modal.show();
                modal.find('#updateFacilityClass').attr('data-facilityclassid', $(this).attr('data-facilityclassid'));
            }
            else {
                // display save input and button
                $(this).parent('.current-qty').hide();
                $(this).parent().siblings('.updated-qty').show();
            }
        });

        // continues on to updated a package qty
        $('#updateFacilityClass').click(function (e) {
            e.preventDefault();

            $('#facilityPackageWarningModal').hide();

            var facilityClass = $(`#${$(this).attr('data-facilityclassid')}`);

            facilityClass.find('.current-qty').hide();
            facilityClass.find('.updated-qty').show();
        });

        // saving updated facility class qty
        $(document).on('click', '.save-facility-class-qty', function (e) {
            e.preventDefault();

            $('#facilityProcessLoadingModal').show();

            $.ajax({
                url: '/ManageFacilitiesWidget/UpdateFacilityClassQty',
                method: 'POST',
                data: {
                    'facilityClassID': $(this).attr('data-facilityclassid'),
                    'classID': $(this).attr('data-classid'),
                    'facilityID': $(this).attr('data-facilityid'),
                    'invoiceNumber': $(this).attr('data-invoicenumber'),
                    'quantity': $(this).siblings('.input-qty').val()
                },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    $('#facilityProcessLoadingModal').hide();

                    // check to see if the item(s) were removed
                    if (data.Removed) {
                        // remove all package items
                        if (data.UpdatedClasses.length > 0) {
                            for (var i = 0; i < data.UpdatedClasses.length; i++) {
                                var facilityClass = $(`#${data.UpdatedClasses[i]}`).parent();
                                facilityClass.remove();
                            }
                        }
                        // remove single item
                        else {
                            var facilityClass = $(`#${data.FacilityClassID}`).parent();
                            facilityClass.remove();
                        }
                    }
                    else {
                        // update all bundle items qtys
                        if (data.UpdatedClasses.length > 0) {
                            for (var i = 0; i < data.UpdatedClasses.length; i++) {
                                var facilityClass = $(`#${data.UpdatedClasses[i]}`);

                                // current one shown
                                if (data.UpdatedClasses[i] == data.FacilityClassID) {
                                    var currentQtyContainer = facilityClass.find('.current-qty');
                                    currentQtyContainer.show();
                                    currentQtyContainer.find('.qty-purchased-txt').html(data.Quantity);

                                    var updatedQtyContainer = facilityClass.find('.updated-qty');
                                    var updatedQtyInput = updatedQtyContainer.find('.input-qty');

                                    updatedQtyInput.val(data.Quantity);
                                    updatedQtyInput.attr('max', data.Quantity);

                                    updatedQtyContainer.hide();
                                }
                                // others
                                else {
                                    var currentQtyContainer = facilityClass.find('.current-qty');
                                    currentQtyContainer.find('.qty-purchased-txt').html(data.Quantity);

                                    var updatedQtyContainer = facilityClass.find('.updated-qty');
                                    var updatedQtyInput = updatedQtyContainer.find('.input-qty');

                                    updatedQtyInput.val(data.Quantity);
                                    updatedQtyInput.attr('max', data.Quantity);
                                }
                            }
                        }
                        // update single class qty
                        else {
                            var facilityClass = $(`#${data.FacilityClassID}`);

                            var currentQtyContainer = facilityClass.find('.current-qty');
                            currentQtyContainer.show();
                            currentQtyContainer.find('.qty-purchased-txt').html(data.Quantity);

                            var updatedQtyContainer = facilityClass.find('.updated-qty');
                            var updatedQtyInput = updatedQtyContainer.find('.input-qty');

                            updatedQtyInput.val(data.Quantity);
                            updatedQtyInput.attr('max', data.Quantity);

                            updatedQtyContainer.hide();
                        }
                    }

                    var facilityClasses = $('.facility-class');
                    if (facilityClasses.length == 0) {
                        var facilityClassContainer = $('#facilityClassListing');
                        facilityClassContainer.find('.no-facility-classes-txt').show();
                        facilityClassContainer.find('#facilityClassListing').hide();
                    }

                    $('.updated-qty').each(function () {
                        var newQty = $(this).find('input').val();
                        $(this).siblings('.current-qty').find('.qty-purchased-txt').html(newQty);

                        $(this).hide();
                    });
                    $('.current-qty').show();
                }
            });
        });

        // closes facility class package warning modal
        $('.close-facility-package-warning-modal').click(function (e) {
            e.preventDefault();

            $('#facilityPackageWarningModal').hide();
        });

        // setting up the update facility membership qty
        $(document).on('click', '.update-facility-mem-qty', function (e) {
            e.preventDefault();

            // display save input and button
            $(this).parent('.current-qty').hide();
            $(this).parent().siblings('.updated-qty').show();
        });

        // saving updated facility membership qty
        $(document).on('click', '.save-facility-mem-qty', function (e) {
            e.preventDefault();

            $('#facilityProcessLoadingModal').show();

            $.ajax({
                url: '/ManageFacilitiesWidget/UpdateFacilityMembershipQty',
                method: 'POST',
                data: {
                    'facilityID': $(this).attr('data-facilityid'),
                    'membershipID': $(this).attr('data-membershipid'),
                    'invoiceNumber': $(this).attr('data-invoicenumber'),
                    'quantity': $(this).siblings('.input-qty').val()
                },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    $('#facilityProcessLoadingModal').hide();

                    // check to see if the membership was removed
                    if (data.Removed) {
                        var membership = $(`#${data.MembershipID}`).parent();
                        membership.remove();
                    }
                    else {
                        var membership = $(`#${data.MembershipID}`);

                        var currentQtyContainer = membership.find('.current-qty');
                        currentQtyContainer.show();
                        currentQtyContainer.find('.qty-purchased-txt').html(data.Quantity);

                        var updatedQtyContainer = membership.find('.updated-qty');
                        var updatedQtyInput = updatedQtyContainer.find('.input-qty');

                        updatedQtyInput.val(data.Quantity);
                        updatedQtyInput.attr('max', data.Quantity);

                        updatedQtyContainer.hide();
                    }

                    var memberships = $('.facility-membership');
                    if (memberships.length == 0) {
                        $('#facilityMembershipListing').append('<p id="noFacilityMembershipsTxt">No memberships found.</p>');
                    }

                    $('.updated-qty').each(function () {
                        var newQty = $(this).find('input').val();
                        $(this).siblings('.current-qty').find('.qty-purchased-txt').html(newQty);

                        $(this).hide();
                    });
                    $('.current-qty').show();
                }
            });
        });

        // manage enrollment for regular facility classes
        $(document).on('click', '.class-manage-enrollment', function (e) {
            e.preventDefault();

            // show loading modal
            $('#facilityProcessLoadingModal').show();

            // run an ajax call to get the staff members in this facility
            $.ajax({
                url: '/ManageFacilitiesWidget/GetFacilityClassEnrollmentData',
                method: 'GET',
                data: {
                    'facilityID': $(this).attr('data-facilityid'),
                    'classID': $(this).attr('data-classid'),
                    'invoiceNum': $(this).attr('data-invoicenumber')
                },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    // close loading modal
                    $('#facilityProcessLoadingModal').hide();

                    // hide facility class listing & title
                    var panelHeader = $('#classesPanelHeader');
                    panelHeader.find('#classesPanelTitle').hide();
                    panelHeader.find('#backToFacilityClassListing').show();

                    var courseTitle = panelHeader.find('#manageCourseTitle');
                    courseTitle.show();
                    courseTitle.html(data.ClassName);

                    $('#facilityClassListing').hide();

                    // show manage enrollment section
                    $('#manageEnrollment').show();

                    // populate the section with staff members
                    updateManageEnrollmentStaffList(data.Students, 0);
                }
            });            
        });

        // takes user from the manage enrollment section back to the facility class listing section
        $('#classesPanelHeader').on('click', '#backToFacilityClassListing', function () {
            $(this).hide();
            $('#manageEnrollment').hide();
            $(this).siblings('#manageCourseTitle').html('');
            $(this).siblings('#classesPanelTitle').show();

            $('#classesTab').click();
        });

        // assigns the class to the student
        $(document).on('click', '.assign-class', function () {
            // show loading modal
            $('#facilityProcessLoadingModal').show();

            // make an ajax call to assign the course to the student
            $.ajax({
                url: '/ManageFacilitiesWidget/AssignClass',
                method: 'POST',
                data: {
                    'studentID': $(this).attr('data-studentid'),
                    'classID': $(this).attr('data-classid'),
                    'facilityID': $(this).attr('data-facilityid'),
                    'invoiceNumber': $(this).attr('data-invoicenumber'),
                    'facilityMemID': $(this).attr('data-facilitymemID'),
                    'facilityClassListing': $(this).attr('data-facilityClassListing')
                },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    // hide loading modal
                    $('#facilityProcessLoadingModal').hide();

                    if (data.FacilityClassListing) {
                        // refresh the facility class list
                        updateManageStudentClassListing(data.Students, data.StudentID);
                    }
                    else {
                        // refresh the student list
                        updateManageEnrollmentStaffList(data.Students, data.MembershipID);
                    }
                }
            });
        });

        // unassigns the class from the student
        $(document).on('click', '.unassign-class', function () {
            // show loading modal
            $('#facilityProcessLoadingModal').show();

            // make an ajax call to unassign the course from the student
            $.ajax({
                url: '/ManageFacilitiesWidget/UnassignClass',
                method: 'POST',
                data: {
                    'studentID': $(this).attr('data-studentid'),
                    'classID': $(this).attr('data-classid'),
                    'facilityID': $(this).attr('data-facilityid'),
                    'invoiceNumber': $(this).attr('data-invoicenumber'),
                    'facilityMemID': $(this).attr('data-facilitymemID'),
                    'facilityClassListing': $(this).attr('data-facilityClassListing')
                },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    // hide loading modal
                    $('#facilityProcessLoadingModal').hide();

                    if (data.FacilityClassListing) {
                        // refresh the facility class list
                        updateManageStudentClassListing(data.Students, data.StudentID);
                    }
                    else {
                        // refresh the student list
                        updateManageEnrollmentStaffList(data.Students, data.MembershipID);
                    }
                }
            });
        });

        //manage enrollment for facility membership classes
        $(document).on('click', '.membership-class-manage-enrollment', function (e) {
            e.preventDefault();

            // show loading modal
            $('#facilityProcessLoadingModal').show();

            // run an ajax call to get the staff members in this facility
            $.ajax({
                url: '/ManageFacilitiesWidget/GetFacilityMembershipClassEnrollmentData',
                method: 'GET',
                data: {
                    'facilityID': $(this).attr('data-facilityid'),
                    'classID': $(this).attr('data-classid'),
                    'invoiceNum': $(this).attr('data-invoicenumber'),
                    'membershipID': $(this).attr('data-membershipid')
                },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    // close loading modal
                    $('#facilityProcessLoadingModal').hide();

                    // hide facility class listing & title
                    var panelHeader = $('#membershipsPanelHeader');
                    panelHeader.find('#membershipsPanelTitle').hide();
                    panelHeader.find('#backToFacilityMembershipListing').show();

                    var courseTitle = panelHeader.find('#manageCourseTitle');
                    courseTitle.show();
                    courseTitle.html(data.ClassName);

                    $('#facilityMembershipListing').hide();

                    // show manage enrollment section
                    $('#manageEnrollment').show();

                    // populate the section with staff members
                    updateManageEnrollmentStaffList(data.Students, data.MembershipID);
                }
            });
        });

        // takes user from the manage enrollment section back to the facility membership listing section
        $('#membershipsPanelHeader').on('click', '#backToFacilityMembershipListing', function () {
            $(this).hide();
            $('#manageEnrollment').hide();
            $(this).siblings('#manageCourseTitle').html('');
            $(this).siblings('#membershipsPanelTitle').show();
            $('#facilityMembershipListing').show();

            $('#membershipsTab').click();
        });

        // clears the staff member filter form
        $('#clearStaffMemberFilter').click(function () {
            $(this).parents('#staffMembersFilter').find('.staff-member-filter-txt').val('');

            GetStaffMembersPagedData(1);
        });

        // filters the staff members
        $('#staffMembersFilter').submit(function (e) {
            e.preventDefault();

            GetStaffMembersPagedData(1);
        });

        // select all students to print their certificates
        $('#selectAllStudentCerts').click(function () {
            var inputs = $(this).siblings('#studentCertsList').find('.checkboxes');

            if ($(this).children('input').first().is(':checked')) {
                inputs.each(function () {
                    $(this).prop('checked', true);
                });
            }
            else {
                inputs.each(function () {
                    $(this).prop('checked', false);
                });
            }
        });
    });
});

function GetFacilitiesPagedData(pageNum) {
    var facilityTable = $("#facilityTable");
    facilityTable.html('');

    var facilityList = $("#facilityList");
    var facilityLoading = $("#facilityLoading");
    var facilityPagination = $('#facilityPagination');

    facilityPagination.hide();
    facilityList.hide();
    facilityLoading.show();

    $.getJSON("/ManageFacilitiesWidget/GetPagedData", `${$("#facilityFilterForm").serialize()}&pageNumber=${pageNum}&pageSize=16`, function (response) {
        var rowData = '';
        for (var i = 0; i < response.Data.length; i++) {
            rowData += `<div class="col-12 col-md-4 col-lg-3 my-3">
                            <div class="corporation">
                                <h4><strong>${response.Data[i].Name}</strong></h4>
                                <h5 class="ms-2">${response.Data[i].FacilityCode}</h5>
                                <div class="row mt-auto">
                                    <div class="col-12 col-lg-4 my-auto">
                                        <button class="update-facility-btn p-0" data-facilityID="${response.Data[i].FacilityID}"><i class="fa fa-pencil"></i></button>`;

            if (response.Data[i].IsSiteAdmin) {
                rowData += `<a class="fa fa-trash delete-facility-btn ms-2" href="" data-facilityID="${response.Data[i].FacilityID}" data-facilityName="${response.Data[i].Name}"></a>`;
            }

            rowData += `            </div>
                                    <div class="col-12 col-lg-8 text-lg-end">
                                        <a href="" data-facilityID="${response.Data[i].FacilityID}" class="d-block manage-staff-btn">Manage Staff</a>
                                        <a href="" data-facilityID="${response.Data[i].FacilityID}" class="d-block manage-class-btn">Manage Classes</a>
                                    </div>
                                </div>
                            </div>
                        </div>`;
        }

        facilityLoading.hide();
        facilityList.show();
        facilityTable.html('');

        if (response.Data.length > 0) {
            $("#facilities").show();
            $("#noFacilities").hide();
            facilityTable.append(rowData);
        }
        else {
            $("#facilities").hide();
            $("#noFacilities").show();
        }

        $("#facilityPagination").html("");
        facilityPagination.show();

        if (response.TotalPages > 0) {
            PagingTemplate(response.TotalPages, response.CurrentPage, "#facilityPagination", "GetFacilitiesPagedData");
        }

        $("html, body").scrollTop(0);
    });
}

function GetStaffMembersPagedData(pageNum) {
    $("html, body").scrollTop(0);

    var staffMembersList = $("#staffMembersList");
    staffMembersList.html('');

    var staffMembersLoading = $("#staffMembersLoading");
    var staffMembersPagination = $('#staffMembersPagination');
    var noStaffMembersTxt = $('#noStaffMembersTxt');

    staffMembersPagination.hide();
    staffMembersList.hide();
    noStaffMembersTxt.hide();
    staffMembersLoading.show();

    $.getJSON("/ManageFacilitiesWidget/GetFacilityStaffMembers", `${$("#staffMembersFilter").serialize()}&pageNumber=${pageNum}&pageSize=9`, function (response) {
        $('#staffMembersCount').html(`(${response.Data.length})`);

        var rowData = '';
        for (var i = 0; i < response.Data.length; i++) {
            var student = response.Data[i];

            rowData += `
                                    <div class="col col-12 col-md-6 col-lg-4 p-3">
                                        <div class="staff-member p-3">
                                            <div class="row">
                                                <div class="col-9 pe-lg-2">
                                                    <a href="" class="manage-student-classes" data-contactID="${student.ContactID}">Manage Classes</a>
                                                </div>
                                                <div class="col-3 d-lg-flex justify-content-end">
                                                        <a href="" class="fa fa-pencil me-2 edit-student" data-contactID="${student.ContactID}"></a>
                                                        <a href="" class="fa fa-trash delete-student" data-contactID="${student.ContactID}" data-contactName="${student.FirstName} ${student.LastName}"></a>
                                                </div>
                                            </div>
                                            <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Name:</strong> ${student.FirstName} ${student.LastName}</div>
                                                </div>
                                            </div>`;

            if (student.Email != null && student.Email != "") {
                rowData += `                    <div class="row my-2">
                                                <div class="col col-12">
                                                    <strong>Email:</strong>
                                                    <div>${student.Email}</div>
                                                </div>                                                
                                            </div>`;
            }

            if (student.Phone != "" && student.Phone) {
                rowData += `                    <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Phone:</strong> ${formatPhoneNumber(student.Phone)}</div>
                                                </div>
                                            </div>`;
            }

            if (student.UserName != "" && student.UserName != null) {
                rowData += `            
                                            <div class="row my-2">
                                                <strong>User Name:</strong>
                                                <div>${student.UserName}</div>
                                            </div>`;
            }

            rowData += `
                                        </div>
                                    </div>`;
        }

        staffMembersLoading.hide();
        staffMembersList.show();

        if (response.Data.length > 0) {
            staffMembersList.append(rowData);
        }
        else {
            noStaffMembersTxt.show();
        }

        if (response.TotalPages > 0) {
            staffMembersPagination.html("");
            staffMembersPagination.show();

            PagingTemplate(response.TotalPages, response.CurrentPage, "#staffMembersPagination", "GetStaffMembersPagedData");
        }
    });
}

function showFacilityForms() {
    $('.facility-forms').show();
    $('.facility-listing-objects').hide();
    $('#facility-code').hide();
    $('#add-facility-btn').show();
    $('#manageCorpsBtn').hide();
    $('#newFacility').hide();
    $('#viewAllFacilitiesBtn').hide();
    $('#backToFacilityListingBtn').show();
}

function updateFacilityContactList(contacts) {
    $('#contactsContainer').html('');

    if (contacts.length > 0) {
        for (var i = 0; i < contacts.length; i++) {
            var rowData = `
                                    <div class="col col-12 col-md-6 col-lg-4 p-3">
                                        <div class="contact p-3">
                                            <div class="row mb-3">
                                                <div class="col col-12">
                                                    <div class="d-flex justify-content-end">
                                                        <a class="fa fa-pencil me-3 edit-contact" data-contactID="${contacts[i].ContactID}"></a>
                                                        <a class="fa fa-trash delete-contact" data-contactID="${contacts[i].ContactID}" data-contactName="${contacts[i].FirstName} ${contacts[i].LastName}"></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-12">
                                                    <h4><strong>${contacts[i].TypeName}</strong></h4>
                                                </div>
                                            </div>
                                            <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Name:</strong> ${contacts[i].FirstName} ${contacts[i].LastName ?? ""}</div>
                                                </div>
                                            </div>`;

            if (contacts[i].Email != null && contacts[i].Email != "") {
                rowData += `<div class="row my-2">
                                                <div class="col col-12">
                                                    <strong>Email:</strong>
                                                    <div>${contacts[i].Email}</div>
                                                </div>                                                
                                            </div>`;
            }

            if (contacts[i].Phone != "" && contacts[i].Phone) {
                rowData += `    <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Phone:</strong> ${formatPhoneNumber(contacts[i].Phone)}</div>
                                                </div>
                                            </div>`;
            }

            if (contacts[i].UserName != "" && contacts[i].UserName != null) {
                rowData += `            
                                            <div class="row my-2">
                                                <strong>User Name:</strong>
                                                <div>${contacts[i].UserName}</div>
                                            </div>`;
            }

            rowData += `<div class="row">
                                                <div class="col col-12">
                                                    <div><strong>Can Make Facility Purchases?</strong> ${(contacts[i].AllowOnlinePurchases ? "Yes" : "No")}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;

            $('#contactsContainer').append(rowData);
        }
    }
    else {
        $('#contactsContainer').append('<p id="noFacilityContactsTxt">No contacts found.</p>');
    }
}

function updateFacilityStaffMembersList(students) {
    $('#staffMembersList').html('');

    if (students.length > 0) {
        for (var i = 0; i < students.length; i++) {
            var rowData = `
                                    <div class="col col-12 col-md-4 p-3">
                                        <div class="staff-member p-3">
                                            <div class="row">
                                                <div class="col col-12">
                                                    <div class="d-flex">
                                                        <a href="" class="me-auto manage-student-classes" data-contactID="${students[i].ContactID}">Manage Classes</a>
                                                        <a href="" class="fa fa-pencil me-3 edit-student" data-contactID="${students[i].ContactID}"></a>
                                                        <a href="" class="fa fa-trash delete-student" data-contactID="${students[i].ContactID}" data-contactName="${students[i].FirstName} ${students[i].LastName}"></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Name:</strong> ${students[i].FirstName} ${students[i].LastName}</div>
                                                </div>
                                            </div>`;

            if (students[i].Email != null && students[i].Email != "") {
                rowData += `                    <div class="row my-2">
                                                <div class="col col-12">
                                                    <strong>Email:</strong>
                                                    <div>${students[i].Email}</div>
                                                </div>                                                
                                            </div>`;
            }

            if (students[i].Phone != "" && students[i].Phone) {
                rowData += `                    <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Phone:</strong> ${formatPhoneNumber(students[i].Phone)}</div>
                                                </div>
                                            </div>`;
            }

            if (students[i].UserName != "" && students[i].UserName != null) {
                rowData += `            
                                            <div class="row my-2">
                                                <strong>User Name:</strong>
                                                <div>${students[i].UserName}</div>
                                            </div>`;
            }

            rowData += `
                                        </div>
                                    </div>`;

            $('#staffMembersList').append(rowData);
        }
    }
    else {
        $('#staffMembersList').append('<p id="noFacilityStaffMembersTxt">No staff members found.</p>');
    }
}

function clearFacilityContactForm() {
    $('.contactID').val('');
    $('#contactUserID').val('');

    $('#contactErrMsg').html('');

    $('#facilityContactNeedsUserAcct').show();
    $('#facilityContactUserName').children('div').html('');
    $('#facilityContactUserName').hide();

    $('#userPassRecipientForm').hide();
    var emailInput = $('#facilityContactEmail');
    emailInput.show();
    emailInput.siblings('label').show();

    $('.contact-data-txt').val('');
    $('.contact-data-validation').val('');
    $('.contact-data-dropdown').val($('.contact-data-dropdown option:nth-child(1)').val());
    $('.contact-data-checkbox').prop('checked', false);
}

function closeNewFacilityContactForm() {
    $('#contactsContainer').show();
    $('#updateFacilityContactForm').hide();
    $('#addNewContactBtn').show();
}

function closeDeleteFacilityContactModal() {
    var modal = $('#deleteContactConfirmModal');

    modal.hide();
    modal.find('#deleteContactName').html('');
    modal.find('#removeContactBtn').attr('data-contactid', '');
}

function closeDeleteFacilityModal() {
    var modal = $('#deleteFacilityConfirmModal');

    modal.hide();
    modal.find('#facilityName').html('');
    modal.find('#removeFacilityBtn').attr('data-facilityid', '');
}

function closeAddExistingStudentForm() {
    // show/hide things
    $('#addExistingStaffBtn').show();
    $('#staffMembersList').show();
    $('#addNewStaffBtn').show();
    $('#staffMembersFilter').show();
    $('#staffMembersPagination').show();
    $('#addNewStudent').hide();
    $('#addExisitingStudent').hide();

    // clear all fields
    $('.student-data-txt').val('');
    $('.student-data-validation').val('');
    $('#addExistingStudentErrMsg').html('');
}

function closeAddNewStudentForm() {
    // show/hide things
    $('#addNewStaffBtn').show();
    $('#staffMembersList').show();
    $('#addExistingStaffBtn').show();
    $('#staffMembersFilter').show();
    $('#staffMembersPagination').show();
    $('#addNewStudent').hide();
    $('#addExisitingStudent').hide();

    // remove all students and add one back
    $('.student').each(function () { $(this).remove(); });
    $('#addStudentInput').click();

    // clear all fields
    $('.student-data-txt').val('');
    $('.student-data-validation').val('');
    $('#addNewStudentErrMsg').html('');
}

function updateStudentLicenseList(licenses) {
    $('#studentLicenseList').html('');

    for (var i = 0; i < licenses.length; i++) {
        var rowData = `<div class="col-12 col-md-4 p-3">
                            <div class="license">
                                <div class="d-flex justify-content-end">
                                    <a href="" class="fa fa-pencil edit-student-license me-2" data-studentLicenseID="${licenses[i].LicenseID}"></a>
                                    <a href="" class="fa fa-trash delete-student-license" data-studentLicenseID="${licenses[i].LicenseID}"></a>
                                </div>
                                <div class="type-name">${licenses[i].LicenseTypeName}</div>
                                <div class="ms-2">${licenses[i].LicenseNumber}</div>
                                <div class="ms-2">${licenses[i].LicenseState}</div>
                            </div>
                        </div>`;

        $('#studentLicenseList').append(rowData);
    }
}

function clearNewStudentLicenseForm() {
    // clear the student license form fields
    $('#studentLicenseID').val(0);
    $('#studentLicenseTypeDropdown').val(0);
    $('#studentLicenseNumber').val('');
    $('#studentLicenseState').val($('#studentLicenseState option:nth-child(1)').val());

    // show/hide the correct buttons
    $('#addStudentLicenseBtn').show();
    $('#updateStudentLicenseBtn').hide();
    $('#cancelUpdateStudentLicenseBtn').hide();
}

function updateFacilityClassList(classes) {
    if (classes.length > 0) {
        $('#facilityClassListing').show();
        $('.no-facility-classes-txt').hide();
        for (var i = 0; i < classes.length; i++) {
            var rowData = `<div class="facility-class row">
                            <div class="col-12 d-md-flex">
                                <h4 class="me-md-3 me-lg-0"><strong>${classes[i].ClassName}</strong></h4>
                                <a href="" class="class-manage-enrollment ms-auto" data-facilityID="${classes[i].FacilityID}" data-invoiceNumber="${classes[i].InvoiceNumber}" data-classID="${classes[i].ClassID}">Manage Enrollment</a>
                            </div>
                            <div class="col-12 col-md-4 col-lg-2 px-md-1 px-lg-0">
                                ${$('#isSiteAdmin').val() == 'true' && classes[i].InvoiceNumber != null && classes[i].InvoiceNumber != "" ? `<div class="m-2"><strong>Invoice #:</strong> ${classes[i].InvoiceNumber}</div>` : ""}
                            </div>
                            <div class="col-12 col-md-5 col-lg-2 px-md-1 px-lg-0">
                                <div class="m-2 current-qty">${$('#isSiteAdmin').val() == 'true' && classes[i].InvoiceNumber != null && classes[i].InvoiceNumber != "" ? `<a href="" class="fa fa-pencil me-2 update-facility-class-qty" data-inPackage="${classes[i].InPackage}" data-facilityClassID="${classes[i].FacilityClassID}"></a>` : ""}<strong>Purchased:</strong> <text class="qty-purchased-txt">${classes[i].QuantityPurchased}</text></div>
                                <div class="m-2 updated-qty"><a href="" class="fa fa-save me-2 save-facility-class-qty" data-invoiceNumber="${classes[i].InvoiceNumber}" data-facilityClassID="${classes[i].FacilityClassID}" data-classID="${classes[i].ClassID}" data-facilityID="${classes[i].FacilityID}"></a><strong>Purchased:</strong> <input class="form-control input-qty" onkeydown="return false" type="number" value="${classes[i].QuantityPurchased}" max="${classes[i].QuantityPurchased}" min="${classes[i].AmountUsed}" /></div>
                            </div>
                            <div class="col-12 col-md-3 col-lg-2 px-md-1 px-lg-0">
                                <div class="m-2"><strong>Used:</strong> ${classes[i].AmountUsed}</div>
                            </div>
                            <div class="col-12 col-md-4 col-lg-2 px-md-1 px-lg-0">
                                <div class="m-2"><strong>Not Started:</strong> ${classes[i].AmountNotStarted}</div>
                            </div>                            
                            <div class="col-12 col-md-4 col-lg-2 px-md-1 px-lg-0">
                                <div class="m-2"><strong>In Progress:</strong> ${classes[i].AmountInProgress}</div>
                            </div>
                            <div class="col-12 col-md-4 col-lg-2 px-md-1 px-lg-0">
                                <div class="m-2"><strong>Completed:</strong> ${classes[i].AmountCompleted}</div>
                            </div>
                        </div>`;

            $('#facilityClassListing').append(rowData);
        }
    }
    else {
        $('#facilityClassListing').hide();
        $('.no-facility-classes-txt').show();
    }
}

function updateFacilityMembershipList(memberships) {
    if (memberships.length > 0) {
        for (var i = 0; i < memberships.length; i++) {
            var rowData = `<div class="accordion-item facility-membership">
                            <h2 class="accordion-header" id="${memberships[i].MembershipName.replace(/\s+/g, '-').toLowerCase()}-${memberships[i].MembershipID}">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#${memberships[i].MembershipName.replace(/\s+/g, '-').toLowerCase()}-${memberships[i].MembershipID}-fold" aria-expanded="false" aria-controls="${memberships[i].MembershipName.replace(/\s+/g, '-').toLowerCase()}-${memberships[i].MembershipID}-fold">
                                    ${memberships[i].MembershipName}
                                </button>
                            </h2>
                            <div id="${memberships[i].MembershipName.replace(/\s+/g, '-').toLowerCase()}-${memberships[i].MembershipID}-fold" class="accordion-collapse collapse" aria-labelledby="${memberships[i].MembershipName.replace(/\s+/g, '-').toLowerCase()}-${memberships[i].MembershipID}" data-bs-parent="#facilityMembershipListing">
                                <div class="accordion-body">
                                    <div class="row justify-content-center align-items-baseline">
                                            ${$('#isSiteAdmin').val() == 'true' && memberships[i].InvoiceNumber != null && memberships[i].InvoiceNumber != "" ? `<div class="col-12 col-md-4"><strong>Invoice #:</strong> ${memberships[i].InvoiceNumber}</div>` : ""}
                                        <div class="col-12 col-md-4">
                                            <div class="m-2 current-qty">${$('#isSiteAdmin').val() == 'true' && memberships[i].InvoiceNumber != null && memberships[i].InvoiceNumber != "" ? `<a href="" class="fa fa-pencil me-2 update-facility-mem-qty"></a>` : ""}<strong>Quantity Purchased:</strong> <text class="qty-purchased-txt">${memberships[i].MembershipQty}</text></div>
                                            <div class="m-2 updated-qty"><a href="" class="fa fa-save me-2 save-facility-mem-qty" data-invoiceNumber="${memberships[i].InvoiceNumber}" data-membershipID="${memberships[i].MembershipID}" data-facilityID="${memberships[i].FacilityID}"></a><strong>Quantity Purchased:</strong> <input class="form-control input-qty" onkeydown="return false" type="number" value="${memberships[i].MembershipQty}" max="${memberships[i].MembershipQty}" min="0" /></div>
                                        </div>
                                    </div>`;
            if (memberships[i].FacilityClasses.length > 0) {
                rowData += '<div class="row membership-class-listing">';

                for (var x = 0; x < memberships[i].FacilityClasses.length; x++) {
                    var memClass = memberships[i].FacilityClasses[x];
                    rowData += `<div class="facility-class row">
                                    <div class="col-12 d-md-flex">
                                        <h4 class="me-md-3"><strong>${memClass.ClassName}</strong></h4>
                                        <a href="" class="membership-class-manage-enrollment ms-auto" data-facilityID="${memberships[i].FacilityID}" data-invoiceNumber="${memClass.InvoiceNumber}" data-classID="${memClass.ClassID}" data-membershipID="${memberships[i].MembershipID}">Manage Enrollment</a>
                                    </div>
                                    <div class="col-12 col-md-2 col-lg-3">
                                        <div class="m-2"><strong>Used:</strong> ${memClass.AmountUsed}</div>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <div class="m-2"><strong>Not Started:</strong> ${memClass.AmountNotStarted}</div>
                                    </div>                            
                                    <div class="col-12 col-md-3">
                                        <div class="m-2"><strong>In Progress:</strong> ${memClass.AmountInProgress}</div>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-3">
                                        <div class="m-2"><strong>Completed:</strong> ${memClass.AmountCompleted}</div>
                                    </div>
                                </div>`;
                }

                rowData += '</div>';
            }


             rowData +=         `</div>
                            </div>
                        </div>`;

            $('#facilityMembershipListing').append(rowData);
        }
    }
    else {
        $('#facilityMembershipListing').append('<p id="noFacilityMembershipsTxt">No memberships found.</p>');
    }
}

function updateManageEnrollmentStaffList(students, memID) {
    var studentsContainer = $('#manageEnrollment').find('#staffListing');

    studentsContainer.show();
    studentsContainer.html('');

    if (students.length > 0) {
        $('#noFacilityStaffMembersTxt').hide();

        var rowData = '';
        for (var i = 0; i < students.length; i++) {
            console.log(students[i]);
            rowData += `<div class="student-membership-class">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-1 px-2 px-lg-0 d-flex justify-content-start">
                                    <button class="fa fa-plus-square assign-class" ${students[i].LimitReached ? "disabled" : ""} data-classID="${students[i].ClassID}" data-studentID="${students[i].StudentID}" data-invoiceNumber="${students[i].InvoiceNumber}" data-facilityID="${students[i].FacilityID}" data-facilityMemID="${memID != 0 ? memID : ""}" data-facilityClassListing="false"></button>
                                    <button class="fa fa-minus-square unassign-class" ${!students[i].Unassignable ? "disabled" : ""} data-classID="${students[i].ClassID}" data-studentID="${students[i].StudentID}" data-invoiceNumber="${students[i].InvoiceNumber}" data-facilityID="${students[i].FacilityID}" data-facilityMemID="${memID != 0 ? memID : ""}" data-facilityClassListing="false"></button>
                                </div>
                                <div class="col-9 col-md-4 col-lg-3 px-2 px-lg-0">
                                    <span>${students[i].StudentName}</span>
                                </div>
                                <div class="col-6 col-md-4 col-lg-2 px-2 px-lg-0 mt-3 mt-md-0">
                                    <span><strong>Purchased:</strong> ${students[i].QuantityPurchased}</span>
                                </div>
                                <div class="col-6 col-md-2 col-lg-2 px-2 px-md-0 mt-3 mt-md-0">
                                    <span><strong>Used:</strong> ${students[i].AmountUsed}</span>
                                </div>
                                <div class="col-12 col-lg-3 px-2 px-md-0 mt-3 mt-lg-0">
                                    <span>${students[i].Status}</span>
                                </div>
                            </div>
                        </div>`;
        }

        studentsContainer.append(rowData);
    }
    else {
        studentsContainer.hide();
        $('#noFacilityStaffMembersTxt').show();
    }
}

function updateManageStudentClassListing(facilityClasses, studentID) {
    var classListContainer = $('#manageStudentClasses').find('#facilityClassList');

    classListContainer.show();
    classListContainer.html('');

    // check if class list is empty
    if (facilityClasses.length > 0) {
        $('.no-facility-classes-txt').hide();

        var rowData = '';
        for (var i = 0; i < facilityClasses.length; i++) {
            var classData = facilityClasses[i];
            console.log(classData);

            rowData += `<div class="facility-class">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-1 d-flex justify-content-start px-2 px-lg-0">
                                    <button class="fa fa-plus-square assign-class" ${classData.LimitReached ? "disabled" : ""} data-classID="${classData.ClassID}" data-studentID="${studentID}" data-invoiceNumber="${classData.InvoiceNumber != null ? classData.InvoiceNumber : ""}" data-facilityID="${classData.FacilityID}" data-facilityMemID="${classData.FacilityMembershipID != 0 ? classData.FacilityMembershipID : ""}" data-facilityClassListing="true"></button>
                                    <button class="fa fa-minus-square unassign-class" ${!classData.Unassignable ? "disabled" : ""} data-classID="${classData.ClassID}" data-studentID="${studentID}" data-invoiceNumber="${classData.InvoiceNumber != null ? classData.InvoiceNumber : ""}" data-facilityID="${classData.FacilityID}" data-facilityMemID="${classData.FacilityMembershipID != 0 ? classData.FacilityMembershipID : ""}" data-facilityClassListing="true"></button>
                                </div>
                                <div class="col-9 col-lg-5 px-2 px-lg-0">
                                    <span>${classData.ClassName}</span>
                                </div>
                                <div class="col-3 col-md-3 col-lg-2 mt-3 mt-lg-0 px-2 px-lg-0">
                                    <span><strong>Used:</strong> ${classData.AmountUsed}</span>
                                </div>
                                <div class="col-5 col-md-5 col-lg-2 mt-3 mt-lg-0 px-2 px-lg-0">
                                    <span><strong>Purchased:</strong> ${classData.QuantityPurchased}</span>
                                </div>
                                <div class="col-12 col-md-4 col-lg-2 px-2 px-2 mt-3 mt-lg-0">
                                    <span>${classData.Status}</span>
                                </div>
                            </div>
                        </div>`;
        }

        classListContainer.append(rowData);
    }
    else {
        classListContainer.hide();
        $('.no-facility-classes-txt').show();
    }
}

function resetAllTabsToDefault() {
    // global values
    $('.facilityID').val(0);
    $('.contactID').val(0);

    // facility info
    $('#facilityCode').html('');
    $('#facilityName').val('');
    $('#facilityParentDropdown').val(0);
    $('#facilityEnabled').prop('checked', true);
    $('#facilityAddressLine1').val('');
    $('#facilityAddressLine2').val('');
    $('#facilityAddressCity').val('');
    $('#facilityAddressState').val(0);
    $('#facilityAddressZip').val('');

    closeDeleteFacilityModal();

    // preferences
    $('#facilityPrefEmailMonthlyProgress').prop('checked', false);
    $('#facilityPrefAssignHowToPedagogy').prop('checked', false);

    // contacts
    $('#contactsContainer').html('');

    clearFacilityContactForm();
    closeNewFacilityContactForm();
    closeDeleteFacilityContactModal();

    // staff members
    $('#staffMembersList').html('');
    $('#editStudent').hide();

    clearNewStudentLicenseForm();
    closeAddExistingStudentForm();
    closeAddNewStudentForm();

    // print certificates
    $('#studentCertsData').show();
    $('#noStudentCertsTxt').hide();

    $('#studentCertsList').html('');

    // memberships
    $('#facilityMembershipListing').html('');

    var memBackBtn = $('#membershipsPanelHeader').find('#backToFacilityMembershipListing');
    memBackBtn.hide();
    memBackBtn.siblings('#manageCourseTitle').html('');
    memBackBtn.siblings('#membershipsPanelTitle').show();
    $('#facilityMembershipListing').show();

    // classes
    $('#facilityClassListing').html('');
    $('#facilityClassListing').hide();

    var classesBackBtn = $('#classesPanelHeader').find('#backToFacilityClassListing');
    classesBackBtn.hide();
    classesBackBtn.siblings('#manageCourseTitle').html('');
    classesBackBtn.siblings('#classesPanelTitle').show();

    // enrollment
    var enrollmentContainer = $('#manageEnrollment');
    enrollmentContainer.find('#noFacilityStaffMembersTxt').hide();
    enrollmentContainer.hide();
}