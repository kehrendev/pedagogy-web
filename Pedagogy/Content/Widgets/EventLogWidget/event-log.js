﻿$(function () {
    console.log('event-log.js loaded..');

    $(document).ready(function () {
        if ($('#events').length) {
            GetPageData(1);
        }

        $("#filter-events-btn").click(function (e) {
            GetPageData(1);
        });
    });
});

function ClearEventFilter() {
    $("#from-date").val("");
    $("#course-name").val("");
    $("#student-name").val("");
    $("#to-date").val("");
    $("#users-name").val("");
    $("#student-user-name").val("");
    $("#exception-type").val("");
    $("#users-user-name").val("");
    $('#organizationName').val('');

    GetPageData(1);
}

function GetPageData(pageNum) {
    $.getJSON("/EventLogWidget/GetPagedData", $("#filter-events-form").serialize() + `&pageNumber=${pageNum}&pageSize=${$(".event-log-widget").find('#eventsPerPage').val()}`,
        function (response) {
            var rowData = "";
            for (var i = 0; i < response.Data.length; i++) {
                rowData = rowData + '<tr><td><form action="/EventLogWidget/DeleteEvent" method="post">'
                    + `<input type="hidden" name="returnUrl" value="${$(".event-log-widget").find('#returnURL').val()}" />`
                    + '<input id="eventID" name="eventID" type="hidden" value="' + response.Data[i].EventLogID + '" />'
                    + '<button type="submit" class="fa fa-trash" style="background-color:transparent;border:unset;"></button></form></td>'
                    + '<td>' + response.Data[i].CreatedOn + '</td><td>' + response.Data[i].UsersName + ' / ' + response.Data[i].UsersUserName + '<td>'
                    + response.Data[i].StudentName + ' / ' + response.Data[i].StudentUserName + '</td><td>' + (response.Data[i].OrganizationName ?? "") + '</td><td>'
                    + (response.Data[i].CourseName ?? "") + '<td>' + response.Data[i].EventLogTypeName + '</tr>';
            }

            $("#events").css('display', 'block');
            $(".loading-icon").hide();

            $("#events-list-group").html("");
            if (rowData != "") {
                $("#events-list-group").append(rowData);
            }
            else {
                var noResults = $('.event-log-widget').find('.no-events-text').html();

                $("#event-log-contents").html("");
                $("#event-log-contents").append($("#event-filter"));
                $("#event-log-contents").append(noResults);
            }

            $("#event-log-pagination").html("");
            if (response.TotalPages != 1) {
                PagingTemplate(response.TotalPages, response.CurrentPage, "#event-log-pagination", "GetPageData");
            }

            if (pageNum != 1) {
                $("html, body").scrollTop(70);
            }
        });
}