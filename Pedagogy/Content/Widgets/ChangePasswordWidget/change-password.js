﻿$(function () {
    console.log('change-password.js loaded..');
});

function displayChangePasswordMsg(data) {
    var msgContainer = document.getElementById("err-success-msg");
    for (var x = 0; x < data.length; x++) {
        msgContainer.innerHTML = data[x];
    }

    document.getElementById("current-password").value = "";
    document.getElementById("new-password").value = "";
    document.getElementById("confirm-new-password").value = "";
}