﻿$(function () {
    console.log("licensing-info.js loaded..");

    $(document).ready(function () {
        $("#not-applicable-checkbox").click(function () {
            if (this.checked) {
                $.ajax({
                    url: "/LicensingInfoWidget/AddNotApplicableLicense",
                    method: "POST",
                    success: function () {
                        $(".license-list").css("display", "none");
                        $("#new-license-form").css("display", "none");
                    }
                });
            }
            else {
                $.ajax({
                    url: "/LicensingInfoWidget/RemoveNotApplicableLicense",
                    method: "POST",
                    success: function () {
                        $(".license-list").css("display", "block");
                        $("#new-license-form").css("display", "block");
                    }
                });
            }
        });

        $("#student-license-type").on('change', function () {
            var option = $("#student-license-type option:selected").html();
            if (option == 'Not Applicable') {
                $("#sl-num").hide();
                $("#sl-state").hide();
                $("#student-license-number").val("");
                $("#student-license-state").val(0);
            }
            else {
                $("#sl-num").show();
                $("#sl-state").show();
            }
        });
    });
});

function updateStudentLicense(slID) {
    document.getElementById("student-license-type-validation").innerHTML = "";
    document.getElementById("student-license-number-validation").innerHTML = "";
    document.getElementById("student-license-state-validation").innerHTML = "";

    var addBtn = document.getElementById("add-student-license");
    var updateBtn = document.getElementById("update-student-license");
    var cancelBtn = document.getElementById("cancel-update-license");

    cancelBtn.style.display = "block";
    addBtn.style.display = "none";
    updateBtn.style.display = "block";

    var editBtn = document.getElementById(`update-student-license-${slID}`);
    document.getElementById("hidden-student-id").value = editBtn.getAttribute("data-studentID");
    document.getElementById("hidden-return-url").value = editBtn.getAttribute("data-return-url");
    document.getElementById("student-license-type").value = editBtn.getAttribute("data-license-type");
    document.getElementById("hidden-student-license-id").value = editBtn.getAttribute("data-student-license-id");

    var slNum = editBtn.getAttribute("data-license-num");
    if (slNum == "") {
        $("#sl-num").hide();
        $("#sl-state").hide();
    } else {
        document.getElementById("student-license-number").value = slNum;
    }

    var slState = editBtn.getAttribute("data-license-state");
    if (slState == 0) {
        $("#sl-num").hide();
        $("#sl-state").hide();
    } else {
        document.getElementById("student-license-state").value = slState;
    }
}

function cancelUpdateLicense() {
    var addBtn = document.getElementById("add-student-license");
    var updateBtn = document.getElementById("update-student-license");
    var cancelBtn = document.getElementById("cancel-update-license");

    cancelBtn.style.display = "none";
    addBtn.style.display = "block";
    updateBtn.style.display = "none";

    document.getElementById("student-license-type").value = "";
    document.getElementById("student-license-type-validation").innerHTML = "";

    document.getElementById("student-license-number").value = "";
    document.getElementById("student-license-number-validation").innerHTML = "";

    document.getElementById("student-license-state").value = "";
    document.getElementById("student-license-state-validation").innerHTML = "";


    document.getElementById("hidden-student-license-id").value = "";
}