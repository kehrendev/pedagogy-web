﻿$(function () {
    console.log('manage-corporations.js loaded..');

    $(document).ready(function () {
        if ($('#corpList').length) {
            GetCorporationsPagedData(1);
        }

        $('#corpFilterForm').submit(function (e) {
            e.preventDefault();
            GetCorporationsPagedData(1);
        });

        // displaying the new corporation form
        $('#newCorp').click(function () {
            showCorpForms();
            $('#corpInfoTab').click();
        });

        // going back from the corporation forms to the listing
        $('#backToCorpListingBtn').click(function () {
            $('.corp-forms').hide();
            $('.corp-listing-objects').show();
            $('#corp-code').show();
            $('#add-corp-btn').hide();
            $('#manageFacilitiesBtn').show();
            $(this).hide();
            $('#viewAllCorpsBtn').show();
            $('#newCorp').show();

            $('.corpID').val('');
            $('.contactID').val('');
            $('#contactUserID').val('');

            $('#corpCode').val('');
            $('#corpName').val('');
            $('#corpParentDropdown').val('');
            $('#corpEnabled').attr('checked', 'checked');
            $('#corpAddressLine1').val('');
            $('#corpAddressLine2').val('');
            $('#corpAddressCity').val('');
            $('#corpAddressState').val('');
            $('#corpAddressZip').val('');

            $('#contactsContainer').html('');
            $('#contactsContainer').append('<p id="noCorpContactsTxt">No contacts found.</p>');

            GetCorporationsPagedData(1);
        });

        // show all of the corporations
        $('#viewAllCorpsBtn').click(function () {
            // TODO: show & hide things?

            GetCorporationsPagedData(1);
        });

        // switching between the corporation tabs
        $('#corpTabs').on('click', '.nav-link', function (e) {
            e.preventDefault();
            var corpID = $('.corpID').val();

            $('#corpTabs .nav-link').removeClass('active show');
            $('#corpTabContent .tab-pane').removeClass('active show');

            $(this).toggleClass('active show');
            $($(this).attr('href')).toggleClass('active show');

            switch ($(this).attr('id')) {

                case 'contactsTab':
                    // get the contacts list if any
                    if (corpID != 0) {
                        $('#contactListLoading').show();
                        $('#contactsContainer').html('');

                        $.ajax({
                            url: '/ManageCorporationsWidget/GetCorpContacts',
                            method: 'GET',
                            data: { 'corpID': corpID },
                            success: function (obj) {
                                $('#contactListLoading').hide();
                                updateCorpContactList(JSON.parse(obj));
                            }
                        });
                    }

                    break;

                default:
                    // display the corp info tab by default
                    if (corpID != 0) {
                        $.ajax({
                            url: '/ManageCorporationsWidget/GetCorpData',
                            method: 'GET',
                            data: { 'corpID': corpID },
                            success: function (obj) {
                                var data = JSON.parse(obj);
                                console.log(data);
                                //populate all the fields
                                $('.corpID').val(data.FacilityID);

                                // corp data
                                $('#corpCode').val(data.FacilityCode);
                                $('#corpName').val(data.Name);
                                $('#corpParentDropdown').val(data.FacilityParentID);
                                $('#corpEnabled').attr('checked', 'checked');
                                $('#corpAddressLine1').val(data.AddressLine1);
                                $('#corpAddressLine2').val(data.AddressLine2);
                                $('#corpAddressCity').val(data.City);
                                $('#corpAddressState').val(data.StateID);
                                $('#corpAddressZip').val(data.Zip);
                            }
                        });
                    }
            }
        });

        // updating or creating the corp info
        $('#updateCorpInfoForm').submit(function (e) {
            e.preventDefault();

            var errs = $(this).find('.field-validation-error');
            if (errs.length == 0) {
                $('#facilityProcessLoadingModal').css('display', 'flex');

                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);
                        $('#facilityProcessLoadingModal').hide();

                        $('.corpID').val(data.FacilityID);
                        $('#saveChangesModal').css('display', 'flex');
                    }
                });
            }
        });

        // clearing the corp filter form
        $('#clearCorpFilterBtn').click(function () {
            $('.corp-filter-txt').val('');
            $('.corp-filter-dropdown').val($('.corp-filter-dropdown option:nth-child(1)').val());

            GetCorporationsPagedData(1);
        });

        // clicking the edit corp button
        $(document).on('click', '.update-corp-btn', function () {
            $('html').scrollTop(0);

            showCorpForms();

            $('.corpID').val($(this).attr('data-corpid'));
            $('#corpInfoTab').click();
        });

        // showing the new contact form
        $(document).on('click', '#addNewContactBtn', function () {
            $('#contactsContainer').hide();
            $('#updateCorpContactForm').show();
            $(this).hide();
        });

        // populating the contact form with an existing contacts data
        $(document).on('click', '.edit-contact', function () {
            $('#facilityProcessLoadingModal').css('display', 'flex');

            $.ajax({
                url: '/ManageCorporationsWidget/GetContactData',
                method: 'GET',
                data: { 'contactID': $(this).attr('data-contactid') },
                success: function (obj) {
                    var data = JSON.parse(obj);
                    $('#facilityProcessLoadingModal').hide();

                    $('.contactID').val(data.ContactID);
                    $('#contactUserID').val(data.ContactUserID)

                    $('#corpContactTypeDropdown').val(data.TypeID)
                    $('#corpContactFirstName').val(data.FirstName);
                    $('#corpContactLastName').val(data.LastName);
                    $('#corpContactEmail').val(data.Email);
                    $('#corpContactPhone').val(formatPhoneNumber(data.Phone));

                    if (data.AllowOnlinePurchases) {
                        $('#corpContactAllowPurchases').attr('checked', 'checked');
                    }

                    if (data.HasUserAcct) {
                        $('#corpContactNeedsUserAcct').hide();
                        $('#corpContactUserName').show();
                        $('#corpContactUserName').children('div').html(data.UserName);
                    }

                    $('#addNewContactBtn').hide();
                    $('#contactsContainer').hide();
                    $('#updateCorpContactForm').show();
                }
            });
        });

        // canceling out of the contact form
        $('#cancelContactForm').click(function () {
            $('#contactsContainer').show();
            $('#updateCorpContactForm').hide();
            $('#addNewContactBtn').show();

            clearCorpContactForm();
        });

        // updating an existing contacts info
        $('#updateCorpContactForm').submit(function (e) {
            e.preventDefault();

            var errs = $(this).find('.field-validation-error');
            if (errs.length == 0) {
                $('#facilityProcessLoadingModal').css('display', 'flex');

                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (obj) {
                        var data = JSON.parse(obj);
                        $('#facilityProcessLoadingModal').hide();

                        if (data.Err) {
                            $('#contactErrMsg').html(data.ErrMsg);
                        }
                        else {
                            clearCorpContactForm();

                            $('#contactsContainer').show();
                            $('#updateCorpContactForm').hide();
                            $('#addNewContactBtn').show();

                            //update contact list
                            updateCorpContactList(data.Contacts);
                        }
                    }
                });
            }
        });

        // showing confirm delete modal for deleting a contact
        $(document).on('click', '.delete-contact', function () {
            var contactID = $(this).attr('data-contactid');
            var contactName = $(this).attr('data-contactname');

            var modal = $('#deleteContactConfirmModal');
            $(modal).find('#deleteContactName').html(contactName);
            $(modal).find('#removeContactBtn').attr('data-contactid', contactID);

            $(modal).show();
        });

        // close delete contact confirm modal
        $('.close-delete-contact').click(function (e) {
            e.preventDefault();

            closeDeleteCorpContactModal();
        });

        // deleting contact
        $('.manage-corporations').on('click', '#removeContactBtn', function (e) {
            e.preventDefault();
            $('#deleteContactConfirmModal').hide();
            $('#facilityProcessLoadingModal').show();

            $.ajax({
                url: '/ManageCorporationsWidget/RemoveContact',
                method: 'POST',
                data: { 'contactID': $(this).attr('data-contactid'), 'corpID': $('.corpID').val() },
                success: function (obj) {
                    var data = JSON.parse(obj);
                    closeDeleteCorpContactModal();
                    $('#facilityProcessLoadingModal').hide();

                    if (data.Err) {
                        $('#contactErrMsg').html(data.ErrMsg);
                    }
                    else {
                        updateCorpContactList(data.Contacts);
                    }
                }
            });
        });

        // displaying an input for a new users user name and password email recipient
        $('#needsUserAcct').click(function () {
            var userPassInput = $('#userPassRecipientForm');
            var emailInput = $('#corpContactEmail');

            if ($(this).find('input[type=checkbox]').prop('checked') == true && emailInput.val() == "") {
                userPassInput.css('display', 'block');
                emailInput.hide();
                emailInput.siblings('label').hide();
            }
            else {
                userPassInput.css('display', 'none');
                emailInput.show();
                emailInput.siblings('label').show();
            }
        });

        // shows a confirm delete corp modal
        $(document).on('click', '.delete-corp-btn', function (e) {
            e.preventDefault();

            var corpID = $(this).attr('data-corpid');
            var corpName = $(this).attr('data-corpname');

            var modal = $('#deleteCorpConfirmModal');

            modal.find('#corpName').html(corpName);
            modal.find('#removeCorpBtn').attr('data-corpid', corpID);
            modal.show();
        });

        // closes confirm delete corp modal
        $('.close-delete-corp-modal').click(function (e) {
            e.preventDefault();
            closeDeleteCorpModal();
        });

        // removes a corp
        $('#removeCorpBtn').click(function (e) {
            e.preventDefault();

            var corpID = $(this).attr('data-corpid');

            closeDeleteCorpModal();
            $('#facilityProcessLoadingModal').css('display', 'flex');

            $.ajax({
                url: '/ManageCorporationsWidget/RemoveCorporation',
                method: 'POST',
                data: { 'corpID': corpID },
                success: function () {
                    $('#facilityProcessLoadingModal').hide();

                    GetCorporationsPagedData(1);
                }
            })
        });
    });
});

function GetCorporationsPagedData(pageNum) {
    var corpTable = $('#corpTable');
    corpTable.html('');

    var corpList = $('#corpList');
    var corpLoading = $('#corpLoading');
    var corpPagination = $('#corpPagination');

    corpPagination.hide();
    corpLoading.show();
    corpList.hide();

    $.getJSON('/ManageCorporationsWidget/GetPagedData', `${$("#corpFilterForm").serialize()}&pageNumber=${pageNum}&pageSize=16`, function (response) {
        var rowData = '';
        for (var i = 0; i < response.Data.length; i++) {
            var corp = response.Data[i];

            rowData += `<div class="col-12 col-md-4 col-lg-3 my-3">
                            <div class="corporation">
                                <h4><strong>${corp.Name}</strong></h4>
                                <h5 class="ms-2">${corp.FacilityCode}</h5>
                                <div class="d-flex align-items-center mt-auto">
                                    <button class="update-corp-btn p-0" data-corpID="${corp.FacilityID}"><i class="fa fa-pencil"></i></button>`;

            if (corp.IsSiteAdmin) {
                rowData += `        <a class="fa fa-trash delete-corp-btn ms-2" href="" data-corpID="${corp.FacilityID}" data-corpName="${corp.Name}"></a>`;
            }

            rowData += `            <a class="ms-auto ms-md-2 ms-lg-auto text-end" href="${MANAGE_FACILITY_PAGE}?corporationID=${corp.FacilityID}">`;

            if (corp.HasChildFacilities) {
                rowData += `Manage Facilities`;
            }
            else {
                rowData += `Add Facilities`;
            }

            rowData += `
                                    </a>
                                </div>
                            </div>
                        </div>
                        `;
        }

        corpLoading.hide();
        corpList.show();
        corpTable.html('');

        if (response.Data.length > 0) {
            $('#corps').show();
            $('#noCorpsText').hide();
            corpTable.append(rowData);
        }
        else {
            $('#corps').hide();
            $('#noCorpsText').show();
        }

        $('#corpPagination').html('');
        corpPagination.show();

        if (response.TotalPages != 1) {
            PagingTemplate(response.TotalPages, response.CurrentPage, '#corpPagination', 'GetCorporationsPagedData');
        }

        $('html, body').scrollTop(0);
    });
}

function showCorpForms() {
    $('.corp-forms').show();
    $('.corp-listing-objects').hide();
    $('#corp-code').hide();
    $('#add-corp-btn').show();
    $('#manageFacilitiesBtn').hide();
    $('#newCorp').hide();
    $('#viewAllCorpsBtn').hide();
    $('#backToCorpListingBtn').show();
}

function updateCorpContactList(contacts) {
    $('#contactsContainer').html('');

    if (contacts.length > 0) {
        for (var i = 0; i < contacts.length; i++) {
            var rowData = `
                                    <div class="col col-12 col-md-6 col-lg-4 p-3">
                                        <div class="contact p-3">
                                            <div class="row mb-3 mb-lg-0">
                                                <div class="col col-12">
                                                    <div class="d-flex justify-content-end">
                                                        <a class="fa fa-pencil me-3 edit-contact" data-contactID="${contacts[i].ContactID}"></a>
                                                        <a class="fa fa-trash delete-contact" data-contactID="${contacts[i].ContactID}" data-contactName="${contacts[i].FirstName} ${contacts[i].LastName}"></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-12">
                                                    <h4><strong>${contacts[i].TypeName}</strong></h4>
                                                </div>
                                            </div>
                                            <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Name:</strong> ${contacts[i].FirstName} ${contacts[i].LastName}</div>
                                                </div>
                                            </div>`;

            if (contacts[i].Email != null && contacts[i].Email != "") {
                rowData += `<div class="row my-2">
                                                <div class="col col-12">
                                                    <strong>Email:</strong>
                                                    <div>${contacts[i].Email}</div>
                                                </div>                                                
                                            </div>`;
            }

            if (contacts[i].Phone != "" && contacts[i].Phone) {
                rowData += `    <div class="row my-2">
                                                <div class="col col-12">
                                                    <div><strong>Phone:</strong> ${formatPhoneNumber(contacts[i].Phone)}</div>
                                                </div>
                                            </div>`;
            }

            if (contacts[i].UserName != "" && contacts[i].UserName != null) {
                rowData += `            
                                            <div class="row my-2">
                                                <strong>User Name:</strong>
                                                <div>${contacts[i].UserName}</div>
                                            </div>`;
            }

            rowData += `<div class="row">
                                                <div class="col col-12">
                                                    <div><strong>Can Make Facility Purchases?</strong> ${(contacts[i].AllowOnlinePurchases ? "Yes" : "No")}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;

            $('#contactsContainer').append(rowData);
        }
    }
    else {
        $('#contactsContainer').append('<p id="noCorpContactsTxt">No contacts found.</p>');
    }
}

function clearCorpContactForm() {
    $('.contactID').val('');
    $('#contactUserID').val('');

    $('#contactErrMsg').html('');

    $('#corpContactNeedsUserAcct').show();
    $('#corpContactUserName').children('div').html('');
    $('#corpContactUserName').hide();

    $('#userPassRecipientForm').hide();
    var emailInput = $('#corpContactEmail');
    emailInput.show();
    emailInput.siblings('label').show();

    $('.contact-data-txt').val('');
    $('.contact-data-validation').val('');
    $('.contact-data-dropdown').val($('.contact-data-dropdown option:nth-child(1)').val());
    $('.contact-data-checkbox').prop('checked', false);
}

function closeDeleteCorpContactModal() {
    var modal = $('#deleteContactConfirmModal');

    modal.hide();
    modal.find('#deleteContactName').html('');
    modal.find('#removeContactBtn').attr('data-contactid', '');
}

function closeDeleteCorpModal() {
    var modal = $('#deleteCorpConfirmModal');

    modal.hide();
    modal.find('#corpName').html('');
    modal.find('#removeCorpBtn').attr('data-corpid', '');
}