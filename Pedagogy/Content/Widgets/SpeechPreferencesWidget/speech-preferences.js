﻿$(function () {
    console.log('speech-preferences.js loaded..');

    var player;
    var speechBaseUrl = 'http://speech.pedagogy-inc.com/api/speak';
    var content = $(".speech-preferences-widget").find("#previewTxt");

    $('#preview-speech-btn').on('click', function (evt) {
        evt.preventDefault();

        if (player) {
            if (!player.paused) {
                player.pause();
            }

            player.remove();
        }
        $('#preview-speech-section').html('');

        var voice;
        var rate;

        var voiceDrpDwn = $('#voice');
        if (voiceDrpDwn) {
            voice = $("#voice option:selected").html();
        }

        var rateDrpDown = $('#rate');
        if (rateDrpDown) {
            rate = parseFloat($("#rate option:selected").html());
        }

        if (!voice) return;
        if (!rate)
            rate = 1.00;

        var fullUrl = speechBaseUrl + '?content=' + content + '&voice=' + voice;

        $('#preview-speech-btn').prop('disabled', true);
        $('#preview-speech-section').html('<audio id="preview-audio" preload="auto">' +
            '<source src="' +
            fullUrl +
            '" type="audio/mp3" />' +
            '</audio>');

        player = new MediaElementPlayer('preview-audio', {
            //pluginPath: 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/',
            pluginPath: '/Scripts/Vendor/MediaElement/mediaelement-and-player.min.js',
            shimScriptAccess: 'always',
            loop: false,
            defaultSpeed: rate,
            features: ['playpause', 'speed', 'progress', 'current', 'duration', 'fullscreen'],
            success: function (mediaElement, originalNode) {
                $('#preview-speech-btn').prop('disabled', false);
                mediaElement.play();
            }
        });
    });

    $(document).ready(function () {
        $("#close-save-changes-modal").click(function () {
            $("#save-changes-modal").css("display", "none");
        });

        $('#speechForm').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr("action"),
                method: $(this).attr("method"),
                data: $(this).serialize(),
                success: function () {
                    $("#saveChangesModal").css("display", "block");
                }
            });
        });
    });
});