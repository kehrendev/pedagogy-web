﻿$(function () {
    console.log('reports.js loaded..');

    $(document).ready(function () {
        $('#prog-filter-facilities').val($('#progReportFacilityDropdown').val());

        if ($('#corp-report-content').length) {
            GetCorporateReportPagedData(1);
        }

        if ($('#prog-report-content').length) {
            GetProgressReportPagedData(1);
        }

        $("#filter-prog-report").click(function (e) {
            e.preventDefault();
            GetProgressReportPagedData(1);
        });

        $('#progReportFacilityDropdown').change(function () {
            $('#prog-filter-facilities').val($(this).val());

            GetProgressReportPagedData(1);
        });

        $('#progReportExportToExcel').click(function (e) {
            e.preventDefault();

            $("#filter-progress-report").attr("action", "/ReportsWidget/ExportProgressReportToExcel");
            $("#filter-progress-report").submit();

        });

        $('#progReportExportToCSV').click(function (e) {
            e.preventDefault();

            $("#filter-progress-report").attr("action", "/ReportsWidget/ExportProgressReportToCSV");
            $("#filter-progress-report").submit();

        });

        $('#progReportExportToPDF').click(function (e) {
            e.preventDefault();

            $("#filter-progress-report").attr("action", "/ReportsWidget/xportProgressReportToPDF");
            $("#filter-progress-report").submit();
        });

        $('#corpReportFacilityDropdown').change(function () {
            GetCorporateReportPagedData(1);
        });

        $('#corpReportExportToExcel').click(function (e) {
            e.preventDefault();

            var facilityID = $('#corpReportFacilityDropdown').val();
            window.location.href = $(this).attr('href') + '?facilityID=' + facilityID;

        });

        $('#corpReportExportToCSV').click(function (e) {
            e.preventDefault();

            var facilityID = $('#corpReportFacilityDropdown').val();
            window.location.href = $(this).attr('href') + '?facilityID=' + facilityID;

        });

        $('#corpReportExportToPDF').click(function (e) {
            e.preventDefault();

            var facilityID = $('#corpReportFacilityDropdown').val();
            window.location.href = $(this).attr('href') + '?facilityID=' + facilityID;
        });
    });

    

    
});

function GetProgressReportPagedData(pageNum) {
    $('#progDataLoading').show();
    $('#progReportData').hide();
    $('#progReportFilter').hide();

    $.getJSON("/ReportsWidget/GetProgressReportPagedData", $("#filter-progress-report").serialize() + `&pageNumber=${pageNum}`, function (response) {
        $('#progDataLoading').hide();

        var data = JSON.parse(response);

        if (data.HasReportItems) {
            var classDropdown = $('#prog-filter-classes');
            classDropdown.html('<option value></option>');

            if (data.Filters.Classes.length > 0) {
                for (var i = 0; i < data.Filters.Classes.length; i++) {
                    classDropdown.append(`<option value="${data.Filters.Classes[i].Value}">${data.Filters.Classes[i].Text}</option>`);
                }
            }

            var studentDropdown = $('#prog-filter-students');
            studentDropdown.html('<option value></option>');

            if (data.Filters.Students.length > 0) {
                for (var i = 0; i < data.Filters.Students.length; i++) {
                    studentDropdown.append(`<option value="${data.Filters.Students[i].Value}">${data.Filters.Students[i].Text}</option>`);
                }
            }

            $('#progReportData').show();
            $('#progReportFilter').show();

            var rowData = "";
            for (var i = 0; i < data.PagingData.Data.length; i++) {
                var item = data.PagingData.Data[i];

                rowData += '<tr><th>' + item.FacilityName + '</th><th>' + item.ClassName + '</th><th>' + item.StudentName + '</th><th>' + item.Status + '</th><th>'
                    + item.TestScore + '</th><th>' + item.LastAccessDay + '</th><th>' + item.Source + '</th><th>' + item.ClassTime + '</th><th>' + item.TestTime
                    + '</th></tr>';
            }

            $("#prog-report-content").html("");
            if (rowData != "") {
                $('#progReportNoneFoundTxt').hide();
                $("#prog-report-content").append(rowData);
            }
            else {
                $('#progReportData').hide();
                $('#progReportNoneFoundTxt').show();
            }

            $("#progress-report-pagination").html("");
            if (data.PagingData.TotalPages > 1) {
                PagingTemplate(data.PagingData.TotalPages, data.PagingData.CurrentPage, "#progress-report-pagination", "GetProgressReportPagedData");
            }
        }
        else {
            $('#progReportNoneFoundTxt').show();
        }


        $("html, body").scrollTop(0);
    });
}

function GetCorporateReportPagedData(pageNum) {
    var facilityID = $('#corpReportFacilityDropdown').val();
    $('#corpReportData').hide();
    $('#corpDataLoading').show();

    $.getJSON("/ReportsWidget/GetCorporateReportPagedData", { pageNumber: pageNum, facilityID: facilityID }, function (response) {
        $('#corpDataLoading').hide();
        $('#corpReportData').show();

        var rowData = "";
        for (var i = 0; i < response.Data.length; i++) {
            rowData = rowData + '<tr><td>' + response.Data[i].FacilityName + '</td><td>' + response.Data[i].ClassName + '</td><td>' + response.Data[i].QtyNotStarted
                + '</td><td>' + response.Data[i].QtyInProgress + '</td><td>' + response.Data[i].QtyCompleted + '</td><td>' + response.Data[i].ClassTotal + '</td></tr>';
        }

        $("#corp-report-content").html("");
        if (rowData != "") {
            $('#corpReportNoneFoundTxt').hide();
            $("#corp-report-content").append(rowData);
        }
        else {
            $('#corpReportData').hide();
            $('#corpReportNoneFoundTxt').show();
        }

        $("#corporate-report-pagination").html("");
        if (response.TotalPages > 1) {
            PagingTemplate(response.TotalPages, response.CurrentPage, "#corporate-report-pagination", "GetCorporateReportPagedData");
        }

        $("html, body").scrollTop(0);
    });
}

function clearProgressFilter() {
    $("#prog-filter-classes").val("");
    $("#prog-filter-statuses").val("");
    $("#prog-filter-students").val("");
    $("#last-access-begin-date").val("");
    $("#last-access-end-date").val("");

    GetProgressReportPagedData(1);
}