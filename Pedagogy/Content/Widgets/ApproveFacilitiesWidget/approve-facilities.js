﻿$(function () {
    console.log('approve-facilities.js loaded..');

    $(document).ready(function () {
        if ($('#facility-pending-list-group').length) {
            GetPendingPageData(1);
        }

        if ($('#facility-rejected-list-group').length) {
            GetRejectedPageData(1);
        }

        $(".pending-facilities, .rejected-facilities").on('submit', '.approve-facility-form', function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (evt) {
                    if (evt.Err) {
                        $(".err-msg").text(evt.ErrMsg);
                        $("html, body").scrollTop(0);
                    }
                    else {
                        $(".err-msg").html("");
                        window.location.href = evt.ReturnUrl;
                    }
                }
            });
        });

        $(".pending-facilities").on('click', '.reject-facility', function (e) {
            e.preventDefault();

            $("#hidden-itemID").val($(this).attr("data-itemID"));
            $("#reject-confirmation-modal").show();
        });

        $(".rejected-facilities").on('click', '.delete-facility', function (e) {
            e.preventDefault();

            $("#name").val($(this).attr("data-facility-name"));
            $("#hidden-facility-itemID").val($(this).attr("data-itemID"));
            $("#delete-confirmation-modal").show();
        });

        $("#close").click(function () {
            $("#reject-confirmation-modal").hide();
            $("#hidden-itemID").val("");
        });

        $(".close-delete-permanently-modal").click(function (e) {
            e.preventDefault();

            $("#delete-confirmation-modal").hide();
            $("#hidden-facility-itemID").val("");
            $("#name").val("");
        });
    });
});

function GetPendingPageData(pageNum) {
    $.getJSON("/ApproveFacilitiesWidget/GetPendingPagedData", { pageNumber: pageNum, pageSize: $(".approve-facilities-widget").find("#facilitiesPerPage").val() },
        function (response) {
            var rowData = "";
            for (var i = 0; i < response.Data.length; i++) {
                rowData = rowData +
                    `<div class="col-12 col-md-5 m-2">
                        <div class="facility p-3">
                            <div class="row">
                                <h3>Admin Info</h3>
                                <div class="col-12 col-md-6">
                                    <div class="m-2"><strong>UserID:</strong> ${response.Data[i].AdminUserID}</div>
                                    <div class="m-2"><strong>First Name:</strong> ${response.Data[i].AdminFirstName}</div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="m-2"><strong>Email:</strong> ${response.Data[i].AdminEmail}</div>
                                    <div class="m-2"><strong>Last Name:</strong> ${response.Data[i].AdminLastName}</div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <h3>${response.Data[i].IsFacility ? "Facility" : "Corporate"} Info</h3>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="m-2"><strong>Name:</strong> ${response.Data[i].FacilityName}</div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="m-2 is-facility"><strong>Is a Facility?</strong> ${response.Data[i].IsFacility ? "Yes" : "No"}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="m-2"><strong>Address 1:</strong> ${response.Data[i].FacilityAddress1}</div>
                                        <div class="m-2"><strong>Address 2:</strong> ${response.Data[i].FacilityAddress2}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                       <div class="m-2"><strong>City:</strong> ${response.Data[i].FacilityCity}</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="m-2"><strong>State:</strong> ${response.Data[i].FacilityState}</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="m-2"><strong>Zip:</strong> ${response.Data[i].FacilityZip}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">`;

                if (response.Data[i].IsFacility) {
                    if (response.Data[i].NoApplicableParentCorp) {
                        rowData += "<div>No applicable parent corporation.</div>";
                    }
                    else {
                        rowData += `<div class="m-2"><strong>Parent Corporation Name:</strong> ${response.Data[i].ParentCorpName}</div>`;
                        rowData += `<div class="m-2"><strong>Parent Corporation Website URL:</strong> ${response.Data[i].ParentCorpWebsiteURL}</div>`;
                    }
                }

                rowData +=
                    `</div>
                                </div>
                            </div>
                            <div class="${response.Data[i].IsFacility ? "" : "d-none"}">
                                <form action="/ApproveFacilitiesWidget/ApproveFacility" method="POST" class="approve-facility-form">
                                    <input type="hidden" name="itemID" value="${response.Data[i].ItemID}" />
                                    <div class="row mt-3" id="corp-${response.Data[i].ItemID}">
                                        ${$(".corp-dropdown").html()}
                                    </div>
                                    <div class="d-flex justify-content-around mt-4">
                                        <input type="submit" value="Accept" />
                                        <a href="" class="reject-facility" data-itemID="${response.Data[i].ItemID}" role="button">Reject</a>
                                    </div>
                                </form>
                            </div>
                            <div class="${response.Data[i].IsFacility ? "d-none" : ""}">
                                <form action="/ApproveFacilitiesWidget/ApproveFacility" method="POST" class="approve-facility-form">
                                    <input type="hidden" name="itemID" value="${response.Data[i].ItemID}" />
                                    <div class="d-flex justify-content-around mt-4">
                                        <input type="submit" value="Accept" />
                                        <a href="" class="reject-facility" data-itemID="${response.Data[i].ItemID}" role="button">Reject</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>`;
            }
            $("#facility-pending-list-group").html("");
            if (rowData != "") {
                $("#facility-pending-list-group").append(rowData);
            }
            else {
                var noResults = $('.approve-facilities-widget').find('#noPendingFacilitiesTxt').html();
                $("#pending").html("");
                $("#pending").append(noResults);
            }
            $("#pending-facilities-pagination").html("");
            if (response.TotalPages != 1) {
                PagingTemplate(response.TotalPages, response.CurrentPage, "#pending-facilities-pagination", "GetPendingPageData");
            }
            $("html, body").scrollTop(0);
        });
}

function GetRejectedPageData(pageNum) {
    $.getJSON("/ApproveFacilitiesWidget/GetRejectedPagedData", {
        pageNumber: pageNum, pageSize: $(".approve-facilities-widget").find("#facilitiesPerPage").val()
    }, function (response) {
        var rowData = "";
        for (var i = 0; i < response.Data.length; i++) {
            rowData = rowData +
                `<div class="col-12 col-md-5 m-2">
                        <div class="facility p-3">
                            <div class="row">
                                <h3>Admin Info</h3>
                                <div class="col-12 col-md-6">
                                    <div class="m-2"><strong>UserID:</strong> ${response.Data[i].AdminUserID}</div>
                                    <div class="m-2"><strong>First Name:</strong> ${response.Data[i].AdminFirstName}</div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="m-2"><strong>Email:</strong> ${response.Data[i].AdminEmail}</div>
                                    <div class="m-2"><strong>Last Name:</strong> ${response.Data[i].AdminLastName}</div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <h3>${response.Data[i].IsFacility ? "Facility" : "Corporate"} Info</h3>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="m-2"><strong>Name:</strong> ${response.Data[i].FacilityName}</div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="m-2 is-facility"><strong>Is a Facility?</strong> ${response.Data[i].IsFacility ? "Yes" : "No"}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="m-2"><strong>Address 1:</strong> ${response.Data[i].FacilityAddress1}</div>
                                        <div class="m-2"><strong>Address 2:</strong> ${response.Data[i].FacilityAddress2}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                       <div class="m-2"><strong>City:</strong> ${response.Data[i].FacilityCity}</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="m-2"><strong>State:</strong> ${response.Data[i].FacilityState}</div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="m-2"><strong>Zip:</strong> ${response.Data[i].FacilityZip}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">`;

            if (response.Data[i].IsFacility) {
                if (response.Data[i].NoApplicableParentCorp) {
                    rowData += "<div>No applicable parent corporation.</div>";
                }
                else {
                    rowData += `<div class="m-2"><strong>Parent Corporation Name:</strong> ${response.Data[i].ParentCorpName}</div>`;
                    rowData += `<div class="m-2"><strong>Parent Corporation Website URL:</strong> ${response.Data[i].ParentCorpWebsiteURL}</div>`;
                }
            }

            rowData +=
                `               </div>
                                </div>
                            </div>
                            <div class="${response.Data[i].IsFacility ? "" : "d-none"}">
                                <form action="/ApproveFacilitiesWidget/ApproveFacility" method="POST" class="approve-facility-form">
                                    <input type="hidden" name="itemID" value="${response.Data[i].ItemID}" />
                                    <div class="row mt-3" id="corp-${response.Data[i].ItemID}">
                                        ${$(".corp-dropdown").html()}
                                    </div>
                                    <div class="d-flex justify-content-around mt-4">
                                        <input type="submit" value="Accept" />
                                        <a href="" class="delete-facility" data-itemID="${response.Data[i].ItemID}" role="button">Delete Permanently</a>
                                    </div>
                                </form>
                            </div>
                            <div class="${response.Data[i].IsFacility ? "d-none" : ""}">
                                <form action="/ApproveFacilitiesWidget/ApproveFacility" method="POST" class="approve-facility-form">
                                    <input type="hidden" name="itemID" value="${response.Data[i].ItemID}" />
                                    <div class="d-flex justify-content-around mt-4">
                                        <input type="submit" value="Accept" />
                                        <a href="" class="delete-facility" data-itemID="${response.Data[i].ItemID}" role="button">Delete Permanently</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>`;
        }
        $("#facility-rejected-list-group").html("");
        if (rowData != "") {
            $("#facility-rejected-list-group").append(rowData);
        }
        else {
            var noResults = $('.approve-facilities-widget').find('#noRejectedFacilitiesTxt').html();
            $("#rejected").html("");
            $("#rejected").append(noResults);
        }
        $("#rejected-facilities-pagination").html("");
        if (response.TotalPages != 1) {
            PagingTemplate(response.TotalPages, response.CurrentPage, "#rejected-facilities-pagination", "GetRejectedPageData");
        }
        $("html, body").scrollTop(0);
    });
}