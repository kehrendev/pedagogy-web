﻿$(function () {
    console.log('personal-info.js loaded..');

    $(document).ready(function () {

        $('#updatePersonalInfoForm').submit(function (e) {
            e.preventDefault();

            var errs = $(this).find('.field-validation-error');
            if (errs.length == 0) {
                $.ajax({
                    url: $(this).attr('action'),
                    method: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function () {
                        $('#saveChangesModal').css('display', 'block');
                    }
                });
            }
        });

        $('#updatePersonalAddressesForm').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (data) {
                    $('#addressForm').css('display', 'block');
                    $('#newAddress').css('display', 'none');
                    $('#updateAddressBtn').css('display', 'block');
                    $('#addAddressBtn').css('display', 'none');

                    var addressData = JSON.parse(data);
                    console.log(addressData);

                    $('#line1').val(addressData.Line1);
                    $('#line2').val(addressData.Line2);
                    $('#city').val(addressData.City);
                    $('#postalCode').val(addressData.PostalCode);
                    $('#countryID').val(addressData.CountryID);
                    $('#countryID').parent().data('stateselectedid', addressData.StateID);
                    $('.js-country-selector').change();
                }
            });
        });

        $('#cancelAddressUpdate').click(function () {
            $('#addressForm').css('display', 'none');
            $('#newAddress').css('display', 'block');
            $('#updateAddressBtn').css('display', 'none');
            $('#addAddressBtn').css('display', 'none');
            $('.no-addresses-txt').show();

            $('#line1').val("");
            $('#line2').val("");
            $('#city').val("");
            $('#postalCode').val("");
            $('#countryID').val(0);
            $('#stateID').val(0);
            $('#addressID').val(0);

            $('#line1Validation').html("");
            $('#cityValidation').html("");
            $('#postalCodeValidation').html("");

            $("html, body").scrollTop(0);
        });

        $('#newAddress').click(function () {
            $('#newAddress').css('display', 'none');
            $('#addressForm').css('display', 'block');
            $('#addAddressBtn').css('display', 'block');
            $('#updateAddressBtn').css('display', 'none');
            $('.no-addresses-txt').hide();
        });

        // Executes whenever a country is selected
        $('.js-country-selector').change(function () {
            var $countrySelector = $(this),
                $countryStateSelector = $countrySelector.parents('.js-country-state-selector'),
                $stateSelector = $countryStateSelector.parents('.col').siblings('.col').find('.js-state-selector'),
                $stateSelectorContainer = $countryStateSelector.parents('.col').siblings('.col').find('.js-state-selector-container'),
                selectedStateId = $countryStateSelector.data('stateselectedid'),
                url = $countryStateSelector.data('statelistaction'),
                postData = {
                    countryId: $countrySelector.val()
                };

            $stateSelectorContainer.hide();

            if (!postData.countryId) {
                return;
            }

            // Sends a POST request to the 'CountryStates' endpoint of the 'CheckoutController'
            $.post(url, postData, function (data) {
                $countryStateSelector.data('stateselectedid', 0);
                $stateSelector.val(null);

                if (!data.length) {
                    return;
                }

                // Fills and shows the state selector element
                fillStateSelector($stateSelector, data);
                $stateSelectorContainer.show();

                if (selectedStateId > 0) {
                    $stateSelector.val(selectedStateId);
                }
            });
        });


        // Sets the default option for the state selector
        $('.js-country-state-selector').each(function () {
            var $selector = $(this),
                $countrySelector = $selector.find('.js-country-selector'),
                countryId = $selector.data('countryselectedid');

            if (countryId > 0) {
                $countrySelector.val(countryId);
            }

            $countrySelector.change();
            $selector.data('countryselectedid', 0);
        });

        // Fills the state selector with retrieved states
        function fillStateSelector($stateSelector, data) {
            var items = '';

            $.each(data, function (i, state) {
                items += '<option value="' + state.id + '">' + state.name + '</option>';
            });

            $stateSelector.html(items);
        }
    });
});