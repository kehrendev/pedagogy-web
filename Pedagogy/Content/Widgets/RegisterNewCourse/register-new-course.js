﻿$(function () {
    console.log('register-new-course.js loaded..');

    $(document).ready(function () {

        $("#class-dropdown").change(function () {
            $(".description").each(function () {
                $(this).css("display", "none");
            });

            $(`#class-desc-${$(this).val()}`).css("display", "block");
        });
                
        $(".add-to-cart-form").submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr("action"),
                method: $(this).attr("method"),
                data: $(this).serialize(),
                success: function (data) {
                    $("html, body").scrollTop(0);

                    $(".description").each(function () {
                        $(this).css("display", "none");
                    });

                    $("#class-dropdown").val("");

                    $("#modal-msg").html(data);
                    $("#view-cart-confirmation-modal").css("display", "block");
                }
            });
        });

        $(".close").click(function () {
            $("#view-cart-confirmation-modal").css("display", "none");
        });

    });
});