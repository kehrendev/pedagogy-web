﻿$(function () {
    console.log('manage-education-plans.js loaded..');

    $(document).ready(function () {
        if ($('#available-classes').length) {
            GetAddClassesPagedData(1);
        }

        $("#add-classes-btn").click(function (e) {
            e.preventDefault();
            $("#select-classes-modal").show();
        });
    });
});

function displayNewEducationPlanForm() {
    $("#new-education-plan").show();
    $("#new-education-plan-btn").hide();
    $("#education-plan-list").hide();
}

function displayEducationPlans() {
    $("#new-education-plan").hide();
    $("#new-education-plan-btn").show();
    $("#education-plan-list").show();
    $("html, body").scrollTop(0);
}

function closeSelectClassModal() {
    $("#select-classes-modal").hide();
}

function GetAddClassesPagedData(pageNum) {
    $.getJSON("/ManageEducationPlansWidget/GetAddClassesPagedData", { pageNum }, function (response) {
        var rowData = "";
        for (var key in response.Data) {
            rowData = rowData + '<label><input type="checkbox" name="classes[]" value="' + key + '" /> ' + response.Data[key] + '</label>';
        }
        $("#available-classes").html("");
        $("#available-classes").append(rowData);
        $("#plan-add-class-pagination").html("");
        if (response.TotalPages != 1) {
            PagingTemplate(response.TotalPages, response.CurrentPage, "#plan-add-class-pagination", GetAddClassesPagedData);
        }
    });
}