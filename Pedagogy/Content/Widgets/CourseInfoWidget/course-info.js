﻿$(function () {
    console.log('course-info.js loaded..');

    $(document).ready(function () {
        // completed tab click event
        $('.completed-tab').click(function () {
            $('#noCompletedCoursesTxt').hide();
            $('#completedCoursesLoading').show();

            // run an ajax call to get the completed courses
            $.ajax({
                url: '/CourseInfoWidget/GetCompletedCourses',
                method: 'GET',
                data: {},
                success: function (obj) {
                    $('#completedCoursesLoading').hide();

                    var data = JSON.parse(obj);

                    if (data.Courses.length > 0) {
                        $('#noCompletedCoursesTxt').hide();
                        $('.completed-course-data').show();

                        var completedCourses = $('#completedCourses');
                        completedCourses.html('');

                        for (var i = 0; i < data.Courses.length; i++) {
                            var course = data.Courses[i];

                            var rowData = `<tr>
                                            <td>${course.ClassName}</td>
                                            <td>${course.TestScore}%</td>
                                            <td>${course.DateCompletedOn}</td>
                                            <td><a href="${course.ClassPageUrl}">View Course</a></td>
                                            <td>`;

                            var attempts = Object.keys(course.AttemptUrls);
                            if (attempts.length > 0) {
                                rowData += '<ul class="test-attempts">';

                                if (attempts.length > 2) {

                                    var startingAttempts = attempts.slice(0, 2);
                                    var endingAttempts = attempts.slice(2, attempts.length);

                                    for (var y = 0; y < startingAttempts.length; y++) {
                                        rowData += `<li class="attempt"><a href="${startingAttempts[y]}">Attempt ${startingAttempts[y]}</a></li>`;
                                    }

                                    for (var x = 0; x < endingAttempts.length; x++) {
                                        rowData += `<li class="attempt hidden"><a href="${endingAttempts[x]}">Attempt ${endingAttempts[x]}</a></li>`;
                                    }

                                    rowData += `<li class="view-all-attempts"><a href="" role="button">View all</a></li>
                                                <li class="view-less-attempts"><a href="" role="button">View less</a></li>`;
                                } else {

                                    for (var y = 0; y < Object.keys(course.AttemptUrls).length; y++) {
                                        rowData += `<li class="attempt"><a href="${Object.values(course.AttemptUrls)[y]}">Attempt ${Object.keys(course.AttemptUrls)[y]}</a></li>`;
                                    }
                                }

                                rowData += '</ul>';
                            }

                                rowData += `</td>
                                            <td class="print-icon"><a href="${course.PrintCertUrl}" target="_blank"><i class="fa fa-print"></i></a></td>
                                        </tr>`;

                            completedCourses.append(rowData);
                        }
                    }
                    else {
                        $('.completed-course-data').hide();
                        $('#noCompletedCoursesTxt').show();
                    }
                }
            });
        });

        // membership tab click event
        $('.memberships-tab').click(function () {
            $('#noMembershipsTxt').hide();
            $('#membershipCoursesLoading').show();

            var memberships = $('.membership-data');
            memberships.html('');

            // run an ajax call to get the current memberships
            $.ajax({
                url: '/CourseInfoWidget/GetMemberships',
                method: 'GET',
                data: {},
                success: function (obj) {
                    $('#membershipCoursesLoading').hide();

                    var data = JSON.parse(obj);

                    if (data.Memberships.length > 0) {
                        $('#noMembershipsTxt').hide();

                        memberships.show();

                        for (var x = 0; x < data.Memberships.length; x++) {
                            var rowData = `<div class="membership">
                                            <h3>${data.Memberships[x].MembershipName}</h3>
                                            <table class="table membership-table">
                                                <thead>
                                                    <tr>
                                                        <th>Course Title</th>
                                                        <th>Expiration Date</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>`;

                            for (var y = 0; y < data.Memberships[x].Classes.length; y++) {
                                var course = data.Memberships[x].Classes[y];

                                rowData += `<tr style="${(course.OnStart ? "" : "background-color:lightgray;")}">
                                                <td>${course.ClassName}</td>
                                                <td>${course.ExpDate}</td>
                                                <td>
                                                    <form method="POST" action="/CourseInfoWidget/CheckRequirements" class="start-mem-class-form">
                                                        <input type="hidden" name="classUrl" value="${course.ClassPageUrl}" />
                                                        <button type="submit" style="border:unset;background-color:transparent;${(course.OnStart ? "" : "color:black;")}" ${(course.OnStart ? "" : "disabled")}>${course.WorkflowStep}</button>
                                                    </form>
                                                </td>
                                            </tr>`;
                            }

                            rowData +=         `</tbody>
                                            </table>
                                        </div>`;

                            memberships.append(rowData);
                        }
                    }
                    else {
                        $('.membership-data').hide();
                        $('#noMembershipsTxt').show();
                    }
                }
            });
        });

        $(document).on('click', '.start-mem-class-form', function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (obj) {
                    var data = JSON.parse(obj);
                    console.log(data);
                    if (data.Err) {
                        // display warning modal and populate with error messages
                        var errList = $('#start-class-err-list-for-memberships');
                        for (var x = 0; x < data.ErrMsg.length; x++) {
                            var li = document.createElement("li");
                            li.setAttribute("style", "list-style:disc;");
                            li.setAttribute("class", "my-3");
                            li.innerHTML = data.ErrMsg[x];
                            errList.append(li);
                        }

                        $('#confirmation-modal-for-memberships').show();
                    }
                    else {
                        // navigate to the course
                        window.location.href = data.Url
                    }
                }
            });
        });

        $('.check-reqsfor-class').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (obj) {
                    var data = JSON.parse(obj);

                    if (data.Err) {
                        // display warning modal and populate with error messages
                        var errList = $('#start-class-err-list');
                        for (var x = 0; x < data.ErrMsg.length; x++) {
                            var li = document.createElement("li");
                            li.setAttribute("style", "list-style:disc;");
                            li.setAttribute("class", "my-3");
                            li.innerHTML = data.ErrMsg[x];
                            errList.append(li);
                        }

                        $('#confirmation-modal').show();
                    }
                    else {
                        // navigate to the course
                        window.location.href = data.Url
                    }
                }
            });
        });

        // view all test attempts button click
        $(document).on('click', '.view-all-attempts a', function (e) {
            e.preventDefault();

            $(this).parent().siblings('.attempt.hidden').each(function () {
                $(this).removeClass('hidden');
                $(this).addClass('show');
            });

            $(this).parent().siblings('.view-less-attempts').show();
            $(this).parent().hide();
        });

        // view less test attempts button click
        $(document).on('click', '.view-less-attempts a', function (e) {
            e.preventDefault();

            $(this).parent().siblings('.attempt.show').each(function () {
                $(this).removeClass('show');
                $(this).addClass('hidden');
            });

            $(this).parent().siblings('.view-all-attempts').show();
            $(this).parent().hide();
        });
    });
});

function closeStudentRequirementsModalForMemberships() {
    document.getElementById("confirmation-modal-for-memberships").style.display = "none";
    document.getElementById("start-class-err-list-for-memberships").innerHTML = "";
}

function closeStudentRequirementsModal() {
    document.getElementById("confirmation-modal").style.display = "none";
    document.getElementById("start-class-err-list").innerHTML = "";
}

function showStudentClassDeleteModal(item) {
    var studentClassID = document.querySelector("#studentClassID");
    studentClassID.value = item.getAttribute("data-studentclassid");

    var warningText = document.querySelector(".warning-text");
    warningText.innerHTML = "";
    warningText.innerHTML = item.getAttribute("data-warningtext");

    var deleteModal = document.querySelector(".delete-student-class-modal");
    deleteModal.style.display = "block";
}

function closeStudentClassDeleteModal() {
    $(".delete-student-class-modal").attr("data-studentclassid", "");
    $(".delete-student-class-modal").hide();
}