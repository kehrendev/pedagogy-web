﻿$(function () {
    console.log('info-section.js loaded..');

    $('#infoSectionTabs a').on('click', function (e) {
        e.preventDefault();

        var tab = new bootstrap.Tab($(this));
        tab.show();
    });
});