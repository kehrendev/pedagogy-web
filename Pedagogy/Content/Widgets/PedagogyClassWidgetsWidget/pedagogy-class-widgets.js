﻿$(function () {
    console.log('pedagogy-class-widget.js loaded..');

    $(document).ready(function () {
        $("#generate-scripts-form").submit(function (e) {
            e.preventDefault();

            $("#generated-code").css("display", "block");

            var container = $("#pedagogy-widget-script");
            container.html("");

            var classWidget = $("#pedagogy-class-widget");
            classWidget.html("");

            container.append('&lt;div class=pedagogy-widget>');
            container.append("<br />");

            container.append('&lt;script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></scrip' + 't>');
            container.append("<br />");

            classWidget.append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></scrip' + 't>');

            container.append('&lt;link rel="stylesheet" type="text/css" href="//dev.pedagogyeducation.com/Content/css/ClassFeeds.css" />');
            container.append("<br />");

            classWidget.append('<link rel="stylesheet" type="text/css" href="//dev.pedagogyeducation.com/Content/css/ClassFeeds.css" />');

            if ($("#widget-type").val() == "classListing") {

                container.append('&lt;script type="text/javascript" src="//dev.pedagogyeducation.com/Scripts/pedagogyClassWidget/PedagogyClasses.js"></scrip' + 't>');
                container.append("<br />");

                classWidget.append('<script type="text/javascript" src="//dev.pedagogyeducation.com/Scripts/pedagogyClassWidget/PedagogyClasses.js"></scrip' + 't>');

                var color = $("input[name='ColorStyle']:checked").val();
                if (color == "Red") {
                    container.append('&lt;div id="PedagogyClasses" class="Default">&lt;/div>');
                    container.append("<br />");

                    classWidget.append('<div id="PedagogyClasses" class="Default"></div>');
                }
                else if (color == "Blue") {
                    container.append('&lt;div id="PedagogyClasses" class="Basic">&lt;/div>');
                    container.append("<br />");

                    classWidget.append('<div id="PedagogyClasses" class="Basic"></div>');
                }
                else {
                    container.append('&lt;div id="PedagogyClasses" class="Advanced">&lt;/div>');
                    container.append("<br />");

                    classWidget.append('<div id="PedagogyClasses" class="Advanced"></div>');
                }
            }
            else {
                container.append('&lt;script type="text/javascript" src="//dev.pedagogyeducation.com/Scripts/pedagogyClassWidget/PedagogyFeaturedClasses.js"></scrip' + 't>');
                container.append("<br />");

                classWidget.append('<script type="text/javascript" src="//dev.pedagogyeducation.com/Scripts/pedagogyClassWidget/PedagogyFeaturedClasses.js"></scrip' + 't>');

                var color = $("input[name='ColorStyle']:checked").val();
                if (color == "Red") {
                    container.append('&lt;div id="PedagogyFeaturedClasses" class="Default">&lt;/div>');
                    container.append("<br />");

                    classWidget.append('<div id="PedagogyFeaturedClasses" class="Default"></div>');
                }
                else if (color == "Blue") {
                    container.append('&lt;div id="PedagogyFeaturedClasses" class="Basic">&lt;/div>');
                    container.append("<br />");

                    classWidget.append('<div id="PedagogyFeaturedClasses" class="Basic"></div>');
                }
                else {
                    container.append('&lt;div id="PedagogyFeaturedClasses" class="Advanced">&lt;/div>');
                    container.append("<br />");

                    classWidget.append('<div id="PedagogyFeaturedClasses" class="Advanced"></div>');
                }
            }

            if ($("#code").val() != "") {
                container.append(`&lt;input type="hidden" id="hdnLocationID" value="${$("#code").val()}" />`);
                container.append("<br />");

                classWidget.append(`input type="hidden" id="hdnLocationID" value="${$("#code").val()}" />`);
            }

            container.append('&lt;/div>');
        });
    });
});