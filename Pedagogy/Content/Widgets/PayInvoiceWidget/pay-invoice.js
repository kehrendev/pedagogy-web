﻿$(function () {
    console.log('pay-invoice.js loaded..');

    $(document).ready(function () {
        $("#payment-form").submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr("action"),
                method: $(this).attr("method"),
                data: $(this).serialize(),
                success: function (obj) {
                    var data = JSON.parse(obj);

                    if (!data.Err) {
                        $("#order-paid-msg").html("");
                        $("#order-paid-msg").hide();
                        $("#order-invoice").html("");
                        $("#order-details").hide();
                        $("#orderID").val("");
                        $("#payment-confirmation").show();
                        $("#cvv-code").val("");
                        $("#card-number").val("");
                        $("#month").val($("#month option:nth-child(1)").val());
                        $("#year").val($("#year option:nth-child(1)").val());
                    }
                    else {
                        $("#payment-confirmation").hide();
                        $("#card-errors").html("");
                        $("#card-errors").html(data.ErrMsg);
                    }
                }
            });
        });
    });
});