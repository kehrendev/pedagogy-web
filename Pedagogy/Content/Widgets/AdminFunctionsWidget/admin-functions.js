﻿$(function () {
    console.log('admin-functions.js loaded..');

    $(document).ready(function () {
        if ($('#usersListGroup').length) {
            GetUsersPageData(1);
        }

        $("#switch-user").click(function (e) {
            e.preventDefault();
            $("#select-user-confirmation-modal").css("display", "flex");
        });

        $("#close-btn").click(function () {
            $('#impersonateUserErrMsg').html('');
            $('#impersonateUserErrMsg').hide();
            $("#select-user-confirmation-modal").css("display", "none");
        });

        $("#filter-users-form").on('submit', function (e) {
            e.preventDefault();
            GetUsersPageData(1);
        });

        $("#cancel-impersonation").click(function (e) {
            e.preventDefault();
            $.ajax({
                url: "/LoginManager/CancelImpersonation",
                method: "POST",
                success: function (data) {
                    $('#cencel-impersonation').css("display", "none");
                    $("#switch-user").css("display", "block");

                    window.location.href = data;
                }
            });
        });

        $("#usersListGroup").on('click', '.impersonate-user', function () {

            $('#impersonateUserErrMsg').html('');
            $('#impersonateUserErrMsg').hide();

            $.ajax({
                url: "/LoginManager/ImpersonateUser",
                method: "POST",
                data: { 'userID': $(this).attr("data-userid") },
                success: function (obj) {
                    var data = JSON.parse(obj);

                    if (data.Err) {
                        $('#impersonateUserErrMsg').show();
                        $('#impersonateUserErrMsg').html(data.ErrMsg);
                    }
                    else {
                        $("#switch-user").css("display", "none");
                        $("#cancel-inpersonation").css("display", "block");

                        window.location.href = data.ReturnUrl;
                    }
                }
            });
        });
    });
});

function GetUsersPageData(pageNum) {
    $.getJSON("/AdminFunctionsWidget/GetPagedData", $("#filter-users-form").serialize() + `&pageNumber=${pageNum}`, function (response) {
        var rowData = "";
        for (var i = 0; i < response.Data.length; i++) {
            rowData = rowData + '<li class="d-block m-2 impersonate-user" data-userID=' + response.Data[i].UserID + '>' + response.Data[i].UserName + '</li>';
        }

        var list = $("#usersListGroup");

        list.html('');
        if (rowData != "") {
            list.append(rowData);
        }
        else {
            var noResults = $('.admin-functions-widget').find('#noUsersMsg').html();
            var filter = $("#filter");
            $("#userContents").html("");
            $("#userContents").append(filter);
            $("#userContents").append(noResults);
        }
        $("#user-pagination").html("");
        if (response.TotalPages > 1) {
            PagingTemplate(response.TotalPages, response.CurrentPage, "#user-pagination", "GetUsersPageData");
        }
    });
}