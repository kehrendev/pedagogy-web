﻿$(function () {
    console.log('courses.js loaded..');

    $(document).ready(function () {
        $(".show-cat-btn").click(function () {
            $(".hide-cat-btn").css("display", "block");
            $(this).css("display", "none");
            $(".category-container").slideDown();
        });

        $(".hide-cat-btn").click(function () {
            $(this).css("display", "none");
            $(".show-cat-btn").css("display", "block");
            $(".category-container").slideUp();
        });
    });
})