﻿$(function () {
    console.log('create-membership.js loaded..');


    $(document).ready(function () {

        $('#membershipDataForm').submit(function (e) {
            e.preventDefault();

            $('#creatingMembershipLoadingModal').show();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (obj) {
                    var data = JSON.parse(obj);

                    $('.checkout-area').find('.err-msg').html('');

                    if (data.Err) {
                        $('#creatingMembershipLoadingModal').hide();

                        $('.checkout-area').find('.err-msg').show();
                        $('.checkout-area').find('.err-msg').html(data.ErrMsg);

                        $('html').scrollTop(100);
                    }
                    else {
                        $('.checkout-area').find('.err-msg').hide();
                        window.location.href = data.Url;
                    }
                }
            });
        });

        if ($("#mem-type").length > 0) {
            SetMembershipDroppable();
            GetCourseDroppablesPagedData(1);
        }

        $(".close").click(function () {
            $(".contact-hour-warning-modal").hide();
            $(".contact-hour-warning-modal").find("#warning-text").html("");
        });

        if ($('#available-classes').length > 0) {
            $("#available-classes").droppable({
                accept: ".membership-class",
                tolerance: "touch",
                drop: function (event, ui) {
                    var skuid = ui.draggable[0].getAttribute("data-skuid");
                    var item = $(`.membership-class[data-skuid=${skuid}]`);

                    $.ajax({
                        url: "/Checkout/RemoveItemFromMembership",
                        method: "POST",
                        data: { itemSkuId: skuid }
                    });

                    var children = item.children("input");
                    children.each(function () {
                        children.remove();
                    })
                    item.appendTo("#available-classes");
                }
            });
        }

        $("#mem-type").change(function () {
            $("#available-classes").html("");
            $(".classes-in-mem").html("");


            //$("#memberships").html("");

            //$("#memberships").append(
            //    `<div class="row" id="membership" data-mem-num="0">
            //            <div class="col-2"></div>
            //            <div class="col-10">
            //                <div class="d-flex justify-content-start">
            //                    <div class="w-75 m-2">
            //                        <h4>Membership Name:</h4>
            //                        @Html.TextBoxFor(m => m.ViewModel.Memberships[0].MembershipName, new { Class = "form-control" })
            //                        @Html.ValidationMessageFor(m => m.ViewModel.Memberships[0].MembershipName, "", new { Class = "text-danger" })
            //                    </div>
            //                    <div class="w-25 m-2">
            //                        <h4>Quantity:</h4>
            //                        @Html.TextBoxFor(m => m.ViewModel.Memberships[0].Quantity, new { Class = "form-control" })
            //                        @Html.ValidationMessageFor(m => m.ViewModel.Memberships[0].Quantity, "", new { Class = "text-danger" })
            //                    </div>
            //                </div>
            //                <div class="card my-4">
            //                    <div class="card-header" id="mem-class-header">
            //                        <h4>Classes In This Membership</h4>
            //                    </div>
            //                    <div class="card-body classes-in-mem"></div>
            //                </div>
            //            </div>
            //        </div>`
            //);

            /*SetMembershipDroppable();*/

            GetCourseDroppablesPagedData(1);
        });

        $("#facility-dropdown").change(function () {
            $.ajax({
                url: "/Checkout/AddFacilityID",
                method: "POST",
                data: { 'facilityID': $(this).val() }
            });
        });

        //$(".expand-course-info").click(function () {
        //    $(this).hide();
        //    $(this).siblings(".collapse-course-info").show();

        //    $(this).parent(".expand-collapse-icons").siblings(".course-info").slideDown();
        //});

        //$(".collapse-course-info").click(function () {
        //    $(this).hide();
        //    $(this).siblings(".expand-course-info").show();

        //    $(this).parent(".expand-collapse-icons").siblings(".course-info").slideUp();
        //});

        //$("#new-mem").on('click', function () {
        //    var count = $("#memberships").children().length;
        //    $("#memberships").append(
        //        `<div id="membership" data-mem-num="${count}" class="row">
        //                <div class="col-2">
        //                    <button style="background-color:transparent;border:unset;color:#1489bc;" id="remove-mem-btn">Remove</button>
        //                </div>
        //                <div class="col-10">
        //                    <div class="d-flex justify-content-start">
        //                        <div class="w-75 m-2">
        //                            <h4>Membership Name:</h4>
        //                            <input class="form-control input-validation-error" data-val="true" data-val-required="Membership Name is required" id="ViewModel_Memberships_${count}__MembershipName" name="ViewModel.Memberships[${count}].MembershipName" type="text" value="" aria-describedby="ViewModel_Memberships_${count}__MembershipName-error" aria-invalid="true">
        //                            <span class="text-danger field-validation-error" data-valmsg-for="ViewModel.Memberships[${count}].MembershipName" data-valmsg-replace="true"></span>
        //                        </div>
        //                        <div class="w-25 m-2">
        //                            <h4>Quantity:</h4>
        //                            <input class="form-control input-validation-error" data-val="true" data-val-number="The field Quantity must be a number." data-val-range="Enter a valid number that is at least 1 or more" data-val-range-max="2147483647" data-val-range-min="1" data-val-required="Quantity is required" id="ViewModel_Memberships_${count}__Quantity" name="ViewModel.Memberships[${count}].Quantity" type="text" aria-describedby="ViewModel_Memberships_${count}__Quantity-error">
        //                            <span class="text-danger field-validation-error" data-valmsg-for="ViewModel.Memberships[${count}].Quantity" data-valmsg-replace="true"></span>
        //                        </div>
        //                    </div>
        //                    <div class="card my-4">
        //                        <div class="card-header" id="mem-class-header">
        //                            <h4>Classes In This Membership</h4>
        //                        </div>
        //                        <div class="card-body classes-in-mem"></div>
        //                    </div>
        //                </div>
        //            </div>`
        //    );

        //    setDroppables();
        //});

        //$(document).on('click', '#remove-mem-btn', function () {
        //    var mem = $(this).closest('#membership');
        //    var items = mem.find('.membership-class')
        //    items.each(function () {
        //        $(this).appendTo("#available-classes");
        //    });

        //    mem.remove();
        //});
    });
});

function SetMembershipDroppable() {
    if ($('.classes-in-mem').length > 0) {
        $(".classes-in-mem").droppable({
            accept: ".membership-class",
            tolerance: "touch",
            drop: function (event, ui) {
                var skuid = ui.draggable[0].getAttribute("data-skuid");
                var item = $(`.membership-class[data-skuid=${skuid}]`);

                item.appendTo($(".classes-in-mem"));

                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("value", item.attr("data-classID"));
                var id = item.closest('#membership').attr("data-mem-num");
                input.setAttribute("name", `ViewModel.Memberships[${id}].ClassIDs`);
                item.append(input);

                $.ajax({
                    url: "/Checkout/AddItemToMembershipCart",
                    method: "POST",
                    data: { 'skuid': item.attr("data-skuid"), 'units': 1 },
                    success: function (data) {
                        if (typeof data == 'string') {
                            $(".msg").html(data);
                            $(".error-msg-container").css("display", "block");

                            $(".contact-hour-warning-modal").find("#warning-text").html(data);
                            $(".contact-hour-warning-modal").show();

                            var children = item.children("input");
                            children.each(function () {
                                $(this).remove();
                            });

                            item.appendTo("#available-classes");

                            var duplicate = $("#available-classes").find(`.membership-class[data-skuid=${skuid}]`)[1];
                            if (duplicate != null) {
                                duplicate.remove();
                            }
                        }
                    }
                });
            }
        });
    }
}

function GetCourseDroppablesPagedData(pageNum) {
    $("#available-classes").append('<div class="text-center mt-5">Loading... <i class="fa fa-spinner fa-spin"></i></div>');

    $.getJSON("/Checkout/GetClassesInMembership", { category: $("#mem-type").val(), pageNumber: pageNum, pageSize: $('#memPageSize').val() }, function (response) {

        var rowData = "";
        for (var i = 0; i < response.Data.length; i++) {
            var item = $(".classes-in-mem").find(`.membership-class[data-classid=${response.Data[i].CourseSKUNumber}]`)[0];

            if (item == null) {
                rowData += '<div data-skuid="' + response.Data[i].SKUID + '" data-classID="' + response.Data[i].CourseSKUNumber
                    + '" class="d-block m-2 btn btn-outline-primary p-2 membership-class"><span>' + response.Data[i].CourseTitle
                    + '</span><br /><span class="contact-hours">Contact Hours: ' + response.Data[i].CourseContactHours + '</span></div>';
            }

            //rowData +=
            //    `
            //        <div class="course-item m-2">
            //            <div data-skuid="${response.Data[i].SKUID}" data-classID="${response.Data[i].CourseSKUNumber}" class="d-block btn btn-outline-primary p-2 membership-class">
            //                <span class="me-5">${response.Data[i].CourseTitle}</span>
            //            </div>
            //            <div class="expand-collapse-icons">
            //                <i class="fa fa-plus-square fa-lg expand-course-info"></i>
            //                <i class="fa fa-minus-square fa-lg collapse-course-info"></i>
            //            </div>
            //            <div class="course-info p-3">
            //                <span>Contact Hours: ${response.Data[i].CourseContactHours}</span>
            //            </div>
            //        </div>
            //    `;
        }

        $("#available-classes").html("");

        if (response.Data.length > 0) {
            $("#available-classes").append(rowData);
        }

        $("#available-classes-pagination").html("");

        if (response.TotalPages > 1) {
            PagingTemplate(response.TotalPages, response.CurrentPage, "#available-classes-pagination", "GetCourseDroppablesPagedData");
        }

        $(".membership-class").draggable({
            revert: "invalid",
            containment: "document",
            cursor: "move",
            helper: "clone"
        });
    });
}