﻿jQuery(document).ready(function ($) {
	var first = 0;
	var speed = 700;
	var pause = 8000;

	if ($('#PedagogyFeaturedClasses').length <= 0) {
		$('#pedagogyfeaturedscript').parent().append('<div id="PedagogyFeaturedClasses" class="Default"></div>');
	}

	getFeaturedClasses(true);

	interval = setInterval(removeFirst, pause);


	function removeFirst() {
		first = $('#PedagogyClassScroller div:first');
		$('#PedagogyClassScroller div:first').stop()
			.slideUp('slow', function () {

				$('#PedagogyClassScroller').append(this);
				$(this).show();

			});

	}

	function getFeaturedClasses(openNewWindow) {

		$.getJSON("https://localhost/JSON/FeaturedClassList.ashx?jsoncallback=?", function (result) {
			var classList = '<div class="Title">Featured Classes</div><div id="PedagogyClassScroller">';
			$.each(result, function (i, Class) {
				classList += '\n<div class="Class">';
				classList += '\n<h2><a href="' + Class.ClassURL + ($('#hdnLocationID').val() ? '?cmp=F1.' + $('#hdnLocationID').val() : '') + (openNewWindow ? '" target="_blank"' : '" ') + '>' + Class.CourseName + '</a></h2>';
				classList += '\n<div class="Cost">Cost: ' + Class.Cost + '</div><br />';
				classList += '\n<div class="Description">' + Class.ShortDescription + '</div><br />';
				classList += '\n</div>';

			});
			$("#PedagogyFeaturedClasses").append(classList + "</div>");
			return true;
		});
	}
});