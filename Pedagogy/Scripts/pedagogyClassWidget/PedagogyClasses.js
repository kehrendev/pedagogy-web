﻿jQuery(document).ready(function ($) {
    if ($('#PedagogyClasses').length <= 0) {
        $('#pedagogyscript').parent().append('<div id="PedagogyClasses"  class="Default"></div>');
    }

    getClasses(true);

    function getClasses(openNewWindow) {

        $.getJSON("https://localhost/JSON/ClassList.ashx?jsoncallback=?", function (result) {
            var classList = '<div class="Title">Pedagogy Classes</div><div class="Categories FeedScroller">';
            $.each(result, function (i, Class) {
                if (i == 0)
                    classList += '<div class="PedagogyClassList">\n';
                classList += '<div class="PedagogyClass"><a href="' + Class.ClassURL + ($('#hdnLocationID').val() ? '?cmp=F1.' + $('#hdnLocationID').val() : '') + (openNewWindow ? '" target="_blank"' : '" ') + '>' + Class.ClassName + '</a></div>\n';
                if (i == result.length - 1)
                    classList += '</div>';
            });

            $("#PedagogyClasses").append(classList + '</div></div>');
            return true;
        });
    }
});