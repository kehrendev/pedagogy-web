var $stickyChainOffset = $('#sticky-header').offset();
var $originalOffset = $stickyChainOffset.top;
var $stickyChain = $('#sticky-header');

var $docHeight = $(document).height();
var $stickyHeight = $stickyChain.height();

function checkScroll() {
    $stickyChainOffset = $('#sticky-header').offset();
    if ($docHeight >= $stickyHeight * 2 && $(window).scrollTop() >= $originalOffset) {
        $("#sticky-header").addClass("sticky");
    } else if ($(window).scrollTop() < $originalOffset) {
        $("#sticky-header").removeClass("sticky");
    }
};

(function ($) {
    "use strict";
    // TOP Menu Sticky
    $(window).on('scroll', function () {
        checkScroll();
    });

    $(document).ready(function () {
        // main nav hover
        $('.dropdown-toggle').mouseenter(function () {
            $(this).siblings('.dropdown-menu').show();
        });

        $('.dropdown-menu').mouseenter(function () {
            $(this).show();
        });

        $('.dropdown-toggle').mouseleave(function () {
            $(this).siblings('.dropdown-menu').hide();
        });

        $('.dropdown-menu').mouseleave(function () {
            $(this).hide();
        });

        if ($(document).width() <= 768) {
            $('.nav-link').each(function () {
                console.log($(this).attr('data-has-children'));
                if ($(this).attr('data-has-children') == 'True') {
                    $(this).attr('role', 'button');
                    $(this).attr('data-bs-toggle', 'dropdown');
                    $(this).attr('aria-expanded', 'false');
                }
            });

            $('.nav-link').click(function (e) {

                if ($(this).attr('data-has-children') == 'True') {
                    e.preventDefault();

                    var menu = $(this).siblings('.dropdown-menu');

                    if (menu.hasClass('show')) {
                        menu.show();
                    }
                    else {
                        menu.hide();
                    }
                }
            });
        }


        $(".password-input").keyup(function (e) {
            checkPassword(e);
        });

        $('.phone-number').each(function () {
            var phoneNumber = this.value.replace(/[^\d]/g, "");

            if (phoneNumber != "") {
                $(this).val(`(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3, 6)}-${phoneNumber.slice(6, 10)}`);
            }
        });

        $('.phone-number').keypress(function () {
            if ($(this).val().length < 14) {
                $(this).val(formatPhoneNumber($(this).val()));
            }
            else {
                return false;
            }
        });

        $("#facility-dropdown").change(function () {
            $.ajax({
                url: "/Checkout/AddFacilityID",
                method: "POST",
                data: { 'facilityID': $(this).val() },
                success: function (id) {
                    $(".description").each(function () {
                        $(this).css("display", "none");
                    });

                    if (id != 0) {
                        $(".facility-qty-box").each(function () {
                            $(this).css("display", "block");
                        });

                        $(".free-course-add-btn").val("Add To Facility Class List");
                    }

                    $("#class-dropdown").val("");
                }
            });
        });

        $('#closeSaveChangesModal').click(function () {
            $('#saveChangesModal').css('display', 'none');
        });

        $(".show-card-body").click(function () {
            $(this).css("display", "none");
            $(this).siblings(".hide-card-body").css("display", "block");
            $(this).parents(".card-header").siblings(".card-body").slideDown();
        });

        $(".hide-card-body").click(function () {
            $(this).css("display", "none");
            $(this).siblings(".show-card-body").css("display", "block");
            $(this).parents(".card-header").siblings(".card-body").slideUp();
        });

        $('#course-tabs a').on('click', function (e) {
            e.preventDefault();

            var tab = new bootstrap.Tab($(this));
            tab.show();
        })

        $("#hide-card-body").click(function () {
            var div = $("#the-card-body").slideUp();
            $(this).hide();
            $("#show-card-body").show();
        });

        $("#show-card-body").click(function () {
            var div = $("#the-card-body").slideDown();
            $(this).hide();
            $("#hide-card-body").show();
        });

        $(".ktc-checkbox").each(function (x) {
            var checkbox = $(this).find('input[type="checkbox"]');
            checkbox.removeClass("form-control");
        });

        // mobile_menu
        var menu = $('#navigation');
        var mainNavItems = $('ul#main-nav');
        if (mainNavItems.length) {
            menu.slicknav({
                label: "",
                prependTo: ".mobile_menu",
                closedSymbol: '+',
                openedSymbol: '-',
                allowParentLinks: true
            });
        };

        var utilityNavItems = $('ul#utility-nav');
        if (utilityNavItems.length) {
            menu.slicknav({
                label: "",
                prependTo: ".mobile_menu",
                closedSymbol: '+',
                openedSymbol: '-',
                allowParentLinks: true
            });
        }

        // review-active
        $('.slider_active').owlCarousel({
            loop: true,
            margin: 0,
            items: 1,
            autoplay: true,
            nav: true,
            navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
            dots: false,
            autoplayHoverPause: true,
            autoplaySpeed: 800,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                },
                767: {
                    items: 1,
                    nav: false,
                },
                992: {
                    items: 1,
                    nav: false
                },
                1200: {
                    items: 1,
                    nav: false
                },
                1600: {
                    items: 1,
                    nav: true
                }
            }
        });

        // review-active
        $('.testmonial_active').owlCarousel({
            loop: true,
            margin: 0,
            items: 1,
            autoplay: true,
            navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
            nav: true,
            dots: false,
            autoplayHoverPause: true,
            autoplaySpeed: 800,
            responsive: {
                0: {
                    items: 1,
                    dots: false,
                    nav: false,
                },
                767: {
                    items: 1,
                    dots: false,
                    nav: false,
                },
                992: {
                    items: 1,
                    nav: false
                },
                1200: {
                    items: 1,
                    nav: false
                },
                1500: {
                    items: 1
                }
            }
        });

        // review-active
        $('.card-slider').owlCarousel({
            loop: false,
            margin: 30,
            items: 1,
            autoplay: true,
            navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
            nav: true,
            dots: false,
            autoplayHoverPause: true,
            autoplaySpeed: 800,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                767: {
                    items: 2,
                    nav: false
                },
                992: {
                    items: 3
                },
                1200: {
                    items: 4
                },
                1500: {
                    items: 4
                }
            }
        });

        $('.image-slideshow').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            autoplay: false,
            nextArrow: '<i class="ti-angle-right next-arrow"></i>',
            prevArrow: '<i class="ti-angle-left prev-arrow"></i>'
        });

        // for filter
        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: 1
            }
        });

        // filter items on button click
        $('.portfolio-menu').on('click', 'button', function () {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({ filter: filterValue });
        });

        //for menu active class
        $('.portfolio-menu button').on('click', function (event) {
            $(this).siblings('.active').removeClass('active');
            $(this).addClass('active');
            event.preventDefault();
        });

        // wow js
        new WOW().init();

        //counter 
        $('.counter').counterUp({
            delay: 10,
            time: 10000
        });

        /* magnificPopup img view */
        $('.popup-image').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });

        /* magnificPopup img view */
        $('.img-pop-up').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });

        /* magnificPopup video view */
        $('.popup-video').magnificPopup({
            type: 'iframe'
        });


        // scrollIt for smoth scroll
        $.scrollIt({
            upKey: 38,             // key code to navigate to the next section
            downKey: 40,           // key code to navigate to the previous section
            easing: 'linear',      // the easing function for animation
            scrollTime: 600,       // how long (in ms) the animation takes
            activeClass: 'active', // class given to the active nav element
            onPageChange: null,    // function(pageIndex) that is called when page is changed
            topOffset: 0           // offste (in px) for fixed top navigation
        });

        // blog-page

        //brand-active
        $('.brand-active').owlCarousel({
            loop: true,
            margin: 30,
            items: 1,
            autoplay: true,
            nav: false,
            dots: false,
            autoplayHoverPause: true,
            autoplaySpeed: 800,
            responsive: {
                0: {
                    items: 1,
                    nav: false

                },
                767: {
                    items: 4
                },
                992: {
                    items: 7
                }
            }
        });

        // blog-dtails-page

        //project-active
        $('.project-active').owlCarousel({
            loop: true,
            margin: 30,
            items: 1,
            // autoplay:true,
            navText: ['<i class="Flaticon flaticon-left-arrow"></i>', '<i class="Flaticon flaticon-right-arrow"></i>'],
            nav: true,
            dots: false,
            // autoplayHoverPause: true,
            // autoplaySpeed: 800,
            responsive: {
                0: {
                    items: 1,
                    nav: false

                },
                767: {
                    items: 1,
                    nav: false
                },
                992: {
                    items: 2,
                    nav: false
                },
                1200: {
                    items: 1,
                },
                1501: {
                    items: 2,
                }
            }
        });

        if (document.getElementById('default-select')) {
            $('select').niceSelect();
        }

        //about-pro-active
        $('.details_active').owlCarousel({
            loop: true,
            margin: 0,
            items: 1,
            // autoplay:true,
            navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
            nav: true,
            dots: false,
            // autoplayHoverPause: true,
            // autoplaySpeed: 800,
            responsive: {
                0: {
                    items: 1,
                    nav: false

                },
                767: {
                    items: 1,
                    nav: false
                },
                992: {
                    items: 1,
                    nav: false
                },
                1200: {
                    items: 1,
                }
            }
        });

    });

    // resitration_Form
    $(document).ready(function () {
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',

            // When elemened is focused, some mobile browsers in some cases zoom in
            // It looks not nice, so we disable it:
            callbacks: {
                beforeOpen: function () {
                    if ($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });
    });



    //------- Mailchimp js --------//  
    //function mailChimp() {
    //    $('#mc_embed_signup').find('form').ajaxChimp();
    //}
    //mailChimp();



    // Search Toggle
    $("#search_input_box").hide();
    $("#search").on("click", function () {
        $("#search_input_box").slideToggle();
        $("#search_input").focus();
    });
    $("#close_search").on("click", function () {
        $('#search_input_box').slideUp(500);
    });
    // Search Toggle
    $("#search_input_box").hide();
    $("#search_1").on("click", function () {
        $("#search_input_box").slideToggle();
        $("#search_input").focus();
    });

    

})(jQuery);

// paging navigation setup
function PagingTemplate(totalPage, currentPage, appendToElement, functionName) {
    var template = "";
    var TotalPages = totalPage;
    var CurrentPage = currentPage;
    var PageNumberArray = Array();


    var countIncr = 1;
    var startPage;
    if (currentPage == 1) startPage = 1;
    else startPage = currentPage - 1;

    for (var i = startPage; i <= totalPage; i++) {
        PageNumberArray[0] = startPage;
        if (totalPage != currentPage - 1 && PageNumberArray[countIncr - 1] != totalPage) {
            PageNumberArray[countIncr] = i + 1;
        }
        countIncr++;
    };

    PageNumberArray = PageNumberArray.slice(0, 3);
    var FirstPage = 1;
    var LastPage = totalPage;
    if (totalPage != currentPage) {
        var ForwardOne = currentPage + 1;
    }
    var BackwardOne = 1;
    if (currentPage > 1) {
        BackwardOne = currentPage - 1;
    }

    template = template + '<li class="page-item"><a style="cursor:pointer;" class="page-link" aria-label="Previous" onclick="' + functionName + '(' + FirstPage + ')"><span aria-hidden="true">&laquo;</span></a></li>';

    for (var i = 0; i < PageNumberArray.length; i++) {
        if (PageNumberArray[i] == currentPage) {
            template = template + '<li class="page-item active"><a style="cursor:pointer;" class="page-link" onclick="' + functionName + '(' + PageNumberArray[i] + ')">' + PageNumberArray[i] + '</a></li>';
        } else {
            template = template + '<li class="page-item"><a style="cursor:pointer;" class="page-link" onclick="' + functionName + '(' + PageNumberArray[i] + ')">' + PageNumberArray[i] + '</a></li>';
        }
    }

    template = template + '<li class="page-item"><a style="cursor:pointer;" class="page-link" aria-label="Next" onclick="' + functionName + '(' + LastPage + ')"><span aria-hidden="true">&raquo;</span></a></li>';
    $(appendToElement).append(template);
}

// formats phone numbers : (xxx) xxx-xxxx
function formatPhoneNumber(value) {
    // if input value is falsy ex if the user deletes the input, then just return
    if (!value) return value;

    // clean the input for any non-digit values.
    const phoneNumber = value.replace(/[^\d]/g, "");

    // phoneNumberLength is used to know when to apply our formatting for the phone number
    const phoneNumberLength = phoneNumber.length;

    // we need to return the value with no formatting if its less then four digits
    // this is to avoid weird behavior that occurs if you  format the area code to early
    if (phoneNumberLength < 3) return phoneNumber;

    // if phoneNumberLength is greater than 4 and less the 7 we start to return
    // the formatted number
    if (phoneNumberLength < 6) {
        return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
    }

    // finally, if the phoneNumberLength is greater then seven, we add the last
    // bit of formatting and return it.
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3, 6)}-${phoneNumber.slice(6, 9)}`;
}

// checks to make sure the password meets the requirements
function checkPassword(e) {
    var password = $(e.target).val();

    if (/^(?=.{8,}$)/.test(password)) {
        $(".characters").addClass("text-success");
    }
    else {
        $(".characters").removeClass("text-success");
    }

    if (/^(?=.*[a-z])/.test(password)) {
        $(".lowercase").addClass("text-success");
    }
    else {
        $(".lowercase").removeClass("text-success");
    }

    if (/^(?=.*[A-Z])/.test(password)) {
        $(".uppercase").addClass("text-success");
    }
    else {
        $(".uppercase").removeClass("text-success");
    }

    if (/^(?=.*[0-9])/.test(password)) {
        $(".numeric").addClass("text-success");
    }
    else {
        $(".numeric").removeClass("text-success");
    }
}

//Managing Contacts
function contactDisplayNone(contactID) {
    var contact = document.getElementById(`contact-${contactID}`);
    contact.style.display = "none";
}

function removeContact(i) {
    var removeBtn = document.getElementById(`remove-btn-${i}`);
    var url = removeBtn.getAttribute("data-url");

    if (url != null) {
        $.ajax({
            url: url,
            method: "POST"
        });

        $(`confirm-modal-${i}`).hide();
        $(`remove-btn-${i}`).closest(".contact").remove();
    } else {
        closeConfirmationModal(i);
        $(`#remove-btn-${i}`).closest(".contact").remove();
    }
}

function fixContactId(i, url) {

    var confirmModal = document.getElementById(`confirmation-modal-${i}`);
    confirmModal.remove();

    var closeBtn = document.getElementById(`close-btn-${i}`);
    closeBtn.remove();

    var html = "";
    html += '<span class="close ms-5" id="close-btn-' + i + '" onclick="displayConfirmationModal(' + i + ')">&times;</span>' +
        '<div id="confirmation-modal-' + i + '" style = "display:none;" class="modal confirmation-modal" >' +
        '<div class="modal-content">' +
        '<span class="close align-self-end" id="close-0" onclick="closeConfirmationModal(0)">&times;</span>' +
        '<p class="mb-4 mt-2">Are you sure you want to delete?</p>' +
        '<div class="d-flex justify-content-end">' +
        '<span id="remove-btn-' + i + '" onclick="removeContact(' + i + ')" style="cursor:pointer;" data-url="' + url + '">Yes</span>' +
        '<a id="cancel-btn-0" style="cursor:pointer;" onclick="closeConfirmationModal(' + i + ')" class="ms-3">Cancel</a></div></div></div>';

    $(`#confirm-modal-${i}`).append(html);
}

function clearContactForm(contactNum) {
    var firstOption = $(`#contact-type-${contactNum} > option:first-child`).val();
    $(`#contact-type-${contactNum}`).val(firstOption);

    document.getElementById(`contact-first-name-${contactNum}`).value = "";
    document.getElementById(`contact-first-name-validation-${contactNum}`).innerHTML = "";
    document.getElementById(`contact-last-name-${contactNum}`).value = "";
    document.getElementById(`contact-last-name-validation-${contactNum}`).innerHTML = "";
    document.getElementById(`contact-phone-${contactNum}`).value = "";
    document.getElementById(`contact-phone-validation-${contactNum}`).innerHTML = "";
    document.getElementById(`contact-email-${contactNum}`).value = "";
    //document.getElementById(`contact-email-validation-${contactNum}`).innerHTML = "";
    //document.getElementById(`new-user-password-${contactNum}`).value = "";
    //document.getElementById(`new-user-password-validation-${contactNum}`).innerHTML = "";
    //document.getElementById(`new-user-confirm-password-${contactNum}`).value = "";
    //document.getElementById(`new-user-confirm-password-validation-${contactNum}`).innerHTML = "";
    var checkbox = document.getElementById(`new-user-acct-${contactNum}`);
    if (checkbox != null) {
        checkbox.checked = false;
    }
    //document.getElementById(`new-user-form-${contactNum}`).style.display = "none";
}

function displayConfirmationModal(id) {
    var confirmModal = document.getElementById(`confirmation-modal-${id}`);
    confirmModal.style.display = "block";
}

function closeConfirmationModal(id) {
    var confirmModal = document.getElementById(`confirmation-modal-${id}`);
    confirmModal.style.display = "none";
}

var confirmModal = document.getElementsByClassName("modal");
window.onclick = function (e) {
    if (e.target == confirmModal) {
        confirmModal.style.display = "none";
    }
}

function newUserForm(t) {
    var parent = $(t).parents(".contact");
    var emailInput = parent.find(".contact-email");
    var checkbox = $(t).find(".new-user-acct");
    var recipientForm = parent.find(".user-pass-recipient-form");

    if (checkbox.is(":checked")) {
        if (emailInput.val() == "" || emailInput == null) {
            recipientForm.css("display", "block");
            emailInput.css("display", "none");
        }
        else {
            recipientForm.css("display", "none");
        }
    }
    else {
        recipientForm.css("display", "none");
        parent.find(".recipient-email").val("");
        parent.find(".recipient-email-validation").html("");
        emailInput.css("display", "block");
    }
}

function displayFacilityInfoForm() {
    var form = document.getElementById("facility-info-form");
    var checkbox = document.getElementById("facility-purchase-checkbox");

    if (checkbox.checked) {
        form.style.display = "block";
    }
    else {
        form.style.display = "none";
    }
}

function displayWarningModal(data) {
    if (typeof (data) == 'object') {
        var errList = document.getElementById("start-class-err-list");
        for (var x = 0; x < data.length; x++) {
            var li = document.createElement("li");
            li.setAttribute("style", "list-style:disc;");
            li.setAttribute("class", "my-3");
            li.innerHTML = data[x];
            errList.appendChild(li);
        }
        document.getElementById("confirmation-modal").style.display = "block";
    }
    else {
        window.location = data;
    }
}

function displayWarningModalForMemberships(data) {
    if (typeof (data) == 'object') {
        var errList = document.getElementById("start-class-err-list-for-memberships");
        for (var x = 0; x < data.length; x++) {
            var li = document.createElement("li");
            li.setAttribute("style", "list-style:disc;");
            li.setAttribute("class", "my-3");
            li.innerHTML = data[x];
            errList.appendChild(li);
        }
        document.getElementById("confirmation-modal-for-memberships").style.display = "block";
    }
    else {
        window.location = data;
    }
}
