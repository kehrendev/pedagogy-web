﻿var
	//mapWidth		= 500;
	mapHeight = 387;

shadowWidth = 1;
shadowOpacity = 0.3;
shadowColor = "black";
shadowX = 1;
shadowY = 2;

iPhoneLink = true,

	isNewWindow = false,

	borderColor = "#ffffff",
	borderColorOver = "#ffffff",

	nameColor = "#ffffff",
	nameFontSize = "11px",
	nameFontWeight = "bold",
	nameStroke = false,

	overDelay = 400,

	map_data = {
		st1: {
			id: 1,
			name: "Alabama",
			shortname: "AL",
			link: "/Resources/State-Boards-of-Nursing/Alabama-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st2: {
			id: 2,
			name: "Alaska",
			shortname: "AK ",
			link: "/Resources/State-Boards-of-Nursing/Alaska-State-Board-of-Nursing",
			comment: "",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st3: {
			id: 3,
			name: "Arizona",
			shortname: "AZ ",
			link: "/Resources/State-Boards-of-Nursing/Arizona-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st4: {
			id: 4,
			name: "Arkansas",
			shortname: "AR",
			link: "/Resources/State-Boards-of-Nursing/Arkansas-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st5: {
			id: 5,
			name: "California",
			shortname: "CA",
			link: "/Resources/State-Boards-of-Nursing/California-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st6: {
			id: 6,
			name: "Colorado",
			shortname: "CO",
			link: "/Resources/State-Boards-of-Nursing/Colorado-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st7: {
			id: 7,
			name: "Connecticut",
			shortname: "CT",
			link: "/Resources/State-Boards-of-Nursing/Connecticut-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st8: {
			id: 8,
			name: "Delaware",
			shortname: "DE",
			link: "/Resources/State-Boards-of-Nursing/Delaware-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st9: {
			id: 9,
			name: "District of Columbia",
			shortname: "DC",
			link: "/Resources/State-Boards-of-Nursing/District-of-Columbia-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st10: {
			id: 10,
			name: "Florida",
			shortname: "FL",
			link: "/Resources/State-Boards-of-Nursing/Florida-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st11: {
			id: 11,
			name: "Georgia",
			shortname: "GA",
			link: "/Resources/State-Boards-of-Nursing/Georgia-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st12: {
			id: 12,
			name: "Hawaii",
			shortname: "HI",
			link: "/Resources/State-Boards-of-Nursing/Hawaii-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st13: {
			id: 13,
			name: "Idaho",
			shortname: "ID",
			link: "/Resources/State-Boards-of-Nursing/Idaho-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st14: {
			id: 14,
			name: "Illinois",
			shortname: "IL",
			link: "/Resources/State-Boards-of-Nursing/Illinois-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st15: {
			id: 15,
			name: "Indiana",
			shortname: "IN",
			link: "/Resources/State-Boards-of-Nursing/Indiana-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st16: {
			id: 16,
			name: "Iowa",
			shortname: "IA",
			link: "/Resources/State-Boards-of-Nursing/Iowa-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st17: {
			id: 17,
			name: "Kansas",
			shortname: "KS",
			link: "/Resources/State-Boards-of-Nursing/Kansas-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st18: {
			id: 18,
			name: "Kentucky",
			shortname: "KY",
			link: "/Resources/State-Boards-of-Nursing/Kentucky-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st19: {
			id: 19,
			name: "Louisiana",
			shortname: "LA",
			link: "/Resources/State-Boards-of-Nursing/Louisiana-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st20: {
			id: 20,
			name: "Maine",
			shortname: "ME",
			link: "/Resources/State-Boards-of-Nursing/Maine-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st21: {
			id: 21,
			name: "Maryland",
			shortname: "MD",
			link: "/Resources/State-Boards-of-Nursing/Maryland-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st22: {
			id: 22,
			name: "Massachusetts",
			shortname: "MA",
			link: "/Resources/State-Boards-of-Nursing/Massachusetts-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st23: {
			id: 23,
			name: "Michigan",
			shortname: "MI",
			link: "/Resources/State-Boards-of-Nursing/Michigan-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st24: {
			id: 24,
			name: "Minnesota",
			shortname: "MN",
			link: "/Resources/State-Boards-of-Nursing/Minnesota-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st25: {
			id: 25,
			name: "Mississippi",
			shortname: "MS",
			link: "/Resources/State-Boards-of-Nursing/Mississippi-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st26: {
			id: 26,
			name: "Missouri",
			shortname: "MO",
			link: "/Resources/State-Boards-of-Nursing/Missouri-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st27: {
			id: 27,
			name: "Montana",
			shortname: "MT",
			link: "/Resources/State-Boards-of-Nursing/Montana-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st28: {
			id: 28,
			name: "Nebraska",
			shortname: "NE",
			link: "/Resources/State-Boards-of-Nursing/Nebraska-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st29: {
			id: 29,
			name: "Nevada",
			shortname: "NV",
			link: "/Resources/State-Boards-of-Nursing/Nevada-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st30: {
			id: 30,
			name: "New Hampshire",
			shortname: "NH",
			link: "/Resources/State-Boards-of-Nursing/New-Hampshire-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st31: {
			id: 31,
			name: "New Jersey",
			shortname: "NJ",
			link: "/Resources/State-Boards-of-Nursing/New-Jersey-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st32: {
			id: 32,
			name: "New Mexico",
			shortname: "NM",
			link: "/Resources/State-Boards-of-Nursing/New-Mexico-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st33: {
			id: 33,
			name: "New York",
			shortname: "NY",
			link: "/Resources/State-Boards-of-Nursing/New-York-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st34: {
			id: 34,
			name: "/Resources/State-Boards-of-Nursing/North-Carolina-State-Board-of-Nursing",
			shortname: "NC",
			link: "State-Boards-of-Nursing/North-Carolina.aspx",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st35: {
			id: 35,
			name: "North Dakota",
			shortname: "ND",
			link: "/Resources/State-Boards-of-Nursing/North-Dakota-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st36: {
			id: 36,
			name: "Ohio",
			shortname: "OH",
			link: "/Resources/State-Boards-of-Nursing/Ohio-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st37: {
			id: 37,
			name: "Oklahoma",
			shortname: "OK",
			link: "/Resources/State-Boards-of-Nursing/Oklahoma-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st38: {
			id: 38,
			name: "Oregon",
			shortname: "OR",
			link: "/Resources/State-Boards-of-Nursing/Oregon-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st39: {
			id: 39,
			name: "Pennsylvania",
			shortname: "PA",
			link: "/Resources/State-Boards-of-Nursing/Pennsylvania-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st40: {
			id: 40,
			name: "Rhode Island",
			shortname: "RI",
			link: "/Resources/State-Boards-of-Nursing/Rhode-Island-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st41: {
			id: 41,
			name: "South Carolina",
			shortname: "SC",
			link: "/Resources/State-Boards-of-Nursing/South-Carolina-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st42: {
			id: 42,
			name: "South Dakota",
			shortname: "SD",
			link: "/Resources/State-Boards-of-Nursing/South-Dakota-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st43: {
			id: 43,
			name: "Tennessee",
			shortname: "TN",
			link: "/Resources/State-Boards-of-Nursing/Tennessee-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st44: {
			id: 44,
			name: "Texas",
			shortname: "TX",
			link: "/Resources/State-Boards-of-Nursing/Texas-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st45: {
			id: 45,
			name: "Utah",
			shortname: "UT",
			link: "/Resources/State-Boards-of-Nursing/Utah-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st46: {
			id: 46,
			name: "Vermont",
			shortname: "VT",
			link: "/Resources/State-Boards-of-Nursing/Vermont-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st47: {
			id: 47,
			name: "Virginia",
			shortname: "VA",
			link: "/Resources/State-Boards-of-Nursing/Virginia-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st48: {
			id: 48,
			name: "Washington",
			shortname: "WA",
			link: "/Resources/State-Boards-of-Nursing/Washington-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st49: {
			id: 49,
			name: "West Virginia",
			shortname: "WV",
			link: "/Resources/State-Boards-of-Nursing/West-Virginia-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st50: {
			id: 50,
			name: "Wisconsin",
			shortname: "WI",
			link: "/Resources/State-Boards-of-Nursing/Wisconsin-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st51: {
			id: 51,
			name: "Wyoming",
			shortname: "WY",
			link: "/Resources/State-Boards-of-Nursing/Wyoming-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st52: {
			id: 52,
			name: "",
			shortname: "",
			link: "",
			comment: "",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		}
	};