﻿var
	//mapWidth		= 500;
	mapHeight = 387;

shadowWidth = 1;
shadowOpacity = 0.3;
shadowColor = "black";
shadowX = 1;
shadowY = 2;

iPhoneLink = true,

	isNewWindow = false,

	borderColor = "#ffffff",
	borderColorOver = "#ffffff",

	nameColor = "#ffffff",
	nameFontSize = "11px",
	nameFontWeight = "bold",
	nameStroke = false,

	overDelay = 400,

	map_data = {
		st1: {
			id: 1,
			name: "Alabama",
			shortname: "AL",
			link: "/Resources/State-Boards-of-Social-Work/Alabama-Board-of-Social-Work-Examiners",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st2: {
			id: 2,
			name: "Alaska",
			shortname: "AK ",
			link: "/Resources/State-Boards-of-Social-Work/Alaska-Board-of-Social-Work-Examiners",
			comment: "",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st3: {
			id: 3,
			name: "Arizona",
			shortname: "AZ ",
			link: "/Resources/State-Boards-of-Social-Work/Arizona-Board-of-Social-Work-Examiners",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st4: {
			id: 4,
			name: "Arkansas",
			shortname: "AR",
			link: "/Resources/State-Boards-of-Social-Work/Arkansas-Board-of-Social-Work-Examiners",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st5: {
			id: 5,
			name: "California",
			shortname: "CA",
			link: "/Resources/State-Boards-of-Social-Work/California-Board-of-Behavioral-Sciences",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st6: {
			id: 6,
			name: "Colorado",
			shortname: "CO",
			link: "/Resources/State-Boards-of-Social-Work/Colorado-Board-of-Social-Work-Examiners",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st7: {
			id: 7,
			name: "Connecticut",
			shortname: "CT",
			link: "/Resources/State-Boards-of-Social-Work/Connecticut-Board-of-Social-Work-Licensure",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st8: {
			id: 8,
			name: "Delaware",
			shortname: "DE",
			link: "/Resources/State-Boards-of-Social-Work/Delaware-Board-of-Social-Work-Examiners",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st9: {
			id: 9,
			name: "District of Columbia",
			shortname: "DC",
			link: "/Resources/State-Boards-of-Social-Work/District-of-Colombia-Board-of-Social-Work-Examiner",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st10: {
			id: 10,
			name: "Florida",
			shortname: "FL",
			link: "/Resources/State-Boards-of-Social-Work/Florida-Board-of-Clinical-Social-Work",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st11: {
			id: 11,
			name: "Georgia",
			shortname: "GA",
			link: "/Resources/State-Boards-of-Social-Work/Georgia-Board-of-Social-Workers",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st12: {
			id: 12,
			name: "Hawaii",
			shortname: "HI",
			link: "/Resources/State-Boards-of-Social-Work/Hawaii-Social-Worker-Program	",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st13: {
			id: 13,
			name: "Idaho",
			shortname: "ID",
			link: "/Resources/State-Boards-of-Social-Work/Idaho-Board-of-Social-Work-Examiners",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st14: {
			id: 14,
			name: "Illinois",
			shortname: "IL",
			link: "/Resources/State-Boards-of-Social-Work/Illinois-Department-of-Financial-and-Professional",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st15: {
			id: 15,
			name: "Indiana",
			shortname: "IN",
			link: "/Resources/State-Boards-of-Social-Work/Indiana-Behavioral-Health-and-Human-Services-Licen",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st16: {
			id: 16,
			name: "Iowa",
			shortname: "IA",
			link: "/Resources/State-Boards-of-Social-Work/Iowa-Board-of-Social-Work",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st17: {
			id: 17,
			name: "Kansas",
			shortname: "KS",
			link: "/Resources/State-Boards-of-Social-Work/Kansas-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st18: {
			id: 18,
			name: "Kentucky",
			shortname: "KY",
			link: "/Resources/State-Boards-of-Social-Work/Kentucky-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st19: {
			id: 19,
			name: "Louisiana",
			shortname: "LA",
			link: "/Resources/State-Boards-of-Social-Work/Louisiana-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st20: {
			id: 20,
			name: "Maine",
			shortname: "ME",
			link: "/Resources/State-Boards-of-Social-Work/Maine-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st21: {
			id: 21,
			name: "Maryland",
			shortname: "MD",
			link: "/Resources/State-Boards-of-Social-Work/Maryland-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st22: {
			id: 22,
			name: "Massachusetts",
			shortname: "MA",
			link: "/Resources/State-Boards-of-Social-Work/Massachusetts-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st23: {
			id: 23,
			name: "Michigan",
			shortname: "MI",
			link: "/Resources/State-Boards-of-Social-Work/Michigan-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st24: {
			id: 24,
			name: "Minnesota",
			shortname: "MN",
			link: "/Resources/State-Boards-of-Social-Work/Minnesota-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st25: {
			id: 25,
			name: "Mississippi",
			shortname: "MS",
			link: "/Resources/State-Boards-of-Social-Work/Mississippi-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st26: {
			id: 26,
			name: "Missouri",
			shortname: "MO",
			link: "/Resources/State-Boards-of-Social-Work/Missouri-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st27: {
			id: 27,
			name: "Montana",
			shortname: "MT",
			link: "/Resources/State-Boards-of-Social-Work/Montana-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st28: {
			id: 28,
			name: "Nebraska",
			shortname: "NE",
			link: "/Resources/State-Boards-of-Social-Work/Nebraska-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st29: {
			id: 29,
			name: "Nevada",
			shortname: "NV",
			link: "/Resources/State-Boards-of-Social-Work/Nevada-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st30: {
			id: 30,
			name: "New Hampshire",
			shortname: "NH",
			link: "/Resources/State-Boards-of-Social-Work/New-Hampshire-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st31: {
			id: 31,
			name: "New Jersey",
			shortname: "NJ",
			link: "/Resources/State-Boards-of-Social-Work/New-Jersey-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st32: {
			id: 32,
			name: "New Mexico",
			shortname: "NM",
			link: "/Resources/State-Boards-of-Social-Work/New-Mexico-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st33: {
			id: 33,
			name: "New York",
			shortname: "NY",
			link: "/Resources/State-Boards-of-Social-Work/New-York-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st34: {
			id: 34,
			name: "/Resources/State-Boards-of-Social-Work/North-Carolina-State-Board-of-Nursing",
			shortname: "NC",
			link: "State-Boards-of-Nursing/North-Carolina.aspx",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st35: {
			id: 35,
			name: "North Dakota",
			shortname: "ND",
			link: "/Resources/State-Boards-of-Social-Work/North-Dakota-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st36: {
			id: 36,
			name: "Ohio",
			shortname: "OH",
			link: "/Resources/State-Boards-of-Social-Work/Ohio-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st37: {
			id: 37,
			name: "Oklahoma",
			shortname: "OK",
			link: "/Resources/State-Boards-of-Social-Work/Oklahoma-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st38: {
			id: 38,
			name: "Oregon",
			shortname: "OR",
			link: "/Resources/State-Boards-of-Social-Work/Oregon-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st39: {
			id: 39,
			name: "Pennsylvania",
			shortname: "PA",
			link: "/Resources/State-Boards-of-Social-Work/Pennsylvania-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st40: {
			id: 40,
			name: "Rhode Island",
			shortname: "RI",
			link: "/Resources/State-Boards-of-Social-Work/Rhode-Island-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st41: {
			id: 41,
			name: "South Carolina",
			shortname: "SC",
			link: "/Resources/State-Boards-of-Social-Work/South-Carolina-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st42: {
			id: 42,
			name: "South Dakota",
			shortname: "SD",
			link: "/Resources/State-Boards-of-Social-Work/South-Dakota-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st43: {
			id: 43,
			name: "Tennessee",
			shortname: "TN",
			link: "/Resources/State-Boards-of-Social-Work/Tennessee-State-Board-of-Nursing	",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st44: {
			id: 44,
			name: "Texas",
			shortname: "TX",
			link: "/Resources/State-Boards-of-Social-Work/Texas-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st45: {
			id: 45,
			name: "Utah",
			shortname: "UT",
			link: "/Resources/State-Boards-of-Social-Work/Utah-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st46: {
			id: 46,
			name: "Vermont",
			shortname: "VT",
			link: "/Resources/State-Boards-of-Social-Work/Vermont-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st47: {
			id: 47,
			name: "Virginia",
			shortname: "VA",
			link: "/Resources/State-Boards-of-Social-Work/Virginia-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st48: {
			id: 48,
			name: "Washington",
			shortname: "WA",
			link: "/Resources/State-Boards-of-Social-Work/Washington-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st49: {
			id: 49,
			name: "West Virginia",
			shortname: "WV",
			link: "/Resources/State-Boards-of-Social-Work/West-Virginia-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st50: {
			id: 50,
			name: "Wisconsin",
			shortname: "WI",
			link: "/Resources/State-Boards-of-Social-Work/Wisconsin-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st51: {
			id: 51,
			name: "Wyoming",
			shortname: "WY",
			link: "/Resources/State-Boards-of-Social-Work/Wyoming-State-Board-of-Nursing",
			comment: "Name Surname<br>Business Development Manager<br>Phone: (000) 123-4567<br>Fax: (000) 123-4567<br>E-mail: name@domainname.com",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		},
		st52: {
			id: 52,
			name: "",
			shortname: "",
			link: "",
			comment: "",
			image: "",
			color_map: "#222",
			color_map_over: "#4f0504"
		}
	};