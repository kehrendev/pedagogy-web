﻿$(function () {
    console.log('search.js loaded..');

    var searchModal = $('#searchModal');
    var openBtn = $('#openBtn');
    var closeBtn = $('#closeBtn');
    var overlayContent = $('#overlay-content');

    searchModal.click(function () {
        $(this).css('display', 'none');
        overlayContent.css('display', 'none');
    });

    openBtn.click(function () {
        searchModal.css('display', 'block');
        overlayContent.css('display', 'block');
    });

    closeBtn.click(function () {
        searchModal.css('display', 'none');
        overlayContent.css('display', 'none');
    });
});