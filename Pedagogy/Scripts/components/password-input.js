﻿$(function () {
    console.log('password-input.js loaded..');

    $(document).ready(function () {

        $('.show-pass').click(function () {
            $(this).hide();
            $(this).siblings('input').attr('type', 'text');
            $(this).siblings('.hide-pass').show();
        });

        $('.hide-pass').click(function () {
            $(this).hide();
            $(this).siblings('input').attr('type', 'password');
            $(this).siblings('.show-pass').show();
        });
    });
});