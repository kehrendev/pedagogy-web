﻿using CMS.DataEngine;
using CMS.Helpers;

namespace Pedagogy
{
    public class ContentIdentifiers
    {
        public static int HOW_TO_COURSEID = SettingsKeyInfoProvider.GetIntValue("howToCourseID");

        #region Custom Data Column Names

        public const string MEMBERSHIP_PURCHASE_CUSTOM_DATA_COLUMN = "MembershipPurchase";
        public const string MEMBERSHIP_NAME_CUSTOM_DATA_COLUMN = "MembershipName";
        public const string MEMBERSHIP_QTY_CUSTOM_DATA_COLUMN = "MembershipQty";
        public const string MEMBERSHIP_COUNT_CUSTOM_DATA_COLUMN = "MembershipCount";
        public const string MEMBERSHIP_CATEGORY_CUSTOM_DATA_COLUMN = "MembershipCategory";

        public const string FACILITY_ID_CUSTOM_DATA_COLUMN = "FacilityID";
        public const string FACILITY_NAME_CUSTOM_DATA_COLUMN = "FacilityName";
        public const string FACILITY_CODE_CUSTOM_DATA_COLUMN = "FacilityCode";

        public const string IS_DEFAULT_PASSWORD_CUSTOM_DATA_COLUMN = "IsDefaultPassword";

        #endregion

        #region Cookies

        public const string CURRENT_USERNAME_COOKIE = "CurrentUsername";
        public const string FACILITY_ID_COOKIE = "FacilityID";
        public const string MEMBERSHIP_CATEGORY_COOKIE = "MembershipCategory";
        public const string STUDENT_ID_COOKIE = "StudentID";

        #endregion

        #region Roles

        public const string SITE_ADMIN_ROLE = "siteAdmin";
        public const string CORP_ADMIN_ROLE = "corporateAdmin";
        public const string FACILITY_ADMIN_ROLE = "facilityAdmin";
        public const string FACILITY_PURCHASER_ROLE = "facilityPurchaser";
        public const string STUDENT_ROLE = "student";

        #endregion

        #region Categories

        //Navigation
        public const string NAV_MAIN_CATEGORY = "Nav.Main";
        public const string NAV_FOOTER_CATEGORY = "Nav.Footer";
        public const string NAV_ACCOUNT_ACTIONS_CATEGORY = "Nav.AccountAction";
        public const string NAV_ADMIN_FUNCTIONS_CATEGORY = "Nav.AdminFunction";

        #endregion

        #region Emails

        public const string NO_REPLY_EMAIL = "no-reply@pedagogyeducation.com";
        public const string SUPPORT_EMAIL = "support@pedagogyeducation.com";

        #endregion

        #region Paths

        public const string ROOT = "/";
        public const string HOME_PAGE = "/Home";
        public const string NOT_FOUND = "/Not-Found";
        public const string ERROR = "/Error";

        public const string COURSE_CATALOG = "/Courses";

        public const string SOLUTIONS = "/Education-Solutions";

        public const string NEWS_ARTICLES = "/News";

        public const string RESOURCE_PAGE = "/Resources";
        public const string STATE_BOARD_OF_NURSING_MAP = "/Resources/State-Boards-of-Nursing";
        public const string STATE_BOARD_OF_SOCIAL_WORK_MAP = "/Resources/State-Boards-of-Social-Work";

        public const string REFUND_POLICY_PAGE = "/Utilities/Refund-Policy";
        public const string ACCURACY_POLICY_PAGE = "/Utilities/Accuracy-Policy";
        public const string PRIVACY_POLICY_PAGE = "/Utilities/Privacy-Policy";

        public const string LOGIN_PAGE = "/Account/Login";
        public const string FACILITY_PENDING_APPROVAL_PAGE = "/Account/Facility-Pending-Approval";

        public const string DASHBOARD = "/Student-Dashboard";
        public const string ADMIN_MANAGE_FUNCTIONS_FOLDER = "/Student-Dashboard/Admin-Functions/Manage";
        public const string MANAGE_CORPORATION_PAGE = "/Student-Dashboard/Admin-Functions/Manage/Corporations";
        public const string MANAGE_FACILITY_PAGE = "/Student-Dashboard/Admin-Functions/Manage/Facilities";
        public const string MANAGE_STAFF_PAGE = "/Student-Dashboard/Admin-Functions/Manage/Staff";
        public const string MANAGE_EDUCATION_PLANS_PAGE = "/Student-Dashboard/Admin-Functions/Manage/Education-Plan";
        public const string MANAGE_STUDENT_CLASSES = "/Student-Dashboard/Admin-Functions/Manage/Student-Classes";
        public const string PRINT_STUDENT_CERTS = "/Student-Dashboard/Admin-Functions/Manage/Print-Student-Certificates";
        public const string ARCHIVED_COURSES = "/Student-Dashboard/Account-Details/Archived-Courses";
        public const string UPDATE_STAFF_INFO = "/Student-Dashboard/Admin-Functions/Manage/Update-Staff-Info";
        public const string APPROVE_FACILITIES = "/Student-Dashboard/Admin-Functions/Approve-Facilities";
        public const string MANAGE_FACILITY_CLASSES = "/Student-Dashboard/Admin-Functions/Manage/Facility-Classes";
        public const string MANAGE_ENROLLMENT = "/Student-Dashboard/Admin-Functions/Manage/Enrollment";
        public const string EVENT_LOG = "/Student-Dashboard/Admin-Functions/Event-Log";

        public const string PAY_INVOICE = "/Pay-Invoice";

        public const string CREATE_MEMBERSHIP = "/Checkout/CreateMembership";
        public const string DELIVERY_DETAILS = "/Checkout/DeliveryDetails";

        public const string PRINT_CERTIFICATE = "/Portal/Handlers/ClassCertificate.ashx";
        public const string PRINT_ALL_STUDENT_CERTS = "/Portal/Handlers/StudentCertificates.ashx";
        public const string CLASS_EVAL = "/Portal/ClassEvaluation";
        public const string TEST = "/Portal/Test";
        public const string CLASS = "/Portal/Class";

        public const string SOCIAL_MEDIA_ITEMS_FOLDER = "/Social-Links";

        #endregion

        #region Facility Wait List Statuses

        public const string PENDING_STATUS = "Pending";
        public const string REJECTED_STATUS = "Rejected";

        #endregion

        #region Workflow Code Names

        public const string START_CLASS_WF_CODE_NAME = "startClass";
        public const string RETAKE_TEST_WF_CODE_NAME = "retakeTest";
        public const string PRINT_CERT_WF_CODE_NAME = "printCertificate";
        public const string ARCHIVED_WF_CODE_NAME = "archived";
        public const string PAY_FOR_CLASS_WF_CODE_NAME = "payForClass";
        public const string COMPLETE_EVAL_CODE_NAME = "completeEval";
        public const string COMPLETE_TEST_WF_CODE_NAME = "completeTest";
        public const string COMPLETED_WF_CODE_NAME = "completed";
        public const string CONTINUE_WF_CODE_NAME = "continue";
        public const string SCORE_TEST_WF_CODE_NAME = "scoreTest";

        #endregion

        #region Email Templates

        public const string FACILITY_APPROVAL_ET_IDENTIFIER = "facilityApproval";
        public const string FACILITY_REJECTION_ET_IDENTIFIER = "facilityRegistrationRejection";
        public const string FACILITY_REGISTRATION_DATA_ET_IDENTIFIER = "facilityRegistrationData";
        public const string FACILITY_REGISTRATION_CONFIRMATION_ET_IDENTIFIER = "facilityRegistrationRequest";
        public const string CONTACT_CREATION_ET_INDENTIFIER = "contactCreationData";
        public const string CONTACT_CREATION_FACILITY_ET_IDENTIFIER = "contactCreationDataForAdmin";
        public const string FACILITY_PO_ORDER_SUBMITTED_ET_IDENTIFIER = "facilityPayoffOrderSubmitted";
        public const string FACILITY_PO_ORDER_CONFIRMATION_ET_IDENTIFER = "facilityPayoffOrderConfirmation";
        public const string EXISTING_USER_ADDED_TO_FACILITY_ET_IDENTIFER = "existingUserAddedToFacility";
        public const string NEW_FACILITY_STAFF_LISTING_ET_IDENTIFIER = "listOfNewFacilityStaff";
        public const string NEW_FACILITY_STAFF_ET_IDENTIFIER = "facilityStaffPasswordDirections";

        #endregion

        #region Contact Type Code Names

        public const string CORP_ADMIN_CODE_NAME = "corporateAdmin";
        public const string FACILITY_ADMIN_CODE_NAME = "facilityAdmin";
        public const string STUDENT_CODE_NAME = "student";
        public const string PRIMARY_CONTACT_CODE_NAME = "primaryContact";
        public const string ALTERNATE_CONTACT_CODE_NAME = "alternateContact";
        public const string EDUCATOR_CONTACT_CODE_NAME = "educatorContact";
        public const string ACCTS_PAYABLE_CONTACT_CODE_NAME = "accountsPayableContact";
        public const string DIRECTOR_CONTACT = "directorOfNursingContact";

        #endregion

        #region License Category Code Names

        public const string NURSING_CODE_NAME = "nursing";
        public const string RESPIRATORY_CODE_NAME = "respiratory";
        public const string DIETETICS_CODE_NAME = "dieteticsAndNutrition";
        public const string ADMINISTRATORS_CODE_NAME = "administrators";
        public const string RADIOLOGIC_CODE_NAME = "radiologicTechnologist";
        public const string MASSAGE_THERAPY_CODE_NAME = "massageTherapy";

        #endregion

        #region License Type Code Names

        public const string LT_ARNP_CODE_NAME = "advRegisteredNursePractitioner";
        public const string LT_CNS_CODE_NAME = "clinicalNurseSpecialist";
        public const string LT_CNM_CODE_NAME = "licensedMidwife";
        public const string LT_RN_CODE_NAME = "registeredNurse";
        public const string LT_LPN_CODE_NAME = "licensedPracticalVocationalNurse";
        public const string LT_CNA_CODE_NAME = "certifiedNursingAssistant";
        public const string LT_CERTIFIED_MEDICATION_AIDE_CODE_NAME = "certifiedMedicationAideTechnician";
        public const string LT_HHA_CODE_NAME = "homeHealthAide";
        public const string LT_RRT_CODE_NAME = "registeredRespiratoryTherapist";
        public const string LT_CRT_CODE_NAME = "certifiedRespiratoryTherapist";
        public const string LT_CERTIFIED_DIETITIAN_CODE_NAME = "certifiedDietitianNutritionist";
        public const string LT_NHA_CODE_NAME = "nursingHomeAdministrator";
        public const string LT_RC_CODE_NAME = "residentialCareAssistedLivingAdmin";
        public const string LT_NA_CODE_NAME = "notApplicable";
        public const string LT_RCIS_CODE_NAME = "registeredCardiovascularInvasiveSpecialist";
        public const string LT_ARRT_CODE_NAME = "americanRegistryOfRadiologicTechnologists";
        public const string LT_RRA_CODE_NAME = "registeredRadiologicAssistants";
        public const string LT_FLORIDA_NURSING_HOME_ADMIN_CODE_NAME = "floridaNursingHomeAdmin";
        public const string LT_FLORIDA_ASSISTED_LIVING_ADMIN_CODE_NAME = "floridaAssistedLivingAdmin";

        #endregion

        #region Facility Membership Validation Statuses

        public const string TOO_LOW = "low";
        public const string TOO_HIGH = "high";

        #endregion

        #region Discount Codenames

        public const string TEMP_FACILITY_MEMBERSHIP_DISCOUNT_NAME = "facilityMembershipDiscount-";
        public const string TEMP_FACILITY_MEMBERSHIP_DISCOUNT_COUPON_CODE = "FacilityMembershipDiscount-";

        #endregion

        #region Payment Status Codenames

        public const string NEW_PAYMENT_STATUS = "newOrder";
        public const string PAID_PAYMENT_STATUS = "paid";
        public const string PAYMENT_FAILED_PAYMENT_STATUS = "paymentFailed";
        public const string IN_PROGRESS_PAYMENT_STATUS = "inProgress";
        public const string COURSES_DROPPED_PAYMENT_STATUS = "coursesDropped";
        public const string COMPLETED_PAYMENT_STATUS = "completed";
        public const string PARTIAL_REFUND_PAYMENT_STATUS = "partialRefund";
        public const string REFUND_PAYMENT_STATUS = "refunded";

        #endregion

        #region Payment Method Codenames

        public const string CREDIT_CARD_PAYMENT_METHOD = "creditCard";
        public const string PAYOFF_PAYMENT_METHOD = "payoff";

        #endregion

        #region Event Type Codenames

        public const string ADMIN_CREATED_ET_CODENAME = "adminCreated";
        public const string STUDENT_CREATED_ET_CODENAME = "studentCreated";
        public const string ADMIN_REMOVED_FROM_FACILITY_ET_CODENAME = "adminRemovedFromFacility";
        public const string STUDENT_REMOVED_FROM_FACILITY_ET_CODENAME = "studentRemovedFromFacility";
        public const string COURSE_ASSIGNED_ET_CODENAME = "courseAssignedToStudent";
        public const string COURSE_UNASSIGNED_ET_CODENAME = "courseUnassignedFromStudent";

        #endregion
    }
}